# py-spinnaker2

## Introduction
py-spinnaker2 provides a light-weight Python interface for running experiments on the SpiNNaker2 neuromorphic chip.

### SNN Python Interface
High-level users can define spiking neural networks (SNN) and hybrid DNN/SNN models in py-spinnaker2. The networks are automatically partitioned and mapped to a SpiNNaker2 chip, the SNN simulation is executed via the experiment runner and results are provided back to the user in the Python interface. The API for defining SNN is inspired by [PyNN](http://neuralensemble.org/PyNN/). If no SpiNNaker2 chip is available, SNN models can also be executed using the [Brian 2 simulator](https://briansimulator.org/). Deep SNN (SNN trained with deep learning methods) are supported via the [Neuromorphic Intermediate Representation (NIR)](https://github.com/neuromorphs/NIR).

### Experiment Runner
For low-level users, the so-called *experiment runner* allows to define and run reproducible batch experiments on single SpiNNaker2 chips. An experiment is comprised of the *mem-files* for the SpiNNaker2 ARM cores (i.e. the instruction code), input data to be sent to specific memory locations, and memory regions to be read after the experiment.

### SpiNNaker2 Hardware
For a general intro to the SpiNNaker2 chip, see [spinnaker2.gitlab.io](https://spinnaker2.gitlab.io).

It is required that you have access to the respective code for the FPGA or SpiNNaker2 chip:
- **SpiNNaker2 Chip**: [s2-sim2lab-app](https://gitlab.com/spinnaker2/s2-sim2lab-app) *This gitlab project is private. Please ask Bernhard Vogginger (bernhard.vogginger@tu-dresden.de) to provide access*
- **FPGA prototype**: [qpe-software-fixed](https://gitlab.com/tud-hpsn/internal/qpe-software-fixed) *This gitlab project is private. Please ask Bernhard Vogginger (bernhard.vogginger@tu-dresden.de) to provide access*

## Getting started

## Installation

### 0. Prerequisites

<details>
<summary>
Use a virtual environment like `conda` or `venv`
</summary>

We highly recommend to install this software inside a virtual environment, e.g. `conda` or `venv`

**Example for `conda`**:
1. Create conda environment
```
conda create --name spinnaker2 python=3.9
```
2. Always activate your environment
```
conda activate spinnaker2
```
3. To deactivate the virtual environment, run:
```
conda deactivate
```

**Example for `venv`**:
1. Create Python virtual environment
```
python -m venv spinnaker2
```
2. Always activate your environment
```
source spinnaker2/bin/activate
```
3. To deactivate the virtual environment, run:
```
deactivate
```

**Optional**:
For some examples you might need:
```
pip install tensorflow
pip install tensorflow_datasets
pip install h5py==3.6 # required for tf_datasets
```
</details>

### 1. Clone py-spinnaker2 repository
```
git clone https://gitlab.com/spinnaker2/py-spinnaker2.git
```

### 2. Install Python module
Install `spinnaker2` python module in editable mode
```
cd py-spinnaker2
pip install -e .
```

## SpiNNaker2 Chip Software

### 3. Clone `s2-sim2lab-app`
```
cd ..
git clone git@gitlab.com:spinnaker2/s2-sim2lab-app.git
```

### 4. Compile code in `s2-sim2lab-app`

**Make sure that you have installed all the required softare and libraries for `s2-sim2lap-app`, see [README](https://gitlab.com/spinnaker2/s2-sim2lab-app/-/blob/main/README.md)**

Compile host software and chip software libaries
```
cd s2-sim2lab-app
make

cd host/experiment/app/
cmake CMakeLists.txt
make
cd -

cd host/experiment/
./make_all_apps.py
```

## SpiNNaker2 FPGA Software

### 5. Clone `qpe-software-fixed`, branch `py-spinnaker2`
```
cd ..
git clone --branch py-spinnaker2 git@gitlab.com:tud-hpsn/internal/qpe-software-fixed.git
```

### 6. Compile code in `qpe-software-fixed`

**Make sure that the FPGA has been programmed with the correct bitstream and that ping works, see [Wiki](https://gitlab.com/tud-hpsn/internal/qpe-software-fixed/-/wikis/home)**

Compile host software and chip software libaries
```
cd qpe-software-fixed
source quadpe_env.sh

cd host-software/rev2/experiment/app/
cmake CMakeLists.txt
make
cd -

cd host-software/rev2/experiment/
./make_all_apps.py
```

## Usage

Run one of the examples from this repository:
```
cd examples/snn
python lif_neuron.py
```

## Documentation

Documentation is available at [https://spinnaker2.gitlab.io/py-spinnaker2](https://spinnaker2.gitlab.io/py-spinnaker2).

Information on how to add new neuron model is available in the [wiki](https://gitlab.com/spinnaker2/py-spinnaker2/-/wikis/Adding-a-new-neuron-model).

### Building the Documentation Locally

Follow these steps to build the documentation locally:

#### 1. Sync and Update the Submodules
Run the following commands to sync and update submodules:
```bash
git submodule sync
git submodule update --init --recursive
```

#### 2. Install the package with dependencies for MkDocs
This will install the necessary MkDocs dependencies:
```bash
pip install -e ".[doc]"
```

#### 3. Start the MkDocs Server
To start the MkDocs server and view the documentation locally, run:
```bash
mkdocs serve
```

## Support
Please contact Bernhard Vogginger (bernhard.vogginger@tu-dresden.de)


## Roadmap

### Scope of this software
This software is for running experiments on a single-chip SpiNNaker2 test board
or the SpiNNaker2 FPGA prototype with 4 QuadPEs.
The scope of this software is to explore the capabilities of SpiNNaker2,
providing a simple interface to run experiments on all 152 PEs and to make use
of the accelerators (MAC array, exp/log, RNG).
It is also used to explore hybrid networks containing both spiking and
artificial neural network layers.
Further, low-level integration of sensors using various chip interfaces is
planned.

It is currently not planned to make this software work with multiple chips or
multiple 48-node boards.
Instead, for large-scale SpiNNaker2 applications the sPyNNaker software stack
will be ported to support both PyNN simulations and arbitrary applications
using the SpiNNaker Graph Frontend. The adaption of the low-level software has
been started already.
If you want to port existing PyNN models from SpiNNaker 1 to SpiNNaker2, it is
recommended to wait for sPyNNaker2 being ready in the course of 2024.


### Next steps planned for py-spinnaker2

See [Milestones](https://gitlab.com/spinnaker2/py-spinnaker2/-/milestones).

## Contributing
We are open to any contributions on the software like improving the code-quality, adding new neuron models etc.

### pre-commit hooks

We use `black` and `ruff` for code formatting and linting. Please enable pre-commit hooks before pushing to gitlab:

Install pre-commit package
```
pip install pre-commit
```

Set up pre-commit hook scripts
```
pre-commit install
```

## Citation

If you use py-spinnaker2 in your work, please cite it as follows:

```
@software{vogginger2024pyspinnaker2,
  author       = {Vogginger, Bernhard and
                  Kelber, Florian and
                  Jobst, Matthias and
                  Béna, Gabriel and
                  Arfa, Sirine and
                  Yan, Yexin and
                  Gerhards, Pascal and
                  Weih, Martin and
                  Akl, Mahmoud and
                  Gonzalez, Hector A. and
                  Mayr, Christian},
  title        = {py-spinnaker2},
  month        = aug,
  year         = 2024,
  publisher    = {Zenodo},
  doi          = {10.5281/zenodo.10202109},
  url          = {https://zenodo.org/doi/10.5281/zenodo.10202109}
}
```

For papers using py-spinnaker2, see [docs/papers.md](docs/papers.md).

## Acknowledgment

The development of py-spinnaker2 was partially funded by the following projects:
- [KI-ASIC](https://www.elektronikforschung.de/projekte/ki-asic) by the German Federal Ministry of Education and Reseach (BMBF) under contracts 16ES0996 and 16ES0993.
- [ESCADE](https://escade-project.de/) by the German Federal Ministry for Economic Affairs and Climate Action (BMWK) under contract 01MN23004F.
- [ScaDS.AI](https://scads.ai/) by the German Federal Ministry of Education and Research (BMBF) and the free state of Saxony.
- [SpiNNode](https://spinncloud.com/spinnode/) by the European Innovation Council (EIC) Transition program under the "SpiNNode" project (grant number 101112987).
- [CeTI](https://ceti.one) by the German Research Foundation as part of Germany’s Excellence Strategy – EXC 2050/1 – Project ID 390696704 – Cluster of Excellence "Centre for Tactile Internet with Human-in-the-Loop" of Technische Universität Dresden.

## License
General license: **Apache License 2.0**. See [LICENSE](LICENSE) for details.

Some files have been copied from elsewhere and are shared with a the following licenses:
 - File [conv2d.py](src/spinnaker2/mla/conv2d.py) is re-distributed under the MIT License.
 - [Jupyter notebook](examples/hybrid/mnist_cnn/keras_example.ipynb) for training the keras model of the hybrid example is shared under Apache License 2.0
