import os
import time

import spinnaker2.mapper
import spinnaker2.neuron_models
from spinnaker2.configuration import Experiment
from spinnaker2.coordinates import PE
from spinnaker2.helpers import read_log, read_spikes, read_time_done, read_voltages


def process_results(experiment, mapper, sim_cfg, debug):
    # for each PE: read and print log
    if debug:
        for pe in mapper.hardware.used_cores:
            log_raw = experiment.get_result(pe, "log")
            result_string = read_log(log_raw)
            print("Log ", pe)
            print(result_string.decode("ASCII"))

    t_start = time.time()
    # for each PE: get spikes
    for pe in mapper.hardware.used_cores:
        pop_slice = mapper.mapping.get_population_slice(pe)

        # for apps with non-empty list of recordables, read user-defined values
        # TODO: this call could be made unnecessary if recording is disabled by default
        if pop_slice.pop.app_class.recordables:
            if "spikes" in pop_slice.pop.record:
                print("Read spike record")
                spikes_raw = experiment.get_result(pe, "spike_record")
                spike_times = read_spikes(
                    spikes_raw,
                    n_neurons=pop_slice.size(),
                    time_steps=sim_cfg["n_simulation_ticks"],
                    max_atoms_per_core=pop_slice.pop.app_class.max_atoms_per_core,
                )
                pop_slice.pop.add_spikes(spike_times, offset=pop_slice.start)

            if "v" in pop_slice.pop.record:
                print("Read voltage record")
                voltages_raw = experiment.get_result(pe, "voltage_record")
                voltages = read_voltages(
                    raw_data=voltages_raw,
                    n_neurons=pop_slice.size(),
                    time_steps=sim_cfg["n_simulation_ticks"],
                )
                pop_slice.pop.add_voltages(voltages, offset=pop_slice.start)

            if "v_last" in pop_slice.pop.record:
                if "v" in pop_slice.pop.record:
                    print("Read last voltage record")
                    voltages_last_ts = {k: v[-1] for k, v in voltages.items()}
                    pop_slice.pop.add_voltages_last_timestep(voltages_last_ts, offset=pop_slice.start)
                else:
                    print("Read last voltage record")
                    voltages_raw = experiment.get_result(pe, "voltage_record")
                    voltages_last_ts = read_voltages(
                        raw_data=voltages_raw,
                        n_neurons=pop_slice.size(),
                        time_steps=1,
                        v_last=True,
                    )
                    pop_slice.pop.add_voltages_last_timestep(voltages_last_ts, offset=pop_slice.start)

            if "time_done" in pop_slice.pop.record:
                print("Read time_done record")
                time_done_raw = experiment.get_result(pe, "time_done_record")
                time_done_times = read_time_done(pe, time_done_raw, timer_period=sim_cfg["timer_period"])
                pop_slice.pop.add_time_done(time_done_times)

    t_stop = time.time()
    print("Duration recording:", t_stop - t_start)


class Hardware(object):
    """Base class for SpiNNaker2 hardware systems."""

    # reference clock frequency in Hz, to be defined in derived class
    ref_clock_freq = None

    # path to app that runs an experiment on the hardware
    experiment_app_path = None

    def __init__(self):
        """Initializes a Hardware object.

        Attributes:
        - free_cores: A list of cores that are currently free.
        - used_cores: A list of cores that are currently being used.
        - has_run: A boolean indicating whether the hardware has been run.
        - cmd_opts: A list of command line arguments for experiment apps.
        - eth: A boolean indicating whether ethernet is being used.
        - board_ip: The IP address of the board.
        - my_mapper: The mapper object associated with the hardware.
        """

        self.free_cores = []
        self.used_cores = []
        self.has_run = False
        self.cmd_opts = []  # cmd line arguments for experiment apps
        self.eth = False
        self.board_ip = None
        self.my_mapper = None

    def number_of_cores(self):
        """Return total number of cores."""
        return len(self.free_cores) + len(self.used_cores)

    def pop_free_core(self):
        """Return free core and move to list of used cores internally."""
        core = self.free_cores.pop(0)
        if core not in self.used_cores:
            self.used_cores.append(core)
        return core

    def number_of_free_cores(self):
        return len(self.free_cores)

    def number_of_used_cores(self):
        return len(self.used_cores)

    def run(self, network, time_steps, sys_tick_in_s=1.0e-3, **kwargs):
        """Run SNN on hardware.

        args:
            network: the spiking neural network (spinnaker2.snn.Network object)
            time_steps: number of time steps to simulate the network
            sys_tick_in_s: duration of a time step in real time (aka `sys_tick`)
                           in seconds. default: 1.e-3

        kwargs:
            mapping_only (bool): run only mapping and no experiment on hardware. default: False
            debug(bool): read and print debug information of PEs. default: True
            iterative_mapping(bool): use a greedy iterative mapping to automatically solve MemoryError (maximizing
                number of neurons per core). default: False
        """
        if self.has_run:
            raise Exception("Hardware().run(..) can only be called once.")

        debug = kwargs.get("debug", True)

        if not kwargs.get("iterative_mapping", False):
            self.my_mapper = spinnaker2.mapper.Mapper(network, self)
        else:
            self.my_mapper = spinnaker2.mapper.IterativeMapper(network, self)

        exp_config, sim_cfg = self.my_mapper.map_and_generate_experiment_config(
            time_steps=time_steps,
            sys_tick_in_s=sys_tick_in_s,
            debug=debug,
        )

        if kwargs.get("mapping_only", False):
            print("Run mapping only. No experiment is executed on the hardware.")
            return

        # run experiment
        self.check_for_lock()
        experiment = Experiment(exp_config, self.experiment_app_path)
        experiment.board_ip = self.board_ip
        experiment.run(self.cmd_opts)

        # read spikes and voltages; print debug log
        process_results(experiment, self.my_mapper, sim_cfg, debug)

        self.has_run = True

    def get_mem_file_for_neuron_model(self, neuron_model):
        """Returns the memory file for a given neuron model.

        Args:
            neuron_model: The neuron model.

        Raises:
            NotImplementedError: This method is not implemented in the base class.
        """
        raise NotImplementedError

    def check_for_lock(self):
        """Checks for a lock file and waits until it is released.

        Raises:
            RuntimeError: If the lock file is found but waited for 300 seconds, giving up.
        """
        if self.eth and self.board_ip:
            tries = 0
            while f"s2_eth_lock_{self.board_ip}" in os.listdir("/tmp/"):  # noqa S108
                if tries == 0:
                    print(f"Found lock file: /tmp/s2_eth_lock_{self.board_ip} ... waiting for a bit \r")
                tries += 1
                time.sleep(1)
                if tries > 300:
                    raise RuntimeError("Lock file found, but waited for 300 seconds, giving up")


def _experiment_app_path_s2_chip():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    rel_path = "../../../s2-sim2lab-app/host/experiment/app/experiment"
    return os.path.abspath(os.path.join(dir_path, rel_path))


def _experiment_app_path_s2_fpga():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    rel_path = "../../../qpe-software-fixed/host-software/rev2/experiment/app/experiment"
    return os.path.abspath(os.path.join(dir_path, rel_path))


class FPGA_Rev2(Hardware):  # noqa N801
    """Representation of KI-ASIC FPGA prototype with 4 QPE."""

    ref_clock_freq = 10.0e6  # Hz
    experiment_app_path = _experiment_app_path_s2_fpga()

    def __init__(self):
        super().__init__()
        for x in range(2):
            for y in range(1, 3):
                for pe in range(4):
                    self.free_cores.append(PE(x, y, pe))

    def get_mem_file_for_neuron_model(self, neuron_model):
        return spinnaker2.mapper.APP_PATH_FPGA[neuron_model]


class SpiNNaker2Chip(Hardware):
    """Representation of Spinnaker2."""

    ref_clock_freq = 1.0e6  # Hz
    experiment_app_path = _experiment_app_path_s2_chip()

    def __init__(self, **kwargs):
        """
        kwargs:
            jtag_id (int): jtag ID for SpiNNaker2 chip. Typical value: 0
            eth_ip (string): use ethernet for SpiNNaker2 chip.

        One can use either 'jtag_id' or 'eth_ip', not both at the same time.
        If none of the two is specified, JTAG is used.
        """
        super().__init__()
        self.init_free_cores()

        if "jtag_id" in kwargs and "eth_ip" in kwargs:
            raise Exception(
                "Ambiguous kwargs in SpiNNaker2Chip.__init__(): " "Provide 'jtag_id' or 'eth_ip', not both!"
            )
        if "jtag_id" in kwargs:
            self.cmd_opts += ["-D", str(kwargs["jtag_id"])]
        elif "eth_ip" in kwargs:
            self.cmd_opts += ["-e", kwargs["eth_ip"]]
            self.board_ip = kwargs["eth_ip"]
            self.eth = True
        else:  # use jtag without dedicated jtag_id
            pass

    def get_mem_file_for_neuron_model(self, neuron_model):
        return spinnaker2.mapper.APP_PATH[neuron_model]

    def init_free_cores(self):
        self.free_cores = []
        self.used_cores = []

        for x in range(1, 8):
            for y in range(1, 7):
                if (x == 4) and (y >= 3) and (y <= 4):
                    continue
                if (x == 7) and (y >= 3) and (y <= 4):
                    continue
                for pe in range(4):
                    self.free_cores.append(PE(x, y, pe))
