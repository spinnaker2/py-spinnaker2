import json
import os
import subprocess
import time

from spinnaker2.coordinates import PE, WordAddr
from spinnaker2.helpers import global_word_address


class MemoryRegion(object):
    """memory region on a SpiNNaker2 PE.

    A memory region is defined by its start address and the number of 32-bit
    words.

    Attributes:
        word_addr (WordAddr): start address.
        word_count (int): size of memory region in 32-bit words

    """

    def __init__(self, word_addr: WordAddr, word_count: int):
        """Init MemoryRegion.

        Args:
            word_addr: start address.
            word_count: size of memory region in 32-bit words
        """
        assert isinstance(word_addr, WordAddr)
        self.word_addr = word_addr
        self.word_count = word_count


class PEConfig(object):
    """Configuration of a SpiNNaker2 PE.

    This class provides a container for the experiment specification related to
    one SpiNNaker2 processing element.
    It contains the mem-file, the input data blocks and the memory regions to
    read after the experiment as well as the coordinate of the PE.

    Attributes:
        pe: PE coordinate
        app_name: name of application to run on this PE.
        mem_file (str or None): path to mem-file with instruction memory.
        mem_regions_to_read (dict): memory regions to read from the PE after
            the experiment. The dict keys are unique names for the memory
            regions to ease the retrieval of the data after the experiment.
        mem_data_to_send (list): list of memory data blocks to send to the PE.
            Each block is defined by a start address (WordAddr) and a data
            array of 32-bit words (unsigned integer).
    """

    def __init__(self, pe, app_name, mem_file=None):
        """init PE configuration.

        Args:
            pe: PE coordinate.
            app_name: name of application to run on this PE.
            mem_file (str or None): path to mem-file with instruction memory.
        """
        self.pe = pe
        self.app_name = app_name
        self.mem_file = mem_file
        self.mem_regions_to_read = {}
        self.mem_data_to_send = []

    def add_mem_region_to_read(self, name, mem_region):
        """
        add memory region to read.

        Args:
          name : name tag for the memory region (string)
          mem_region: MemoryRegion
        """
        self.mem_regions_to_read[name] = mem_region

    def add_mem_data_to_send(self, start_addr, data_array):
        """
        add memory data to send

        Args:
          start_addr: PE local word address to write
          data_array: list of 32-bit words representing 4 bytes each
        """
        self.mem_data_to_send.append([start_addr, data_array])

    def check_for_overlap_in_data_to_send(self):
        send_data_length_sorted = []
        for addr, data in self.mem_data_to_send:
            send_data_length_sorted.append([addr, len(data)])
        send_data_length_sorted.sort(key=lambda x: x[0])
        if len(send_data_length_sorted) < 2:
            return
        for this_entry, next_entry in zip(send_data_length_sorted[:-1], send_data_length_sorted[1:]):
            this_end_addr = this_entry[0] + this_entry[1]
            if this_end_addr > next_entry[0]:
                raise Exception(
                    f"Conflict in PEConfig of {self.pe}: data "
                    f"starting at {this_entry[0]} with size {this_entry[1]} "
                    f"overlaps with data starting at {next_entry[0]}"
                )


class ExperimentConfig(object):
    """Configuration of experiment on a SpiNNaker2 chip.

    This class provides a container for the experiment specification for one
    SpiNNakere2 chip.
    Besides the configuration of used PEs it contains the runtime and the
    synchronous_start flag.

    Attributes:
        runtime_in_s: experiment runtime in seconds
        synchronous_start (bool): start all PEs synchronously via feedthrough
            interrupt. Default: False
        pe_configs: list of PEConfigs
    """

    def __init__(self, runtime_in_s):
        """init experiment configuration.

        Args:
            runtime_in_s: experiment runtime in seconds
        """
        self.runtime_in_s = runtime_in_s
        self.synchronous_start = False
        self.pe_configs = []

    def add(self, *configs):
        """add one or several PEConfig"""
        for config in configs:
            if isinstance(config, PEConfig):
                self.pe_configs.append(config)
            else:
                raise TypeError("Config other than PEConfig not supported yet")

    def dump_spec(self, json_file="spec.json"):
        """dump experiment specification to JSON file.

        Args:
            json_file (str): name of JSON file
        """
        spec = {
            "active_pes": [],
            "mem_files": [],
            "duration_in_s": self.runtime_in_s,
            "synchronous_start": self.synchronous_start,
            "mem_regions_to_read": {},
            "mem_data_to_send": [],
        }

        for pe_config in self.pe_configs:
            pe = pe_config.pe
            pe_str = "PE_{}_".format(pe.global_id())
            spec["active_pes"].append([pe.x, pe.y, pe.pe])
            spec["mem_files"].append(pe_config.mem_file)
            for name, region in pe_config.mem_regions_to_read.items():
                # add prefix to mem region name
                new_name = pe_str + name
                spec["mem_regions_to_read"][new_name] = [
                    global_word_address(pe.x, pe.y, pe.pe, region.word_addr.to_ByteAddr()),
                    region.word_count,
                ]

            for mem_data in pe_config.mem_data_to_send:
                spec["mem_data_to_send"].append([global_word_address(pe.x, pe.y, pe.pe, mem_data[0] << 2), mem_data[1]])

        with open(json_file, "w") as f:
            json.dump(spec, f, indent=2)


class ExperimentResult(object):
    """Container for results of SpiNNaker2 experiment.

    Args:
        results (dict): results from SpiNNaker2 experiments. Contains the
            mem_regions_to_read of the PEConfig.
    """

    def __init__(self):
        self.results = {}

    def get_result(self, pe, name):
        """get result of recorded memory region on PE.

        Returns the raw data of a memory region recorded after the experiment.

        Args:
            pe: PE coordinate
            name: name of memory region
        """
        pe_str = "PE_{}_".format(pe.global_id())
        return self.results[pe_str + name]


class Experiment(ExperimentResult):
    """Runs experiment on SpiNNaker2 chip and stores results.

    Args:
        config (ExperimentConfig): experiment configuration
        app_path (str): path to experiment runner executable
    """

    def __init__(self, experiment_config, app_path):
        """init experiment.

        Args:
            experiment_config (ExperimentConfig): experiment configuration
            app_path (str): path to experiment runner executable
        """
        super().__init__()
        self.config = experiment_config
        self.app_path = app_path

    def run(self, cmd_opts=[]):
        """run experiment on SpiNNaker2 hardware.

        Args:
            cmd_opts (list of strings): command line options for the experiment runner
        """
        # TODO: make handling of additional command line parameters flexible
        spec_file = "spec.json"
        self.config.dump_spec(json_file=spec_file)

        # Run experiment
        cmd = [self.app_path] + cmd_opts
        subprocess.run(cmd, check=True)  # noqa S603

        result_file = "results.json"
        with open(result_file, "r") as f:
            self.results = json.load(f)
