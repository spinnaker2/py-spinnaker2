import numpy as np

from . import conv2d as c2d


def pack_int8_into_int32(data_to_pack):  # data_to_pack should be a 1D np.array
    packed_list = list()
    for i in range(0, len(data_to_pack) - 3, 4):
        packed_list.append(
            (
                data_to_pack[i] | data_to_pack[i + 1] << 8 | data_to_pack[i + 2] << 16 | data_to_pack[i + 3] << 24
            ).tolist()
        )

    return packed_list


def get_mem_tensors(conv_run):
    """
    provide memory tensors for conv2d operation
    """
    print("NOTE: Get Tensors...")
    batch_size = conv_run["mlparams0_in_batch_s"]
    input_height = conv_run["mlparams0_in_pix_row_s"]
    input_width = conv_run["mlparams0_in_pix_col_s"]
    filter_height = conv_run["mlparams0_fil_pix_row_s"]
    filter_width = conv_run["mlparams0_fil_pix_col_s"]
    in_channels = conv_run["mlparams0_in_channel_s"]
    out_channels = conv_run["mlparams0_out_channel_s"]
    stride_ver = conv_run["mlparams0_stride_in_pix_row"]
    stride_hor = conv_run["mlparams0_stride_in_pix_col"]
    stride_v = 1 << stride_ver
    stride_h = 1 << stride_hor
    output_height = np.ceil(input_height - filter_height + 1)
    output_height = np.uint16((output_height / stride_v) + (0 < (output_height % stride_v)))
    output_width = np.ceil(input_width - filter_width + 1)
    output_width = np.uint8((output_width / stride_h) + (0 < (output_width % stride_h)))
    conv_run["output_height"] = output_height
    conv_run["output_width"] = output_width

    # dtype = 0 (8bit) 1 (16bit) 2 (32bit)
    signed_i = conv_run["signed_i"]
    signed_w = conv_run["signed_w"]
    dtype_i = conv_run["dtype_i"]
    dtype_w = conv_run["dtype_w"]
    dtype_o = conv_run["dtype_o"]
    output_shift = conv_run["output_shift"]

    zero_fill = lambda x, y: (y - (x % y)) % y
    mem_align = lambda x, y: x + (zero_fill(x, y))

    # get random data
    data_type_i, low_i, high_i, bmask_i, align_i = (np.uint8, 0, 2**8, 0xFF, 16)
    data_type_w, low_w, high_w, bmask_w, align_w = (np.uint8, 0, 2**8, 0xFF, 16)
    data_type_o, low_o, high_o, bmask_o, align_o = (np.uint32, 0, 2**32, 0xFFFFFFFF, 4)

    if dtype_w == 0:
        num_rows = 4
    elif dtype_w == 1:
        num_rows = 2

    shape_i = [batch_size, input_height, input_width, in_channels]
    shape_w = [filter_height, filter_width, in_channels, out_channels]
    shape_o = [batch_size, output_height, output_width, out_channels]
    zeros_shape_i = [batch_size, in_channels, input_height, zero_fill(input_width, align_i)]
    zeros_shape_w = [in_channels, filter_height, filter_width, zero_fill(out_channels, align_w)]
    zeros_shape_o = [batch_size, out_channels, output_height, zero_fill(output_width, align_o)]
    shape_w_inter = [
        in_channels,
        filter_height,
        filter_width,
        mem_align(out_channels, align_w) // num_rows,
        num_rows,
    ]

    rand_i = np.random.randint(low_i, high_i, shape_i, dtype=data_type_i)
    rand_w = np.random.randint(low_w, high_w, shape_w, dtype=data_type_w)

    input = np.array(rand_i, dtype=np.float64)
    filter = np.array(rand_w, dtype=np.float64)
    rand_o = c2d.conv2d(input, filter, pad="VALID", stride=(stride_v, stride_h))
    rand_o = np.int64(rand_o)  # cast so shift works
    if not isinstance(rand_o, np.ndarray):
        rand_o = np.array([rand_o])
        rand_o = np.reshape(rand_o, shape_o)

    rand_o[rand_o < low_o] = low_o
    rand_o[rand_o >= high_o] = high_o - 1
    rand_o = np.array(rand_o, dtype=data_type_o)

    # bring axis to shape inside the memory
    ## change axis for correct dimension order
    rand_i = np.moveaxis(rand_i, 3, 1)
    ## fill memory line with 0 zeros due to alignment
    rand_i = np.concatenate((rand_i, np.zeros(zeros_shape_i, dtype=data_type_i)), axis=-1)
    ## flatten tensor and prepare for file generation
    rand_i = np.ravel(rand_i)

    # bring axis to shape inside the memory
    ## change axis for correct dimension order
    rand_w = np.moveaxis(rand_w, 2, 0)
    ## fill memory line with 0 zeros due to alignment
    rand_w = np.concatenate((rand_w, np.zeros(zeros_shape_w, dtype=data_type_w)), axis=-1)
    ## split out_channel into num_rows batches
    rand_w = np.reshape(rand_w, shape_w_inter)
    ## change axis for correct dimension order
    rand_w = np.moveaxis(rand_w, 3, 0)
    print("rand_w.shape:", rand_w.shape)
    ## flatten tensor and prepare for file generation
    rand_w = np.ravel(rand_w)

    # bring axis to shape inside the memory
    ## change axis for correct dimension order
    rand_o = np.moveaxis(rand_o, 3, 1)
    ## fill memory line with 0 zeros due to alignment
    rand_o = np.concatenate((rand_o, np.zeros(zeros_shape_o, dtype=data_type_o)), axis=-1)
    ## flatten tensor and prepare for file generation
    rand_o = np.ravel(rand_o)

    conv_run["rand_i"] = rand_i
    conv_run["rand_w"] = rand_w
    conv_run["rand_o"] = rand_o

    images = rand_i
    conv_run["images"] = pack_int8_into_int32(images)
    # conv_run['images'] = images.tolist()
    weights = rand_w
    conv_run["weights"] = pack_int8_into_int32(weights)
    # conv_run['weights'] = weights.tolist()

    return conv_run


def align_conv2d_input(_x, _dtype):
    """ """
    raise NotImplementedError


def align_conv2d_filter(_x, _dtype):
    """ """
    raise NotImplementedError


def conv1d_output_size(in_size, filter_size, stride):
    """
    Compute output size for 1D convolution
    """
    return int(np.floor((in_size - filter_size) / stride + 1))


def output_shape_conv2d(cfg):
    """
    Return output shape based on Conv2DConfig

    output format: NCHW
    """
    output_shape = (
        cfg.batch_size,
        cfg.out_channels,
        conv1d_output_size(cfg.in_height, cfg.filter_height, cfg.get_stride_y()),
        conv1d_output_size(cfg.in_width, cfg.filter_width, cfg.get_stride_x()),
    )
    return output_shape


def output_shape_conv2d_aligned(cfg):
    n, c, h, w = output_shape_conv2d(cfg)
    itemsize = 2**cfg.output_mode  # bytes per output value
    alignsize = 16 // itemsize
    w_expand = np.ceil(w / alignsize).astype(np.uint32) * alignsize
    return (n, c, h, int(w_expand))


# The following is copied from p_dsp_TI/RadarNet/DNN_quan.py


def prepare_mla_conv_kernel(data: np.ndarray):
    """
    convert conv kernel size from (CO,H,W,CI) to (next_4_CO//4,CI,H,W,4) + zero padding to fit hpsn-mla
    """
    co, h, w, ci = data.shape
    co_expand = np.ceil(co / 4).astype(int) * 4
    data_expanded = np.zeros((co_expand, h, w, ci), dtype=data.dtype)
    data_expanded[:co] = data
    data_expanded = np.moveaxis(data_expanded, -1, 1)  # shape(co_expand,ci,h,w)
    data_expanded = data_expanded.reshape(co_expand // 4, 4, ci, h, w)
    data_expanded = np.moveaxis(data_expanded, 1, -1)  # shape(co_expand//4,ci,h,w,4)

    return data_expanded


def prepare_mla_conv_fmap(data: np.ndarray):
    """
    expands a conv fmap in the last dimension (W for width) with zeros to fit hpsn-mla

    The implementation is flexible and supports e.g. the following cases of
    `data.shape`: (C,H,W), (N,C,H,W), or (T,N,C,H,W)
    """
    shape = data.shape[:-1]
    w = shape[-1]
    alignsize = 16 // data.itemsize
    w_expand = np.ceil(w / alignsize).astype(int) * alignsize
    data_expanded = np.zeros((shape + (w_expand,)), dtype=data.dtype)
    data_expanded[..., :w] = data
    return data_expanded


def prepare_mla_mm_kernel(data: np.ndarray):
    """
    convert mm kernel size from (m,k) to (next_4_k,next_16_m) + zero padding to fit hpsn-mla
    """
    data = data.T  # tflite stores output layer length at first, we exchange
    k, m = data.shape
    step_k = 4
    step_m = 16 // data.itemsize
    dim_k = int((k - 1) / step_k + 1)
    dim_m = int((m - 1) / step_m + 1)
    k_expand = dim_k * step_k
    m_expand = dim_m * step_m
    data_expand = np.zeros((k_expand, m_expand), dtype=data.dtype)
    data_expand[:k, :m] = data
    data_o = np.array([], dtype=data.dtype).reshape(k_expand, 0)

    for dim_m_i in range(dim_m):  # convert dim column wise
        dim_col = data_expand[:, dim_m_i * step_m : (dim_m_i + 1) * step_m]
        dim_col = dim_col.reshape(-1, step_k, step_m)
        dim_col = np.moveaxis(dim_col, 0, 1).reshape(-1, step_m)
        data_o = np.hstack((data_o, dim_col))

    return data_o


def prepare_mla_mm_fmap(data: np.ndarray):
    """
    convert mm input size from (n,k) to (next_4_n,next_4_k) + zero padding to fit hpsn-mla
    """
    n, k = data.shape
    step_n = 4 // data.itemsize
    step_k = 4
    dim_n = int((n - 1) / step_n + 1)
    dim_k = int((k - 1) / step_k + 1)
    n_expand = dim_n * step_n
    k_expand = dim_k * step_k
    data_expand = np.zeros((n_expand, k_expand), dtype=data.dtype)
    data_expand[:n, :k] = data

    data_expand = data_expand.reshape(dim_n, step_n, dim_k, step_k)
    data_expand = np.swapaxes(data_expand, 1, 3)
    data_expand = data_expand.reshape(n_expand, k_expand)

    return data_expand


def prepare_mla_mm_result(data: np.ndarray):
    """
    expand dense result with zero padding to fit the mla result.
    """
    n, m = data.shape
    step_n = 4 // data.itemsize
    step_m = 16
    dim_n = int((n - 1) / step_n + 1)
    dim_m = int((m - 1) / step_m + 1)
    n_expand = dim_n * step_n
    m_expand = dim_m * step_m
    data_expand = np.zeros((n_expand, m_expand), dtype=data.dtype)
    data_expand[:n, :m] = data
    return data_expand
