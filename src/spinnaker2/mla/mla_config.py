import math
from dataclasses import dataclass, field

import numpy as np


@dataclass
class MatMulConfig(object):
    """
    Matrix multiplication config for the machine learning accelerator.
    """

    op_a_addr: int = 0x8000
    op_b_addr: int = 0x9000
    target_addr: int = 0x10000
    op_a_use_noc: bool = False

    op_a_is_signed: bool = False
    op_b_is_signed: bool = False
    op_a_is_16bit: bool = False
    op_b_is_16bit: bool = False
    output_shift_width: int = 0
    output_mode: int = 2  # 0 -> 8 bit, 1 -> 16 bit, 2 -> 32 bit
    relu_en: bool = False

    # MatMul specific
    # The following are the dimensions for configuring the MLA, not the dimenstions of the matrices!
    matmul_rows_a: int = 1
    matmul_cols_a_rows_b: int = 1
    matmul_cols_b: int = 1

    def to_param_struct(self):
        raw = np.empty(4 + 7 + 3, dtype=np.uint32)
        raw[0] = self.op_a_addr
        raw[1] = self.op_b_addr
        raw[2] = self.target_addr

        raw[3] = self.op_a_use_noc
        raw[4] = self.op_a_is_signed
        raw[5] = self.op_b_is_signed
        raw[6] = self.op_a_is_16bit
        raw[7] = self.op_b_is_16bit
        raw[8] = self.output_shift_width
        raw[9] = self.output_mode
        raw[10] = self.relu_en

        raw[11] = self.matmul_rows_a
        raw[12] = self.matmul_cols_a_rows_b
        raw[13] = self.matmul_cols_b
        return raw


@dataclass
class Conv2DConfig(object):
    """
    Conv2D config for the machine learning accelerator
    """

    op_a_addr: int = 0x8000
    op_b_addr: int = 0x9000
    target_addr: int = 0x10000
    op_a_use_noc: bool = False

    op_a_is_signed: bool = False
    op_b_is_signed: bool = False
    op_a_is_16bit: bool = False
    op_b_is_16bit: bool = False
    output_shift_width: int = 0
    output_mode: int = 2  # 0 -> 8 bit, 1 -> 16 bit, 2 -> 32 bit
    relu_en: bool = False

    # Conv2D specific
    in_width: int = 1
    in_height: int = 1
    in_channels: int = 1
    filter_width: int = 1
    filter_height: int = 1
    stride_x_2_power_n: int = 0
    stride_y_2_power_n: int = 0
    out_channels: int = 1
    batch_size: int = 1

    def set_stride_x(self, stride):
        if stride not in [1, 2, 4, 8]:
            raise ValueError("Unsupported stride value.\n Possible values: [1,2,4,8]")
        self.stride_x_2_power_n = int(math.log2(stride))

    def get_stride_x(self):
        return 2**self.stride_x_2_power_n

    def set_stride_y(self, stride):
        if stride not in [1, 2, 4, 8]:
            raise ValueError("Unsupported stride value.\n Possible values: [1,2,4,8]")
        self.stride_y_2_power_n = int(math.log2(stride))

    def get_stride_y(self):
        return 2**self.stride_y_2_power_n

    def to_param_struct(self):
        raw = np.empty(4 + 7 + 9, dtype=np.uint32)
        raw[0] = self.op_a_addr
        raw[1] = self.op_b_addr
        raw[2] = self.target_addr

        raw[3] = self.op_a_use_noc
        raw[4] = self.op_a_is_signed
        raw[5] = self.op_b_is_signed
        raw[6] = self.op_a_is_16bit
        raw[7] = self.op_b_is_16bit
        raw[8] = self.output_shift_width
        raw[9] = self.output_mode
        raw[10] = self.relu_en

        raw[11] = self.batch_size
        raw[12] = self.in_height
        raw[13] = self.in_width
        raw[14] = self.filter_height
        raw[15] = self.filter_width
        raw[16] = self.in_channels
        raw[17] = self.out_channels
        raw[18] = self.stride_y_2_power_n
        raw[19] = self.stride_x_2_power_n

        return raw
