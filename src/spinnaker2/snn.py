import copy
from abc import ABCMeta, abstractmethod

import numpy as np
import scipy.sparse

import spinnaker2.neuron_models
from spinnaker2 import ann2snn_helpers


class Population(object):
    """
    Group of neurons with the same neuron model.
    """

    _pop_count = 0

    def __init__(self, size, neuron_model, params, name=None, record=[]):
        """
        TODO: think of setting the record flag to [] per default.
        """
        assert isinstance(size, (int, np.integer))
        self.size = int(size)
        self.app_class = spinnaker2.neuron_models.get_application(neuron_model)
        self.neuron_model = neuron_model
        self.splittable = self.app_class.splittable
        self.params = params
        self.name = name or "Population_{:d}".format(Population._pop_count)
        Population._pop_count += 1
        self.spike_times = {}
        self.voltages = {}
        self.voltages_last_timestep = {}
        self.time_done_times = {}

        if not isinstance(record, (list, tuple)):
            record = [record]
        for item in record:
            assert item in self.app_class.recordables
        self.record = record

        self.set_max_atoms_per_core(self.app_class.max_atoms_per_core)

    def add_spikes(self, spike_times, offset=0):
        for key, times in spike_times.items():
            self.spike_times[key + offset] = times

    def get_spikes(self):
        return self.spike_times

    def add_voltages(self, voltages, offset=0):
        for key, values in voltages.items():
            self.voltages[key + offset] = values

    def get_voltages(self):
        return self.voltages

    def add_voltages_last_timestep(self, voltages_last_ts, offset=0):
        for key, values in voltages_last_ts.items():
            self.voltages_last_timestep[key + offset] = values

    def get_last_voltages(self):
        return self.voltages_last_timestep

    def clear_records(self):
        self.spike_times = {}
        self.voltages = {}
        self.voltages_last_timestep = {}
        self.time_done_times = {}

    def add_time_done(self, time_done_times):
        self.time_done_times.update(time_done_times)

    def get_time_done_times(self):
        return self.time_done_times

    def set_max_atoms_per_core(self, max_atoms, force=False):
        """
        set maximum number of atoms per core for this population.
        This is typically used to reduce the number of neurons per core so that
        all synapses fit into the core.

        The value cannot be larger than the maximum number of atoms supported
        by the ARM application.

        Parameters
        ----------
        max_atoms : int
            maximum number of atoms per core
        force : bool, default: False
            force setting of value although it exceeds the global value.
            Use this only when you know what you are doing!
        """
        max_value_app = self.app_class.max_atoms_per_core
        if max_atoms > max_value_app:
            if not force:
                raise Exception(
                    f"max_atoms_per_core cannot not be larger than "
                    f"the global value of {max_value_app} for neuron model "
                    f"'{self.neuron_model}'."
                )
        else:
            self.max_atoms_per_core = int(max_atoms)

    def get_max_atoms_per_core(self):
        """
        get maximum number of atoms per core for this population.
        """
        return self.max_atoms_per_core


class BaseProjection(object, metaclass=ABCMeta):
    """Base class for Projection between two populations.

    Attributes:
        pre: presynaptic population
        post: postsynaptic population
        name: name of projection
    """

    _proj_count = 0

    @abstractmethod
    def __init__(self, pre: Population, post: Population, name: str = None):
        """Initialize BaseProjection.

        Args:
          pre: presynaptic population
          post: postsynaptic population
          name: name of projections (optional)
        """
        assert isinstance(pre, Population)
        assert isinstance(post, Population)
        self.pre = pre
        self.post = post
        self.name = name or "Projection_{:d}".format(BaseProjection._proj_count)
        BaseProjection._proj_count += 1


class Projection(BaseProjection):
    """Projection with connection list.

    Group of synapses between a two populations with same synapse type

    Attributes:
        pre(Population): presynaptic population
        post(Population): postsynaptic population
        name(str): name of projection
        sparse_weights(scipy.sparse.csc_matrix): sparse weight matrix
        sparse_delays(scipy.sparse.csc_matrix): sparse delay matrix
    """

    def __init__(self, pre: Population, post: Population, connections: list, name: str = None):
        """Initialize Projection.

        Args:
            pre: presynaptic population
            post: postsynaptic population
            connections: synaptic connections between pre and post connections.
              A list of lists where each inner list has 4 entries (pre_idx,
              post_idx, weight, delay)
            name: name of projections (optional)
        """

        super().__init__(pre, post, name)
        self._store_connections(connections)

    def _store_connections(self, connections):
        """
        store connections as sparse matrices for delays and weights.

        Also checks if connections are valid, i.e., all indices are within the
        range of the size of the pre and post population.
        """

        # TODO: define whether multapses are allowed or not
        conn_array = np.array(connections)

        pre_ids = conn_array[:, 0].astype(int)
        post_ids = conn_array[:, 1].astype(int)
        weights = conn_array[:, 2]
        delays = conn_array[:, 3]

        assert np.any(0 <= pre_ids)
        assert np.any(pre_ids < self.pre.size)

        assert np.any(0 <= post_ids)
        assert np.any(post_ids < self.post.size)

        # TODO: it might be more efficient to generate a dtype for
        # (weight,delay) instead of having to matrices.
        shape = (self.pre.size, self.post.size)
        # store as CSC sparse matrix to allow efficient column slicing
        self.sparse_weights = scipy.sparse.coo_matrix((weights, (pre_ids, post_ids)), shape).tocsc()
        self.sparse_delays = scipy.sparse.coo_matrix((delays, (pre_ids, post_ids)), shape).tocsc()


class Conv2DProjection(BaseProjection):

    default_params = {
        "in_height": 1,
        "in_width": 1,
        "stride_x": 1,
        "stride_y": 1,
        "pool_x": 1,
        "pool_y": 1,
        "pad_top": 0,
        "pad_bottom": 0,
        "pad_left": 0,
        "pad_right": 0,
    }

    def __init__(self, pre: Population, post: Population, weights: np.ndarray, params: dict, name: str = None):
        """Initialize Conv2D Projection .

        Args:
          pre: presynaptic population
          post: postsynaptic population
          weights: Conv2D weights in shape (CI, CO, H, W)
          params: Conv2D parameters
          name: name of projections (optional)
        """

        super().__init__(pre, post, name)
        self.weights = weights
        self._check_and_store_params(params)
        self.sparse_weights = None
        self.sparse_delays = None

    def _check_and_store_params(self, params):
        """check and store conv2d parameters."""
        self.params = copy.deepcopy(self.default_params)

        for key, value in params.items():
            if key in self.params:
                self.params[key] = value
            else:
                raise Exception(f"Conv2DProjection got unexpected key `{key}` in argument `params`")

    def compute_sparse_weights(self, delay, force=False):
        # only do the compute when necessary or forced
        if self.sparse_weights is None or self.sparse_delays is None or force:
            print(self.weights.shape)
            input_shape = (self.weights.shape[0], self.params["in_height"], self.params["in_width"])
            pool = self.params["pool_x"], self.params["pool_y"]
            stride = (self.params["stride_x"], self.params["stride_y"])
            padding = (self.params["pad_top"], self.params["pad_left"])
            weight = self.weights.swapaxes(0, 1)

            def has_pool(pool):
                return pool[0] > 1 or pool[1] > 1

            if has_pool(pool):
                # C_out, C_in, H, W
                sumpool2d_conns, sumpool2d_output_shape = ann2snn_helpers.connection_list_for_sumpool2d(
                    input_shape=input_shape,
                    stride=pool,
                    kernel_size=pool,
                    padding=(0, 0),
                    delay=delay,
                    data_order="torch",
                )
                conv_input_shape = sumpool2d_output_shape
            else:
                conv_input_shape = input_shape

            conv2d_conns, conv2d_output_shape = ann2snn_helpers.connection_list_from_conv2d_weights(
                weight,  # *self.post.nir_w_scale,
                input_shape=conv_input_shape,
                stride=stride,
                padding=padding,
                delay=delay,
                data_order="torch",
            )
            assert np.prod(conv2d_output_shape) == self.post.size
            if has_pool(pool):
                conns = ann2snn_helpers.join_conn_lists(sumpool2d_conns, conv2d_conns)
            else:
                conns = conv2d_conns
            proj = Projection(pre=self.pre, post=self.post, connections=conns)
            self.sparse_weights = proj.sparse_weights
            self.sparse_delays = proj.sparse_delays


class Network(object):
    # TODO describe
    def __init__(self, name=None):
        self.name = name or "Network"
        self.projections = []
        self.populations = []

    def add(self, *components):
        """
        adds one or several components to the network

        network.add(pop1, pop2, proj1)
        """
        for component in components:
            if isinstance(component, Population):
                if component in self.populations:
                    raise Exception(
                        f"Population '{component.name}'({component}) already added to network."
                        f" It cannot be added multiple times."
                    )
                self.populations.append(component)
            elif isinstance(component, BaseProjection):
                if component in self.projections:
                    raise Exception(
                        f"Projection '{component.name}'({component}) already added to network."
                        f" It cannot be added multiple times."
                    )
                self.projections.append(component)
            else:
                raise TypeError("Only Population and Projection objects supported")

    def validate(self):
        """
        For all checks whether the pre- and post populations are part of the
        network
        """
        for proj in self.projections:
            if proj.pre not in self.populations:
                raise Exception("pre population not in network")
            if proj.post not in self.populations:
                raise Exception("post population not in network")

    def reset(self):
        """
        reset (clear) recorded spikes and voltages
        """
        for pop in self.populations:
            pop.clear_records()
