"""
Coordinates for accessing processing elements and memories in SpiNNaker2


TODO: it is not optimal that both WordAddr and ByteAddr are derived from int
The following condition is true: ByteAddr(11) == WordAddr(11)
"""


class WordAddr(int):
    """
    Word address.

    Each word consists of 4 bytes (32-bit)
    """

    def __new__(cls, val):
        if val < 0:
            raise ValueError("WordAddr can not be negative")
        return super(WordAddr, cls).__new__(cls, val)

    def to_ByteAddr(self):  # noqa: N802
        return ByteAddr(self << 2)


class ByteAddr(int):
    """Byte address"""

    def __new__(cls, val):
        if val < 0:
            raise ValueError("ByteAddr can not be negative")
        return super(ByteAddr, cls).__new__(cls, val)

    def to_WordAddr(self):  # noqa: N802
        if self & 0b11 > 0:
            raise Exception("Cannot convert ByteAddr to WordAddr with 2 LSB of" "ByteAddr being nonzero")
        return WordAddr(self >> 2)


def align_addr_to_next_multiple_of_other(addr, other):
    assert type(addr) == type(other)
    residual = addr % other
    if residual != 0:
        return ((addr // other) + 1) * other
    else:
        return addr


class PE(object):
    def __init__(self, quad_x, quad_y, pe):
        self.x = quad_x
        self.y = quad_y
        self.pe = pe

    def __repr__(self):
        return "PE(%s, %s, %s)" % (self.x, self.y, self.pe)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.pe == other.pe

    def __hash__(self):
        return hash((self.x, self.y, self.pe))

    def global_id(self):
        """return global PE ID on SpiNNaker2 chip"""
        global_id = self.pe & 0x3
        global_id |= (self.y & 0x7) << 2
        global_id |= (self.x & 0x7) << 5
        return global_id


class QPE(object):
    def __init__(self, quad_x, quad_y):
        self.x = quad_x
        self.y = quad_y

    def __repr__(self):
        return "QPE(%s, %s)" % (self.x, self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))
