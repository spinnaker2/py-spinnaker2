import copy

import brian2
import numpy as np

# brian2.BrianLogger.log_level_debug()
import spinnaker2.neuron_models
from spinnaker2 import snn

brian2.prefs.codegen.target = "numpy"  # use the Python fallback
# Leaky integrate and fire model
eqs_lif = """
dv/dt = -v / tau  + i_offset/dt: 1
v_th : 1
i_offset : 1
v_reset : 1
tau : second
"""

eqs_lif_curr_exp = """
dI_syn_exc/dt = -I_syn_exc/tau_exc : 1
dI_syn_inh/dt = -I_syn_inh/tau_inh : 1
dv/dt = -v / tau  + I_syn_exc/dt - I_syn_inh/dt + i_offset/dt: 1 (unless refractory)
v_th : 1
i_offset : 1
v_reset : 1
tau : second
tau_exc : second
tau_inh : second
ref : second
"""

# reset by subtraction
lif_reset = {}
lif_reset["reset_by_subtraction"] = "v -= v_th"
lif_reset["reset_to_v_reset"] = "v = v_reset"

# on_pre action for post synaptic neuron models
on_pre_action = {}
on_pre_action["lif_curr_exp"] = {
    "exc": "I_syn_exc += w",
    "inh": "I_syn_inh += w",
}
on_pre_action["lif"] = {
    "exc": "v += w",
    "inh": "v -= w",
}
on_pre_action["lif_curr_exp_no_delay"] = on_pre_action["lif_curr_exp"]
on_pre_action["lif_no_delay"] = on_pre_action["lif"]
on_pre_action["lif_conv2d"] = on_pre_action["lif"]


def apply_neuron_param_to_lif_neuron_group(group, params, dt):
    group.v_th = params["threshold"]
    # TODO: check divide by 0 for tau
    if np.isscalar(params["alpha_decay"]):  # same value for all
        group.tau = dt / (1 - params["alpha_decay"])  # unclear if it works for arrays
    else:  # array like
        tau = [dt / (1 - value) for value in params["alpha_decay"]]
        if len(tau) == 1:
            group.tau = tau[0]
        else:
            group.tau = tau
    group.i_offset = params["i_offset"]
    group.v_reset = params["v_reset"]


def apply_neuron_param_to_lif_curr_exp_neuron_group(group, params, dt):
    group.v_th = params["threshold"]

    # TODO: check divide by 0 for tau
    if np.isscalar(params["alpha_decay"]):  # same value for all
        group.tau = dt / (1 - params["alpha_decay"])  # unclear if it works for arrays
    else:  # array like
        tau = [dt / (1 - value) for value in params["alpha_decay"]]
        if len(tau) == 1:
            group.tau = tau[0]
        else:
            group.tau = tau

    if np.isscalar(params["exc_decay"]):  # same value for all
        group.tau_exc = dt / (1 - params["exc_decay"])  # unclear if it works for arrays
    else:  # array like
        tau_exc = [dt / (1 - value) for value in params["exc_decay"]]
        if len(tau_exc) == 1:
            group.tau_exc = tau_exc[0]
        else:
            group.tau_exc = tau_exc

    if np.isscalar(params["inh_decay"]):  # same value for all
        group.tau_inh = dt / (1 - params["inh_decay"])  # unclear if it works for arrays
    else:  # array like
        tau_inh = [dt / (1 - value) for value in params["inh_decay"]]
        if len(tau_inh) == 1:
            group.tau_inh = tau_inh[0]
        else:
            group.tau_inh = tau_inh

    group.i_offset = params["i_offset"]
    group.v_reset = params["v_reset"]
    group.ref = np.uint32(params["t_refrac"] + 1) * dt


class Brian2Backend(object):

    def __init__(self):
        self.pops = {}
        self.spike_monitors = {}
        self.state_monitors = {}

    def run(self, network, time_steps, **kwargs):
        """
        run SNN on Brian2

        args:
            network: the spiking neural network (spinnaker2.snn.Network object)
            time_steps: number of time steps to simulate then network

        kwargs:
            quantize_weights (bool): quantize weights and delays. default
                behaviour: True
        """
        quantize_weights = kwargs.get("quantize_weights", True)

        net = brian2.Network()
        # default_schedule = ['start', 'groups', 'thresholds', 'synapses', 'resets', 'end']
        net.schedule = ["start", "groups", "synapses", "thresholds", "resets", "end"]

        brian2.defaultclock.dt = 1 * brian2.ms
        timestep = brian2.defaultclock.dt

        for pop in network.populations:
            if pop.neuron_model in [
                "lif",
                "lif_no_delay",
                "lif_conv2d",
                "lif_curr_exp",
                "lif_curr_exp_no_delay",
            ]:
                # get default params
                lif_app = spinnaker2.neuron_models.NEURON_MODEL_TO_APPLICATION[pop.neuron_model]
                params = copy.copy(lif_app.default_params)
                params.update(pop.params)  # this might be inefficient for large networks
                if pop.neuron_model in ["lif", "lif_no_delay", "lif_conv2d"]:
                    group = brian2.NeuronGroup(
                        pop.size,
                        eqs_lif,
                        threshold="v > v_th",
                        reset=lif_reset[params["reset"]],
                        method="euler",
                    )
                    # set neuron params
                    apply_neuron_param_to_lif_neuron_group(group, params, timestep)

                elif pop.neuron_model in ["lif_curr_exp", "lif_curr_exp_no_delay"]:
                    group = brian2.NeuronGroup(
                        pop.size,
                        eqs_lif_curr_exp,
                        threshold="v > v_th",
                        reset=lif_reset[params["reset"]],
                        refractory="ref",
                        method="euler",
                    )
                    # set neuron params
                    apply_neuron_param_to_lif_curr_exp_neuron_group(group, params, timestep)

                spike_mon = brian2.SpikeMonitor(group)
                state_mon = brian2.StateMonitor(group, "v", record=True, when="end")  # TODO: change to "before_resets"
                net.add(group, spike_mon, state_mon)

                self.pops[pop] = group
                if "spikes" in pop.record:
                    self.spike_monitors[pop] = spike_mon
                if "v" in pop.record:
                    self.state_monitors[pop] = state_mon

            elif pop.neuron_model == "spike_list":
                indices = []
                times = []
                for idx, spike_times in pop.params.items():
                    indices += [idx] * len(spike_times)
                    times += spike_times
                group = brian2.SpikeGeneratorGroup(pop.size, indices=indices, times=times * timestep)
                net.add(group)
                self.pops[pop] = group

            else:
                print(f"Neuron model '{pop.neuron_model}' not supported by Brian2Backend")

        for proj in network.projections:
            pre = self.pops[proj.pre]
            post = self.pops[proj.post]
            on_pre = on_pre_action[proj.post.neuron_model]
            S_exc = brian2.Synapses(pre, post, model="""w:1""", on_pre=on_pre["exc"])
            S_inh = brian2.Synapses(pre, post, model="""w:1""", on_pre=on_pre["inh"])
            # if the projection is a Conv2dProjection, convert the conv weights into sparse connections
            if isinstance(proj, snn.Conv2DProjection):
                proj.compute_sparse_weights(delay=0)
            # convert to LIL sparse matrix as it has desired format
            sm_w = proj.sparse_weights.tolil()
            sm_d = proj.sparse_delays.tolil()
            assert np.all(sm_w.rows == sm_d.rows)  # should be true!

            # clip weights and delays
            post_app = spinnaker2.neuron_models.NEURON_MODEL_TO_APPLICATION[proj.post.neuron_model]
            sw_spec = post_app.sw_spec
            max_weight = sw_spec.max_weight()
            max_delay = sw_spec.max_delay()

            def clip_values(values, max_value, var_name="value"):
                values_out_of_bounds = np.where(values > max_value)
                if values_out_of_bounds[0].size > 0:
                    print(
                        f"Warning: {values_out_of_bounds[0].size} {var_name}s"
                        f" are out of bounds, will be clipped. "
                        f"Max absolute {var_name} is {max_value}."
                    )
                    values[values_out_of_bounds] = max_value
                return values

            def quantize_and_clip_weights(values, max_value):
                """return quantized and clipped absolute values"""
                abs_q_values = np.abs(values).astype(np.uint32)
                return clip_values(abs_q_values, max_value, "weight")

            def quantize_and_clip_delays(values, max_value):
                q_values = np.abs(values).astype(np.uint32)  # TODO: this turn negative delays to positive ones ...
                return clip_values(q_values, max_value, "delay")

            i_exc, j_exc, w_exc, d_exc = [], [], [], []
            i_inh, j_inh, w_inh, d_inh = [], [], [], []

            for i, (tgts, ws, ds) in enumerate(zip(sm_w.rows, sm_w.data, sm_d.data)):
                sign_bits = np.signbit(ws)
                inh = sign_bits == True
                exc = sign_bits == False

                tgts = np.array(tgts, dtype=np.int32)
                ws = np.array(ws)
                ds = np.array(ds)

                i_inh.append(i * inh[inh])
                i_exc.append(i * exc[exc])

                j_inh.append(tgts[inh])
                j_exc.append(tgts[exc])

                w_inh.append(ws[inh])
                w_exc.append(ws[exc])

                d_inh.append(ds[inh])
                d_exc.append(ds[exc])

            i_exc = np.concatenate(i_exc, dtype=np.int32)
            j_exc = np.concatenate(j_exc, dtype=np.int32)
            w_exc = np.concatenate(w_exc)
            d_exc = np.concatenate(d_exc)
            i_inh = np.concatenate(i_inh, dtype=np.int32)
            j_inh = np.concatenate(j_inh, dtype=np.int32)
            w_inh = np.concatenate(w_inh)
            d_inh = np.concatenate(d_inh)

            if quantize_weights:
                w_exc = quantize_and_clip_weights(w_exc, max_weight)
                w_inh = quantize_and_clip_weights(w_inh, max_weight)
            else:
                # make inhibitory weights positive, inhibition is handled in neuron dynamics
                w_inh = np.abs(w_inh)

            d_exc = quantize_and_clip_delays(d_exc, max_delay)
            d_inh = quantize_and_clip_delays(d_inh, max_delay)

            inh_count = i_inh.shape[0]
            if inh_count > 0:
                S_inh.connect(i=i_inh, j=j_inh)
                S_inh.w[:] = w_inh
                S_inh.delay[:] = d_inh * timestep
            exc_count = i_exc.shape[0]
            if exc_count > 0:
                S_exc.connect(i=i_exc, j=j_exc)
                S_exc.w[:] = w_exc
                S_exc.delay[:] = d_exc * timestep

            # print(f"Brian2Backend: {proj.name}: {exc_count} exc weigths, {inh_count} inh weigths")
            if len(S_exc):
                net.add(S_exc)
            if len(S_inh):
                net.add(S_inh)

        # print(brian2.scheduling_summary(net))
        # run brian2 simulation
        net.run(time_steps * timestep, report="stdout")
        print("finished run")

        # gather spikes
        for pop, monitor in self.spike_monitors.items():
            print("getting spikes from ", pop.name)
            spike_times = (monitor.t / timestep).astype(np.uint32)
            indices = monitor.i
            spike_times_dict = {}

            for i in range(pop.size):
                spike_times_dict[i] = []

            for t, i in zip(spike_times, indices):
                spike_times_dict[i].append(t)

            pop.add_spikes(spike_times_dict)

        # gather voltages
        # TODO: make flexible for other state variables than 'v'
        for pop, monitor in self.state_monitors.items():
            print("getting voltages from ", pop.name)
            voltages = monitor.v
            voltages = {i: voltages[i, :] for i in range(pop.size)}

            pop.add_voltages(voltages)
