import json

import matplotlib.pyplot as plt
import numpy as np


def global_word_address(quad_x, quad_y, pe, byte_address):
    """Calculate global word address based on PE coordinate and local byte address."""
    global_addr_dst = global_byte_address(quad_x, quad_y, pe, byte_address)
    return global_addr_dst >> 2


def global_byte_address(quad_x, quad_y, pe, byte_address):
    """Calculate global byte address based on PE coordinate and local byte address."""
    global_addr_dst = 0xF0000000  # global setting for memory access on chip to PE memory
    global_addr_dst |= (quad_y & 0x7) << 19
    global_addr_dst |= (quad_x & 0x7) << 22
    global_addr_dst |= (pe & 0x3) << 17
    global_addr_dst |= byte_address & 0x1FFFF
    return global_addr_dst


def read_log(results):
    """Reads log generated via log_info() function.

    args:
      results: list of unsigned integers representing 32-bit raw data

    returns:
      a string of the valid data
    """

    debug_size = results[0]
    debug_size += 1
    print("Debug size:", debug_size)
    n_entries = debug_size // 4
    if debug_size % 4:
        n_entries += 1
    print("n_entries:", n_entries)
    entries = results[1 : 1 + n_entries]
    Bytes = []
    for entry in entries:
        Bytes.append(entry.to_bytes(4, "little"))
    # print(Bytes)
    string = b"".join(Bytes)
    # print(string.decode("ASCII"))
    return string


def read_spikes_old(raw_data, n_neurons, time_steps):
    """Read spikes from log file of one PE.

    old version that reads from spike records created via log_info, as used in
    Yexin's Synfire Chain (Testcase jib2_top.tc_nn).

    args:
      raw_data: the raw data of the log
      n_neurons: number of neurons on core
      time_steps: number of time steps to consider

    returns:
      dict with neuron ids as keys and list of spike times as values
    """

    log_str = read_log(raw_data)
    ascii_str = log_str.decode("ASCII")
    lines = ascii_str.split("\n")
    """
    Format of log spikes:
    For each timestep
    line 0: core id
    line 1: time step
    line 2-9: spike record
    line 10: warnings
    line 11: pm_levels_hex
    """
    lines_per_time_step = 12
    spike_row_count = 8

    spike_times = {}
    for neuron in range(n_neurons):
        spike_times[neuron] = []

    for t in range(time_steps):
        offset = t * lines_per_time_step
        core_id_read = int(lines[offset][2:10], 16)
        time = int(lines[offset + 1][2:10], 16)
        for n in range(spike_row_count):
            spike_row = int(lines[offset + 2 + n], 16)
            spike_bool_list = ((1 << np.arange(32)) & spike_row > 0).astype(int)
            spike_indices = np.argwhere(spike_bool_list)
            if spike_indices.size > 0:
                for index in np.nditer(spike_indices):
                    spike_times[n * 32 + index].append(t)

    return spike_times


def read_spikes(raw_data, n_neurons, time_steps, max_atoms_per_core):
    """Read spikes from log file of one PE.

    args:
      raw_data: the raw data of the log
      n_neurons: number of neurons on core
      time_steps: number of time steps to consider

    returns:
      dict with neuron ids as keys and list of spike times as values
    """
    print(f"read_spikes(n_neurons={n_neurons}, time_steps={time_steps}, max_atoms={max_atoms_per_core})")

    spike_word_count = (max_atoms_per_core + 31) // 32  # number of 32-bit words per time step with actual counts
    SPIKE_RECORD_LENGTH = spike_word_count + 2  # spike record length per time step with 2-word header

    spike_records = np.array(raw_data, dtype=np.uint32)
    spike_records = spike_records.reshape(time_steps, SPIKE_RECORD_LENGTH)

    spike_times = {}
    for neuron in range(n_neurons):
        spike_times[neuron] = []

    for t in range(time_steps):
        core_id_read = spike_records[t, 0]
        time = spike_records[t, 1]
        if t != time:
            print(
                f"read_spikes2(): timestep of spike_record ({time}) does not match expected time ({t}), continue with"
                f"next timestep"
            )
            continue

        for n in range(spike_word_count):
            spike_bool_list = ((1 << np.arange(32)) & spike_records[t, 2 + n] > 0).astype(int)
            spike_indices = np.argwhere(spike_bool_list)
            if spike_indices.size > 0:
                for index in np.nditer(spike_indices):
                    spike_times[n * 32 + index].append(t)

    return spike_times


def spike_times_dict_to_arrays(spike_times):
    """Convert a spike times dictionary to two arrays with ids and times."""
    times = []
    indices = []
    for index, ts in spike_times.items():
        if len(ts) > 0:
            times += ts
            indices += [index] * len(ts)

    return np.asarray(indices), np.asarray(times)


def spike_times_to_matrix(spike_times, n_timesteps=None):
    """Convert a spike times dictionary to a spikes matrix of the shape (n_neurons,
    n_timesteps)

    args:
      spike_times: spike times dictionary with neuron ids as keys and spike times lists as values
      n_timesteps [optional]: maximum number of time steps, otherwise obtained from spike_times
    """
    max_time_step = (
        max([max(times) if times else 0 for times in spike_times.values()]) + 1 if n_timesteps is None else n_timesteps
    )
    arr = np.zeros((len(spike_times), max_time_step), dtype=int)
    for row, times in enumerate(spike_times.values()):
        arr[row, times] = 1

    return arr


def read_voltages(raw_data, n_neurons, time_steps, v_last=False):
    """
    read voltages from one PE

    args:
      raw_data: the raw data of the log
      n_neurons: number of neurons on core
      time_steps: number of time steps to consider
      v_last: boolean flag inidcating last timestep voltage recording

    returns:
      dict with neuron ids as keys and list of spike times as values
    """
    print(f"read_voltages(n_neurons={n_neurons}, time_steps={time_steps})")

    words_per_timestep = 1 + n_neurons
    v_records = np.array(raw_data, dtype=np.uint32)
    v_records = v_records.reshape(time_steps, words_per_timestep)

    # make sure that timesteps are correct
    # discard check if only last time step is recorded
    if not v_last:
        assert np.array_equal(v_records[:, 0], np.arange(time_steps, dtype=np.uint32))

    voltages = {}
    if v_last:
        for neuron in range(n_neurons):
            voltages[neuron] = v_records[-1, neuron + 1].view(np.float32)
    else:
        for neuron in range(n_neurons):
            voltages[neuron] = v_records[:, neuron + 1].view(np.float32)

    return voltages


def read_time_done(pe, raw_data, timer_period):
    """Read time_done from one PE.

    args:
      raw_data: the raw data of the log
      timer_period: timer period used in ARM core during simulation

    returns:
      dict with PE ids as keys and list of time_done times as values
    """
    time_done_times = (timer_period - np.array(raw_data, dtype=np.uint32)) / timer_period

    return dict({str(pe): time_done_times})


def save_dict_to_npz(this_dict: dict, filename: str):
    """
    saves a given dict of PE: time_done_times as a .npz file with a given filename, that should be a string
    """
    keys_save = np.array([])
    values_save = np.array([])
    dict_list = list(this_dict.items())
    for i in range(len(dict_list)):
        keys_save = np.append(keys_save, [dict_list[i][0]])
        values_save = np.append(values_save, [dict_list[i][1]])
    np.savez(filename, pes=keys_save, times=values_save)


def init_times_done_plot(filepath):
    """Initializes variables needed for plot_times_done_multiple_pes_one_plot_vertical()
    and plot_times_done_multiple_pes_one_plot_horizontal()"""
    npzfile = np.load(filepath, allow_pickle=True)
    pes = npzfile["pes"]
    times = npzfile["times"]
    simticks = int(len(times) / len(pes))  # get number of simticks
    sys_tick_in_s = (
        1e-3  # set length of timestep, has to be same as sys_tick_in_s in hw.run() TODO: don't set this value manually
    )
    x_data = np.arange(simticks)
    splitarray = np.array_split(times, len(pes))

    return npzfile, pes, sys_tick_in_s, times, x_data, splitarray


def plot_times_done_multiple_pes_one_plot_vertical(filepath):
    """Delivers a plot showing the discrete simulation timesteps on the x-axis and
    execution times on the y-axis with a red line showing the maximum allowed execution
    time."""
    npzfile, pes, sys_tick_in_s, _, x_data, splitarray = init_times_done_plot(filepath)
    if len(npzfile["pes"]) == 1:
        fig, ax = plt.subplots(1, 1, sharex=True)
        ax.bar(
            x_data * sys_tick_in_s, splitarray[0] * sys_tick_in_s, width=0.0005
        )  # x-axis data in s, execution time in s, small width so plot fits
        ax.set_ylim(0, sys_tick_in_s + 0.1e-3)  # set y-limit to slightly above max execution time
        ax.axhline(
            y=sys_tick_in_s,
            color="r",
            linestyle="-",
            label="maximum execution time available",
        )  # make a vertical red line at max. execution time
        ax.set_ylabel(pes[0])  # set PE ids as labels
    else:
        fig, axs = plt.subplots(len(pes), 1, sharex=True)
        for i in range(len(npzfile["pes"])):
            axs[i].bar(
                x_data * sys_tick_in_s,
                splitarray[i] * sys_tick_in_s,
                width=0.0005,
            )
            axs[i].set_ylim(0, sys_tick_in_s + 0.1e-3)
            axs[i].axhline(
                y=sys_tick_in_s,
                color="r",
                linestyle="-",
                label="maximum execution time available",
            )

            axs[i].set_ylabel(pes[i])
    plt.xlabel("time [s]")
    plt.show()


def plot_times_done_multiple_pes_one_plot_horizontal(filepath):
    """Delivers a plot showing time on the x-axis with red lines showing every new
    discrete timestep and blue boxes representing the execution time needed."""
    npzfile, pes, sys_tick_in_s, _, x_data, splitarray = init_times_done_plot(filepath)
    list_of_tupels = list()
    if len(npzfile["pes"]) == 1:
        fig, ax = plt.subplots(1, 1, sharex=True)
        for j in x_data:
            list_of_tupels.append(
                (x_data[j] * sys_tick_in_s, splitarray[0][j] * sys_tick_in_s)
            )  # timesteps in s, execution times in s
            ax.axvline(
                x=x_data[j] * sys_tick_in_s,
                color="r",
                linestyle="-",
                ymin=0.05,
                ymax=0.95,
            )  # new red line for every discrete timestep
        ax.broken_barh(list_of_tupels, (1, 1))
        ax.set_ylabel(pes[0])
        ax.yaxis.set_ticks([])  # height of boxes does not show anything, remove ticks
        plt.xlabel("time [s]")
        plt.show()

    else:
        fig, axs = plt.subplots(len(pes), 1, sharex=True)
        for i in range(len(npzfile["pes"])):
            for j in x_data:
                list_of_tupels.append(
                    (
                        x_data[j] * sys_tick_in_s,
                        splitarray[i][j] * sys_tick_in_s,
                    )
                )
                axs[i].axvline(
                    x=x_data[j] * sys_tick_in_s,
                    color="r",
                    linestyle="-",
                    ymin=0.05,
                    ymax=0.95,
                )
            axs[i].broken_barh(list_of_tupels, (1, 1))
            axs[i].set_ylabel(pes[i])
            axs[i].yaxis.set_ticks([])
        plt.xlabel("time [s]")
        plt.show()
