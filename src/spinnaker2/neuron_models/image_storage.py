import numpy as np

from spinnaker2 import mapper
from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.mapper import SynapseWordSize, SynapseWordSpec
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    N_WORDS_MPT_ENTRY,
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class ImageStorageApplication(BaseApplication):
    """
    The ImageStorage population is used to store input data in the SRAM that can be accessed by other PEs.
    E.g. in a time series classification, the input images are stored and a convolution layer can read a single image in
    each time step for processing. This relaxes the required memory on other processing elements and does not require
    distant DRAM.
    """

    profiling = False

    max_atoms_per_core = 1
    splittable = False
    recordables = []

    # Fixed addresses
    data_spec_addr = ByteAddr(0x9F00)
    data_spec_max_size = 16  # in words
    log_addr = ByteAddr(0x1E000)
    log_size = 1000  # Byte

    def __init__(self):
        app_name = "image_storage"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """

        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, self.log_size)

        pop_slice = mapper.mapping.get_population_slice(pe)
        params = pop_slice.pop.params

        ################
        # Input images #
        ################

        image_addr = ByteAddr(params["image_addr"])
        images_raw = params["images"]
        image_addr_end = ByteAddr(image_addr + len(images_raw) * 4)

        ################
        # timer config #
        ################

        timer_config_addr = align_addr_to_next_multiple_of_other(image_addr_end, ByteAddr(0x10))
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)
        timer_config_addr_end = timer_config_addr + len(sim_config) * 4

        assert (
            timer_config_addr_end < self.log_addr
        ), f"image data too large:, {hex(image_addr_end)}, {hex(self.log_addr)}"

        # #####################
        # data specification #
        # #####################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            image_addr,  # start of input images
            timer_config_addr,  # start of timer config
            self.log_addr,  # start of log (dummy)
        ]

        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)
        config.add_mem_data_to_send(image_addr.to_WordAddr(), images_raw)

        return config
