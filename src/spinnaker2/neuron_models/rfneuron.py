import itertools

import numpy as np

from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.mapper import APP_PATH, SynapseWordSize, SynapseWordSpec
from spinnaker2.mla import mla_config, mla_helpers
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    N_WORDS_MPT_ENTRY,
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class RFNeuronApplication(BaseApplication):

    sw_spec = SynapseWordSpec(word_size=SynapseWordSize.SIZE_16, weight=4, delay=3, synapse_type=1, target=8)

    default_params = {
        "freqs": 2,
        "threshold": 100,
        "spike_prob": 0.8,
        "decay_const": 0.0,
        "spike_fn": 0,
        "reset_fn": 0,
    }

    max_atoms_per_core = 250
    splittable = True
    recordables = ["spikes", "v"]

    neuron_params_addr = ByteAddr(0xF000)
    data_spec_addr = ByteAddr(0x10000)
    log_addr = ByteAddr(0x1E000)
    data_spec_max_size = 16  # in words

    def __init__(self):
        app_name = "rfneuron"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, 4000)

        #####################
        # neuron parameters #
        #####################
        pop_slice = mapper.mapping.get_population_slice(pe)
        neuron_params = pop_slice.pop.params
        reset_method = neuron_params.get("reset_fn", self.default_params["reset_fn"])
        spike_method = neuron_params.get("spike_fn", self.default_params["spike_fn"])
        neuron_params_raw = self.convert_lif_neuron_params_to_raw_data(pop_slice)

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = mapper.key_offsets[pe]

        rt_addr = ByteAddr(self.data_spec_addr + self.data_spec_max_size * 4)
        routing_targets_addr = ByteAddr(rt_addr + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_addr.to_WordAddr(), rt_data)

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(ByteAddr(rt_addr + len(rt_data) * 4), ByteAddr(0x10))
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        #################
        # global params #
        #################

        n_neurons = pop_slice.size()
        record_spikes = "spikes" in pop_slice.pop.record
        record_v = "v" in pop_slice.pop.record
        global_params_raw = [
            n_neurons,
            int(record_spikes),
            int(record_v),
            int(reset_method),
            int(spike_method),
        ]
        global_params_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )
        global_params_addr = ByteAddr(global_params_addr)
        config.add_mem_data_to_send(global_params_addr.to_WordAddr(), global_params_raw)

        ############################################
        # master population table and synapse rows #
        ############################################

        # Master population table info
        mpt_info_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(global_params_addr + len(global_params_raw) * 4), ByteAddr(0x10)
        )
        mpt_info_addr = ByteAddr(mpt_info_addr)
        mpt_info_len = 2

        # 1: estimate size of MPT
        mpt_length = mapper.estimate_master_pop_table_length(pe)

        mpt_addr = align_addr_to_next_multiple_of_other(ByteAddr(mpt_info_addr + mpt_info_len * 4), ByteAddr(0x10))
        mpt_addr = ByteAddr(mpt_addr)
        mpt_n_bytes = mpt_length * N_WORDS_MPT_ENTRY * 4

        syn_row_addr = align_addr_to_next_multiple_of_other(ByteAddr(mpt_addr + mpt_n_bytes), ByteAddr(0x10))
        syn_row_addr = ByteAddr(syn_row_addr)

        all_syn_rows_raw, pop_table_raw = mapper.synapse_rows_and_master_pop_table(pe, self.sw_spec, syn_row_addr)
        syn_row_addr_end = ByteAddr(syn_row_addr + len(all_syn_rows_raw) * 4)

        # Master population table info
        mpt_info_raw = [mpt_addr, mpt_length]
        assert len(mpt_info_raw) == mpt_info_len  # make sure that the addresses don't overlap
        config.add_mem_data_to_send(mpt_info_addr.to_WordAddr(), mpt_info_raw)

        ###################
        # spike recording #
        ###################

        SPIKE_RECORD_LENGTH = (self.max_atoms_per_core + 31) // 32 + 2
        timesteps_to_record = sim_cfg["n_simulation_ticks"]
        spike_recording_total_words = SPIKE_RECORD_LENGTH * timesteps_to_record

        spike_record_addr = align_addr_to_next_multiple_of_other(syn_row_addr_end, ByteAddr(0x10))
        spike_record_addr = ByteAddr(spike_record_addr)

        if record_spikes:
            config.add_mem_region_to_read(
                "spike_record",
                MemoryRegion(spike_record_addr.to_WordAddr(), spike_recording_total_words),
            )
        spike_record_addr_end = ByteAddr(spike_record_addr + spike_recording_total_words * 4)

        #####################
        # voltage recording #
        #####################
        if record_v:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            voltage_record_addr = align_addr_to_next_multiple_of_other(spike_record_addr_end, ByteAddr(0x10))
            voltage_record_addr = ByteAddr(voltage_record_addr)
            # for each timestep: 1 word header + n_neurons*voltages
            voltage_recording_total_words = (1 + n_neurons) * timesteps_to_record
            config.add_mem_region_to_read(
                "voltage_record",
                MemoryRegion(voltage_record_addr.to_WordAddr(), voltage_recording_total_words),
            )
            voltage_record_addr_end = ByteAddr(voltage_record_addr + voltage_recording_total_words * 4)
        else:
            voltage_record_addr = spike_record_addr_end
            voltage_record_addr_end = spike_record_addr_end

        # check that recording does not go into log region
        if voltage_record_addr_end > self.log_addr:
            print("syn_rows too large:", hex(voltage_record_addr_end), hex(self.log_addr))
            assert voltage_record_addr_end <= self.log_addr

        ######################
        # data specification #
        ######################

        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            rt_addr,  # start of routing table
            timer_config_addr,  # start of timer config
            global_params_addr,  # start of global params
            mpt_info_addr,  # master population table info address
            syn_row_addr,  # start of synapse rows
            self.neuron_params_addr,  # start of neuron params
            spike_record_addr,  # start of spike records
            voltage_record_addr,  # start of voltage records
            self.log_addr,  # start of log (dummy)
        ]
        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)
        config.add_mem_data_to_send(mpt_addr.to_WordAddr(), pop_table_raw)
        config.add_mem_data_to_send(syn_row_addr.to_WordAddr(), all_syn_rows_raw)
        config.add_mem_data_to_send(self.neuron_params_addr.to_WordAddr(), neuron_params_raw)

        return config

    @classmethod
    def convert_lif_neuron_params_to_raw_data(cls, pop_slice):
        """
        convert the lif neuron params to raw data.

        In the ARM C program, each neuron has a struct:
        >>>
            typedef float REAL;
            typedef struct neuron_params_t {
                REAL alpha_decay;
                REAL threshold;
                REAL I_offset;
                REAL V_reset;
            } neuron_params_t;
            neuron_params_t neuron_params_array[N_NEURONS];
        <<<

        returns a list of uint32 representing the raw data to be sent to SpiNNaker2
        """

        ordered_param_names = ["freqs", "threshold", "spike_prob", "decay_const"]
        n_params = len(ordered_param_names)
        max_neurons = cls.max_atoms_per_core

        float_array = np.zeros(max_neurons * n_params, dtype=np.float32)

        # default values
        freqs_default = cls.default_params["freqs"]
        threshold_default = cls.default_params["threshold"]
        spike_prob_default = cls.default_params["spike_prob"]
        decay_const_default = cls.default_params["decay_const"]

        # populate with default values
        for i in range(max_neurons):
            float_array[i * n_params + 0] = freqs_default
            float_array[i * n_params + 1] = threshold_default
            float_array[i * n_params + 2] = spike_prob_default
            float_array[i * n_params + 3] = decay_const_default

        # fill custom values
        params = pop_slice.pop.params
        n_neurons = pop_slice.size()
        for i, key in enumerate(ordered_param_names):
            value = params.get(key, cls.default_params[key])
            if np.isscalar(value):  # same value for all
                float_array[i : i + n_params * n_neurons : n_params] = value
            else:  # array like
                assert len(value) == pop_slice.pop.size
                float_array[i : i + n_params * n_neurons : n_params] = value[pop_slice.start : pop_slice.stop]

        # convert to uint32 array and return as list
        raw_data = np.frombuffer(float_array.data, dtype=np.uint32)

        return raw_data.tolist()
