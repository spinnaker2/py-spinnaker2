import numpy as np

from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.mapper import SynapseWordSize, SynapseWordSpec
from spinnaker2.mla import mla_config, mla_helpers
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class Conv2DIFRateCodeApplication(BaseApplication):

    # TODO: move default parameters here

    max_atoms_per_core = 1024
    splittable = False
    recordables = ["spikes", "time_done"]

    # fixed addresses
    neuron_params_addr = ByteAddr(0xE000)
    data_spec_addr = ByteAddr(0x10000)  # until 0x10200 as 16x32bit word
    log_addr = ByteAddr(0x1B000)
    data_spec_max_size = 16  # in words

    def __init__(self):
        app_name = "conv2d_if_neuron_rate_code"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, 4000)

        #####################
        # neuron parameters #
        #####################
        pop_slice = mapper.mapping.get_population_slice(pe)
        params = pop_slice.pop.params
        neuron_params = dict(threshold=params["threshold"])
        neuron_params = [neuron_params] * pop_slice.size()  # one dict for each neuron
        neuron_params_raw = self.convert_if_neuron_params_to_raw_data(neuron_params)

        #####################
        # MLA configuration #
        #####################
        c2d_config = mla_config.Conv2DConfig()

        x_in = params["image"]
        assert len(x_in.shape) == 3, "image should have 3 dimension (CI,H,W)"
        x_in = np.expand_dims(x_in, 0)  # add batch dimension

        c2d_config.batch_size = x_in.shape[0]
        c2d_config.in_channels = x_in.shape[1]
        c2d_config.in_height = x_in.shape[2]
        c2d_config.in_width = x_in.shape[3]
        c2d_config.set_stride_y(params["stride_y"])
        c2d_config.set_stride_x(params["stride_x"])

        x_spinn = mla_helpers.prepare_mla_conv_fmap(x_in)

        w_in = params["weights"]
        assert len(w_in.shape) == 4, "weight should have 4 dimension (H,W,CI,CO)"
        assert w_in.shape[2] == c2d_config.in_channels

        c2d_config.filter_height = w_in.shape[0]
        c2d_config.filter_width = w_in.shape[1]
        c2d_config.out_channels = w_in.shape[3]

        w_in = np.moveaxis(w_in, -1, 0)
        w_spinn = mla_helpers.prepare_mla_conv_kernel(w_in)

        assert w_spinn.dtype in (np.int8, np.int16, np.uint8, np.uint16)
        assert x_spinn.dtype in (np.int8, np.int16, np.uint8, np.uint16)

        c2d_config.op_a_is_signed = w_spinn.dtype in (np.int8, np.int16)
        c2d_config.op_b_is_signed = x_spinn.dtype in (np.int8, np.int16)
        c2d_config.op_a_is_16bit = w_spinn.dtype.itemsize == 2
        c2d_config.op_b_is_16bit = x_spinn.dtype.itemsize == 2
        c2d_config.output_shift_width = 0
        c2d_config.output_mode = 2
        c2d_config.relu_en = False

        x_spinn_list = x_spinn.ravel().view(np.uint32).tolist()
        w_spinn_list = w_spinn.ravel().view(np.uint32).tolist()

        # TODO: make this address dynamic
        mla_config_addr = ByteAddr(0x11000)
        mla_config_raw_size = c2d_config.to_param_struct().shape[0]
        op_a_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(mla_config_addr + mla_config_raw_size * 4), ByteAddr(0x10)
        )
        op_a_addr = ByteAddr(op_a_addr)
        op_b_addr = align_addr_to_next_multiple_of_other(ByteAddr(op_a_addr + len(w_spinn_list) * 4), ByteAddr(0x10))
        op_b_addr = ByteAddr(op_b_addr)
        target_addr = align_addr_to_next_multiple_of_other(ByteAddr(op_b_addr + len(x_spinn_list) * 4), ByteAddr(0x10))
        target_addr = ByteAddr(target_addr)

        c2d_config.op_a_addr = int(op_a_addr)
        c2d_config.op_b_addr = int(op_b_addr)
        c2d_config.target_addr = int(target_addr)
        c2d_config.op_a_use_noc = False

        c2d_config_raw = c2d_config.to_param_struct()

        # compute output shape and size of MLA result with memory alignment
        out_shape = mla_helpers.output_shape_conv2d(c2d_config)  # NCHW
        out_size = np.prod(out_shape)
        assert out_size == pop_slice.size(), (out_size, pop_slice.size())

        out_shape_aligned = mla_helpers.output_shape_conv2d_aligned(c2d_config)  # NCHW
        itemsize = 2**c2d_config.output_mode
        out_size_aligned = np.prod(out_shape_aligned)
        out_size_words = out_size_aligned * itemsize / 4
        mla_result_addr_end = ByteAddr(target_addr + out_size_aligned * itemsize)
        out_size_aligned = np.prod(out_shape_aligned)

        #################
        # global params #
        #################
        scale_raw = np.frombuffer(np.float32(params["scale"]).data, dtype=np.uint32)[0]
        time_done_flag = "time_done" in pop_slice.pop.record
        record_spikes = "spikes" in pop_slice.pop.record
        global_params_raw = [
            out_shape[3],  # out_width
            out_shape_aligned[3],  # out_width_aligned
            out_shape[2],  # out_height
            int(scale_raw),  # scale
            int(record_spikes),
            int(time_done_flag),
        ]
        global_params_addr = align_addr_to_next_multiple_of_other(mla_result_addr_end, ByteAddr(0x10))
        global_params_addr_end = ByteAddr(global_params_addr + len(global_params_raw) * 4)

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = mapper.key_offsets[pe]

        rt_address = ByteAddr(0x10040)
        routing_targets_addr = ByteAddr(rt_address + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_address.to_WordAddr(), rt_data)

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(rt_address + len(rt_data) * 4), ByteAddr(0x10)
        )
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        ###################
        # spike recording #
        ###################

        if record_spikes:
            SPIKE_RECORD_LENGTH = (self.max_atoms_per_core + 31) // 32 + 2
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            spike_recording_total_words = SPIKE_RECORD_LENGTH * timesteps_to_record
            spike_record_addr = align_addr_to_next_multiple_of_other(global_params_addr_end, ByteAddr(0x10))
            spike_record_addr = ByteAddr(spike_record_addr)
            config.add_mem_region_to_read(
                "spike_record",
                MemoryRegion(spike_record_addr.to_WordAddr(), spike_recording_total_words),
            )
            spike_record_addr_end = ByteAddr(spike_record_addr + spike_recording_total_words * 4)
        else:
            spike_record_addr = global_params_addr_end
            spike_record_addr_end = global_params_addr_end

        #######################
        # time done recording #
        #######################
        if time_done_flag:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            time_done_addr = align_addr_to_next_multiple_of_other(spike_record_addr_end, ByteAddr(0x10))
            time_done_addr = ByteAddr(time_done_addr)
            config.add_mem_region_to_read(
                "time_done_record", MemoryRegion(time_done_addr.to_WordAddr(), timesteps_to_record)
            )
            time_done_addr_end = ByteAddr(time_done_addr + timesteps_to_record * 4)

        else:
            time_done_addr = spike_record_addr
            time_done_addr_end = spike_record_addr_end

        # check that recording does not go into log region
        if time_done_addr_end > self.log_addr:
            raise MemoryError(
                f"Not enough memory in population {pop_slice.pop.name}: "
                f"{hex(time_done_addr_end)}, {hex(self.log_addr)} "
                f"by {time_done_addr_end- self.log_addr} byte"
            )

        ######################
        # data specification #
        ######################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            rt_address,  # start of routing table
            timer_config_addr,  # start of timer config
            mla_config_addr,  # master population table address
            global_params_addr,  # start of global params
            self.neuron_params_addr,  # start of neuron params
            spike_record_addr,  # start of spike records
            time_done_addr,  # start of time_done records
            self.log_addr,  # start of log (dummy)
        ]
        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)
        config.add_mem_data_to_send(mla_config_addr.to_WordAddr(), c2d_config_raw.tolist())
        config.add_mem_data_to_send(global_params_addr.to_WordAddr(), global_params_raw)
        config.add_mem_data_to_send(self.neuron_params_addr.to_WordAddr(), neuron_params_raw)
        config.add_mem_data_to_send(op_a_addr.to_WordAddr(), w_spinn_list)
        config.add_mem_data_to_send(op_b_addr.to_WordAddr(), x_spinn_list)

        return config

    @classmethod
    def convert_if_neuron_params_to_raw_data(cls, neuron_params):
        """
        convert the lif neuron params to raw data.

        In the ARM C program, each neuron has a struct:
        >>>
            typedef float REAL;
            typedef struct neuron_params_t {
                REAL threshold;
            } neuron_params_t;
            neuron_params_t neuron_params_array[N_NEURONS];
        <<<

        returns a list of uint32 representing the raw data to be sent to SpiNNaker2

        TODO:
          - could be made flexible to also support other neuron types
          - put default values to another place
        """
        N_PARAMS_PER_NEURON = 1

        float_array = np.zeros(cls.max_atoms_per_core * N_PARAMS_PER_NEURON, dtype=np.float32)

        # default values
        threshold_default = 1.0

        # populate with default values
        for i in range(cls.max_atoms_per_core):
            float_array[i * N_PARAMS_PER_NEURON] = threshold_default

        # fill custom values
        for i, params in enumerate(neuron_params):
            float_array[i * N_PARAMS_PER_NEURON] = params["threshold"]

        # convert to uint32 array and return as list
        raw_data = np.frombuffer(float_array.data, dtype=np.uint32)

        return raw_data.tolist()
