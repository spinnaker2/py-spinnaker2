import math

import numpy as np

from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.mapper import SynapseWordSize, SynapseWordSpec
from spinnaker2.mla.mla_helpers import pack_int8_into_int32
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    N_WORDS_MPT_ENTRY,
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class RelayApplication(BaseApplication):
    """The relay application relays an incoming spike for a given delay.

    A relay neuron ignores weights and delays of incoming and outgoing connections.
    It fires if it receives a spike, and imposes a delay to the outgoing connections.
    """

    sw_spec = SynapseWordSpec(word_size=SynapseWordSize.SIZE_16, weight=0, delay=0, synapse_type=0, target=16)
    profiling = False

    default_params = {"delay": 1}

    max_atoms_per_core = 250
    splittable = True
    recordables = ["spikes", "time_done"]

    # fixed addresses
    neuron_params_addr = ByteAddr(0xE400)
    data_spec_addr = ByteAddr(0x10000)  # until 0x10200 as 16x32bit word
    log_addr = ByteAddr(0x1B000)
    data_spec_max_size = 16  # in words

    def __init__(self):
        app_name = "relay"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, 4000)

        #####################
        # neuron parameters #
        #####################
        pop_slice = mapper.mapping.get_population_slice(pe)
        neuron_params = pop_slice.pop.params
        neuron_params_raw = self.convert_lif_neuron_params_to_raw_data(pop_slice)

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = mapper.key_offsets[pe]

        rt_addr = ByteAddr(self.data_spec_addr + self.data_spec_max_size * 4)
        routing_targets_addr = ByteAddr(rt_addr + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_addr.to_WordAddr(), rt_data)

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(ByteAddr(rt_addr + len(rt_data) * 4), ByteAddr(0x10))
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        #################
        # global params #
        #################
        n_neurons = pop_slice.size()
        record_spikes = "spikes" in pop_slice.pop.record
        time_done_flag = "time_done" in pop_slice.pop.record
        global_params_raw = [
            n_neurons,
            int(record_spikes),
            0,  # TODO: remove voltage recording flag
            int(self.profiling),
            int(time_done_flag),
        ]
        global_params_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )
        global_params_addr = ByteAddr(global_params_addr)
        config.add_mem_data_to_send(global_params_addr.to_WordAddr(), global_params_raw)

        ############################################
        # master population table and synapse rows #
        ############################################

        # Master population table info
        mpt_info_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(global_params_addr + len(global_params_raw) * 4), ByteAddr(0x10)
        )
        mpt_info_addr = ByteAddr(mpt_info_addr)
        mpt_info_len = 2

        # 1: estimate size of MPT
        mpt_length = mapper.estimate_master_pop_table_length(pe)

        mpt_addr = align_addr_to_next_multiple_of_other(ByteAddr(mpt_info_addr + mpt_info_len * 4), ByteAddr(0x10))
        mpt_addr = ByteAddr(mpt_addr)
        mpt_n_bytes = mpt_length * N_WORDS_MPT_ENTRY * 4

        syn_row_addr = align_addr_to_next_multiple_of_other(ByteAddr(mpt_addr + mpt_n_bytes), ByteAddr(0x10))
        syn_row_addr = ByteAddr(syn_row_addr)

        all_syn_rows_raw, pop_table_raw = mapper.synapse_rows_and_master_pop_table(pe, self.sw_spec, syn_row_addr)
        syn_row_addr_end = ByteAddr(syn_row_addr + len(all_syn_rows_raw) * 4)

        # Master population table info
        mpt_info_raw = [mpt_addr, mpt_length]
        assert len(mpt_info_raw) == mpt_info_len  # make sure that the addresses don't overlap
        config.add_mem_data_to_send(mpt_info_addr.to_WordAddr(), mpt_info_raw)

        ###################
        # spike recording #
        ###################
        if record_spikes:
            SPIKE_RECORD_LENGTH = (self.max_atoms_per_core + 31) // 32 + 2
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            spike_recording_total_words = SPIKE_RECORD_LENGTH * timesteps_to_record
            spike_record_addr = align_addr_to_next_multiple_of_other(syn_row_addr_end, ByteAddr(0x10))
            spike_record_addr = ByteAddr(spike_record_addr)
            config.add_mem_region_to_read(
                "spike_record", MemoryRegion(spike_record_addr.to_WordAddr(), spike_recording_total_words)
            )
            spike_record_addr_end = ByteAddr(spike_record_addr + spike_recording_total_words * 4)
        else:
            spike_record_addr = syn_row_addr_end
            spike_record_addr_end = syn_row_addr_end

        #####################
        # voltage recording #
        #####################
        # TODO: remove voltage recording
        voltage_record_addr = spike_record_addr_end
        voltage_record_addr_end = spike_record_addr_end

        #######################
        # time done recording #
        #######################
        if time_done_flag:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            time_done_addr = align_addr_to_next_multiple_of_other(voltage_record_addr_end, ByteAddr(0x10))
            time_done_addr = ByteAddr(time_done_addr)
            config.add_mem_region_to_read(
                "time_done_record", MemoryRegion(time_done_addr.to_WordAddr(), timesteps_to_record)
            )
            time_done_addr_end = ByteAddr(time_done_addr + timesteps_to_record)

        else:
            time_done_addr = voltage_record_addr
            time_done_addr_end = voltage_record_addr_end

        # check that recording does not go into log region
        if time_done_addr_end > self.log_addr:
            print(
                "syn_rows too large in population", pop_slice.pop.name, ":", hex(time_done_addr_end), hex(self.log_addr)
            )
            assert time_done_addr_end <= self.log_addr

        ######################
        # data specification #
        ######################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            rt_addr,  # start of routing table
            timer_config_addr,  # start of timer config
            global_params_addr,  # start of global params
            mpt_info_addr,  # master population table info address
            syn_row_addr,  # start of synapse rows
            self.neuron_params_addr,  # start of neuron params
            spike_record_addr,  # start of spike records
            voltage_record_addr,  # start of voltage records
            time_done_addr,  # start of time_done records
            self.log_addr,  # start of log (dummy)
        ]
        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)
        config.add_mem_data_to_send(mpt_addr.to_WordAddr(), pop_table_raw)
        config.add_mem_data_to_send(syn_row_addr.to_WordAddr(), all_syn_rows_raw)
        config.add_mem_data_to_send(self.neuron_params_addr.to_WordAddr(), neuron_params_raw)

        return config

    @classmethod
    def convert_lif_neuron_params_to_raw_data(cls, pop_slice):
        """
        convert the lif neuron params to raw data.
        delay is stored as a single uint8_t
        returns a list of uint32 representing the raw data to be sent to SpiNNaker2
        """

        max_neurons = cls.max_atoms_per_core
        raw_data = np.zeros(math.ceil(max_neurons / 4), dtype=np.uint32)

        value = pop_slice.pop.params.get("delay", cls.default_params["delay"])
        n_neurons = pop_slice.size()
        if np.isscalar(value):
            value = np.uint32(value - 1)
            value += value << 8 + value << 16 + value << 24
            for i in range(math.ceil(n_neurons / 4)):
                raw_data[i] = value
        else:
            for i in range(n_neurons):
                raw_data[i // 4] += np.uint32(value[pop_slice.start + i] - 1) << i % 4 * 8

        return raw_data.tolist()
