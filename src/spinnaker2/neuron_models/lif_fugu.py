import numpy as np

from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.mapper import SynapseWordSize, SynapseWordSpec
from spinnaker2.mla.mla_helpers import pack_int8_into_int32
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    N_WORDS_MPT_ENTRY,
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class LIFApplicationFugu(BaseApplication):
    """
    A variant of LIF Neuron that supports the following extra features:
    * Initial voltage can be specified. An initial value of 0 makes this the same as the original LIF Neuron.
    * Spiking is probabilistic. Specifically, when neuron reaches spike threshold, there is a random draw to determine
        whether or not it fires. A probability of 1 effectively disables this feature.
    * Up to 32 steps of sending-side delay. A shift buffer supports multiple spikes in flight.

    Parameters:
    V      -- Activation level of neuron, generally in [-1,1].
    decay  -- The amount of V to retain during each simulation cycle.
    Vspike -- Spike threshold for V.
    Vbias  -- Amount of voltage added to V during each simulation cycle.
    Vreset -- V goes to this value after a successful spike.
    p      -- Probability of firing, in [0,1].
    delay  -- Number cycles before spike is received by downstream neuron. Can't be any less than 1.
    """

    sw_spec = SynapseWordSpec(word_size=SynapseWordSize.SIZE_16, weight=7, delay=0, synapse_type=1, target=8)
    profiling = False

    default_params = {
        "decay": 1.0,  # V <-- V*decay. 1 means V is unchanged (no decay). 0 means all voltage leaks away in one cycle.
        "Vspike": 1.0,
        "Vbias": 0.0,
        "Vreset": 0.0,
        "V": 0.0,
        "p": 1.0,
        "delay": 1,
    }

    max_atoms_per_core = 250
    splittable = True
    recordables = ["spikes", "v", "time_done"]

    # fixed addresses
    neuron_params_addr = ByteAddr(0xE400)
    data_spec_addr = ByteAddr(0x10000)  # until 0x10200 as 16x32bit word
    log_addr = ByteAddr(0x1B000)
    data_spec_max_size = 16  # in words

    def __init__(self):
        app_name = "lif_fugu"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, 4000)

        #####################
        # neuron parameters #
        #####################
        pop_slice = mapper.mapping.get_population_slice(pe)
        neuron_params = pop_slice.pop.params
        neuron_params_raw = self.convert_lif_neuron_params_to_raw_data(pop_slice)

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = mapper.key_offsets[pe]

        rt_addr = ByteAddr(self.data_spec_addr + self.data_spec_max_size * 4)
        routing_targets_addr = ByteAddr(rt_addr + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_addr.to_WordAddr(), rt_data)

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(ByteAddr(rt_addr + len(rt_data) * 4), ByteAddr(0x10))
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        #################
        # global params #
        #################
        n_neurons = pop_slice.size()
        record_spikes = "spikes" in pop_slice.pop.record
        record_v = "v" in pop_slice.pop.record
        time_done_flag = "time_done" in pop_slice.pop.record
        global_params_raw = [
            n_neurons,
            pop_slice.start,
            int(record_spikes),
            int(record_v),
            int(self.profiling),
            int(time_done_flag),
        ]
        global_params_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )
        global_params_addr = ByteAddr(global_params_addr)
        config.add_mem_data_to_send(global_params_addr.to_WordAddr(), global_params_raw)

        ############################################
        # master population table and synapse rows #
        ############################################

        # Master population table info
        mpt_info_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(global_params_addr + len(global_params_raw) * 4), ByteAddr(0x10)
        )
        mpt_info_addr = ByteAddr(mpt_info_addr)
        mpt_info_len = 2

        # 1: estimate size of MPT
        mpt_length = mapper.estimate_master_pop_table_length(pe)

        mpt_addr = align_addr_to_next_multiple_of_other(ByteAddr(mpt_info_addr + mpt_info_len * 4), ByteAddr(0x10))
        mpt_addr = ByteAddr(mpt_addr)
        mpt_n_bytes = mpt_length * N_WORDS_MPT_ENTRY * 4

        syn_row_addr = align_addr_to_next_multiple_of_other(ByteAddr(mpt_addr + mpt_n_bytes), ByteAddr(0x10))
        syn_row_addr = ByteAddr(syn_row_addr)

        all_syn_rows_raw, pop_table_raw = mapper.synapse_rows_and_master_pop_table(pe, self.sw_spec, syn_row_addr)
        syn_row_addr_end = ByteAddr(syn_row_addr + len(all_syn_rows_raw) * 4)

        # Master population table info
        mpt_info_raw = [mpt_addr, mpt_length]
        assert len(mpt_info_raw) == mpt_info_len  # make sure that the addresses don't overlap
        config.add_mem_data_to_send(mpt_info_addr.to_WordAddr(), mpt_info_raw)

        ###################
        # spike recording #
        ###################
        if record_spikes:
            SPIKE_RECORD_LENGTH = (self.max_atoms_per_core + 31) // 32 + 2
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            spike_recording_total_words = SPIKE_RECORD_LENGTH * timesteps_to_record
            spike_record_addr = align_addr_to_next_multiple_of_other(syn_row_addr_end, ByteAddr(0x10))
            spike_record_addr = ByteAddr(spike_record_addr)
            config.add_mem_region_to_read(
                "spike_record", MemoryRegion(spike_record_addr.to_WordAddr(), spike_recording_total_words)
            )
            spike_record_addr_end = ByteAddr(spike_record_addr + spike_recording_total_words * 4)
        else:
            spike_record_addr = syn_row_addr_end
            spike_record_addr_end = syn_row_addr_end

        #####################
        # voltage recording #
        #####################
        if record_v:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            voltage_record_addr = align_addr_to_next_multiple_of_other(spike_record_addr_end, ByteAddr(0x10))
            voltage_record_addr = ByteAddr(voltage_record_addr)
            # for each timestep: 1 word header + n_neurons*voltages
            voltage_recording_total_words = (1 + n_neurons) * timesteps_to_record
            config.add_mem_region_to_read(
                "voltage_record", MemoryRegion(voltage_record_addr.to_WordAddr(), voltage_recording_total_words)
            )
            voltage_record_addr_end = ByteAddr(voltage_record_addr + voltage_recording_total_words * 4)
        else:
            voltage_record_addr = spike_record_addr_end
            voltage_record_addr_end = spike_record_addr_end

        #######################
        # time done recording #
        #######################
        if time_done_flag:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            time_done_addr = align_addr_to_next_multiple_of_other(voltage_record_addr_end, ByteAddr(0x10))
            time_done_addr = ByteAddr(time_done_addr)
            config.add_mem_region_to_read(
                "time_done_record", MemoryRegion(time_done_addr.to_WordAddr(), timesteps_to_record)
            )
            time_done_addr_end = ByteAddr(time_done_addr + timesteps_to_record)

        else:
            time_done_addr = voltage_record_addr
            time_done_addr_end = voltage_record_addr_end

        # check that recording does not go into log region
        if time_done_addr_end > self.log_addr:
            print(
                "syn_rows too large in population", pop_slice.pop.name, ":", hex(time_done_addr_end), hex(self.log_addr)
            )
            assert time_done_addr_end <= self.log_addr

        ######################
        # data specification #
        ######################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            rt_addr,  # start of routing table
            timer_config_addr,  # start of timer config
            global_params_addr,  # start of global params
            mpt_info_addr,  # master population table info address
            syn_row_addr,  # start of synapse rows
            self.neuron_params_addr,  # start of neuron params
            spike_record_addr,  # start of spike records
            voltage_record_addr,  # start of voltage records
            time_done_addr,  # start of time_done records
            self.log_addr,  # start of log (dummy)
        ]
        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)
        config.add_mem_data_to_send(mpt_addr.to_WordAddr(), pop_table_raw)
        config.add_mem_data_to_send(syn_row_addr.to_WordAddr(), all_syn_rows_raw)
        config.add_mem_data_to_send(self.neuron_params_addr.to_WordAddr(), neuron_params_raw)

        return config

    @classmethod
    def convert_lif_neuron_params_to_raw_data(cls, pop_slice):
        """
        convert the lif neuron params to raw data.

        In the ARM C program, each neuron has a struct:
        >>>
            typedef float REAL;
            typedef struct neuron_params_t {
                REAL     decay;
                REAL     Vspike;
                REAL     Vbias;
                REAL     Vreset;
                REAL     V;
                REAL     p;
                uint32_t delay;
            } neuron_params_t;
            neuron_params_t neuron_params_array[N_NEURONS];
        <<<

        returns a list of uint32 representing the raw data to be sent to SpiNNaker2
        """

        ordered_param_names = [
            "decay",
            "Vspike",
            "Vbias",
            "Vreset",
            "V",
            "p",
        ]  # don't include "delay", to simplify processing below
        n_params = len(ordered_param_names) + 1  # padd 1 place for "delay"
        max_neurons = cls.max_atoms_per_core

        float_array = np.zeros(max_neurons * n_params, dtype=np.float32)

        # fill custom values
        params = pop_slice.pop.params
        n_neurons = pop_slice.size()
        for i, key in enumerate(ordered_param_names):
            value = params.get(key, cls.default_params[key])
            if np.isscalar(value):  # same value for all
                float_array[i : i + n_params * n_neurons : n_params] = value
            else:  # array like
                assert len(value) == pop_slice.pop.size
                float_array[i : i + n_params * n_neurons : n_params] = value[pop_slice.start : pop_slice.stop]

        # convert to uint32 array and return as list
        raw_data = np.frombuffer(float_array.data, dtype=np.uint32)

        # touch up with bit masks for delay
        delay_index = 6  # zero-based position in data structure
        value = params.get("delay", cls.default_params["delay"])
        if np.isscalar(value):
            value = np.uint32(value - 1)
            for i in range(delay_index, delay_index + n_params * n_neurons, n_params):
                raw_data[i] = value
        else:
            for i in range(n_neurons):
                raw_data[delay_index + n_params * i] = np.uint32(value[pop_slice.start + i] - 1)

        return raw_data.tolist()
