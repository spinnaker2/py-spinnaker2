from enum import Enum

from spinnaker2.configuration import Experiment, ExperimentConfig, MemoryRegion, PEConfig
from spinnaker2.coordinates import WordAddr


class BaseApplication(object):
    """
    Base class for SpiNNaker2 PE applications

    Attributes:
      name: application name
      mem_file: path to instruction memory file
      memory_layout: layout of data memory in PE SRAM

    """

    # maximum number of neurons or spike sources per core
    max_atoms_per_core = 1

    # application can be split to multiple PEs
    splittable = True

    # list of recordables of the application, e.g., "v" or "spikes"
    recordables = []

    def __init__(self, name, mem_file=None):
        self.name = name
        self.mem_file = mem_file
        self.memory_layout = []

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        raise NotImplementedError


"""
principles
==========

1. Every application has a memory layout
2. Every application can have a data spec, but does not need to have one (TODO)
3. The memory layout is not fixed, but the region names are. The size and
   addresses of the regions may vary based on each instance
4. After mapping, for each PE application instance:
    a. Memory Layout is designed
    b. Data specification is generated
    c. PE config is generated
5. Based on Application and Memory layout, the results are processed after the experiment.
        

"""


class PEMemory(object):
    # TODO: size in words or bytes?

    def __init__(self, size=128 * 1024):
        """
        size in bytes
        """
        self.size = size
        self.regions = []

    def insert(self, mem_region):
        self.regions.append(mem_region)
        # TODO: check for overlap with other regions

    def instruction_memory(self, size=32 * 1024):
        """
        add instruction memory

        size in bytes
        """
        self.insert(NamedMemoryRegion("ITCM", MemoryRegionType.RESERVED, WordAddr(0x0), size))


class MemoryRegionType(Enum):
    UNUSED = 0  # unused, free memory region
    INPUT = 1  # input data written from host
    OUTPUT = 2  # result data generated by ARM core
    BUFFER = 3  # buffer for read write operations (e.g. DMA)
    RESERVED = 4  # reserved memory block (e.g. stack)


# TODO: separate MemBlock (size only) and MemRegion (size and start)?


class NamedMemoryRegion(MemoryRegion):
    def __init__(self, name, type, word_addr, word_count):
        super().__init__(word_addr, word_count)
        self.name = name
        self.type = type

    def __str__(self):
        return f"MemoryRegion('{self.name}', {self.type}, {self.word_addr}, {self.word_count})"
