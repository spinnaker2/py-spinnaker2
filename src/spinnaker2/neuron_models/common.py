from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import QPE, ByteAddr

N_WORDS_MPT_ENTRY = 3  # number of words per master population table


def add_log_memory_region(pe_config, byte_addr=0x0001B000, word_count=4000):
    # for all apps, add debug info
    # debug register locaction (log_info)
    SPIKE_SOURCE_DATA_BASE = byte_addr
    SPIKE_SOURCE_DEBUG_START = 3
    SPIKE_SOURCE_DEBUG_SIZE = word_count

    # base byte address
    base_address = ByteAddr(SPIKE_SOURCE_DATA_BASE + SPIKE_SOURCE_DEBUG_START * 4).to_WordAddr()
    pe_config.add_mem_region_to_read("log", MemoryRegion(base_address, SPIKE_SOURCE_DEBUG_SIZE))


def add_log_memory_region_small(pe_config):
    return add_log_memory_region(pe_config, 0x0001E000, 1000)


def format_routing_table(target_cores):
    """
    format a core-to-core routing table based on the target cores

    returns unsigned int with 16 valid bits
    format (16b): MSB-> A1,A2,A3,A4,B1,B2,B3,B4,C1,C2,C3,C4,D1,D2,D3,D4 <- LSB
    # A: QPE(x=0, y=1), B: QPE(0,2), C: QPE(1,1), QPE(1,2)

    # TODO: this implementation is specific to the 4QPE FPGA design and needs
    # to be updated for SpiNNaker2
    """
    routing_table = 0x0
    for core in target_cores:
        # check for PE coordinates in range
        assert 0 <= core.x < 2 and 0 < core.y < 3 and 0 <= core.pe < 4
        shift = core.x * 8 + (core.y - 1) * 4 + core.pe
        routing_table |= 1 << (15 - shift)
    return routing_table


def collect_routing_targets(target_cores) -> dict:
    """
    collect routing targets based on list of target cores.

    One `routing target` contains 1 to 4 PEs on one QuadPE.
    The routing target is defined by the QPE coordinate and a list of 4 bits.
    List[0] is for PE 0, List[1] for PE 1 etc.

    returns a dictionary with:
        key: ~spinnaker2.coordinates.QPE
        value: list of 4 bits. The list index represents the PE.
    """
    targets = {}
    for core in target_cores:
        qpe = QPE(core.x, core.y)
        if qpe not in targets:
            targets[qpe] = [0, 0, 0, 0]
        targets[qpe][core.pe] = 1
    return targets


def format_routing_targets(target_qpes_and_pes) -> list:
    """
    generates an array with raw presentation of routing targets

    struct routing_target {
      uint8_t qpe_x;
      uint8_t qpe_y;
      uint8_t pes; // 4 valid bits. MSB: PE0, LSB: PE3
      };

    routing_target[] routing_targets;

    returns a list of integers
    """
    raw = []
    for qpe, pes in target_qpes_and_pes.items():
        value = qpe.x & 0x7
        value |= (qpe.y & 0x7) << 8
        pe_int = 0
        for bit in pes:
            pe_int = (pe_int << 1) | bit
        value |= (pe_int & 0xF) << 16
        raw.append(value)
    return raw
