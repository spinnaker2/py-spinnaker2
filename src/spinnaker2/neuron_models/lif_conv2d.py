import ctypes

import numpy as np

from spinnaker2 import snn
from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.mapper import Neuron, SynapseWordSize, SynapseWordSpec
from spinnaker2.mla import mla_helpers
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    N_WORDS_MPT_ENTRY,
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)
from spinnaker2.neuron_models.lif_neuron import LIFApplication

# replicas of C structs in s2-sim2lab-app/chip/app-pe/s2app/lif_conv2d/conv2d.h


class Conv2DCoord(ctypes.Structure):
    _fields_ = [("row", ctypes.c_short), ("col", ctypes.c_short)]


class Conv2DShape(ctypes.Structure):
    _fields_ = [("height", ctypes.c_short), ("width", ctypes.c_short)]


class Conv2DStride(ctypes.Structure):
    _fields_ = [("x", ctypes.c_short), ("y", ctypes.c_short)]


class Conv2DPadding(ctypes.Structure):
    _fields_ = [
        ("left", ctypes.c_short),
        ("right", ctypes.c_short),
        ("top", ctypes.c_short),
        ("bottom", ctypes.c_short),
    ]


class Conv2DConfig(ctypes.Structure):
    _fields_ = [
        ("in_shape", Conv2DShape),
        ("in_channels", ctypes.c_uint),
        ("filter_shape", Conv2DShape),
        ("stride", Conv2DStride),
        ("pool", Conv2DStride),
        ("pad", Conv2DPadding),
        ("out_channels", ctypes.c_uint),
        # optional
        ("out_shape", Conv2DShape),
        ("out_start", Conv2DCoord),
        ("out_end", Conv2DCoord),
    ]


class LIFConv2dApplication(LIFApplication):
    """Application class for `lif_conv2d` neuron model.

    This class generates the configuration of the SpiNNaker2 ARM applications
    for the `lif_conv2d` neuron model.

    Note that the `lif_conv2d` neuron model can only be used with an incoming
    `Conv2DProjection`.
    """

    sw_spec = SynapseWordSpec(word_size=SynapseWordSize.SIZE_16, weight=7, delay=0, synapse_type=1, target=8)

    max_atoms_per_core = 1024

    # fixed addresses
    neuron_params_addr = ByteAddr(0xAC00)

    def __init__(self):
        app_name = "lif_conv2d"
        BaseApplication.__init__(self, name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, 4000)

        #####################
        # neuron parameters #
        #####################
        pop_slice = mapper.mapping.get_population_slice(pe)
        neuron_params = pop_slice.pop.params
        reset_method = neuron_params.get("reset", self.default_params["reset"])
        neuron_params_raw = self.convert_lif_neuron_params_to_raw_data(pop_slice)

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = mapper.key_offsets[pe]

        rt_addr = ByteAddr(self.data_spec_addr + self.data_spec_max_size * 4)
        routing_targets_addr = ByteAddr(rt_addr + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_addr.to_WordAddr(), rt_data)

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(ByteAddr(rt_addr + len(rt_data) * 4), ByteAddr(0x10))
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        #################
        # global params #
        #################
        n_neurons = pop_slice.size()
        record_spikes = "spikes" in pop_slice.pop.record
        record_v_all = "v" in pop_slice.pop.record
        record_v_last = "v_last" in pop_slice.pop.record
        record_v = 1 if record_v_all else 2 if record_v_last else 0
        time_done_flag = "time_done" in pop_slice.pop.record
        reset_by_subtraction = self.is_reset_by_subtraction(reset_method)
        global_params_raw = [
            n_neurons,
            int(record_spikes),
            int(record_v),
            int(self.profiling),
            reset_by_subtraction,
            int(time_done_flag),
        ]
        global_params_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )
        global_params_addr = ByteAddr(global_params_addr)
        config.add_mem_data_to_send(global_params_addr.to_WordAddr(), global_params_raw)

        ######################################
        # convolution weights and parameters #
        ######################################

        pop = pop_slice.pop
        # search for projection
        projs = []
        for proj in mapper.network.projections:
            if proj.post == pop and isinstance(proj, snn.Conv2DProjection):
                projs.append(proj)
        assert len(projs) == 1
        conv2d_proj = projs[0]

        weights = conv2d_proj.weights
        assert weights.dtype == np.int8
        conv_params = conv2d_proj.params

        in_channels, out_channels, filter_height, filter_width = weights.shape

        # Which output channels are mapped onto this PE?
        output_shape = (
            out_channels,  # C
            mla_helpers.conv1d_output_size(
                (conv_params["in_height"] // conv_params["pool_x"]),
                filter_height,
                conv_params["stride_y"],
            )
            + (conv_params["pad_top"] + conv_params["pad_bottom"]) // conv_params["stride_x"],  # H
            mla_helpers.conv1d_output_size(
                (conv_params["in_width"] // conv_params["pool_y"]),
                filter_width,
                conv_params["stride_x"],
            )
            + (conv_params["pad_left"] + conv_params["pad_right"]) // conv_params["stride_y"],  # W
        )

        # 1. check if actual pop size matches conv2d output size
        assert np.prod(output_shape) == pop.size
        # 2. check if pop slice is one or multiple channels
        fmap_size = np.prod(output_shape[1:])
        assert pop_slice.start % fmap_size == 0
        assert pop_slice.size() % fmap_size == 0

        channels_start = pop_slice.start // fmap_size
        channels_stop = pop_slice.stop // fmap_size
        channels_this_pe = channels_stop - channels_start

        weights_this_pe = weights[:, channels_start:channels_stop, :, :]

        cfg = Conv2DConfig()
        cfg.in_shape.height = conv_params["in_height"]
        cfg.in_shape.width = conv_params["in_width"]
        cfg.in_channels = in_channels
        cfg.filter_shape.height = filter_height
        cfg.filter_shape.width = filter_width
        cfg.stride.x = conv_params["stride_x"]
        cfg.stride.y = conv_params["stride_y"]
        cfg.pool.x = conv_params["pool_x"]
        cfg.pool.y = conv_params["pool_y"]
        cfg.pad.top = conv_params["pad_top"]
        cfg.pad.bottom = conv_params["pad_bottom"]
        cfg.pad.left = conv_params["pad_left"]
        cfg.pad.right = conv_params["pad_right"]
        cfg.out_channels = channels_this_pe
        cfg.out_shape.height = output_shape[1]
        cfg.out_shape.width = output_shape[2]
        cfg.out_start.row = 0
        cfg.out_start.col = 0
        cfg.out_end.row = output_shape[1] - 1
        cfg.out_end.col = output_shape[2] - 1

        cfg_raw = np.frombuffer(bytearray(cfg), dtype=np.uint32).tolist()

        # Get key_offset of source population
        source_key_offset = mapper.get_routing_key(Neuron(conv2d_proj.pre, 0))

        conv2d_params_region_raw = [int(source_key_offset)] + cfg_raw

        # Conv2D params
        conv2d_params_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(global_params_addr + len(global_params_raw) * 4), ByteAddr(0x10)
        )
        conv2d_params_addr = ByteAddr(conv2d_params_addr)
        conv2d_params_len = len(conv2d_params_region_raw)
        config.add_mem_data_to_send(conv2d_params_addr.to_WordAddr(), conv2d_params_region_raw)

        # Conv2D weights
        conv2d_weights_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(conv2d_params_addr + conv2d_params_len * 4), ByteAddr(0x10)
        )
        conv2d_weights_addr = ByteAddr(conv2d_weights_addr)

        # if array size is not multiple of 4, append 0s.
        if remainder := weights_this_pe.size % 4:
            weights_this_pe = np.append(weights_this_pe, np.zeros(4 - remainder, dtype=weights_this_pe.dtype))
        weights_this_pe_list = weights_this_pe.ravel().view(np.uint32).tolist()
        conv2d_weights_addr_end = ByteAddr(conv2d_weights_addr + len(weights_this_pe_list) * 4)

        ###################
        # spike recording #
        ###################
        if record_spikes:
            SPIKE_RECORD_LENGTH = (self.max_atoms_per_core + 31) // 32 + 2
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            spike_recording_total_words = SPIKE_RECORD_LENGTH * timesteps_to_record
            spike_record_addr = align_addr_to_next_multiple_of_other(conv2d_weights_addr_end, ByteAddr(0x10))
            spike_record_addr = ByteAddr(spike_record_addr)
            config.add_mem_region_to_read(
                "spike_record",
                MemoryRegion(spike_record_addr.to_WordAddr(), spike_recording_total_words),
            )
            spike_record_addr_end = ByteAddr(spike_record_addr + spike_recording_total_words * 4)
        else:
            spike_record_addr = conv2d_weights_addr_end
            spike_record_addr_end = conv2d_weights_addr_end

        #####################
        # voltage recording #
        #####################
        if record_v > 0:
            timesteps_to_record = sim_cfg["n_simulation_ticks"] if record_v_all else 1
            voltage_record_addr = align_addr_to_next_multiple_of_other(spike_record_addr_end, ByteAddr(0x10))
            voltage_record_addr = ByteAddr(voltage_record_addr)
            # for each timestep: 1 word header + n_neurons*voltages
            voltage_recording_total_words = (1 + n_neurons) * timesteps_to_record
            config.add_mem_region_to_read(
                "voltage_record",
                MemoryRegion(voltage_record_addr.to_WordAddr(), voltage_recording_total_words),
            )
            voltage_record_addr_end = ByteAddr(voltage_record_addr + voltage_recording_total_words * 4)
        else:
            voltage_record_addr = spike_record_addr_end
            voltage_record_addr_end = spike_record_addr_end

        #######################
        # time done recording #
        #######################
        if time_done_flag:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            time_done_addr = align_addr_to_next_multiple_of_other(voltage_record_addr_end, ByteAddr(0x10))
            time_done_addr = ByteAddr(time_done_addr)
            config.add_mem_region_to_read(
                "time_done_record", MemoryRegion(time_done_addr.to_WordAddr(), timesteps_to_record)
            )
            time_done_addr_end = ByteAddr(time_done_addr + timesteps_to_record * 4)

        else:
            time_done_addr = voltage_record_addr
            time_done_addr_end = voltage_record_addr_end

        # check that recording does not go into log region
        if time_done_addr_end > self.log_addr:
            raise MemoryError(
                f"Not enough memory in population {pop_slice.pop.name}: "
                f"{hex(time_done_addr_end)}, {hex(self.log_addr)} "
                f"by {time_done_addr_end- self.log_addr} byte"
            )

        ######################
        # data specification #
        ######################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            rt_addr,  # start of routing table
            timer_config_addr,  # start of timer config
            global_params_addr,  # start of global params
            conv2d_params_addr,  # start of conv2d params
            conv2d_weights_addr,  # start of conv2d weights
            self.neuron_params_addr,  # start of neuron params
            spike_record_addr,  # start of spike records
            voltage_record_addr,  # start of voltage records
            time_done_addr,  # start of time_done records
            self.log_addr,  # start of log (dummy)
        ]
        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)
        config.add_mem_data_to_send(conv2d_weights_addr.to_WordAddr(), weights_this_pe_list)
        config.add_mem_data_to_send(self.neuron_params_addr.to_WordAddr(), neuron_params_raw)

        return config
