import numpy as np

from spinnaker2 import mapper
from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.mapper import SynapseWordSize, SynapseWordSpec
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    N_WORDS_MPT_ENTRY,
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class ConvLifHybridApplication(BaseApplication):
    """
    Hybrid layer of 2D Convolution and LIF neurons. The output of the convolution is used as pseudo current into the LIF
    neurons: The output of the Conv2D is vector-matrix-multiplied with a weight matrix and the result is added to the
    membrane potentials.
    Requires:
        - Image address: SRAM location where the input image is stored.
        - Image size: Size of the Input image in byes (i.e. number of pixels times number of channels in 8bit mode).
        - Result address: SRAM location where the convolution output should be stored.
        - Result size: Size of the convolution result in bytes (i.e. number of pixels in 8bit output).
        - Sim config: Number of simulation steps and timer period.
        - Filter Kernels: The weights of the convolutional layer. Quantized to
          8bit and packed with 4 weights in a 32bit word
        - Forward LIF weights: Weights of the forward connection (Conv2D->LIF).
          Quantized to 8bit and packet with 4 weights in a 32bit word
        - Recurrent LIF weights: Weights of the recurrent connection. Quantized
          to 8bit and packet with 4 weights in a 32bit word
    """

    profiling = False

    default_params = {
        "alpha_decay": 0.8,
        "threshold": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_by_subtraction",
    }

    max_atoms_per_core = 60
    splittable = False
    recordables = ["spikes", "v"]

    # Fixed addresses
    neuron_params_addr = ByteAddr(0x8F00)
    data_spec_addr = ByteAddr(0x9F00)
    data_spec_max_size = 16  # in words
    log_addr = ByteAddr(0x1E000)
    log_size = 1000  # Byte
    reshape_addr = ByteAddr(0x1D000)
    reshape_size = 1000  # Byte

    def __init__(self):
        app_name = "conv_lif_hybrid"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        # TODO: implement MPT and Syn Rows

        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, self.log_size)

        #####################
        # neuron parameters #
        #####################
        pop_slice = mapper.mapping.get_population_slice(pe)
        neuron_params = pop_slice.pop.params
        reset_method = neuron_params.get("reset", self.default_params["reset"])
        neuron_params_raw = self.convert_lif_neuron_params_to_raw_data(pop_slice)

        #################
        # image address #
        #################

        image_addr = ByteAddr(neuron_params["image_addr"])
        # config.add_mem_region_to_read("image", MemoryRegion(image_addr.to_WordAddr(),
        #                                                     int(neuron_params["image_size"] / 4)))

        #################
        # Conv Res Addr #
        #################

        conv_res_addr = ByteAddr(neuron_params["conv_res_addr"])
        # 2 times size for ping pong buffer
        config.add_mem_region_to_read(
            "result",
            MemoryRegion(conv_res_addr.to_WordAddr(), int(2 * neuron_params["conv_res_size"] / 4)),
        )
        conv_res_addr_end = ByteAddr(conv_res_addr + 2 * neuron_params["conv_res_size"])

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = mapper.key_offsets[pe]

        rt_addr = align_addr_to_next_multiple_of_other(conv_res_addr_end, ByteAddr(0x10))
        rt_addr = ByteAddr(rt_addr)
        routing_targets_addr = ByteAddr(rt_addr + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_addr.to_WordAddr(), rt_data)

        ##################
        # source PE info #
        ##################
        # TODO: make it more general. This appraoch assumes, that each conv layer is in the next core
        if pe.pe != 0:
            source_qpe_x, source_qpe_y, source_pe = pe.x, pe.y, pe.pe - 1
        elif pe.pe == 0 and pe.y > 1:
            source_qpe_x, source_qpe_y, source_pe = pe.x, pe.y - 1, 3
        elif pe.pe == 0 and pe.x > 0:
            source_qpe_x, source_qpe_y, source_pe = pe.x - 1, pe.y + 1, 3
        else:
            raise ValueError("Something went wrong")

        # TODO: More flexible way to give correct locations of input for first and later layers
        if source_qpe_x == 0 and source_qpe_y == 1 and source_pe == 0:
            pe_info_raw = [
                source_qpe_x,
                source_qpe_y,
                source_pe,
                neuron_params["image_addr"],
                neuron_params["image_size"],
                neuron_params["conv_res_size"],
            ]
        else:
            pe_info_raw = [
                source_qpe_x,
                source_qpe_y,
                source_pe,
                neuron_params["conv_res_addr"],
                neuron_params["image_size"],
                neuron_params["conv_res_size"],
            ]
        # config.add_mem_region_to_read("reshaped_array", MemoryRegion(self.reshape_addr.to_WordAddr(),
        #                                                              self.reshape_size))
        pe_info_addr = align_addr_to_next_multiple_of_other(ByteAddr(rt_addr + len(rt_data) * 4), ByteAddr(0x10))
        pe_info_addr = ByteAddr(pe_info_addr)
        config.add_mem_data_to_send(pe_info_addr.to_WordAddr(), pe_info_raw)

        ################
        # timer config #
        ################

        timer_config_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(pe_info_addr + len(pe_info_raw) * 4), ByteAddr(0x10)
        )
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        #################
        # global params #
        #################

        n_neurons = pop_slice.size()
        record_spikes = "spikes" in pop_slice.pop.record
        record_v = "v" in pop_slice.pop.record
        reset_by_subtraction = self.is_reset_by_subtraction(reset_method)
        global_params_raw = [
            n_neurons,
            int(record_spikes),
            int(record_v),
            int(self.profiling),
            reset_by_subtraction,
        ]
        global_params_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )
        global_params_addr = ByteAddr(global_params_addr)
        global_params_addr_end = ByteAddr(global_params_addr + len(global_params_raw) * 4)
        config.add_mem_data_to_send(global_params_addr.to_WordAddr(), global_params_raw)

        ##############
        # MLA config #
        ##############

        # TODO: fix address when syn rows are implemented
        mla_config_addr = align_addr_to_next_multiple_of_other(global_params_addr_end, ByteAddr(0x10))
        mla_config_addr = ByteAddr(mla_config_addr)
        mla_config_raw = self.convert_conv_params_to_raw_data(pop_slice)
        mla_config_addr_end = ByteAddr(mla_config_addr + len(mla_config_raw) * 4)

        ################
        # Conv Weights #
        ################

        conv_weight_addr = align_addr_to_next_multiple_of_other(mla_config_addr_end, ByteAddr(0x10))
        conv_weight_addr = ByteAddr(conv_weight_addr)
        conv_weight_raw = pop_slice.pop.params["conv_weights"]
        conv_weight_addr_end = ByteAddr(conv_weight_addr + len(conv_weight_raw) * 4)

        #######################
        # Forward LIF weights #
        #######################

        forw_lif_weight_addr = align_addr_to_next_multiple_of_other(conv_weight_addr_end, ByteAddr(0x10))
        forw_lif_weight_addr = ByteAddr(forw_lif_weight_addr)
        forw_lif_weight_raw = pop_slice.pop.params["forw_lif_weights"]
        forw_lif_weight_addr_end = ByteAddr(forw_lif_weight_addr + len(forw_lif_weight_raw) * 4)

        #########################
        # Recurrent LIF weights #
        #########################

        rec_lif_weight_addr = align_addr_to_next_multiple_of_other(forw_lif_weight_addr_end, ByteAddr(0x10))
        rec_lif_weight_addr = ByteAddr(rec_lif_weight_addr)
        rec_lif_weight_raw = pop_slice.pop.params["rec_lif_weights"]
        rec_lif_weight_addr_end = ByteAddr(rec_lif_weight_addr + len(rec_lif_weight_raw) * 4)

        ###############
        # Matmul Addr #
        ###############

        matmul_res_addr = align_addr_to_next_multiple_of_other(rec_lif_weight_addr_end, ByteAddr(0x10))
        matmul_res_addr = ByteAddr(matmul_res_addr)
        # config.add_mem_region_to_read("matmul_result", MemoryRegion(matmul_res_addr.to_WordAddr(), n_neurons))
        matmul_res_addr_end = ByteAddr(matmul_res_addr + n_neurons * 4)

        ###################
        # spike recording #
        ###################

        SPIKE_RECORD_LENGTH = (self.max_atoms_per_core + 31) // 32 + 2
        timesteps_to_record = sim_cfg["n_simulation_ticks"]
        spike_recording_total_words = SPIKE_RECORD_LENGTH * timesteps_to_record
        spike_record_addr = align_addr_to_next_multiple_of_other(matmul_res_addr_end, ByteAddr(0x10))
        spike_record_addr = ByteAddr(spike_record_addr)
        config.add_mem_region_to_read(
            "spike_record",
            MemoryRegion(spike_record_addr.to_WordAddr(), spike_recording_total_words),
        )
        spike_record_addr_end = ByteAddr(spike_record_addr + spike_recording_total_words * 4)

        #####################
        # voltage recording #
        #####################
        if record_v:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            voltage_record_addr = align_addr_to_next_multiple_of_other(spike_record_addr_end, ByteAddr(0x10))
            voltage_record_addr = ByteAddr(voltage_record_addr)
            # for each timestep: 1 word header + n_neurons*voltages
            voltage_recording_total_words = (1 + n_neurons) * timesteps_to_record
            config.add_mem_region_to_read(
                "voltage_record",
                MemoryRegion(voltage_record_addr.to_WordAddr(), voltage_recording_total_words),
            )
            voltage_record_addr_end = ByteAddr(voltage_record_addr + voltage_recording_total_words * 4)
        else:
            voltage_record_addr = spike_record_addr_end
            voltage_record_addr_end = spike_record_addr_end

        # check that recording does not go into log region
        assert voltage_record_addr_end <= self.reshape_addr, (
            f"Data too large! last used addr: "
            f"{hex(voltage_record_addr_end)},"
            f"addr space end: {hex(self.reshape_addr)}"
        )

        # #####################
        # data specification #
        # #####################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            image_addr,
            conv_res_addr,  # start of conv result
            rt_addr,  # start of routing table
            pe_info_addr,
            timer_config_addr,  # start of timer config
            global_params_addr,  # start of global params
            mla_config_addr,  # start of MLA config
            conv_weight_addr,  # start of conv weights
            forw_lif_weight_addr,  # start of forward lif weights
            rec_lif_weight_addr,  # start of recurrent lif weights
            matmul_res_addr,  # start of matmul result
            self.neuron_params_addr,  # start of neuron params
            spike_record_addr,  # start of spike records
            voltage_record_addr,  # start of voltage records
            self.reshape_addr,
            self.log_addr,  # start of log (dummy)
        ]

        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)
        config.add_mem_data_to_send(mla_config_addr.to_WordAddr(), mla_config_raw)
        config.add_mem_data_to_send(conv_weight_addr.to_WordAddr(), conv_weight_raw)
        config.add_mem_data_to_send(forw_lif_weight_addr.to_WordAddr(), forw_lif_weight_raw)
        config.add_mem_data_to_send(rec_lif_weight_addr.to_WordAddr(), rec_lif_weight_raw)
        config.add_mem_data_to_send(self.neuron_params_addr.to_WordAddr(), neuron_params_raw)

        return config

    @staticmethod
    def is_reset_by_subtraction(method):
        if method == "reset_by_subtraction":
            return 1
        elif method == "reset_to_v_reset":
            return 0
        else:
            raise TypeError("param 'reset' of lif_neuron must be either " "'reset_by_subtraction' or 'reset_to_v_rest'")

    @classmethod
    def convert_lif_neuron_params_to_raw_data(cls, pop_slice):
        """
        convert the lif neuron params to raw data.

        In the ARM C program, each neuron has a struct:
        >>>
            typedef float REAL;
            typedef struct neuron_params_t {
                REAL alpha_decay;
                REAL threshold;
                REAL I_offset;
                REAL V_reset;
            } neuron_params_t;
            neuron_params_t neuron_params_array[N_NEURONS];
        <<<

        returns a list of uint32 representing the raw data to be sent to SpiNNaker2
        """

        ordered_param_names = ["alpha_decay", "threshold", "i_offset", "v_reset"]
        n_params = len(ordered_param_names)
        max_neurons = cls.max_atoms_per_core

        float_array = np.zeros(max_neurons * n_params, dtype=np.float32)

        # default values
        alpha_decay_default = cls.default_params["alpha_decay"]
        threshold_default = cls.default_params["threshold"]
        i_offset_default = cls.default_params["i_offset"]
        v_reset_default = cls.default_params["v_reset"]

        # populate with default values
        for i in range(max_neurons):
            float_array[i * n_params + 0] = alpha_decay_default
            float_array[i * n_params + 1] = threshold_default
            float_array[i * n_params + 2] = i_offset_default
            float_array[i * n_params + 3] = v_reset_default

        # fill custom values
        params = pop_slice.pop.params["neuron_params"]
        n_neurons = pop_slice.size()
        for i, key in enumerate(ordered_param_names):
            value = params.get(key, cls.default_params[key])
            if np.isscalar(value):  # same value for all
                float_array[i : i + n_params * n_neurons : n_params] = value
            else:  # array like
                assert len(value) == pop_slice.pop.size
                float_array[i : i + n_params * n_neurons : n_params] = value[pop_slice.start : pop_slice.stop]

        # convert to uint32 array and return as list
        raw_data = np.frombuffer(float_array.data, dtype=np.uint32)

        return raw_data.tolist()

    @classmethod
    def convert_conv_params_to_raw_data(cls, pop_slice):
        params = pop_slice.pop.params["mla_config"]

        # send parameters to PE
        conv_parameter_raw = np.array(
            [
                params["op_a_use_noc"],
                params["op_a_is_signed"],
                params["op_b_is_signed"],
                params["op_a_is_16bit"],
                params["op_b_is_16bit"],
                params["output_shift_width"],
                params["output_mode"],
                params["relu_en"],
                params["in_batch_s"],
                params["in_pix_row_s"],
                params["in_pix_col_s"],
                params["fil_pix_row_s"],
                params["fil_pix_col_s"],
                params["in_channel_s"],
                params["out_channel_s"],
                params["stride_in_pix_row"],
                params["stride_in_pix_col"],
            ]
        ).astype("uint32")

        return conv_parameter_raw.tolist()
