import numpy as np

from spinnaker2.coordinates import ByteAddr
from spinnaker2.mapper import SynapseWordSize, SynapseWordSpec
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.lif_neuron import LIFApplication


class LIFCurrExpApplication(LIFApplication):

    default_params = {
        "alpha_decay": 0.8,
        "threshold": 1.0,
        "i_offset": 0.0,
        "v_init": 0.0,
        "v_reset": 0.0,
        "reset": "reset_by_subtraction",
        "exc_decay": 0.5,
        "inh_decay": 0.5,
        "t_refrac": 0,
    }

    max_atoms_per_core = 250
    splittable = True
    recordables = ["spikes", "v", "v_last"]
    # fixed addresses
    neuron_params_addr = ByteAddr(0xDC00)
    data_spec_addr = ByteAddr(0x10000)  # until 0x10080
    log_addr = ByteAddr(0x1B000)
    data_spec_max_size = 16  # in words

    def __init__(self):
        app_name = "lif_curr_exp"
        BaseApplication.__init__(self, name=app_name)

    @classmethod
    def convert_lif_neuron_params_to_raw_data(cls, pop_slice):
        """
        convert the lif neuron params to raw data.

        In the ARM C program, each neuron has a struct:
        >>>
            typedef float REAL;
            typedef struct neuron_params_t {
                REAL alpha_decay;
                REAL threshold;
                REAL I_offset;
                REAL V_init;
                REAL V_reset;
                REAL exc_decay;
                REAL inh_decay;
                uint32_t T_refract;
            } neuron_params_t;
            neuron_params_t neuron_params_array[N_NEURONS];
        <<<

        returns a list of uint32 representing the raw data to be sent to SpiNNaker2
        """

        ordered_param_names = [
            "alpha_decay",
            "threshold",
            "i_offset",
            "v_init",
            "v_reset",
            "exc_decay",
            "inh_decay",
            "t_refrac",
        ]
        n_params = len(ordered_param_names)
        max_neurons = cls.max_atoms_per_core

        float_array = np.zeros(max_neurons * n_params, dtype=np.float32)

        # default values
        alpha_decay_default = cls.default_params["alpha_decay"]
        threshold_default = cls.default_params["threshold"]
        i_offset_default = cls.default_params["i_offset"]
        v_init_default = cls.default_params["v_init"]
        v_reset_default = cls.default_params["v_reset"]
        exc_decay_default = cls.default_params["exc_decay"]
        inh_decay_default = cls.default_params["inh_decay"]
        t_refrac_default = cls.default_params["t_refrac"]

        # float values
        # populate with default values
        for i in range(max_neurons):
            float_array[i * n_params + 0] = alpha_decay_default
            float_array[i * n_params + 1] = threshold_default
            float_array[i * n_params + 2] = i_offset_default
            float_array[i * n_params + 3] = v_init_default
            float_array[i * n_params + 4] = v_reset_default
            float_array[i * n_params + 5] = exc_decay_default
            float_array[i * n_params + 6] = inh_decay_default

        # fill custom values
        params = pop_slice.pop.params
        n_neurons = pop_slice.size()
        for i, key in enumerate(ordered_param_names[:-1]):
            value = params.get(key, cls.default_params[key])
            if np.isscalar(value) or len(value) == 1:  # same value for all
                float_array[i : i + n_params * n_neurons : n_params] = value
            else:  # array like
                assert len(value) == pop_slice.pop.size
                float_array[i : i + n_params * n_neurons : n_params] = value[pop_slice.start : pop_slice.stop]

        # convert to uint32 array and return as list
        raw_data = np.frombuffer(float_array.data, dtype=np.uint32)

        # uint32 values
        # populate with default values
        for i in range(max_neurons):
            raw_data[i * n_params + 7] = t_refrac_default

        for i, key in [(7, "t_refrac")]:
            value = params.get(key, cls.default_params[key])
            if np.isscalar(value):  # same value for all
                raw_data[i : i + n_params * n_neurons : n_params] = value
            else:  # array like
                assert len(value) == pop_slice.pop.size
                raw_data[i : i + n_params * n_neurons : n_params] = value[pop_slice.start : pop_slice.stop]

        return raw_data.tolist()


class LIFCurrExpNoDelayApplication(LIFCurrExpApplication):

    sw_spec = SynapseWordSpec(word_size=SynapseWordSize.SIZE_16, weight=7, delay=0, synapse_type=1, target=8)

    def __init__(self):
        app_name = "lif_curr_exp_no_delay"
        BaseApplication.__init__(self, name=app_name)
