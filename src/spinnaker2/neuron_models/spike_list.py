import struct

import numpy as np

from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class SpikeListApplication(BaseApplication):

    max_atoms_per_core = 500
    splittable = True
    recordables = ["time_done"]

    def __init__(self):
        app_name = "spike_list"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """

        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config)

        #############
        # pop slice #
        #############
        pop_slice = mapper.mapping.get_population_slice(pe)

        data_spec_addr = ByteAddr(0x9000)
        data_spec_size = 16  # default: 16 words
        # check if input spike memory region overlaps with log
        log_addr = ByteAddr(0x0001B000)

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = 0  # key offset is already included in spike rows

        rt_address = align_addr_to_next_multiple_of_other(ByteAddr(data_spec_addr + data_spec_size * 4), ByteAddr(0x10))
        rt_address = ByteAddr(rt_address)
        routing_targets_addr = ByteAddr(rt_address + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_address.to_WordAddr(), rt_data)

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(rt_address + len(rt_data) * 4), ByteAddr(0x10)
        )
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        #################
        # global params #
        #################
        time_done_flag = "time_done" in pop_slice.pop.record
        global_params_raw = [int(time_done_flag)]
        global_params_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )  # timer_config_addr + len(sim_config)*4), ByteAddr(0x10))
        global_params_addr = ByteAddr(global_params_addr)
        config.add_mem_data_to_send(global_params_addr.to_WordAddr(), global_params_raw)

        #######################
        # time done recording #
        #######################
        if time_done_flag:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            time_done_addr = align_addr_to_next_multiple_of_other(
                ByteAddr(global_params_addr + len(global_params_raw) * 4), ByteAddr(0x10)
            )
            time_done_addr = ByteAddr(time_done_addr)
            config.add_mem_region_to_read(
                "time_done_record", MemoryRegion(time_done_addr.to_WordAddr(), timesteps_to_record)
            )
            time_done_addr_end = ByteAddr(time_done_addr + timesteps_to_record * 4)

        else:
            timesteps_to_record = 0
            time_done_addr = global_params_addr
            time_done_addr_end = ByteAddr(global_params_addr + len(global_params_raw) * 4)

        ################
        # input spikes #
        ################

        # convert spikes
        spike_times_dict = pop_slice.pop.params
        spike_times_list_pe = [[] for i in range(pop_slice.size())]

        n_total_spikes_outside_sim_time = 0

        # extract spike sources for this PE
        for idx in spike_times_dict:
            if idx >= pop_slice.start and idx < pop_slice.stop:
                id_on_pe = idx - pop_slice.start
                spike_times = spike_times_dict[idx]

                # select spikes within simulation time
                n_spikes = len(spike_times)
                spike_times = list(filter(lambda x: 0 <= x < sim_cfg["n_simulation_ticks"], spike_times))
                n_spikes_within_sim_time = len(spike_times)
                n_total_spikes_outside_sim_time += n_spikes - n_spikes_within_sim_time

                # convert to uint32 array and store in dict
                spike_times_list_pe[id_on_pe] = np.array(spike_times, dtype=np.uint32)

        if n_total_spikes_outside_sim_time > 0:
            print(
                f"Warning: {n_total_spikes_outside_sim_time} input spikes don't lie within the simulation time on "
                f"{pe}. These spikes will be discarded."
            )

        # generate input spike rows
        spike_times_raw = get_input_spike_rows(spike_times_list_pe, mapper.key_offsets[pe])
        input_spikes_addr = align_addr_to_next_multiple_of_other(ByteAddr(time_done_addr_end), ByteAddr(0x10))
        input_spikes_addr = ByteAddr(input_spikes_addr)
        config.add_mem_data_to_send(input_spikes_addr.to_WordAddr(), spike_times_raw)

        # check that synrows does not go into log region
        input_spikes_addr_end = ByteAddr(input_spikes_addr + len(spike_times_raw) * 4)
        if input_spikes_addr_end > log_addr:
            raise MemoryError(
                f"Input spikes too large in population {pop_slice.pop.name}: "
                f"{hex(input_spikes_addr_end)}, {hex(log_addr)} "
                f"by {input_spikes_addr_end - log_addr} byte"
            )

        ######################
        # data specification #
        ######################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            rt_address,  # start of routing table
            timer_config_addr,  # start of timer config
            global_params_addr,  # start of global params
            time_done_addr,  # start of time done record
            input_spikes_addr,  # start of input spike rows
            log_addr,  # start of log (dummy)
        ]

        config.add_mem_data_to_send(data_spec_addr.to_WordAddr(), data_spec)

        return config


def _format_input_spikes(spike_time_lists, key_offset):
    """
    format input spike times for loading into PE's SRAM

    :param spike_time_lists list of lists of spike times
    :param key_offset key offset on PE
    """
    x = dict()
    time_step_to_keys = dict()
    for idx, time_steps in enumerate(spike_time_lists):
        key = idx + key_offset
        for t in time_steps:
            if t in time_step_to_keys:
                time_step_to_keys[t].append(key)
            else:
                time_step_to_keys[t] = [key]

    bytestring = b""
    for t in sorted(time_step_to_keys.keys()):
        keys = time_step_to_keys[t]
        # for each time step with spikes generate one or more spike rows with
        # up to 5 spikes
        n_keys = len(keys)
        start = 0
        while start < n_keys:
            stop = min(start + 5, n_keys)
            keys_this_loop = keys[start:stop]
            count_this_loop = len(keys_this_loop)
            assert count_this_loop <= 5
            input_spike_row = struct.pack("IH", t, count_this_loop)
            while len(keys_this_loop) < 5:
                keys_this_loop.append(0)
            input_spike_row += struct.pack("5H", *keys_this_loop)
            bytestring += input_spike_row
            start = stop

    # add empty list at t_max+1
    t_max = max(0, *time_step_to_keys.keys(), 0)
    input_spike_row = struct.pack("I6H", t_max + 1, 0, 0, 0, 0, 0, 0)
    bytestring += input_spike_row

    return bytestring


def get_input_spike_rows(spike_times, key_offset):
    """
    get input spike rows from spike times for one PE

    :param spike_times list of lists of spike times. The size of the outer list
        is equal to the neurons mapped to the PE. The size of the inner lists
        with spike times is flexible.
    :param key_offset key offset of PE. Will be added already to the spike row
        data instead of adding it on the ARM core to save time.
    """
    input_spike_rows = _format_input_spikes(spike_times, key_offset)
    n_bytes = len(input_spike_rows)
    words = struct.unpack("{}I".format(n_bytes // 4), input_spike_rows)
    return words
