import struct

import numpy as np

from spinnaker2.configuration import PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class FloatListApplication(BaseApplication):

    max_atoms_per_core = 500
    splittable = False
    recordables = []

    def __init__(self):
        app_name = "float_list"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """

        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config)

        data_spec_addr = ByteAddr(0x9000)
        data_spec_size = 16  # default: 16 words

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = 0  # key offset is already included in spike rows

        rt_address = align_addr_to_next_multiple_of_other(ByteAddr(data_spec_addr + data_spec_size * 4), ByteAddr(0x10))
        rt_address = ByteAddr(rt_address)
        routing_targets_addr = ByteAddr(rt_address + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_address.to_WordAddr(), rt_data)

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(rt_address + len(rt_data) * 4), ByteAddr(0x10)
        )
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        ################
        # input spikes #
        ################

        # convert spikes
        pop_slice = mapper.mapping.get_population_slice(pe)
        floats_dict = pop_slice.pop.params
        floats_list_pe = [[] for i in range(pop_slice.size())]

        # extract spike sources for this PE
        for idx in floats_dict:
            if idx >= pop_slice.start and idx < pop_slice.stop:
                id_on_pe = idx - pop_slice.start
                floats_list_pe[id_on_pe] = np.array(floats_dict[idx])

        # generate input spike rows
        pop_slice = mapper.mapping.get_population_slice(pe)
        n_neurons = pop_slice.pop.size
        floats_raw = get_input_float_rows(floats_list_pe)
        input_spikes_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )
        input_spikes_addr = ByteAddr(input_spikes_addr)
        config.add_mem_data_to_send(input_spikes_addr.to_WordAddr(), [n_neurons] + floats_raw)

        # check if input spike memory region overlaps with log
        log_addr = ByteAddr(0x0001B000)
        # check that synrows does not go into log region
        input_spikes_addr_end = ByteAddr(input_spikes_addr + len(floats_raw) * 4)
        if input_spikes_addr_end > log_addr:
            print("Input spikes too large:", hex(input_spikes_addr_end), hex(log_addr))
            assert input_spikes_addr_end <= log_addr

        ######################
        # data specification #
        ######################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            rt_address,  # start of routing table
            timer_config_addr,  # start of timer config
            input_spikes_addr,  # start of input spike rows
            log_addr,  # start of log (dummy)
        ]

        config.add_mem_data_to_send(data_spec_addr.to_WordAddr(), data_spec)

        return config


def get_input_float_rows(floats_list):
    """
    get input float rows from floats for one PE.

    :param floats_list list of lists of floats. The size of the outer list
        is equal to the neurons mapped to the PE. The size of the inner lists
        with floats is flexible.
    """

    n_neurons = len(floats_list)
    n_timesteps = len(floats_list[0])

    float_array = np.zeros(n_neurons * n_timesteps * 2, dtype=np.float32)

    for time in range(n_timesteps):
        # NOTE only one neuron per population
        # for neuron_idx in range(n_neurons):

        float_array[time * 2] = floats_list[0][time].real
        float_array[time * 2 + 1] = floats_list[0][time].imag

    # convert to uint32 array and return as list
    raw_data = np.frombuffer(float_array.data, dtype=np.uint32)

    return raw_data.tolist()
