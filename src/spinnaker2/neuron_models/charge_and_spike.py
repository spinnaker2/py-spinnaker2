import numpy as np

from spinnaker2.coordinates import ByteAddr
from spinnaker2.mapper import SynapseWordSize, SynapseWordSpec
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.lif_neuron import LIFApplication


class ChargeAndSpikeApplication(LIFApplication):
    """Application class for `charge_and_spike` neuron model.

    This class generates the configuration of the SpiNNaker2 ARM applications
    for the `charge_and_spike` neuron model.
    This implements the neuron model from [Lopez-Randulfe et al.
    2022](https://doi.org/10.1109/TC.2022.3162708) with a charging stage and
    a spiking stage. After `t_silent`, the neuron switches from the charging to
    the spiking stage. During the spiking stage, the neuron can only spike
    once.

    Equation
    --------
    ```
    if T < t_silent:
        I_syn[t] = I_syn[t-1] + input_this_timestep
        v[t] = v[t-1] + I_syn[t]
    else:
        v[t] = v[t-1] + i_offset
    ```
    where `v` is the membrane voltage and `input_this_timestep` refers to the
    accumulated weights in this time step.

    Parameters
    ----------
    threshold : float
        spike threshold, only effective during spiking stage
    i_offset : float
        offset/bias current (float) during spiking stage
    t_silent : int
        duration of silent stage (int, number of time steps)
    """

    sw_spec = SynapseWordSpec(word_size=SynapseWordSize.SIZE_16, weight=7, delay=0, synapse_type=1, target=8)

    default_params = {
        "threshold": 1.0,
        "i_offset": 0.0,
        "v_init": 0.0,
        "t_silent": 1,
        "reset": "reset_to_v_reset",  # not used, just here for
        # consistency with lif_neuron
    }

    max_atoms_per_core = 250
    splittable = True
    recordables = ["spikes", "v", "v_last", "time_done"]

    # fixed addresses
    neuron_params_addr = ByteAddr(0xEC00)
    data_spec_addr = ByteAddr(0x10000)  # until 0x10080
    log_addr = ByteAddr(0x1B000)
    data_spec_max_size = 16  # in words

    def __init__(self):
        app_name = "charge_and_spike"
        BaseApplication.__init__(self, name=app_name)

    @classmethod
    def convert_lif_neuron_params_to_raw_data(cls, pop_slice):
        """
        convert the neuron params to raw data.

        In the ARM C program, each neuron has a struct:
        >>>
            typedef float REAL;
            typedef struct neuron_params_t {
                REAL threshold;
                REAL I_offset;
                REAL V_init;
                uint32_t T_silent;
            } neuron_params_t;
            neuron_params_t neuron_params_array[N_NEURONS];
        <<<

        returns a list of uint32 representing the raw data to be sent to SpiNNaker2
        """

        ordered_param_names = ["threshold", "i_offset", "v_init", "t_silent"]
        n_params = len(ordered_param_names)
        max_neurons = cls.max_atoms_per_core

        float_array = np.zeros(max_neurons * n_params, dtype=np.float32)

        # default values
        threshold_default = cls.default_params["threshold"]
        i_offset_default = cls.default_params["i_offset"]
        v_init_default = cls.default_params["v_init"]
        t_silent_default = cls.default_params["t_silent"]

        # float values
        # populate with default values
        for i in range(max_neurons):
            float_array[i * n_params + 0] = threshold_default
            float_array[i * n_params + 1] = i_offset_default
            float_array[i * n_params + 2] = v_init_default

        # fill custom values
        params = pop_slice.pop.params
        n_neurons = pop_slice.size()
        for i, key in enumerate(ordered_param_names[:-1]):
            value = params.get(key, cls.default_params[key])
            if np.isscalar(value):  # same value for all
                float_array[i : i + n_params * n_neurons : n_params] = value
            else:  # array like
                assert len(value) == pop_slice.pop.size
                float_array[i : i + n_params * n_neurons : n_params] = value[pop_slice.start : pop_slice.stop]

        # convert to uint32 array and return as list
        raw_data = np.frombuffer(float_array.data, dtype=np.uint32)

        # uint32 values
        # populate with default values
        for i in range(max_neurons):
            raw_data[i * n_params + 3] = t_silent_default

        for i, key in [(3, "t_silent")]:
            value = params.get(key, cls.default_params[key])
            if np.isscalar(value):  # same value for all
                raw_data[i : i + n_params * n_neurons : n_params] = value
            else:  # array like
                assert len(value) == pop_slice.pop.size
                raw_data[i : i + n_params * n_neurons : n_params] = value[pop_slice.start : pop_slice.stop]

        return raw_data.tolist()
