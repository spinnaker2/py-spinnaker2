import numpy as np

from spinnaker2 import mapper
from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.mapper import SynapseWordSize, SynapseWordSpec
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    N_WORDS_MPT_ENTRY,
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class Conv2DApplication(BaseApplication):
    """
    2D Convolutional layer with output ping pong buffer.
    Requires:
        - Image address: SRAM location where the input image is stored.
        - Image size: Size of the Input image in byes (i.e. number of pixels times number of channels in 8bit mode).
        - Result address: SRAM location where the convolution output should be stored.
        - Result size: Size of the convolution result in bytes (i.e. number of pixels in 8bit output).
        - Sim config: Number of simulation steps and timer period.
        - Filter Kernels: The weights of the convolutional layer.
    """

    profiling = False

    max_atoms_per_core = 1
    splittable = False
    recordables = []

    # Fixed addresses
    data_spec_addr = ByteAddr(0x9F00)
    data_spec_max_size = 16  # in words
    log_addr = ByteAddr(0x1E000)
    log_size = 1000  # Byte
    reshape_addr = ByteAddr(0x1D000)
    reshape_size = 1000  # Byte

    def __init__(self):
        app_name = "conv2d"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """

        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, self.log_size)

        #####################
        # neuron parameters #
        #####################

        pop_slice = mapper.mapping.get_population_slice(pe)
        neuron_params = pop_slice.pop.params

        #################
        # image address #
        #################

        image_addr = ByteAddr(neuron_params["image_addr"])
        # config.add_mem_region_to_read("image",
        #         MemoryRegion(image_addr.to_WordAddr(), int(neuron_params["image_size"] / 4)))  # 4 values in 1 word
        #################
        # Conv Res Addr #
        #################

        conv_res_addr = ByteAddr(neuron_params["conv_res_addr"])
        # 2 times size for ping pong buffer
        config.add_mem_region_to_read(
            "result",
            MemoryRegion(conv_res_addr.to_WordAddr(), int(2 * neuron_params["conv_res_size"] / 4)),
        )
        conv_res_addr_end = ByteAddr(conv_res_addr + 2 * neuron_params["conv_res_size"])

        ##################
        # source PE info #
        ##################

        # TODO: make it more general. This appraoch assumes, that each conv layer is in the next core
        if pe.pe != 0:
            source_qpe_x, source_qpe_y, source_pe = pe.x, pe.y, pe.pe - 1
        elif pe.pe == 0 and pe.y > 1:
            source_qpe_x, source_qpe_y, source_pe = pe.x, pe.y - 1, 3
        elif pe.pe == 0 and pe.x > 0:
            source_qpe_x, source_qpe_y, source_pe = pe.x - 1, pe.y + 1, 3
        else:
            raise ValueError("Something went wrong")

        # TODO: More flexible way to give correct locations of input for first and later layers
        if source_qpe_x == 0 and source_qpe_y == 1 and source_pe == 0:
            pe_info_raw = [
                source_qpe_x,
                source_qpe_y,
                source_pe,
                neuron_params["image_addr"],
                neuron_params["image_size"],
                neuron_params["conv_res_size"],
            ]
        else:
            pe_info_raw = [
                source_qpe_x,
                source_qpe_y,
                source_pe,
                neuron_params["conv_res_addr"],
                neuron_params["image_size"],
                neuron_params["conv_res_size"],
            ]
        config.add_mem_region_to_read(
            "reshaped_array", MemoryRegion(self.reshape_addr.to_WordAddr(), self.reshape_size)
        )

        source_pe_info_addr = align_addr_to_next_multiple_of_other(conv_res_addr_end, ByteAddr(0x10))
        source_pe_info_addr = ByteAddr(source_pe_info_addr)
        config.add_mem_data_to_send(source_pe_info_addr.to_WordAddr(), pe_info_raw)

        ################
        # timer config #
        ################

        timer_config_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(source_pe_info_addr + len(pe_info_raw) * 4), ByteAddr(0x10)
        )
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        ##############
        # MLA config #
        ##############

        mla_config_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )
        mla_config_addr = ByteAddr(mla_config_addr)
        mla_config_raw = self.convert_conv_params_to_raw_data(pop_slice)
        mla_config_addr_end = ByteAddr(mla_config_addr + len(mla_config_raw) * 4)

        ################
        # Conv Weights #
        ################

        conv_weight_addr = align_addr_to_next_multiple_of_other(mla_config_addr_end, ByteAddr(0x10))
        conv_weight_addr = ByteAddr(conv_weight_addr)
        conv_weight_raw = pop_slice.pop.params["conv_weights"]
        conv_weight_addr_end = ByteAddr(conv_weight_addr + len(conv_weight_raw) * 4)

        # check that recording does not go into reshape region
        assert conv_weight_addr_end <= self.reshape_addr, (
            f"Data too large! last used addr: "
            f"{hex(conv_weight_addr_end)},"
            f"addr space end: {hex(self.reshape_addr)}"
        )

        # #####################
        # data specification #
        # #####################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            image_addr,
            conv_res_addr,  # start of conv result
            source_pe_info_addr,
            timer_config_addr,  # start of timer config
            mla_config_addr,  # start of MLA config
            conv_weight_addr,  # start of conv weights
            self.reshape_addr,
            self.log_addr,  # start of log (dummy)
        ]

        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)
        config.add_mem_data_to_send(mla_config_addr.to_WordAddr(), mla_config_raw)
        config.add_mem_data_to_send(conv_weight_addr.to_WordAddr(), conv_weight_raw)

        return config

    @classmethod
    def convert_conv_params_to_raw_data(cls, pop_slice):
        params = pop_slice.pop.params["mla_config"]

        # send parameters to PE
        conv_parameter_raw = np.array(
            [
                params["op_a_use_noc"],
                params["op_a_is_signed"],
                params["op_b_is_signed"],
                params["op_a_is_16bit"],
                params["op_b_is_16bit"],
                params["output_shift_width"],
                params["output_mode"],
                params["relu_en"],
                params["in_batch_s"],
                params["in_pix_row_s"],
                params["in_pix_col_s"],
                params["fil_pix_row_s"],
                params["fil_pix_col_s"],
                params["in_channel_s"],
                params["out_channel_s"],
                params["stride_in_pix_row"],
                params["stride_in_pix_col"],
            ]
        ).astype("uint32")

        return conv_parameter_raw.tolist()
