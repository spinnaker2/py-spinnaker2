import struct

import numpy as np

from spinnaker2.configuration import PEConfig
from spinnaker2.coordinates import ByteAddr
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import add_log_memory_region, format_routing_table


class SpikesFromImagesApplication(BaseApplication):

    max_atoms_per_core = 1024
    splittable = False
    recordables = []

    def __init__(self):
        app_name = "spikes_from_images"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        # TODO: use sim_cfg
        _ = sim_cfg  # unused

        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config)

        # convert spikes
        pop_slice = mapper.mapping.get_population_slice(pe)  # whole population at the moment
        params_dict = pop_slice.pop.params  # access parameters
        spike_times_dict = dict()
        images = params_dict["images"]
        packed_list = self.process_images(images)
        spike_times_list_pe = [[] for i in range(pop_slice.size())]

        # send frame_size and threshold to PE
        # size_threshold_list = list()
        size_threshold_array = np.array(
            [params_dict["frame_size"], params_dict["threshold"], params_dict["offset"]]
        ).astype("uint32")
        size_threshold_addr = ByteAddr(0x9000)
        config.add_mem_data_to_send(size_threshold_addr.to_WordAddr(), size_threshold_array.tolist())

        # extract spike sources for this PE
        # for idx in range(len(packed_list)):
        #     if idx >= pop_slice.start and idx < pop_slice.stop:
        #         id_on_pe = idx - pop_slice.start
        #         spike_times_list_pe[id_on_pe] = packed_list[idx]

        # generate input spike rows
        # spike_times_raw = get_input_spike_rows(spike_times_list_pe, mapper.key_offsets[pe])
        input_spikes_addr = ByteAddr(0x10000)  # outside of heap-stack area
        config.add_mem_data_to_send(input_spikes_addr.to_WordAddr(), packed_list)  # spike_times_raw)

        # check if input spike memory region overlaps with log
        log_addr = ByteAddr(0x0001B000)
        # check that synrows does not go into log region
        input_spikes_addr_end = ByteAddr(input_spikes_addr + len(spike_times_list_pe) * 4)  # len(spike_times_raw)*4)
        input_spikes_addr_end = ByteAddr(input_spikes_addr + len(spike_times_list_pe) * 4)  # len(spike_times_raw)*4)
        if input_spikes_addr_end > log_addr:
            print("Input spikes too large:", hex(input_spikes_addr_end), hex(log_addr))
            assert input_spikes_addr_end <= log_addr

        # routing table
        target_cores = mapper.routing_targets.get(pe, set())
        routing_table = format_routing_table(target_cores)
        config.add_mem_data_to_send(ByteAddr(0x8F00).to_WordAddr(), [routing_table])

        return config

    def process_images(self, images):
        flattened_images = images.flatten()
        packed_list = list()
        for i in range(0, len(flattened_images) - 3, 4):
            packed_list.append(
                (
                    flattened_images[i]
                    | flattened_images[i + 1] << 8
                    | flattened_images[i + 2] << 16
                    | flattened_images[i + 3] << 24
                ).tolist()
            )

        return packed_list
