import struct

import numpy as np

from spinnaker2.configuration import MemoryRegion, PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import (
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)


class SpikesFromArrayLatencyCodeApplication(BaseApplication):
    """Application class for `spikes_from_array_latency_code` neuron model.

    This class generates the configuration of the SpiNNaker2 ARM applications
    for the `spikes_from_array_latency_code` neuron model.

    Example:
    ```
    import numpy as np
    from spinnaker2 import snn
    params = {
            "t_max": 100,
            "x_max": 1000,
            "data": np.array([100,200,500,1000], dtype=np.uint16)
            }

    stim = snn.Population(size=4,
                          neuron_model="spikes_from_array_latency_code",
                          params=params,
                          name="stim")
    ```

    This population creates input spikes based on the values of the array
    `params["data"]` with the following translation of values to time:
        t = t_max*(x_max-x)/x_max

    The `data` array as well as the parameters `x_max` and `t_max` are
    transmitted to the SpiNNaker2 core and the translation of array values to
    spikes is performed on hardware.
    """

    default_params = {
        "t_max": 100,
        "x_max": 65535,  # max uint16 value
    }

    max_atoms_per_core = 4096
    splittable = True
    recordables = ["time_done"]
    profiling = False

    # fixed addresses
    data_spec_addr = ByteAddr(0x9000)
    log_addr = ByteAddr(0x1B000)
    data_spec_max_size = 16  # in words

    def __init__(self):
        app_name = "spikes_from_array_latency_code"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """

        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config, self.log_addr, 4000)

        ################
        # routing info #
        ################
        target_cores = mapper.routing_targets.get(pe, set())
        tgt_qpes_and_pes = collect_routing_targets(target_cores)
        n_targets = len(tgt_qpes_and_pes)
        routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
        key_offset = mapper.key_offsets[pe]

        rt_address = align_addr_to_next_multiple_of_other(
            ByteAddr(self.data_spec_addr + self.data_spec_max_size * 4), ByteAddr(0x10)
        )
        rt_address = ByteAddr(rt_address)
        routing_targets_addr = ByteAddr(rt_address + 3 * 4)  # start address of routing table
        rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw
        config.add_mem_data_to_send(rt_address.to_WordAddr(), rt_data)

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(rt_address + len(rt_data) * 4), ByteAddr(0x10)
        )
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        #################
        # global params #
        #################
        pop_slice = mapper.mapping.get_population_slice(pe)
        t_max = pop_slice.pop.params.get("t_max", self.default_params["t_max"])
        assert isinstance(t_max, int)
        x_max = pop_slice.pop.params.get("x_max", self.default_params["x_max"])
        assert isinstance(x_max, int)
        time_done_flag = "time_done" in pop_slice.pop.record
        global_params_raw = [
            pop_slice.size(),
            t_max,
            x_max,
            int(self.profiling),
            int(time_done_flag),
        ]

        global_params_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
        )
        global_params_addr = ByteAddr(global_params_addr)
        config.add_mem_data_to_send(global_params_addr.to_WordAddr(), global_params_raw)

        ###############
        # input array #
        ###############
        input_array = pop_slice.pop.params["data"]
        assert input_array.dtype == np.uint16
        input_array_slice = input_array.ravel()[pop_slice.start : pop_slice.stop]
        assert input_array_slice.size == pop_slice.size()

        # if array size odd, append 0 so that it can be cast to uint32 array
        if input_array_slice.size % 2:
            input_array_slice = np.append(input_array_slice, np.zeros(1, dtype=np.uint16))
        input_array_raw = input_array_slice.view(dtype=np.uint32).tolist()

        input_array_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(global_params_addr + len(global_params_raw) * 4), ByteAddr(0x10)
        )
        input_array_addr = ByteAddr(input_array_addr)
        config.add_mem_data_to_send(input_array_addr.to_WordAddr(), input_array_raw)
        input_array_addr_end = ByteAddr(input_array_addr + len(input_array_raw) * 4)

        #######################
        # time done recording #
        #######################
        if time_done_flag:
            timesteps_to_record = sim_cfg["n_simulation_ticks"]
            time_done_addr = align_addr_to_next_multiple_of_other(input_array_addr_end, ByteAddr(0x10))
            time_done_addr = ByteAddr(time_done_addr)
            config.add_mem_region_to_read(
                "time_done_record", MemoryRegion(time_done_addr.to_WordAddr(), timesteps_to_record)
            )
            time_done_addr_end = ByteAddr(time_done_addr + timesteps_to_record * 4)

        else:
            time_done_addr = input_array_addr
            time_done_addr_end = input_array_addr_end

        # check that recording does not go into log region
        if time_done_addr_end > self.log_addr:
            raise MemoryError(
                f"Not enough memory in population {pop_slice.pop.name}: "
                f"{hex(time_done_addr_end)}, {hex(self.log_addr)} "
                f"by {time_done_addr_end- self.log_addr} byte"
            )

        ######################
        # data specification #
        ######################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            rt_address,  # start of routing table
            timer_config_addr,  # start of timer config
            global_params_addr,  # start of global params
            input_array_addr,  # start of input spike rows
            time_done_addr,  # start of time_done records
            self.log_addr,  # start of log (dummy)
        ]

        config.add_mem_data_to_send(self.data_spec_addr.to_WordAddr(), data_spec)

        return config
