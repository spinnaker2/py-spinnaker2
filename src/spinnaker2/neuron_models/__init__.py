from spinnaker2.neuron_models.charge_and_spike import ChargeAndSpikeApplication
from spinnaker2.neuron_models.conv2d import Conv2DApplication
from spinnaker2.neuron_models.conv2d_if_neuron_rate_code import (
    Conv2DIFRateCodeApplication,
)
from spinnaker2.neuron_models.conv_lif_hybrid import ConvLifHybridApplication
from spinnaker2.neuron_models.float_list import FloatListApplication
from spinnaker2.neuron_models.image_storage import ImageStorageApplication
from spinnaker2.neuron_models.lif_conv2d import LIFConv2dApplication
from spinnaker2.neuron_models.lif_curr_exp import (
    LIFCurrExpApplication,
    LIFCurrExpNoDelayApplication,
)
from spinnaker2.neuron_models.lif_fugu import LIFApplicationFugu
from spinnaker2.neuron_models.lif_neuron import (
    LIFApplication,
    LIFDenseApplication,
    LIFNoDelay1024Application,
    LIFNoDelayApplication,
)
from spinnaker2.neuron_models.qubo_neuron import QUBOApplication
from spinnaker2.neuron_models.relay import RelayApplication
from spinnaker2.neuron_models.rfneuron import RFNeuronApplication
from spinnaker2.neuron_models.spike_list import SpikeListApplication
from spinnaker2.neuron_models.spike_receiver import SpikeReceiverApplication
from spinnaker2.neuron_models.spikes_from_array_latency_code import (
    SpikesFromArrayLatencyCodeApplication,
)
from spinnaker2.neuron_models.spikes_from_images import SpikesFromImagesApplication

NEURON_MODEL_TO_APPLICATION = {
    "spike_list": SpikeListApplication,
    "float_list": FloatListApplication,
    "spikes_from_images": SpikesFromImagesApplication,
    "spike_receiver": SpikeReceiverApplication,
    "lif_curr_exp": LIFCurrExpApplication,
    "lif_curr_exp_no_delay": LIFCurrExpNoDelayApplication,
    "lif": LIFApplication,
    "lif_no_delay": LIFNoDelayApplication,
    "lif_no_delay_1024": LIFNoDelay1024Application,
    "lif_dense": LIFDenseApplication,
    "lif_conv2d": LIFConv2dApplication,
    "lif_fugu": LIFApplicationFugu,
    "relay": RelayApplication,
    "conv2d_if_neuron_rate_code": Conv2DIFRateCodeApplication,
    "conv_lif_hybrid": ConvLifHybridApplication,
    "image_storage": ImageStorageApplication,
    "conv2d": Conv2DApplication,
    "rfneuron": RFNeuronApplication,
    "spikes_from_array_latency_code": SpikesFromArrayLatencyCodeApplication,
    "charge_and_spike": ChargeAndSpikeApplication,
    "qubo_neuron": QUBOApplication,
}


def get_application(name):
    return NEURON_MODEL_TO_APPLICATION[name]
