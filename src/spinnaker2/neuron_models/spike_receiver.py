import struct

from spinnaker2.configuration import PEConfig
from spinnaker2.coordinates import ByteAddr, align_addr_to_next_multiple_of_other
from spinnaker2.neuron_models.application import BaseApplication
from spinnaker2.neuron_models.common import add_log_memory_region


class SpikeReceiverApplication(BaseApplication):

    max_atoms_per_core = 1
    splittable = True
    recordables = []

    def __init__(self):
        app_name = "spike_receiver"
        super().__init__(name=app_name)

    def pe_config(self, pe, mapper, sim_cfg, debug=True):
        """
        return PE configuration for a given PE
        """
        _ = mapper  # unused

        config = PEConfig(pe, self.name, self.mem_file)

        if debug:
            add_log_memory_region(config)

        data_spec_addr = ByteAddr(0x9000)
        data_spec_size = 16  # default: 16 words

        ################
        # timer config #
        ################
        timer_config_addr = align_addr_to_next_multiple_of_other(
            ByteAddr(data_spec_addr + data_spec_size * 4), ByteAddr(0x10)
        )
        timer_config_addr = ByteAddr(timer_config_addr)
        sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
        config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

        log_addr = ByteAddr(0x0001B000)

        ######################
        # data specification #
        ######################
        data_spec = [
            0xAD130AD6,  # magic_number
            0x00010000,  # version
            timer_config_addr,  # start of timer config
            log_addr,  # start of log (dummy)
        ]

        config.add_mem_data_to_send(data_spec_addr.to_WordAddr(), data_spec)

        return config
