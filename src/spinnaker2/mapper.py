import math
import os
import struct
from enum import Enum

import numpy as np
import scipy.sparse
from tqdm import tqdm

import spinnaker2
from spinnaker2.configuration import ExperimentConfig

APP_BASE_PATH = os.path.abspath(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "../../../s2-sim2lab-app/chip/app-pe/s2app")
)
APP_BASE_PATH_FPGA = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../../../qpe-software-fixed/pe-software/quadpe-testcases/",
)


APP_PATH = {
    "spike_list": os.path.join(APP_BASE_PATH, "input_spikes_with_routing/binaries/s2app_arm.mem"),
    "spike_receiver": os.path.join(APP_BASE_PATH, "spike_receiver/binaries/s2app_arm.mem"),
    "lif_curr_exp": os.path.join(APP_BASE_PATH, "lif_curr_exp/binaries/s2app_arm.mem"),
    "lif_curr_exp_no_delay": os.path.join(APP_BASE_PATH, "lif_curr_exp_no_delay/binaries/s2app_arm.mem"),
    "lif": os.path.join(APP_BASE_PATH, "lif_neuron/binaries/s2app_arm.mem"),
    "lif_fugu": os.path.join(APP_BASE_PATH, "lif_fugu/binaries/s2app_arm.mem"),
    "relay": os.path.join(APP_BASE_PATH, "relay/binaries/s2app_arm.mem"),
    "lif_no_delay": os.path.join(APP_BASE_PATH, "lif_neuron_no_delay/binaries/s2app_arm.mem"),
    "lif_no_delay_1024": os.path.join(APP_BASE_PATH, "lif_neuron_no_delay_1024/binaries/s2app_arm.mem"),
    "lif_conv2d": os.path.join(APP_BASE_PATH, "lif_conv2d/binaries/s2app_arm.mem"),
    "conv2d_if_neuron_rate_code": os.path.join(APP_BASE_PATH, "conv2d_if_neuron_rate_code/binaries/s2app_arm.mem"),
    "spikes_from_array_latency_code": os.path.join(
        APP_BASE_PATH, "spikes_from_array_latency_code/binaries/s2app_arm.mem"
    ),
    "charge_and_spike": os.path.join(APP_BASE_PATH, "charge_and_spike/binaries/s2app_arm.mem"),
    "qubo_neuron": os.path.join(APP_BASE_PATH, "qubo_neuron/binaries/s2app_arm.mem"),
}

APP_PATH_FPGA = {
    "spike_list": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_input_spikes_with_routing/binaries/arm.mem"),
    "float_list": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_input_floats_with_routing/binaries/arm.mem"),
    "spike_receiver": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_spike_receiver/binaries/arm.mem"),
    "lif_curr_exp": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_lif_curr_exp/binaries/arm.mem"),
    "lif_curr_exp_no_delay": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_lif_curr_exp_no_delay/binaries/arm.mem"),
    "conv_lif_hybrid": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_hybrid_lif/binaries/arm.mem"),
    "lif": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_lif_neuron/binaries/arm.mem"),
    "lif_no_delay": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_lif_neuron_no_delay/binaries/arm.mem"),
    "lif_dense": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_lif_dense/binaries/arm.mem"),
    "conv2d_if_neuron_rate_code": os.path.join(
        APP_BASE_PATH_FPGA, "fpga_4qpe.tc_conv2d_if_neuron_rate_code/binaries/arm.mem"
    ),
    "image_storage": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_image_storage/binaries/arm.mem"),
    "conv2d": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_conv2d/binaries/arm.mem"),
    "spikes_from_images": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_exp_runner_hybrid_input/binaries/arm.mem"),
    "rfneuron": os.path.join(APP_BASE_PATH_FPGA, "fpga_4qpe.tc_rfneuron/binaries/arm.mem"),
    "spikes_from_array_latency_code": os.path.join(
        APP_BASE_PATH_FPGA, "fpga_4qpe.tc_spikes_from_array_latency_code/binaries/arm.mem"
    ),
}


class PopulationSlice(object):
    """A slice of a population that fits onto PE."""

    def __init__(self, pop, start, stop):
        assert isinstance(pop, spinnaker2.snn.Population)
        self.pop = pop
        self.start = start
        self.stop = stop

    def __repr__(self):
        return "PopulationSlice(%s, %s, %s)" % (self.pop, self.start, self.stop)

    def __eq__(self, other):
        return self.pop == other.pop and self.start == other.start and self.stop == other.stop

    def size(self):
        """Returns number of neurons in population slice."""
        return self.stop - self.start


class Neuron(object):
    """A neuron of a population."""

    def __init__(self, pop, idx):
        assert isinstance(pop, spinnaker2.snn.Population)
        self.pop = pop
        self.idx = idx

    def __repr__(self):
        return "Neuron(%s, %s)" % (self.pop.name, self.idx)

    def __eq__(self, other):
        return self.pop == other.pop and self.idx == other.idx


class Mapping(object):
    """Object for storing the mapping from a spiking network model to SpiNNaker2
    hardware.

    Internally holds a dictionary with PEs as keys and PopulationSlices as
    values. Each PE can hold at maximum one PopulationSlice. Populations have
    to be split across multiple PEs if they don't fit onto a single PE.

    TODO:
      - think of having access to a full Population
      - think of adding the key offset for each PE
    """

    def __init__(self):
        self.hw_to_model = {}

    def add(self, pop_slice, pe):
        """Add the mapping of a population slice to a PE."""
        assert isinstance(pop_slice, PopulationSlice)
        assert isinstance(pe, spinnaker2.coordinates.PE)
        if pe not in self.hw_to_model:
            self.hw_to_model[pe] = pop_slice
        else:
            raise Exception("PE already used")

    def get_hardware(self, pop, idx):
        """Returns the hardware location for a given model neuron.

        returns the PE coordinate and the index on the PE
        """
        for pe, pop_slice in self.hw_to_model.items():
            if pop == pop_slice.pop:
                if pop_slice.start <= idx < pop_slice.stop:
                    return pe, idx - pop_slice.start

        raise Exception("No hardware location found for neuron")

    def get_cores(self, pop):
        """Returns the cores (processor elements) to which a population is mapped.

        Returns: list of PE coordinates
        """

        pes = []
        for pe, pop_slice in self.hw_to_model.items():
            if pop == pop_slice.pop:
                pes.append(pe)

        return pes

    def get_neuron(self, pe, idx):
        """Get the model neuron for a given hardware location.

        returns the Population object and the index of the neuron within the Population
        """
        pop_slice = self.hw_to_model[pe]
        if pop_slice.start + idx < pop_slice.stop:
            return pop_slice.pop, pop_slice.start + idx
        else:
            raise Exception(("PE found, but index out of range of " "Population Slice"))

    def get_population_slice(self, pe):
        """Get population slice mapped to PE."""
        return self.hw_to_model[pe]


class Mapper(object):
    """Class to map SNN on SpiNNaker2 hardware.

    Attributes:
        iterative_mapping (bool): Flag indicating whether iterative mapping is enabled.
        network (spinnaker2.snn.Network): The network object.
        hardware (spinnaker2.hardware.Hardware): The hardware object.
        mapping (Mapping): The mapping object.
        max_atoms (dict): The maximum number of atoms per populations. Dict of type {Population : int}. Used in case
            of iterative mapping
        pop_slices (dict): Nested dictionary of each population splits. {Population : {PE : PopulationSlice}}. Used in
            case of iterative mapping
        pe_fits (dict): Dictionary specifying if pop slices fit on PEs. {PE : bool}. Used in case of iterative mapping
    """

    iterative_mapping = False

    def __init__(self, network, hw):
        """Initialize the Mapper object.

        Args:
            network (spinnaker2.snn.Network): The network object.
            hw (spinnaker2.hardware.Hardware): The hardware object.
        """
        assert isinstance(network, spinnaker2.snn.Network)
        assert isinstance(hw, spinnaker2.hardware.Hardware)
        self.network = network
        self.hardware = hw
        self.mapping = Mapping()

        self.max_atoms = None
        self.pop_slices = None
        self.pe_fits = None

    @property
    def all_fitted(self):
        """Check if all processing elements (PEs) are fitted.

        Returns:
            bool: True if all PEs are fitted, False otherwise.
        """
        return self.pe_fits is not None and all(self.pe_fits.values())

    @property
    def pop_fits(self):
        """Returns a dictionary containing fit booleans for each population and its
        associated processing element (PE). The fitness values are obtained from the
        `pe_fits` dictionary, which contains fit values for each PE.

        Returns:
            dict: A dictionary with population-wise fits .
        """
        if self.pe_fits is None:
            return None
        else:
            pop_fits = {
                pop: {pe: self.pe_fits[pe] for pe in self.pe_fits if self.mapping.get_population_slice(pe).pop == pop}
                for pop in self.network.populations
            }
            return {k: v for k, v in pop_fits.items() if len(v) > 0}

    def run_mapping(self, verbose=True):
        """Map network model to the available hardware."""
        # check if network fits into
        self.hardware.init_free_cores()
        self.mapping = Mapping()
        n_cores = self.hardware.number_of_cores()
        if self.pop_slices is None:
            self.pop_slices = {pop: None for pop in self.network.populations}
        if self.max_atoms is None:
            self.max_atoms = {pop: pop.get_max_atoms_per_core() for pop in self.network.populations}

        n_core = 0

        # ok, do the actual mapping
        for pop in self.network.populations:
            # max_neurons_per_core = pop.get_max_atoms_per_core()
            max_neurons_per_core = self.max_atoms[pop]
            neurons_to_map = pop.size

            if self.pop_slices[pop] is None:
                self.pop_slices[pop] = {}
                slices_needed = math.ceil(pop.size / max_neurons_per_core)
                if not pop.splittable:
                    assert (
                        slices_needed == 1
                    ), f"Population {pop.name} requires {slices_needed} cores, but is not splittable."
                # slice_sizes = np.histogram(np.arange(neurons_to_map), bins=slices_needed)[0].tolist()
                slice_sizes = [None] * slices_needed
            else:
                slice_sizes = list(self.pop_slices[pop].values())

            pop_offset = 0
            slice_num = 0

            while neurons_to_map > 0:
                try:
                    neurons_this_core = int(
                        min(neurons_to_map, max_neurons_per_core)
                        if slice_sizes[slice_num] is None
                        else slice_sizes[slice_num]
                    )
                except IndexError:
                    neurons_this_core = int(min(neurons_to_map, max_neurons_per_core))

                pop_slice = PopulationSlice(pop, pop_offset, neurons_this_core + pop_offset)
                core = self.hardware.pop_free_core()
                self.pop_slices[pop][core] = neurons_this_core

                self.mapping.add(pop_slice, core)
                pop_offset += neurons_this_core

                neurons_to_map -= neurons_this_core
                slice_num += 1
                n_core += 1

                if n_core >= n_cores and neurons_to_map > 0:
                    raise MemoryError("Model does not fit onto available hardware!")

        if verbose:
            print("Used %s out of %s cores" % (self.hardware.number_of_used_cores(), self.hardware.number_of_cores()))

        self.extract_synapse_matrices()
        self.extract_routing_targets()
        self.assign_keys()

    def map_and_generate_experiment_config(self, time_steps, sys_tick_in_s=1.0e-3, debug=True, verbose=True):
        """Map SNN to hardware and generate experiment config.

        args:
            time_steps: number of time steps to simulate the network
            sys_tick_in_s: duration of a time step in real time (aka `sys_tick`)
                        in seconds. default: 1.e-3
            debug(bool): read and print debug information of PEs. default: True
            fix_mem_overload(bool): fix memory overload by reducing number of units in population slices across PEs.
                default: True

        returns:
            tuple (exp_config, sim_cfg):
                generated experiment config (`configuration.ExperimentConfig`),
                simulation configuration (dict) with keys "n_simulation_ticks" and
                "timer_period".
        """
        self.run_mapping(verbose=verbose)

        # global simulation config
        # currently only contains the timer config
        # TODO: should this be a dedicated class?

        sim_cfg = dict(
            n_simulation_ticks=time_steps,
            timer_period=int(sys_tick_in_s * self.hardware.ref_clock_freq),
        )
        sim_duration_in_s = sim_cfg["n_simulation_ticks"] * sys_tick_in_s

        # add experiment offset to ensure that simulation has finished when reading the data.
        experiment_offset_time = 0.01
        actual_duration = sim_duration_in_s + experiment_offset_time

        if verbose:
            print(f"Sim duration: {sim_duration_in_s} s")
            print(f"Actual duration (incl. offset): {actual_duration}")

        exp_config = ExperimentConfig(actual_duration)  # run 1 s longer
        exp_config.synchronous_start = True
        for pe in self.hardware.used_cores:
            pop_slice = self.mapping.get_population_slice(pe)
            neuron_model = pop_slice.pop.neuron_model
            app = spinnaker2.neuron_models.get_application(neuron_model)()

            # set app name depending on hardware (S2 Chip or FPGA)
            app.mem_file = self.hardware.get_mem_file_for_neuron_model(app.name)

            if self.iterative_mapping:
                try:
                    pe_config = app.pe_config(pe, self, sim_cfg, debug)
                    pe_config.check_for_overlap_in_data_to_send()
                    exp_config.add(pe_config)
                    pe_fit = True
                except MemoryError:
                    pe_fit = False
                except Exception as e:
                    print(
                        "There was a non-MemoryError during iterative mapping. Maybe iterative mapping is not "
                        "supported for the neuron model. Stop."
                    )
                    raise
                self.pe_fits[pe] = pe_fit

            else:
                pe_config = app.pe_config(pe, self, sim_cfg, debug)
                pe_config.check_for_overlap_in_data_to_send()
                exp_config.add(pe_config)

        return exp_config, sim_cfg

    def extract_synapse_matrices(self):
        """Extract the incoming synapse matrices per core."""
        # Target:
        # collect all the data necessary to create synapse rows, master
        # population tables and routing tables

        # Approach:
        # go through all used PEs and search for the incoming projections
        # output: list of pre-synaptic neurons (Pop + idx) with list of connections to neurons on PE
        self.synapse_matrices = {}
        for pe, pop_slice in self.mapping.hw_to_model.items():
            synapse_matrix = []
            for proj in self.network.projections:
                if isinstance(proj, spinnaker2.snn.Conv2DProjection):
                    continue
                if proj.post == pop_slice.pop:
                    weights_to_slice = proj.sparse_weights[:, pop_slice.start : pop_slice.stop]
                    delays_to_slice = proj.sparse_delays[:, pop_slice.start : pop_slice.stop]

                    # convert to LIL sparse matrix as it has desired format
                    sm_w = weights_to_slice.tolil()
                    sm_d = delays_to_slice.tolil()
                    assert np.all(sm_w.rows == sm_d.rows)  # should be true!

                    for pre_idx, (tgts, ws, ds) in enumerate(zip(sm_w.rows, sm_w.data, sm_d.data)):
                        if len(tgts) > 0:
                            neuron = Neuron(proj.pre, pre_idx)
                            # TODO: currently, we don't search for duplicate synapses or pre-neurons
                            synapse_matrix.append((neuron, tgts, ws, ds))

            # print(synapse_matrix)
            self.synapse_matrices[pe] = synapse_matrix

    def get_dense_weight_matrix(self, pe):
        synapse_data = self.synapse_matrices[pe]
        weights = np.array([x[2] for x in synapse_data], order="C")

        # TODO: this is a dumb workaround. Key on PE is shifted by key_offset. This adds zeros to adjust the weight
        # matrix in memory. Better would be to account this in the PE code.
        max_pre_neurons_per_core = synapse_data[0][0].pop.app_class.max_atoms_per_core
        offset_diff = list(self.key_offsets.values())[1]
        num_fills = offset_diff - max_pre_neurons_per_core

        num_splits = len(weights) // max_pre_neurons_per_core

        for i in range(num_splits * max_pre_neurons_per_core, 0, -max_pre_neurons_per_core):
            for _ in range(num_fills):
                weights = np.insert(weights, i, 0, axis=0)

        return weights

    def extract_routing_targets(self):
        """Extract the routing targets per core."""
        self.routing_targets = {}
        for pe, pop_slice in self.mapping.hw_to_model.items():
            target_pes = set()
            for proj in self.network.projections:
                if proj.pre == pop_slice.pop:
                    for post_core in self.mapping.get_cores(proj.post):
                        if isinstance(proj, spinnaker2.snn.Conv2DProjection):
                            # all-to-all routing for Conv2D projections
                            # TODO: update once feature maps can be split
                            target_pes.add(post_core)
                        else:
                            # check whether are are synapses in the slice of the
                            # projection between neurons from the source core and
                            # neurons on the target core.
                            post_slice = self.mapping.get_population_slice(post_core)
                            proj_weights_slice = proj.sparse_weights[
                                pop_slice.start : pop_slice.stop, post_slice.start : post_slice.stop
                            ]
                            # print("From", pe, "to", post_core)
                            if proj_weights_slice.getnnz() > 0:
                                target_pes.add(post_core)

            self.routing_targets[pe] = target_pes
        # print(self.routing_targets)

    def assign_keys(self):
        """Assigns routing keys to mapped neurons.

        Actually only assigns the key offset per core. The routings keys of individual
        neurons are implicitly defined.
        """
        # TODO: clarify whether this belongs to the Mapper or to the Mapping class
        self.key_offsets = {}  # dict PE to key offset

        # assign continuous keys
        offset = 0
        for pe in self.hardware.used_cores:
            self.key_offsets[pe] = offset
            size = self.mapping.get_population_slice(pe).size()
            offset += size

    def master_pop_table(self):
        pass

    def get_routing_key(self, neuron):
        pe, id_on_pe = self.mapping.get_hardware(neuron.pop, neuron.idx)
        return self.key_offsets[pe] + id_on_pe

    def estimate_master_pop_table_length(self, pe):
        """Estimate the length of master population table.

        returns the number of entries in the population table.
        """
        synapse_matrix = self.synapse_matrices[pe]
        source_keys = set()
        for neuron, tgts, ws, ds in synapse_matrix:
            source_keys.add(self.get_routing_key(neuron))
        return len(source_keys)

    def synapse_rows_and_master_pop_table(self, pe, sw_spec, syn_rows_start):
        """Extract synapse matrices.

        can only be called after run_mapping
        """
        synapse_matrix = self.synapse_matrices[pe]

        rows = {}  # dict: routing key to dict with targets, weights, delays

        # combine synapses from the same source to one row
        for neuron, tgts, ws, ds in synapse_matrix:
            routing_key = self.get_routing_key(neuron)
            if not routing_key in rows:
                rows[routing_key] = dict(targets=tgts, weights=ws, delays=ds)
            else:
                row_data = rows[routing_key]
                # assume lists as entries
                row_data["targets"] += tgts
                row_data["weights"] += ws
                row_data["delays"] += ds
                rows[routing_key] = row_data

        formatted_rows = {}

        # configure synapse rows and master population table
        for source, row_data in rows.items():
            n_synapses = len(row_data["targets"])
            # approach for 16-bit words
            if sw_spec.word_size == SynapseWordSize.SIZE_16:

                ws = np.array(np.abs(row_data["weights"])).astype(np.uint16)
                w_out_of_bounds = np.where(ws > sw_spec.max_weight())
                if w_out_of_bounds[0].size > 0:
                    print(
                        f"Warning: {w_out_of_bounds[0].size} weights are out of bounds, will be clipped. "
                        f"Max absolute value is {sw_spec.max_weight()}."
                    )
                    ws[w_out_of_bounds] = sw_spec.max_weight()

                ds = np.array(row_data["delays"]).astype(np.uint16)
                d_out_of_bounds = np.where(ds > sw_spec.max_delay())
                if d_out_of_bounds[0].size > 0:
                    print(
                        f"Warning: {d_out_of_bounds[0].size} delays are out of bounds, will be clipped. "
                        f"Max absolute value is {sw_spec.max_delay()}."
                    )
                    ds[d_out_of_bounds] = sw_spec.max_delay()

                syn_types = np.signbit(row_data["weights"]).astype(np.uint16)
                tgts = np.array(row_data["targets"]).astype(np.uint16)

                syn_words = np.zeros(ws.shape, dtype=np.uint16)
                syn_words |= (ws & sw_spec.weight_mask) << sw_spec.weight_shift
                syn_words |= (ds & sw_spec.delay_mask) << sw_spec.delay_shift
                syn_words |= (syn_types & sw_spec.synapse_type_mask) << sw_spec.synapse_type_shift
                syn_words |= (tgts & sw_spec.target_mask) << sw_spec.target_shift

                # DEBUG
                # for i in range(syn_words.shape[0]):
                #    print(sw_spec.decode(syn_words[i]))

                formatted_rows[source] = syn_words
            else:
                raise Exception("Only SynapseWordSize.SIZE_16 supported")

        # create synapse row raw data
        """
        Synapse row format
        plastic size
        ... (plastic region) ...
        static size
        plastic control size
        ... (static region) ...
        ... (plastic control region) ...
        """
        pop_table = []
        all_syn_rows = bytes(0)
        # byte address of next syn_row to start
        syn_row_addr = syn_rows_start

        for key, syn_row in formatted_rows.items():

            # header
            row_header = np.zeros(3, dtype=np.uint32)
            row_header[0] = 0  # plastic_size
            row_header[1] = syn_row.shape[0]  # static_size
            row_header[2] = 0  # plastic_control_size

            # combine header, static region and fill up to be aligned in memory
            row_bytes = row_header.tobytes() + syn_row.tobytes()
            n_bytes = len(row_bytes)

            def get_remainder_to_next_multiple(num, multiplier):
                """TODO: maybe merge with mem_align for address?"""
                return int(math.ceil(num / multiplier) * multiplier - num)

            n_bytes_to_expand = get_remainder_to_next_multiple(n_bytes, 128 / 8)
            row_bytes += bytes(n_bytes_to_expand)
            # print(len(row_bytes))
            # print(row_bytes)

            # add to all_syn_rows
            all_syn_rows += row_bytes

            # add entry in master population table
            def pop_table_entry(key, addr, row_length, mask=0xFFFFFFFF):
                return dict(key=key, mask=mask, address_and_row_length=addr << 8 | (row_length & 0xFF))

            row_length = len(row_bytes) // 4  # in words
            pop_table.append(pop_table_entry(key, syn_row_addr, row_length))
            syn_row_addr += row_length * 4  # increase address offset
        # print(pop_table)

        # sort population table by key
        pop_table.sort(key=lambda x: x["key"])

        # generate raw data for pop table
        pop_table_raw = []
        for entry in pop_table:
            pop_table_raw.append(entry["key"])
            pop_table_raw.append(entry["mask"])
            pop_table_raw.append(entry["address_and_row_length"])

        # convert all_syn_rows from bytes into list of uint32
        n_bytes_total = len(all_syn_rows)
        assert n_bytes_total % 4 == 0
        all_syn_rows_raw = struct.unpack("{}I".format(n_bytes_total // 4), all_syn_rows)
        return (all_syn_rows_raw, pop_table_raw)

    def write_experiment_spec(self):
        for pe in self.hardware.used_cores:
            pass

        return None


class IterativeMapper(Mapper):
    """A class representing an iterative mapper.

    This class inherits from the `Mapper`
    class and provides alternative iterative mapping function. This way of mapping
    ensure we maximize the number of neurons and synapses placed on each core. It also
    avoid MemoryError, by automatically splitting populations across cores if needed.
    Finally, this automatically handles recordings and adapt the number of neurons used
    per core accordingly.

    Attributes:
        iterative_mapping (bool): Flag indicating whether iterative mapping is enabled.

    Methods:
        map_and_generate_experiment_config: Maps and generates the experiment configuration.
    """

    iterative_mapping = True

    def map_and_generate_experiment_config(
        self,
        time_steps,
        sys_tick_in_s=1.0e-3,
        debug=True,
    ):
        """Maps and generates the experiment configuration.

        Args:
            time_steps (int): The number of time steps.
            sys_tick_in_s (float): The system tick in seconds.
            debug (bool): Flag indicating whether debug mode is enabled.

        Returns:
            tuple: A tuple containing the experiment configuration and simulation configuration.
        """

        try:
            pbar = tqdm()
            use_tqdm = True
        except NameError:
            use_tqdm = False
        while not self.all_fitted:

            if self.pe_fits is None:
                self.pe_fits = {}

            exp_config, sim_cfg = super().map_and_generate_experiment_config(
                time_steps, sys_tick_in_s, debug=debug, verbose=False
            )

            # check if map successful
            if not self.all_fitted:
                for pop, pe_fits in self.pop_fits.items():
                    if not all(pe_fits.values()):

                        # reduce max_atoms by 2. TODO find a better way to do this !
                        self.max_atoms[pop] -= 2
                        # Only keep pop slices that fit.
                        self.pop_slices[pop] = {
                            pe: (None if not fit else self.mapping.get_population_slice(pe).size())
                            for (pe, fit), _ in zip(pe_fits.items(), range(len(pe_fits) - 1))
                        }
            else:
                print(f"Mapping successful \nFinal mapping: {self.pop_slices}")
                print(
                    "Used %s out of %s cores" % (self.hardware.number_of_used_cores(), self.hardware.number_of_cores())
                )

            if use_tqdm:
                pbar.set_description("Re-mapping due to insufficient memory")

                postfix = {pop.name: self.max_atoms[pop] for pop in self.pop_slices.keys()}
                neurons_fitted = [[s for s in slice.values() if s is not None] for slice in self.pop_slices.values()]
                neurons_fitted = sum([sum(s) for s in neurons_fitted])
                neurons_to_fit = sum([pop.size for pop, s in self.pop_slices.items() if len(s)])
                postfix.update({"n_neurons_fitted": f"{neurons_fitted} / {neurons_to_fit}"})
                pbar.set_postfix(postfix)
                pbar.update(1)

        return exp_config, sim_cfg


class SynapseWordSize(Enum):
    SIZE_16 = 16
    SIZE_32 = 32


class SynapseWordSpec(object):
    """Specification of synapse word format.

    TODO: add padding value?
    """

    def __init__(self, word_size, weight, delay, synapse_type, target):
        assert isinstance(word_size, SynapseWordSize)
        self.word_size = word_size
        total_bits = weight + delay + synapse_type + target
        if total_bits != self.word_size.value:
            raise Exception("Number of total bits does not match word size")

        self.weight_bits = weight
        self.delay_bits = delay
        self.synapse_type_bits = synapse_type
        self.target_bits = target

        # mask before shifting
        self.weight_mask = (1 << self.weight_bits) - 1
        self.delay_mask = (1 << self.delay_bits) - 1
        self.synapse_type_mask = (1 << self.synapse_type_bits) - 1
        self.target_mask = (1 << self.target_bits) - 1

        # how many bits to shift before combing all parts to the word
        self.weight_shift = self.delay_bits + self.synapse_type_bits + self.target_bits
        self.delay_shift = self.synapse_type_bits + self.target_bits
        self.synapse_type_shift = self.target_bits
        self.target_shift = 0

    def max_weight(self):
        return (1 << self.weight_bits) - 1

    def max_delay(self):
        return (1 << self.delay_bits) - 1

    def decode(self, value):
        r = {}
        r["weight"] = (value >> self.weight_shift) & self.weight_mask
        r["delay"] = (value >> self.delay_shift) & self.delay_mask
        r["target"] = (value >> self.target_shift) & self.target_mask
        r["synapse_type"] = (value >> self.synapse_type_shift) & self.synapse_type_mask
        return r
