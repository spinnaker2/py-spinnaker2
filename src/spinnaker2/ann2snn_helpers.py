import numpy as np


def scale_and_convert_weights_to_int8(tf_weights):
    """
    convert weights from float to int8 with normalization
    """
    max_abs_weight = np.abs(tf_weights).max()
    weights_scaled = tf_weights * (127 / max_abs_weight)
    return weights_scaled.astype(np.int8)


def reorder_dense_weights_after_conv2d(dense_weights, channels, height, width):
    """
    reorder dense weights that follow a conv2d layer.

    The MLA outputs the conv2d results in NCHW format. Instead, TensorFlow uses
    NHWC per default. Hence, after Flatten() the neuron order is different,
    which affects the weights of the follow-up dense layer.

    This function re-orders the 0-th dimension of the dense weights accordingly
    """
    assert dense_weights.shape[0] == channels * height * width

    indices = np.arange(dense_weights.shape[0], dtype=int)
    indices = indices.reshape(height, width, channels)  # TF uses HWC
    indices = np.moveaxis(indices, -1, 0)  # turn to CHW
    indices = indices.flatten()

    return dense_weights[indices, :]


def connection_list_from_dense_weights(dense_weights, delay):
    conns = []
    for i in range(dense_weights.shape[0]):
        for j in range(dense_weights.shape[1]):
            conns.append([i, j, dense_weights[i, j], delay])
    return conns


def connection_list_from_conv2d_weights(weights, input_shape, stride, padding, delay, data_order="tf"):
    """
    If data_order is "tf":
    - weight shapes C_in, C_out, H, W
    - input and output shapes as HWC
    If data_order is "torch":
    - weight shapes C_out, C_in, H, W
    - input and output shapes as CHW
    """

    W_shape = weights.shape
    assert len(W_shape) == 4
    assert len(input_shape) == 3

    assert data_order in ("tf", "torch")
    if data_order == "tf":
        W_H = W_shape[0]
        W_W = W_shape[1]
        C_in = W_shape[2]
        C_out = W_shape[3]

        H_in = input_shape[0]
        W_in = input_shape[1]
        assert C_in == input_shape[2]
    else:
        W_H = W_shape[2]
        W_W = W_shape[3]
        C_in = W_shape[1]
        C_out = W_shape[0]

        H_in = input_shape[1]
        W_in = input_shape[2]
        assert C_in == input_shape[0]

    if isinstance(stride, (tuple, list, np.ndarray)):
        strides = stride
    elif isinstance(stride, int):
        strides = (stride, stride)

    if isinstance(padding, str):
        assert padding in (
            "same",
            "valid",
        ), "Invalid padding, only 'same' or 'valid' or tuples of length 2 are allowed"
        if padding == "same":
            # Adapted from https://www.tensorflow.org/api_docs/python/tf/nn#notes_on_padding_2
            if H_in % strides[0] == 0:
                pad_h = max(W_H - strides[0], 0)
            else:
                pad_h = max(W_H - (H_in % strides[0]), 0)

            if W_in % strides[1] == 0:
                pad_w = max(W_W - strides[1], 0)
            else:
                pad_w = max(W_W - (W_in % strides[1]), 0)
            pad_top = pad_h // 2
            pad_bottom = pad_h - pad_top
            pad_left = pad_w // 2
            pad_right = pad_w - pad_left
        elif padding == "valid":
            pad_h = 0
            pad_w = 0
            pad_top = 0
            pad_bottom = 0
            pad_left = 0
            pad_right = 0
    elif isinstance(padding, (tuple, list, np.ndarray)):
        assert len(padding) == 2, "Invalid padding, only 'same' or 'valid' or tuples of length 2 are allowed"
        pad_h = padding[0] * 2
        pad_w = padding[1] * 2
        pad_top = padding[0]
        pad_bottom = padding[0]
        pad_left = padding[1]
        pad_right = padding[1]

    H_out = np.ceil((H_in + pad_h - (W_H - 1)) / strides[0]).astype("int")
    W_out = np.ceil((W_in + pad_w - (W_W - 1)) / strides[1]).astype("int")
    if data_order == "tf":
        output_shape = (H_out, W_out, C_out)
    elif data_order == "torch":
        output_shape = (C_out, H_out, W_out)

    conns = []
    for h_out in range(H_out):
        for w_out in range(W_out):
            for c_in in range(C_in):
                for c_out in range(C_out):
                    for w_h in range(W_H):
                        for w_w in range(W_W):
                            h_in = strides[0] * h_out - pad_top + w_h
                            w_in = strides[1] * w_out - pad_left + w_w
                            if h_in >= 0 and w_in >= 0 and h_in < H_in and w_in < W_in:
                                if data_order == "tf":
                                    in_idx = h_in * (W_in * C_in) + w_in * (C_in) + c_in
                                    out_idx = h_out * (W_out * C_out) + w_out * (C_out) + c_out
                                    conns.append([in_idx, out_idx, weights[w_h, w_w, c_in, c_out], delay])
                                elif data_order == "torch":
                                    # If data_order is "torch":
                                    # - weight shapes C_out, C_in, H, W
                                    # - input and output shapes as CHW
                                    in_idx = c_in * (H_in * W_in) + h_in * (W_in) + w_in
                                    out_idx = c_out * (H_out * W_out) + h_out * (W_out) + w_out
                                    conns.append([in_idx, out_idx, weights[c_out, c_in, w_h, w_w], delay])
    return conns, output_shape


def connection_list_for_sumpool2d(input_shape, stride, kernel_size, padding, delay, data_order="tf"):
    # TODO: add support for sumpool2d from tf
    # TF: assume H,W,C data shape
    # Torch: assume C,H,W data shape

    assert len(input_shape) == 3
    assert data_order in ("tf", "torch")

    if data_order == "tf":
        C_in = input_shape[2]
        H_in = input_shape[0]
        W_in = input_shape[1]
    elif data_order == "torch":
        C_in = input_shape[0]
        H_in = input_shape[1]
        W_in = input_shape[2]
    C_out = C_in

    if isinstance(kernel_size, (tuple, list, np.ndarray)):
        kernel_size = tuple(kernel_size)
    elif isinstance(kernel_size, int):
        kernel_size = (kernel_size, kernel_size)
    W_H = kernel_size[0]
    W_W = kernel_size[1]

    if isinstance(stride, (tuple, list, np.ndarray)):
        stride = tuple(stride)
    elif isinstance(stride, int):
        stride = (stride, stride)
    if stride != kernel_size:
        raise NotImplementedError(
            f"Currently, for SumPool2d only same stride as kernel_size is supported! Got stride={stride} and "
            f"kernel_size={kernel_size}"
        )

    if isinstance(padding, (tuple, list, np.ndarray)):
        assert len(padding) == 2, "Invalid padding, only tuples, lists or np.ndarrays of length 2 are allowed"
        pad_h = padding[0] * 2
        pad_w = padding[1] * 2
        pad_top = padding[0]
        pad_bottom = padding[0]
        pad_left = padding[1]
        pad_right = padding[1]
    elif isinstance(padding, int):
        pad_h = padding * 2
        pad_w = padding * 2
        pad_top = padding
        pad_bottom = padding
        pad_left = padding
        pad_right = padding

    H_out = np.ceil((H_in + pad_h - (W_H - 1)) / stride[0]).astype("int")
    W_out = np.ceil((W_in + pad_w - (W_W - 1)) / stride[1]).astype("int")
    if data_order == "tf":
        output_shape = (H_out, W_out, C_out)
    elif data_order == "torch":
        output_shape = (C_out, H_out, W_out)

    conns = []
    for h_out in range(H_out):
        for w_out in range(W_out):
            for c in range(C_in):
                for w_h in range(W_H):
                    for w_w in range(W_W):
                        h_in = stride[0] * h_out - pad_top + w_h
                        w_in = stride[1] * w_out - pad_left + w_w
                        if h_in >= 0 and w_in >= 0 and h_in < H_in and w_in < W_in:
                            if data_order == "tf":
                                in_idx = h_in * (W_in * C_in) + w_in * (C_in) + c
                                out_idx = h_out * (W_out * C_out) + w_out * (C_out) + c
                                conns.append([in_idx, out_idx, 1, delay])
                            elif data_order == "torch":
                                # If data_order is "torch":
                                # - weight shapes C_out, C_in, H, W
                                # - input and output shapes as CHW
                                in_idx = c * (H_in * W_in) + h_in * (W_in) + w_in
                                out_idx = c * (H_out * W_out) + h_out * (W_out) + w_out
                                conns.append([in_idx, out_idx, 1, delay])

    return conns, output_shape


def join_conn_lists(list_a, list_b):
    # we assume all will use the same delay, given by the first element in list_b

    input_nodes = np.max([inp for inp, out, _, _ in list_a]) + 1
    middle_nodes = np.max([out for inp, out, _, _ in list_a]) + 1
    second_in_nodes = np.max([inp for inp, out, _, _ in list_b]) + 1
    output_nodes = np.max([out for inp, out, _, _ in list_b]) + 1
    assert middle_nodes == second_in_nodes

    arr_a = np.zeros((input_nodes, middle_nodes))
    arr_b = np.zeros((middle_nodes, output_nodes))
    for i, m, w, _ in list_a:
        arr_a[i, m] = w
    for m, o, w, _ in list_b:
        arr_b[m, o] = w
    total_arr = arr_a @ arr_b

    conns = []
    delay = list_b[0][3]
    for i in range(input_nodes):
        for o in range(output_nodes):
            w = total_arr[i, o]
            if w != 0:
                conns.append((i, o, w, delay))
    return conns


def mla_conv2d_max_output_value(kernel_height, kernel_width, in_channels, dtype_in, dtype_kernel):
    max_in_value = np.iinfo(dtype_in).max
    max_kernel_value = np.iinfo(dtype_kernel).max
    return kernel_height * kernel_width * in_channels * max_in_value * max_kernel_value
