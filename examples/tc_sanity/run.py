import json
import os
import subprocess

from spinnaker2.helpers import global_word_address

APPNAME = "../../../s2-sim2lab-app/host/experiment/app/experiment"

PE = (1, 1, 0)
memfile = "../../../s2-sim2lab-app/chip/app-pe/s2app/arm_sanity/binaries/s2app_arm.mem"

exp_spec = {
    "active_pes": [PE],
    "mem_files": [memfile],
    "duration_in_s": 1,
    "mem_regions_to_read": {"result": [global_word_address(PE[0], PE[1], PE[2], 0x8000), 1]},
    #'mem_data_to_send' : [[0x10000//4, [10,20,30]]]
}

cmd_opts = {
    "eth_ip": "192.168.1.1",  # use eth instead of jtag
    "stm_tty": "/dev/ttyACM1",
    "verbose": False,
    "reset_eth_phy": False,
}

print(exp_spec)
with open("spec.json", "w") as f:
    json.dump(exp_spec, f, indent=2)

# Run experiment
cmd = [APPNAME]
if "eth_ip" in cmd_opts:
    cmd = cmd + ["-e" + cmd_opts["eth_ip"]]
if "stm_tty" in cmd_opts:
    cmd = cmd + ["-S" + cmd_opts["stm_tty"]]
if cmd_opts.get("verbose", False):
    cmd = cmd + ["-v"]
if cmd_opts.get("reset_eth_phy", False):
    cmd = cmd + ["-r"]
subprocess.run(cmd)  # noqa: S603

results = {}

with open("results.json", "r") as f:
    results = json.load(f)

print(results)
if results["result"][0] == 0xCAFEBABE:
    print("SUCCESS!")
else:
    print("FAIL!")
