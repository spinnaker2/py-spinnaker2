import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, helpers, snn
from spinnaker2.mla import conv2d, mla_helpers

# create input signal for rf neurons
dt = 0.01
timesteps = 512
t = np.linspace(0, (timesteps - 1) * dt, timesteps)

w = np.array([0.2, 3, 4])
n_targets = len(w)

signal = np.exp(1j * 2 * np.pi * np.einsum("i,j->ji", t, w))
signal = np.sum(signal, axis=0)

rf_input = {0: signal}


# parameters of rf neuron

n_neurons = 10
freqs = np.linspace(2, 5, n_neurons)
threshold = 100
spike_prob = 0.8
decay_const = 0
spike_fn = 0  # 0 : 'poissonspike'
reset_fn = 0  # 0 : 'none'

# do some checks

# NOTE: the number of neurons should equal to the number of frequencies.
assert n_neurons == len(freqs)
# NOTE: if there are more than one input signals, their length should be the same,
# which is the number of time steps.
for idx in range(len(rf_input)):
    assert len(rf_input[int(idx)]) == timesteps


# create population


params = {
    "freqs": freqs,
    "threshold": threshold,
    "spike_prob": spike_prob,
    "decay_const": decay_const,
    "spike_fn": spike_fn,
    "reset_fn": reset_fn,
}

# NOTE: the float list class expects a dict like this:
# {0: signal0, 1: signal1, ...} where 0, 1 are the neuron (input) index,
# and signal0, signal1 are numpy arrays of complex numbers, where each
# number corresponds to the input in a time step. Complex numbers are
# expected here only to keep it general. Please set the imag. part to 0
# if you don't need it, otherwise the neuron will process it.
stim = snn.Population(size=2, neuron_model="float_list", params=rf_input, name="stim")

rf_pop = snn.Population(
    size=n_neurons,
    neuron_model="rfneuron",
    params=params,
    name="rfpopulation",
    record=["v", "spikes"],
)


conns = []
# NOTE: connect neuron 0 with weight 1 (real) and connect neuron 1 with weight -1 (imag).
for j in range(rf_pop.size):
    conns.append([0, j, 1, 1])
    conns.append([1, j, -1, 1])

proj = snn.Projection(pre=stim, post=rf_pop, connections=conns)


# create network with populations and projections
net = snn.Network("rfnetwork")
net.add(stim, rf_pop, proj)
# net.add(stim)
# net.add(rf_pop)
# net.add(rf_pop, stim, proj)

# start simulation on the hardware
hw = hardware.FPGA_Rev2()
# hw.run(net, timesteps, mapping_only=True)
# hw.run(net, timesteps, debug = False)
hw.run(net, timesteps)

# read back results from the hardware
# get results and compare spike times to expected ones
voltages = rf_pop.get_voltages()  # dict with neuron_ids as keys and list of spike times as value

# print(voltages)

plt.plot(voltages[3])
plt.plot(voltages[6])
plt.plot(voltages[9])
# plt.plot(rf_input[0].real)

spike_times = rf_pop.get_spikes()
print(spike_times)

plt.figure()
indices, times = helpers.spike_times_dict_to_arrays(spike_times)
plt.plot(times, indices, ".")
plt.xlim(0, timesteps)
plt.ylim(0, rf_pop.size)
plt.xlabel("time step")
plt.ylabel("neuron")

plt.show()
