"""
Synfire chain example

Reproduction of synfire chain implementation from SpiNNaker2 prototype chips.
References:
1. Implementation on Santos28 chip:
   Höppner, Sebastian, et al. "Dynamic power management for neuromorphic
   many-core systems." IEEE TCAS I(2019).
   https://doi.org/10.1109/TCSI.2019.2911898
2. Implementation on Jib1 chip:
   Höppner, Sebastian, et al. "The SpiNNaker 2 processing element architecture
   for hybrid digital neuromorphic computing." arXiv preprint(2021).
   https://doi.org/10.48550/arXiv.2103.08392
3. Original synfire chain model:
   Kremkow, Jens, et al. "Functional consequences of correlated excitatory and
   inhibitory conductances in cortical networks." Journal of computational
   neuroscience 28.3 (2010): 579-594.
   https://doi.org/10.1007/s10827-010-0240-9
"""

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng
from spinnaker2 import brian2_sim, hardware, helpers, snn

rng = default_rng()

n_groups = 5

n_exc = 200
n_inh = 50
n_pop = n_exc + n_inh
w_exc = 15.0
delay_exc = 7
w_inh = -2.14
delay_inh = 5

n_pre_conn_exc = 60
n_pre_conn_inh = 25


params = {
    "threshold": 1167.67,  # from tune_synfire_chain.py
    "alpha_decay": 0.904837,
    "i_offset": 0.0,
    "v_reset": 0.0,
    "reset": "reset_to_v_reset",
    "exc_decay": 0.513417119,
    "inh_decay": 0.36787944,
    "t_refrac": 2,
}

# Excitatory input
input_spikes_exc = rng.normal(25, 2, n_exc).astype(np.uint)
print(input_spikes_exc)
input_spikes_dict = {}
for key, time in enumerate(input_spikes_exc):
    input_spikes_dict[key] = [time]

print(input_spikes_dict)
stim = snn.Population(n_exc, "spike_list", input_spikes_dict, "stim", record=["time_done"])


pops = []
for i in range(n_groups):
    pop = snn.Population(
        n_pop,
        "lif_curr_exp",
        params=params,
        name=f"group_{i}",
        record=["spikes", "time_done"],
    )
    pop.set_max_atoms_per_core(125)
    pops.append(pop)

pre_pops = [stim] + pops[:-1]

net = snn.Network("Synfire chain")
net.add(stim)
net.add(*pops)

# connections
################
# feed-forward #
################
for pre, post in zip(pre_pops, pops):
    # fixed number pre with 60 conns
    conns = []
    for post_id in range(post.size):
        pre_ids = np.arange(pre.size, dtype=int)
        rng.shuffle(pre_ids)
        for pre_id in pre_ids[:n_pre_conn_exc]:
            conns.append([pre_id, post_id, w_exc, delay_exc])
    proj = snn.Projection(pre=pre, post=post, connections=conns)
    net.add(proj)

####################
# local inhibition #
####################
for pop in pops:
    # fixed number pre with 25 conns
    conns = []
    for post_id in range(n_exc):
        pre_ids = np.arange(n_exc, n_pop, dtype=int)
        rng.shuffle(pre_ids)
        for pre_id in pre_ids[:n_pre_conn_inh]:
            conns.append([pre_id, post_id, w_inh, delay_inh])
    proj = snn.Projection(pre=pop, post=pop, connections=conns)
    net.add(proj)

hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.25")
# hw = brian2_sim.Brian2Backend()
time_steps = 80
hw.run(net, time_steps, debug=True)


# plotting
for i, pop in enumerate(pops):
    spike_times = pop.get_spikes()
    indices, times = helpers.spike_times_dict_to_arrays(spike_times)
    plt.plot(times, indices + i * n_pop, ".", label=pop.name)
plt.legend()
plt.xlim(0, time_steps)
plt.xlabel("time step")
plt.ylabel("neuron")
plt.show()

plot_times_done = False
if plot_times_done:
    time_done_times_synfire = dict()
    for i, pop in enumerate(pops):
        time_done_times_synfire.update(pop.get_time_done_times())

    helpers.save_dict_to_npz(
        time_done_times_synfire,
        "time_done_times_synfire",
    )
    helpers.plot_times_done_multiple_pes_one_plot_vertical("time_done_times_synfire.npz")

    # spike list
    time_done_times_stim = stim.get_time_done_times()
    helpers.save_dict_to_npz(
        time_done_times_stim,
        "time_done_times_stim",
    )
    helpers.plot_times_done_multiple_pes_one_plot_vertical("time_done_times_stim.npz")
