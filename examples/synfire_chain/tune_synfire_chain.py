"""
Script for tuning synfire chain parameters.

original parameters and model from Yexin:

V_rest      = -70
V_reset     = -70
threshold   = -57
T_refract   = 2
exp_TC      = 0.904837
g_membrane  = 29
I_offset    = 0 
V_rev_E     = 0
V_rev_I     = -75
exc_decay   = 0.513417119
inh_decay   = 0.367879441
exc_init    = 0.78554*0.925500993; // scaling factor for increment in each timestep 
inh_init    = 0.78554*1.2025984172; // scaling factor for increment in each timestep 

g_ampa = 1.
g_gaba = 2.

inter_group_delay = 7; 
intra_group_delay = 5; 

model:
    input_exc[t] = input_exc[t-1]*exc_decay + exc_init*weighted_sum_exc
    input_inh[t] = input_inh[t-1]*inh_decay + inh_init*weighted_sum_inh
    I_syn_exc[t] = input_exc[t]*(V_rev_E - v[t-1])
    I_syn_inh[t] = -input_inh[t]*(V_rev_I - v[t-1])
    I_syn[t] = I_syn_exc[t] - I_syn_inh[t]
    v[t] = I_syn[t]/g_membrane + v_rest - exp_TC*(I_syn[t]/g_membrane + v_rest - v[t]) # original
    v[t] = (1-exp_TC)*I_syn[t]/g_membrane + v_rest - exp_TC*(v_rest - v[t]) # rewritten

    v_rest = 0
    v[t] = (1-exp_TC)*I_syn[t]/g_membrane + exp_TC*v[t]

shifted voltages:
V_rest      = 0
V_reset     = 0
threshold   = 13
V_rev_E     = 70
V_rev_I     = -5

Average delta_v for conductance: (Assumption: v=5)
delta_v_exc = 70-5 = 65
delta_v_inh = -5-5 = 10
w_scale_exc = 65*exc_init
w_scale_inh = 10*inh_init
w_exc = 65*exc_init* 1.
w_inh = 65*exc_init* 1.
"""

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import brian2_sim, hardware, helpers, snn

exp_TC = 0.904837
g_membrane = 29
exc_init = 0.78554 * 0.925500993
inh_init = 0.78554 * 1.2025984172
g_ampa = 1.0
g_gaba = 2.0
threshold = 13
V_rev_E = 70
V_rev_I = -5
V_mean = 0
w_exc = (V_rev_E - V_mean) * exc_init * g_ampa
w_inh = (V_rev_I - V_mean) * exc_init * g_gaba
print("w_exc", w_exc)
print("w_inh", w_inh)
print("w_exc/w_inh", w_exc / w_inh)
input_scale = (1 - exp_TC) / g_membrane
print("input_scale", input_scale)
v_scale = 1.0 / input_scale
# threshold_scaled = threshold*v_scale
# print("threshold_scaled", threshold_scaled)

# scale weights to range [-15,15]
w_max = 15
max_abs_w = np.max(np.abs([w_exc, w_inh]))
print(max_abs_w)
w_scale = w_max / max_abs_w
print(w_scale)
w_exc *= w_scale
w_inh *= w_scale
threshold = threshold * v_scale * w_scale
print("w_exc", w_exc)
print("w_inh", w_inh)
print("threshold", threshold)

# exit(-1)

rng = np.random.default_rng()

n_pre_conn_exc = 60
n_pre_conn_inh = 25

params = {
    "threshold": threshold,
    "alpha_decay": 0.904837,
    "i_offset": 0.0,
    "v_reset": 0.0,
    "reset": "reset_to_v_reset",
    "exc_decay": 0.513417119,
    "inh_decay": 0.36787944,
    "t_refrac": 2,
}
print(params)

pop1 = snn.Population(size=1, neuron_model="lif_curr_exp", params=params, name="pop1", record=["spikes", "v"])

# Excitatory input
input_spikes_exc = rng.normal(25, 2, n_pre_conn_exc).astype(np.uint)
print(input_spikes_exc)
input_spikes_dict = {}
for key, time in enumerate(input_spikes_exc):
    input_spikes_dict[key] = [time]

print(input_spikes_dict)
stim = snn.Population(n_pre_conn_exc, "spike_list", input_spikes_dict, "stim")

# w_exc = 5
d = 2
conns = []
for i in range(n_pre_conn_exc):
    conns.append([i, 0, w_exc, d])

proj = snn.Projection(stim, pop1, conns)

input_spikes_inh = rng.normal(25, 2, n_pre_conn_inh).astype(np.uint)
print(input_spikes_exc)
input_spikes_dict = {}
for key, time in enumerate(input_spikes_exc):
    input_spikes_dict[key] = [time]

print(input_spikes_dict)
stim2 = snn.Population(n_pre_conn_exc, "spike_list", input_spikes_dict, "stim2")

# w_inh = -2
d_inh = d + 5
conns = []
for i in range(n_pre_conn_exc):
    conns.append([i, 0, w_inh, d_inh])

proj2 = snn.Projection(stim2, pop1, conns)

# Inhibitory input

net = snn.Network("my network")
net.add(stim, pop1, proj, proj2, stim2)

hw = hardware.SpiNNaker2Chip()
# hw = brian2_sim.Brian2Backend()
timesteps = 50
hw.run(net, timesteps)

# get results and plot

spikes = pop1.get_spikes()[0]
voltages = pop1.get_voltages()
times = np.arange(timesteps)
plt.plot(times, voltages[0], label="Neuron 0")
plt.axhline(y=params["threshold"], c="gray", ls="--", label="threshold")
plt.xlim(0, timesteps)
plt.xlabel("time step")
plt.ylabel("voltage")
plt.legend()
plt.show()
