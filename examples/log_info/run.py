"""
Example for using the log_info() function on the chip and printing it in
python.
"""

import spinnaker2.hardware
from spinnaker2.configuration import Experiment, ExperimentConfig, MemoryRegion, PEConfig
from spinnaker2.coordinates import PE, ByteAddr, WordAddr
from spinnaker2.helpers import read_log
from spinnaker2.neuron_models.common import add_log_memory_region

log_info_base_address = 0x1B000  # byte address
log_info_word_count = 4000

pe = PE(1, 1, 0)
memfile = "../../../s2-sim2lab-app/chip/app-pe/s2app/log_info/binaries/s2app_arm.mem"

# create PE config
pe_config = PEConfig(pe, "log_info", memfile)
add_log_memory_region(pe_config, log_info_base_address, log_info_word_count)

# run experiment
exp_config = ExperimentConfig(runtime_in_s=0.1)
exp_config.add(pe_config)

experiment = Experiment(exp_config, spinnaker2.hardware._experiment_app_path_s2_chip())
cmd_opts = ["-e", "192.168.1.25"]
experiment.run(cmd_opts)

log_raw = experiment.get_result(pe, "log")
result_string = read_log(log_raw)
print("Log ", pe)
print(result_string.decode("ASCII"))
