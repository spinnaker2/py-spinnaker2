import os

import numpy as np
import numpy.random
import spinnaker2.hardware
from spinnaker2.configuration import Experiment, ExperimentConfig, MemoryRegion, PEConfig
from spinnaker2.coordinates import PE, ByteAddr, WordAddr
from spinnaker2.mla import mla_config, mla_helpers

"""
Simple experiment to run Conv2D with the machine learning accelerator.
"""

numpy.random.seed(42)

pe = PE(1, 1, 0)
memfile = "../../../s2-sim2lab-app/chip/app-pe/s2app/conv2d_exp_runner/binaries/s2app_arm.mem"


params_conv = {
    "mlparams0_op_a_addr": 0x10000,
    "mlparams0_op_b_addr": 0x11010,
    "mlparams0_target_addr": 0x12000,
    "mlparams0_op_a_use_noc": 0,
    "mlparams0_op_a_is_signed": 0,
    "mlparams0_op_b_is_signed": 0,
    "mlparams0_op_a_is_16bit": 0,
    "mlparams0_op_b_is_16bit": 0,
    "mlparams0_output_shift_width": 0,
    "mlparams0_output_mode": 2,
    "mlparams0_relu_en": 0,
    "mlparams0_in_batch_s": 1,
    "mlparams0_in_pix_row_s": 31,  # fmap rows for input ->32 for our case
    "mlparams0_in_pix_col_s": 31,
    "mlparams0_fil_pix_row_s": 3,  # filter x dim
    "mlparams0_fil_pix_col_s": 3,  # filter y dim
    "mlparams0_in_channel_s": 1,  # 1
    "mlparams0_out_channel_s": 4,  # 4
    "mlparams0_stride_in_pix_row": 1,  # stride is 2 ^ n, n=0..3
    "mlparams0_stride_in_pix_col": 1,
    "signed_i": 0,
    "signed_w": 0,
    "dtype_i": 0,
    "dtype_w": 0,
    "dtype_o": 2,
    "output_shift": 0,
}

c2d_config = mla_config.Conv2DConfig()

c2d_config.op_a_addr = params_conv["mlparams0_op_a_addr"]
c2d_config.op_b_addr = params_conv["mlparams0_op_b_addr"]
c2d_config.target_addr = params_conv["mlparams0_target_addr"]
c2d_config.op_a_use_noc = params_conv["mlparams0_op_a_use_noc"]

c2d_config.op_a_is_signed = params_conv["mlparams0_op_a_is_signed"]
c2d_config.op_b_is_signed = params_conv["mlparams0_op_b_is_signed"]
c2d_config.op_a_is_16bit = params_conv["mlparams0_op_a_is_16bit"]
c2d_config.op_b_is_16bit = params_conv["mlparams0_op_b_is_16bit"]
c2d_config.output_shift_width = params_conv["mlparams0_output_shift_width"]
c2d_config.output_mode = params_conv["mlparams0_output_mode"]
c2d_config.relu_en = params_conv["mlparams0_relu_en"]

c2d_config.batch_size = params_conv["mlparams0_in_batch_s"]
c2d_config.in_height = params_conv["mlparams0_in_pix_row_s"]
c2d_config.in_width = params_conv["mlparams0_in_pix_col_s"]
c2d_config.filter_height = params_conv["mlparams0_fil_pix_row_s"]
c2d_config.filter_width = params_conv["mlparams0_fil_pix_col_s"]
c2d_config.in_channels = params_conv["mlparams0_in_channel_s"]
c2d_config.out_channels = params_conv["mlparams0_out_channel_s"]
c2d_config.stride_y_2_power_n = params_conv["mlparams0_stride_in_pix_row"]
c2d_config.stride_x_2_power_n = params_conv["mlparams0_stride_in_pix_col"]

# get input and output
value_returned = mla_helpers.get_mem_tensors(params_conv)

op_a_addr = ByteAddr(c2d_config.op_a_addr)
op_b_addr = ByteAddr(c2d_config.op_b_addr)
result_addr = ByteAddr(c2d_config.target_addr)

op_b_list = value_returned["images"]
op_a_list = value_returned["weights"]
result_words = value_returned["rand_o"].shape[0]
print("N results:", result_words)
print("N op A:", len(op_a_list))
print("N op B:", len(op_b_list))

# create PE config
pe_config = PEConfig(pe, "tc_conv2d_exp_runner", memfile)
pe_config.add_mem_region_to_read("result", MemoryRegion(result_addr.to_WordAddr(), result_words))
pe_config.add_mem_data_to_send(op_a_addr.to_WordAddr(), op_a_list)
pe_config.add_mem_data_to_send(op_b_addr.to_WordAddr(), op_b_list)
pe_config.add_mem_region_to_read("op_a", MemoryRegion(op_a_addr.to_WordAddr(), len(op_a_list)))
pe_config.add_mem_region_to_read("op_b", MemoryRegion(op_b_addr.to_WordAddr(), len(op_b_list)))

# create Conv2D config and send to HW
c2d_config_raw = c2d_config.to_param_struct()
print("Conv2D config struct", c2d_config_raw)

c2d_params_addr = ByteAddr(0x9400)
pe_config.add_mem_data_to_send(c2d_params_addr.to_WordAddr(), c2d_config_raw.tolist())

# run experiment
exp_config = ExperimentConfig(runtime_in_s=1.0)
exp_config.add(pe_config)

experiment = Experiment(exp_config, spinnaker2.hardware._experiment_app_path_s2_chip())
experiment.run()

data_out = experiment.get_result(pe, "result")
read_op_a = experiment.get_result(pe, "op_a")
read_op_b = experiment.get_result(pe, "op_b")
vhex = np.vectorize(hex)
print("read op A:", vhex(read_op_a))
print("read op B:", vhex(read_op_b))
print("MLA result     :", vhex(data_out[:20]))
expected_out = value_returned["rand_o"]
expected_out = expected_out.tolist()
print("Expected result:", vhex(expected_out[:20]))
correct_values = np.equal(data_out, expected_out)
print("values at right position:", np.count_nonzero(correct_values))
