import os

import numpy as np
import spinnaker2.hardware
import tensorflow as tf
from spinnaker2.configuration import Experiment, ExperimentConfig, MemoryRegion, PEConfig
from spinnaker2.coordinates import PE, ByteAddr, WordAddr
from spinnaker2.mla import conv2d as c2d
from spinnaker2.mla import mla_config, mla_helpers

batch_size = 1
in_width = 28
in_height = 28
in_channels = 1
out_channels = 4
filter_height = 3
filter_width = 3
stride_y = 2
stride_x = 2

rng = np.random.default_rng()

# NHWC
x_in = rng.integers(0, 2**8, size=(batch_size, in_height, in_width, in_channels), dtype=np.uint8)

# w_in = rng.integers(0, 2**8, size=(filter_height, filter_width, in_channels, out_channels), dtype=np.uint8)
w_in = rng.integers(-(2**7), 2**7, size=(filter_height, filter_width, in_channels, out_channels), dtype=np.int8)
print(x_in.shape)
print(w_in.shape)

# compare TensorFlow to python module conv2d
x = tf.constant(x_in, dtype=tf.int32)
w = tf.constant(w_in, dtype=tf.int32)
print(x.dtype)
print(w.dtype)
out = tf.nn.conv2d(x, w, strides=[1, stride_y, stride_x, 1], padding="VALID")
print(out)

out2 = c2d.conv2d(x_in.astype(np.int32), w_in.astype(np.int32), pad="VALID", stride=(stride_y, stride_x))
print(out2)
print("output shape(orig):", out2.shape)

print(np.array_equal(out, out2))

# TODO: deploy on SpiNNaker2

# INPUT DATA
# change from NHWC to NCHW
x_in = np.moveaxis(x_in, 3, 1)
x_spinn = mla_helpers.prepare_mla_conv_fmap(x_in)

# change from (H,W,CI,CO) to (CO,H,W,CI)
w_in = np.moveaxis(w_in, -1, 0)
w_spinn = mla_helpers.prepare_mla_conv_kernel(w_in)

# expected output
output_height = np.ceil(in_height - filter_height + 1)
output_height = np.uint16((output_height / stride_y) + (0 < (output_height % stride_y)))
output_width = np.ceil(in_width - filter_width + 1)
output_width = np.uint8((output_width / stride_x) + (0 < (output_width % stride_x)))

shape_o = [batch_size, output_height, output_width, out_channels]
zero_fill = lambda x, y: (y - (x % y)) % y
# data_type_o, low_o, high_o, bmask_o, align_o = (np.uint32, 0, 2 ** 32, 0xffffffff, 4)
data_type_o, low_o, high_o, bmask_o, align_o = (np.int32, -(2**31), 2**31, 0xFFFFFFFF, 4)
zeros_shape_o = [batch_size, out_channels, output_height, zero_fill(output_width, align_o)]
out_expected = np.array(out2, dtype=data_type_o)
out_expected_0 = out_expected.flatten()

# bring axis to shape inside the memory
## change axis for correct dimension order
out_expected = np.moveaxis(out_expected, 3, 1)
out_expected_no_pad = out_expected
## fill memory line with 0 zeros due to alignment
out_expected = np.concatenate((out_expected, np.zeros(zeros_shape_o, dtype=data_type_o)), axis=-1)
print("output shape (aligned):", out_expected.shape)
## flatten tensor and prepare for file generation
out_expected = np.ravel(out_expected)

x_spinn_list = x_spinn.ravel().view(np.uint32).tolist()
w_spinn_list = w_spinn.ravel().view(np.uint32).tolist()

c2d_config = mla_config.Conv2DConfig()

c2d_config.op_a_addr = 0x11000
c2d_config.op_b_addr = 0x12000
c2d_config.target_addr = 0x13000
c2d_config.op_a_use_noc = False

c2d_config.op_a_is_signed = w_spinn.dtype in (np.int8, np.int16)
c2d_config.op_b_is_signed = x_spinn.dtype in (np.int8, np.int16)
c2d_config.op_a_is_16bit = w_spinn.dtype.itemsize == 2
c2d_config.op_b_is_16bit = x_spinn.dtype.itemsize == 2
c2d_config.output_shift_width = 0
c2d_config.output_mode = 2
c2d_config.relu_en = False

c2d_config.batch_size = batch_size
c2d_config.in_height = in_height
c2d_config.in_width = in_width
c2d_config.filter_height = filter_height
c2d_config.filter_width = filter_width
c2d_config.in_channels = in_channels
c2d_config.out_channels = out_channels
c2d_config.set_stride_y(stride_y)
c2d_config.set_stride_x(stride_x)

output_shape = mla_helpers.output_shape_conv2d(c2d_config)
output_shape_aligned = mla_helpers.output_shape_conv2d_aligned(c2d_config)
print("output shape (calc):", output_shape)

pe = PE(1, 1, 0)
memfile = "../../../s2-sim2lab-app/chip/app-pe/s2app/conv2d_exp_runner/binaries/s2app_arm.mem"

op_a_addr = ByteAddr(c2d_config.op_a_addr)
op_b_addr = ByteAddr(c2d_config.op_b_addr)
result_addr = ByteAddr(c2d_config.target_addr)
# create PE config
pe_config = PEConfig(pe, "tc_conv2d_exp_runner", memfile)
pe_config.add_mem_region_to_read("result", MemoryRegion(result_addr.to_WordAddr(), out_expected.shape[0]))
pe_config.add_mem_data_to_send(op_a_addr.to_WordAddr(), w_spinn_list)
pe_config.add_mem_data_to_send(op_b_addr.to_WordAddr(), x_spinn_list)

# create Conv2D config and send to HW
c2d_config_raw = c2d_config.to_param_struct()
print("Conv2D config struct", c2d_config_raw)

c2d_params_addr = ByteAddr(0x9400)
pe_config.add_mem_data_to_send(c2d_params_addr.to_WordAddr(), c2d_config_raw.tolist())

# run experiment
exp_config = ExperimentConfig(runtime_in_s=1.0)
exp_config.add(pe_config)

experiment = Experiment(exp_config, spinnaker2.hardware._experiment_app_path_s2_chip())
experiment.run()

data_out = experiment.get_result(pe, "result")
data_out = np.frombuffer(np.array(data_out, dtype=np.uint32), dtype=data_type_o)
data_out_flat = np.copy(data_out)
max_output_value = filter_width * filter_height * 2**7 * 2**8
print(data_out.shape)
print("max_output_value", max_output_value)
print("output max:", data_out.max())
print("output min:", data_out.min())
print("MLA result     :", data_out[:100])
expected_out = out_expected.tolist()
print("Expected result:", expected_out[:100])
for i in range(len(data_out) // 16):
    start = i * 16
    end = (i + 1) * 16
    print(data_out[start:end])
    print(expected_out[start:end])

correct_values = np.equal(data_out, expected_out)
print("values at right position:", np.count_nonzero(correct_values))
print("values total:", len(data_out))
print(out2.shape)

data_out = np.reshape(data_out, output_shape_aligned)
print(data_out.shape)
data_out_valid = data_out[:, :, :, : output_shape[3]]
print(data_out_valid.shape)
print(out_expected_no_pad.shape)
if np.array_equal(data_out_valid, out_expected_no_pad):
    print("SUCCESS!")
else:
    print("FAIL!")

# try to flatten (to make sure flatten in C is fine)
out_expected_flat = out_expected_no_pad.flatten()
out_data_flat = np.zeros_like(out_expected_flat)
index = 0
offset = 0
for n in range(data_out.shape[0]):
    for ch in range(data_out.shape[1]):
        for row in range(data_out.shape[2]):
            for col in range(output_shape[3]):
                out_data_flat[index] = data_out_flat[offset + col]
                index += 1
            offset += data_out.shape[3]
print(np.array_equal(out_expected_flat, out_data_flat))
