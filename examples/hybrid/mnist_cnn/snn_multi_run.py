import itertools
import json
import os
import subprocess
import time

import matplotlib.pyplot as plt
import numpy as np
import spinnaker2.coordinates as coord
import tensorflow as tf
import tensorflow_datasets as tfds
from spinnaker2 import ann2snn_helpers, hardware, mapper, snn
from spinnaker2.configuration import ExperimentResult
from spinnaker2.helpers import global_word_address
from spinnaker2.mla import mla_helpers

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"  # use CPU for TF

t_start = time.time()

# Network definitions
conv2d_out_shape = (4, 12, 12)
conv2d_size = np.prod(conv2d_out_shape)  # 576
dense1_size = 16
dense2_size = 10
delay = 0
timesteps = 50

dummy_input_image_uint8 = np.zeros(shape=(1, 28, 28), dtype=np.uint8)

####################################
# Load weights and convert to int8 #
####################################
tf_model = tf.keras.models.load_model("tf_model_v2.h5")

# conv2d
conv2d_weights = np.array(tf_model.weights[0])
conv2d_weights_scaled_int8 = ann2snn_helpers.scale_and_convert_weights_to_int8(conv2d_weights)

# dense
dense_weights = np.array(tf_model.weights[1])
dense_weights_scaled_int8 = ann2snn_helpers.scale_and_convert_weights_to_int8(dense_weights)
# reshape dense weights
dense_weights_scaled_int8 = ann2snn_helpers.reorder_dense_weights_after_conv2d(
    dense_weights_scaled_int8,
    channels=conv2d_out_shape[0],
    height=conv2d_out_shape[1],
    width=conv2d_out_shape[2],
)

# dense2
dense2_weights = np.array(tf_model.weights[2])
dense2_weights_scaled_int8 = ann2snn_helpers.scale_and_convert_weights_to_int8(dense2_weights)

######################
# Network Definition #
######################

##########
# Conv2D #
##########
max_theo_output_value = ann2snn_helpers.mla_conv2d_max_output_value(5, 5, 1, np.int8, np.uint8)
threshold = 100.0
scale = threshold / max_theo_output_value

params = {
    "image": dummy_input_image_uint8,  # CHW
    "weights": conv2d_weights_scaled_int8,  # (H,W,CI,CO) format
    "scale": scale * 5,  # multiplier of weighted sum to I_offset
    "threshold": threshold,  # spike threshold
    "stride_x": 2,
    "stride_y": 2,
}

input_pop = snn.Population(
    size=conv2d_size,
    neuron_model="conv2d_if_neuron_rate_code",
    params=params,
    name="input_pop",
    record=["spikes"],
)

######################
# Dense Hidden Layer #
######################

max_theo_output_value = dense_weights_scaled_int8.shape[0] * np.iinfo(np.int8).max
neuron_params = {
    "threshold": max_theo_output_value / 50,
    "alpha_decay": 1.0,
}

pop1 = snn.Population(
    size=dense1_size,
    neuron_model="lif_no_delay",
    params=neuron_params,
    name="pop1",
    record=["spikes"],
)

conns = ann2snn_helpers.connection_list_from_dense_weights(dense_weights_scaled_int8, delay)

proj1 = snn.Projection(pre=input_pop, post=pop1, connections=conns)

######################
# Dense Output Layer #
######################

neuron_params_out = {
    "threshold": 1.0e9,  # very high threshold so that it is never reached
    "alpha_decay": 1.0,  # no leakage
}

pop2 = snn.Population(
    size=dense2_size,
    neuron_model="lif_no_delay",
    params=neuron_params_out,
    name="pop2",
    record=["spikes", "v"],
)

conns2 = ann2snn_helpers.connection_list_from_dense_weights(dense2_weights_scaled_int8, delay)

proj2 = snn.Projection(pre=pop1, post=pop2, connections=conns2)

net = snn.Network("my network")
net.add(input_pop, pop1, pop2, proj1, proj2)

#####################
# Run on SpiNNaker2 #
#####################

hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.25")

# run mapping and extract config
debug = False
my_mapper = mapper.Mapper(net, hw)
exp_config, sim_cfg = my_mapper.map_and_generate_experiment_config(timesteps, debug=debug)

multi_run_app_path = "/home/vogginger/project/software/s2-sim2lab-app/host/units/s2_top/tc_s2app_s2_ctrl_experiment_json/binaries/testcase"  # noqa: E501

spec_file = "spec1.json"
exp_config.dump_spec(spec_file)
input_data_file = "input1.json"
cmd = [multi_run_app_path, "-s", spec_file, "-i", input_data_file]
input_data_addr = coord.ByteAddr(69824)  # ByteAddr, manually extracted using print
pe = coord.PE(1, 1, 0)  # S2 Chip

# Load dataset
(ds_train, ds_test), ds_info = tfds.load(
    "mnist",
    split=["train", "test"],
    as_supervised=True,
    with_info=True,
)
print(ds_info)
n_samples = 100
ds = ds_test.take(n_samples)

results = np.zeros(n_samples, dtype=np.uint32)
voltages_all_trials = np.zeros((n_samples, dense2_size, timesteps), dtype=np.float32)
labels = np.zeros(n_samples, dtype=int)

# generate input data for all runs
json_like_obj = []
for sample, (input_image, label) in enumerate(tfds.as_numpy(ds)):
    labels[sample] = label

    # re-format input image
    x_in = input_image.astype(np.uint8)
    x_in = np.moveaxis(x_in, -1, 0)
    assert x_in.shape == (1, 28, 28)  # CHW

    x_in = np.expand_dims(x_in, 0)  # add batch dimension
    x_spinn = mla_helpers.prepare_mla_conv_fmap(x_in)
    x_spinn_list = x_spinn.ravel().view(np.uint32).tolist()

    json_like_obj.append(
        {"mem_data_to_send": [[global_word_address(pe.x, pe.y, pe.pe, int(input_data_addr)), x_spinn_list]]}
    )

with open(input_data_file, "w") as f:
    json.dump(json_like_obj, f, indent=2)

subprocess.run(cmd, check=True)  # noqa: S603

# load and process results of all runs
for i in range(n_samples):
    experiment = ExperimentResult()
    result_file = f"results_{i}.json"
    with open(result_file, "r") as f:
        experiment.results = json.load(f)

    net.reset()

    hardware.process_results(experiment, my_mapper, sim_cfg, debug=debug)

    # get voltage of last timestep of output neurons to find winner
    voltages = pop2.get_voltages()
    v_last_timestep = [vs[-1] for vs in voltages.values()]
    index_max_value = np.argmax(v_last_timestep)
    label = labels[i]
    print("Predicted label:", index_max_value)
    print("Actual label:", label)
    if label == index_max_value:
        print("CORRECT PREDICTION")
        results[i] = True
    for idx, vs in voltages.items():
        voltages_all_trials[sample, idx, :] = vs

t_end = time.time()
duration = t_end - t_start
print(f"Duration: {duration:.3f} seconds")

print(results)
n_correct = np.count_nonzero(results)
accuracy = n_correct / results.size
print(f"Accuracy: {accuracy:.2%}")
