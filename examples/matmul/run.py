import os

import numpy as np
import numpy.random
import spinnaker2.hardware
from spinnaker2.configuration import Experiment, ExperimentConfig, MemoryRegion, PEConfig
from spinnaker2.coordinates import PE, ByteAddr, WordAddr
from spinnaker2.mla import mla_config, mla_helpers

"""
Simple experiment to run a matrix multiplication with the machine learning accelerator.
"""

pe = PE(1, 1, 0)
memfile = "../../../s2-sim2lab-app/chip/app-pe/s2app/matmul_exp_runner/binaries/s2app_arm.mem"

rows_a = 8  # multiple of 4
cols_a_rows_b = 8  # multiple of 4
cols_b = 16  # multiple of 16
rng = numpy.random.default_rng()
op_a = rng.integers(0, 2**8, size=(rows_a, cols_a_rows_b), dtype=numpy.uint8)
op_b = rng.integers(0, 2**8, size=(cols_a_rows_b, cols_b), dtype=numpy.uint8)
print("op_a", op_a)
print("op_b", op_b)

c = np.matmul(op_a, op_b, dtype=np.uint32)
print(c)
print(c.flatten())
print(c.dtype)

op_a_addr = ByteAddr(0x10100)
op_b_addr = ByteAddr(0x11000)
op_a_raw = op_a.transpose()
op_a_raw = np.hsplit(op_a_raw, op_a_raw.shape[1] // 4)
op_a_list = np.ravel(op_a_raw).view(np.uint32).tolist()
op_b_list = op_b.flatten().view(np.uint32).tolist()
# print("op_a_list", op_a_list)
# print(op_b_list)

result_addr = ByteAddr(0x14000)
result_words = rows_a * cols_b
print("N results:", result_words)


pe_config = PEConfig(pe, "tc_matmul_exp_runner", memfile)
pe_config.add_mem_region_to_read("result", MemoryRegion(result_addr.to_WordAddr(), result_words))
pe_config.add_mem_data_to_send(op_a_addr.to_WordAddr(), op_a_list)
pe_config.add_mem_data_to_send(op_b_addr.to_WordAddr(), op_b_list)
pe_config.add_mem_region_to_read("op_a", MemoryRegion(op_a_addr.to_WordAddr(), len(op_a_list)))
pe_config.add_mem_region_to_read("op_b", MemoryRegion(op_b_addr.to_WordAddr(), len(op_b_list)))


# create MM config and send to HW
mm_config = mla_config.MatMulConfig()
mm_config.matmul_rows_a = rows_a // 4
mm_config.matmul_cols_a_rows_b = cols_a_rows_b // 4
mm_config.matmul_cols_b = cols_b // 16

mm_config.op_a_addr = int(op_a_addr)
mm_config.op_b_addr = int(op_b_addr)
mm_config.target_addr = int(result_addr)
mm_config.op_a_use_noc = False

mm_config.op_a_is_signed = 0
mm_config.op_b_is_signed = 0
mm_config.op_a_is_16bit = 0
mm_config.op_b_is_16bit = 0
mm_config.output_shift_width = 0
mm_config.output_mode = 2
mm_config.relu_en = 0

mm_config_raw = mm_config.to_param_struct()
print("MM config struct", mm_config_raw)

mm_params_addr = ByteAddr(0x9400)
pe_config.add_mem_data_to_send(mm_params_addr.to_WordAddr(), mm_config_raw.tolist())

exp_config = ExperimentConfig(runtime_in_s=1.0)
exp_config.add(pe_config)

experiment = Experiment(exp_config, spinnaker2.hardware._experiment_app_path_s2_chip())
experiment.run()

data_out = experiment.get_result(pe, "result")
read_op_a = experiment.get_result(pe, "op_a")
read_op_b = experiment.get_result(pe, "op_b")
print("read op A:", read_op_a)
print("read op B:", read_op_b)
print("MLA result     :", data_out)
print("Expected result:", c.flatten().tolist())
if numpy.array_equal(c.flatten(), data_out):
    print("SUCCESS!")
else:
    print("FAIL!")
