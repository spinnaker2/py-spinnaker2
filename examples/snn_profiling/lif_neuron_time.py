import json

import matplotlib.pyplot as plt

# enable profiling for LIFApplication class
import spinnaker2.neuron_models.lif_neuron
from numpy.polynomial import Polynomial
from spinnaker2 import hardware, snn
from spinnaker2.helpers import read_log

spinnaker2.neuron_models.lif_neuron.LIFApplication.profiling = True


def run_model(n_pre, n_post, timesteps):
    neuron_params = {
        "threshold": 100.0,
        "alpha_decay": 0.9,
    }

    # create as many input spikes as timestep, e.g., 0 spikes in t=0, 1 spike
    # in t=1, etc.
    input_spikes = {}
    for i in range(n_pre):
        input_spikes[i] = range(n_pre - i, n_pre)
    print(input_spikes)

    stim = snn.Population(size=n_pre, neuron_model="spike_list", params=input_spikes, name="stim")

    pop1 = snn.Population(size=n_post, neuron_model="lif", params=neuron_params, name="pop1")

    w = 2.0  # weight
    d = 1  # delay
    conns = []
    for i in range(n_pre):
        for j in range(n_post):
            conns.append([i, j, w, d])

    proj = snn.Projection(pre=stim, post=pop1, connections=conns)

    net = snn.Network("my network")
    net.add(stim, pop1, proj)

    hw = hardware.SpiNNaker2Chip()
    hw.run(net, timesteps, sys_tick_in_s=10e-3, mapping_only=False, jtag_id=0)

    spikes_per_timestep = [0 for i in range(timesteps)]
    for spike_train in input_spikes.values():
        print(spike_train)
        for time in spike_train:
            spikes_per_timestep[time] += 1

    with open("results.json", "r") as f:
        results = json.load(f)
        # log_raw = results["PE_5_log"]
        log_raw = results["PE_37_log"]
        log_string = read_log(log_raw)
        log_string = log_string.decode("ASCII")
        t_spikes_all = []
        for line in log_string.split("\n"):
            if line.startswith("Profiling"):
                print(line)
                times = line.split(":")[1].split(",")
                times = [int(t) for t in times]
                print(times)
                t_spikes = times[0] - times[1]
                t_synapses = times[1] - times[2]
                t_neurons = times[2] - times[3]
                print("t_spikes:", t_spikes)
                print("t_synapses:", t_synapses)
                print("t_neurons:", t_neurons)
                t_spikes_all.append(t_spikes)
        plt.plot(t_spikes_all)
        print("Spikes per timestep", spikes_per_timestep)
        syn_events_per_timestep = [s * n_post for s in spikes_per_timestep]
        p_fitted = Polynomial.fit(syn_events_per_timestep, t_spikes_all, 1)
        p_fitted = p_fitted.convert()
        print(p_fitted)
        plt.show()
        # 38 timer clks per synaptic event


def sweep_1():
    # max number of pre neurons with 250 post neurons
    n_post = 250
    n_pre = 10
    timesteps = n_pre + 1
    run_model(n_pre, n_post, timesteps)


if __name__ == "__main__":
    sweep_1()
