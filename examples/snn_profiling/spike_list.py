from spinnaker2 import hardware, snn


def run_model(spikes_per_timestep, timesteps):

    n_pre = spikes_per_timestep
    spike_times = dict()
    all_timesteps = list(range(timesteps))
    for i in range(spikes_per_timestep):
        spike_times[i] = all_timesteps

    stim = snn.Population(size=n_pre, neuron_model="spike_list", params=spike_times, name="stim")

    net = snn.Network("my network")
    net.add(stim)

    hw = hardware.SpiNNaker2Chip()
    hw.run(net, timesteps, mapping_only=True)


def sweep_best_case():
    # sweep number of spike per timestep at fixed nr of timesteps
    timesteps = 100
    best = 0
    for spikes_per_timestep in range(1, 251, 1):
        try:
            run_model(spikes_per_timestep, timesteps)
            best = spikes_per_timestep
        except AssertionError:
            print(f"Could not map network with {spikes_per_timestep} spikes and {timesteps} timesteps")
            break
    print(best)
    max_spikes = best * timesteps
    return max_spikes


def sweep_worst_case():
    # 1 spike per timestep, sweep timesteps
    best = 0
    spikes_per_timestep = 1
    for timesteps in range(0, 10000, 10):
        try:
            run_model(spikes_per_timestep, timesteps)
            best = timesteps
        except AssertionError:
            print(f"Could not map network with {spikes_per_timestep} spikes and {timesteps} timesteps")
            break
    print(best)
    max_spikes = best * spikes_per_timestep
    return max_spikes


max_spikes_bc = sweep_best_case()
max_spikes_wc = sweep_worst_case()
print("Max spikes best case:", max_spikes_bc)
print("Max spikes worst case:", max_spikes_wc)
