"""
Example of 'time done' recording for LIF neuron
"""

import matplotlib.pyplot as plt
from spinnaker2 import hardware, helpers, snn

neuron_params = {
    "threshold": 1.0,
    "alpha_decay": 0.9,
    "i_offset": 0.0,
}

stim = snn.Population(
    size=10,
    neuron_model="spike_list",
    record=["time_done"],
    params={0: [1, 2, 3], 5: [20, 30]},
    name="stim",
)

pop1 = snn.Population(size=20, neuron_model="lif", params=neuron_params, name="pop1", record=["spikes", "time_done"])

w = 2.0  # weight
d = 1  # delay
conns = []
conns.append([0, 1, w, d])
conns.append([0, 0, -w, d])
conns.append([1, 3, w, d])
conns.append([1, 4, w, d])
conns.append([3, 5, -w, d])

proj = snn.Projection(pre=stim, post=pop1, connections=conns)

net = snn.Network("my network")
net.add(stim, pop1, proj)

hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.72")  # use ethernet
timesteps = 50
hw.run(net, timesteps)

# get results and plot

spike_times = pop1.get_spikes()
print(spike_times)

indices, times = helpers.spike_times_dict_to_arrays(spike_times)
plt.plot(times, indices, ".")
plt.xlim(0, timesteps)
plt.ylim(0, pop1.size)
plt.xlabel("time step")
plt.ylabel("neuron")
plt.show()

# plot time done
time_done_times = pop1.get_time_done_times()
helpers.save_dict_to_npz(time_done_times, "time_done_times")
helpers.plot_times_done_multiple_pes_one_plot_horizontal("time_done_times.npz")
helpers.plot_times_done_multiple_pes_one_plot_vertical("time_done_times.npz")
