from spinnaker2 import hardware, snn


def run_model(n_pre, n_post, timesteps):
    neuron_params = {
        "threshold": 100.0,
        "alpha_decay": 0.9,
    }

    stim = snn.Population(size=n_pre, neuron_model="spike_list", params={0: [1, 2, 3], 5: [20, 30]}, name="stim")

    pop1 = snn.Population(size=n_post, neuron_model="lif", params=neuron_params, name="pop1")

    w = 2.0  # weight
    d = 1  # delay
    conns = []
    for i in range(n_pre):
        for j in range(n_post):
            conns.append([i, j, w, d])

    proj = snn.Projection(pre=stim, post=pop1, connections=conns)

    net = snn.Network("my network")
    net.add(stim, pop1, proj)

    hw = hardware.SpiNNaker2Chip()
    hw.run(net, timesteps, mapping_only=True)


def sweep_1():
    # max number of pre neurons with 250 post neurons
    n_post = 250
    timesteps = 0
    best_n_pre = 0
    for n_pre in range(1, 251, 1):
        try:
            run_model(n_pre, n_post, timesteps)
            best_n_pre = n_pre
        except MemoryError:
            print(f"Could not map network with {n_pre} pre neurons")
            break
    print(best_n_pre)
    max_synapses = best_n_pre * n_post
    return max_synapses


def sweep_2():
    # max number of pre neurons with 1 post neuron
    n_post = 1
    timesteps = 1
    best_n_pre = 0
    for n_pre in range(100, 2500, 10):
        try:
            run_model(n_pre, n_post, timesteps)
            best_n_pre = n_pre
        except MemoryError:
            print(f"Could not map network with {n_pre} pre neurons")
            break
    print(best_n_pre)
    max_synapses = best_n_pre * n_post
    return max_synapses


max_synapses_1 = sweep_1()
max_synapses_2 = sweep_2()
print("Max synapses 1:", max_synapses_1)
print("Max synapses 2:", max_synapses_2)
