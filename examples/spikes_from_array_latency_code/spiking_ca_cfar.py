"""
Example for spikes_from_array_latency_code, here for computing the
cell-averaging constant false alarm rate algorithm from radar processing with
spiking neurons, short: spiking CA-CFAR.

The spikes_from_array_latency_code model creates one spike per element of an
array using the following latency code:
    t = t_max*(x_max-x)/x_max
where `x` is the array value and `t_max` and `x_max` are parameters of the
neuron model (global per population). Spike times are binned to time steps.

The CA-CFAR algorithm uses a sliding window approach to compare whether the
value of a cell under test (CUT) is significantly higher than an average of
surrounding cells. This is e.g. used to detect whether a signal in Radar
range-Doppler map originates from the reflection of a real object or is just
noise.

The CFAR condition is true if:
    x_cut > alpha * P_noise,
where P_noise is a noise estimate obtained by the average the surrounding
cells, also called training cells. As the direct neighbor cells might contain
reflections from the same real object, the direct neighbors are commonly
ignored (also called guard cells).

In 1 D, this looks as follows for the CFAR condiction of x6:
    x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12
    T  T  T  T  T  G  CUT G  T  T  T   T   T

The spiking version uses latency coding to turn the x values to spike times and
uses fixed weights for the connections from the training cells and the CUT to
an output neuron:
    w_train : -alpha/N, where N is the number of train cells
    w_cut : 1

A non-leaky, non-spiking neuron is used to accumulate the weighted input.
The CFAR condition is fulfilled if the membrane voltage of the neuron is
positive at the end of the simulation (t_max).

See https://doi.org/10.3389/fnins.2022.851774 for further details.
"""

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, snn

rng = np.random.default_rng()

#################
# CFAR settings #
#################
num_train_side = 5
num_guard_side = 1
num_train = 2 * num_train_side
num_guard = 2 * num_guard_side
alpha = 5.0

######################
# generate test data #
######################
n_stim = num_guard + num_train + 1
rand_limit_high = 2**10
input_data = rng.integers(rand_limit_high, size=n_stim, dtype=np.uint16)
scale_CUT = 5
input_data[num_guard_side + num_train_side] = input_data.mean() * scale_CUT

##################
# Classical CFAR #
##################
p_noise = (np.sum(input_data[:num_train_side]) + np.sum(input_data[-num_train_side:])) / num_train
CFAR_result = False
if input_data[num_guard_side + num_train_side] > p_noise * alpha:
    CFAR_result = True

################
# Spiking CFAR #
################
weight_scale = 126  # chosen such that ratio of w_train and w_cut is an integer
w_train = -alpha / num_train * weight_scale
w_cut = 1 * weight_scale
delay = 0  # delay
timesteps = 500

stim_params = {
    "t_max": timesteps,
    "x_max": int(np.min([int(rand_limit_high * scale_CUT), 2**16 - 1])),
    # "x_max": int(input_data.max()),
    "data": input_data,  # uint16
}

neuron_params = {
    "threshold": 1.0e6,
    "alpha_decay": 1.0,
    "i_offset": 0.0,
    "v_reset": 0.0,
    "reset": "reset_to_v_reset",
    "exc_decay": 1.0,
    "inh_decay": 1.0,
    "t_refrac": 0,
}
stim = snn.Population(n_stim, "spikes_from_array_latency_code", params=stim_params, name="stim")
pop1 = snn.Population(1, "lif_curr_exp_no_delay", params=neuron_params, name="rcvr", record=["spikes", "v"])

conns = []
# training cells
for i in range(num_train_side):
    conns.append([i, 0, w_train, delay])
    conns.append([n_stim - i - 1, 0, w_train, delay])
# cell under test
conns.append([num_train_side + num_guard_side, 0, w_cut, delay])

proj = snn.Projection(pre=stim, post=pop1, connections=conns)

net = snn.Network("CFAR network")
net.add(stim, pop1, proj)

hw = hardware.SpiNNaker2Chip(eth_ip="192.168.2.33")
# hw = hardware.FPGA_Rev2()
hw.run(net, timesteps)

# get results and plot
voltages = pop1.get_voltages()

# the output of the spiking CFAR is true, if the voltage in the last timestep
# larger than 0.
spiking_CFAR_result = voltages[0][-1] > 0.0

times = np.arange(timesteps)
plt.plot(times, voltages[0], label="Neuron 0")
plt.xlim(0, timesteps)
plt.xlabel("time step")
plt.ylabel("voltage")
plt.legend()
plt.show()

print("CFAR_result:", CFAR_result)
print("Spiking CFAR_result:", spiking_CFAR_result)

if CFAR_result == spiking_CFAR_result:
    print("SUCCESS!")
else:
    print("FAIL!")
