import json
import os

import spinnaker2.hardware
from spinnaker2.configuration import Experiment, ExperimentConfig, MemoryRegion, PEConfig
from spinnaker2.coordinates import PE, ByteAddr, WordAddr

"""
Simple example that takes an input value, sends it to a PE, where it is
multiplied by a constant depending on the PE-ID.

The result is read out after the experiment and compared to the expected value.
"""

input_value = 12

pe = PE(1, 1, 0)
memfile = "../../../s2-sim2lab-app/chip/app-pe/s2app/host_pe_interaction/binaries/s2app_arm.mem"

pe_config = PEConfig(pe, "host_pe_interaction", memfile)
pe_config.add_mem_region_to_read("result", MemoryRegion(ByteAddr(0x10000).to_WordAddr(), 1))
pe_config.add_mem_data_to_send(ByteAddr(0x18000).to_WordAddr(), [input_value])

exp_config = ExperimentConfig(runtime_in_s=1.0)
exp_config.add(pe_config)
exp_config.dump_spec("spec.json")

# Run experiment
experiment = Experiment(exp_config, spinnaker2.hardware._experiment_app_path_s2_chip())
cmd_opts = ["-e", "192.168.1.59"]
experiment.run(cmd_opts)


multiplier = pe.global_id()
print("Multiplier:", multiplier)
expected = input_value * multiplier

my_result = experiment.get_result(pe, "result")  # returns list of uint32
actual = my_result[0]

print("Expected Result:", expected)
print("Actual Result:", actual)

if actual == expected:
    print("SUCCESS!")
else:
    print("FAIL!")
