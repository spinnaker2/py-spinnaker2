import json
import os
import subprocess

from spinnaker2.helpers import global_word_address

"""
Simple example that takes an input value, sends it to a PE, where it is
multiplied by a constant depending on the PE-ID.

The result is read out after the experiment and compared to the expected value.
"""


def global_pe_id(quad_x, quad_y, pe):
    """calculate global PE ID on SpiNNaker2 chip"""
    global_id = pe & 0x3
    global_id |= (quad_y & 0x7) << 2
    global_id |= (quad_x & 0x7) << 5
    return global_id


input_value = 10

APPNAME = "../../../s2-sim2lab-app/host/experiment/app/experiment"

PE = (1, 1, 0)
memfile = "../../../s2-sim2lab-app/chip/app-pe/s2app/host_pe_interaction/binaries/s2app_arm.mem"

exp_spec = {
    "active_pes": [PE],
    "mem_files": [memfile],
    "duration_in_s": 1,
    "mem_regions_to_read": {"result": [global_word_address(PE[0], PE[1], PE[2], 0x10000), 1]},
    "mem_data_to_send": [[global_word_address(PE[0], PE[1], PE[2], 0x18000), [input_value]]],
}

cmd_opts = {
    "eth_ip": "192.168.1.1",  # use eth instead of jtag
    "stm_tty": "/dev/ttyACM0",
    "verbose": False,
    "reset_eth_phy": False,
}

print(exp_spec)
with open("spec.json", "w") as f:
    json.dump(exp_spec, f, indent=2)

# Run experiment
cmd = [APPNAME]
if "eth_ip" in cmd_opts:
    cmd = cmd + ["-e" + cmd_opts["eth_ip"]]
if "stm_tty" in cmd_opts:
    cmd = cmd + ["-S" + cmd_opts["stm_tty"]]
if cmd_opts.get("verbose", False):
    cmd = cmd + ["-v"]
if cmd_opts.get("reset_eth_phy", False):
    cmd = cmd + ["-r"]
subprocess.run(cmd)  # noqa: S603

results = []

with open("results.json", "r") as f:
    results = json.load(f)

multiplier = global_pe_id(*PE)
print("Multiplier:", multiplier)
expected = input_value * multiplier
actual = results["result"][0]
print("Expected Result:", expected)
print("Actual Result:", actual)

if actual == expected:
    print("SUCCESS!")
else:
    print("FAIL!")
