"""
Example for fast running multiple experiments with same snn.Network with
changed input spikes.

Compared to `lif_transfer_curve_multi_run.py`, here the iterations are executed
with a hardware reset of the network and changed input spikes -- instead of
running the full experiment including mapping in each iteration.
"""

import json
import subprocess
import time

import matplotlib.pyplot as plt
import numpy as np
import spinnaker2.coordinates as coord
from spinnaker2 import hardware, mapper, snn
from spinnaker2.configuration import ExperimentResult
from spinnaker2.helpers import global_word_address
from spinnaker2.neuron_models.spike_list import get_input_spike_rows

# track execution time excluding plotting
t_start = time.time()

neuron_params = {
    "threshold": 1.5,
    "alpha_decay": 0.9,
    "i_offset": 0.0,
}

stim = snn.Population(size=1, neuron_model="spike_list", params={}, name="stim")

pop1 = snn.Population(size=1, neuron_model="lif", params=neuron_params, name="pop1", record=["spikes"])

w = 1.0  # weight
d = 0  # delay
conns = []
conns.append([0, 0, w, d])

proj = snn.Projection(pre=stim, post=pop1, connections=conns)

net = snn.Network("my network")
net.add(stim, pop1, proj)

hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.59")

timesteps = 200

# run mapping and extract config
debug = False
my_mapper = mapper.Mapper(net, hw)
exp_config, sim_cfg = my_mapper.map_and_generate_experiment_config(timesteps, debug=debug)

multi_run_app_path = "/home/vogginger/project/software/s2-sim2lab-app/host/units/s2_top/tc_s2app_s2_ctrl_experiment_json/binaries/testcase"  # noqa: E501

spec_file = "spec1.json"
exp_config.dump_spec(spec_file)
input_data_file = "input1.json"
cmd = [multi_run_app_path, "-s", spec_file, "-i", input_data_file]
input_spikes_addr = coord.ByteAddr(36976)  # ByteAddr, manually extracted using print
pe = coord.PE(1, 1, 0)  # S2 Chip

# multiple runs with different input spike frequency
inter_spike_intervals = [20, 16, 12, 8, 4]
output_spike_counts = []

# generate input data for all runs
json_like_obj = []
for i, isi in enumerate(inter_spike_intervals):
    # generate spike trains
    spike_times_dict = {0: np.arange(0, timesteps, isi, dtype=np.uint32)}
    spike_times_list = [[] for i in range(stim.size)]

    # extract spike sources for this PE
    for idx in spike_times_dict:
        spike_times_list[idx] = np.array(spike_times_dict[idx]).astype(np.uint32)
    spike_times_raw = get_input_spike_rows(spike_times_list, my_mapper.key_offsets[pe])

    # TODO: check that input_spikes_addr does not overlap with log_info

    json_like_obj.append(
        {"mem_data_to_send": [[global_word_address(pe.x, pe.y, pe.pe, int(input_spikes_addr)), spike_times_raw]]}
    )

with open(input_data_file, "w") as f:
    json.dump(json_like_obj, f, indent=2)

subprocess.run(cmd, check=True)  # noqa: S603

# load and process results of all runs
for i, isi in enumerate(inter_spike_intervals):
    experiment = ExperimentResult()
    result_file = f"results_{i}.json"
    with open(result_file, "r") as f:
        experiment.results = json.load(f)

    net.reset()

    hardware.process_results(experiment, my_mapper, sim_cfg, debug=debug)
    spike_times = pop1.get_spikes()
    print(spike_times)
    output_spike_counts.append(len(spike_times[0]))

t_end = time.time()
duration = t_end - t_start
print(f"Duration: {duration:.3f} seconds")

# plot transfer function
input_rates = [1.0 / isi for isi in inter_spike_intervals]
output_rates = [count / timesteps for count in output_spike_counts]

plt.plot(input_rates, output_rates)
plt.xlabel("input rate [spikes per timestep]")
plt.ylabel("output rate [spikes per timestep]")
plt.show()
