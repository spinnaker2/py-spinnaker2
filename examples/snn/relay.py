"""
Example of relay neuron.
"""

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, helpers, snn

# create stimulus
input_spikes = {0: [1, 17]}

stim = snn.Population(size=1, neuron_model="spike_list", params=input_spikes, name="stim")

# Create relay neuron
# Most of these parameters are the same as default.
neuron_params = {"delay": 10}

pop1 = snn.Population(size=1, neuron_model="relay", params=neuron_params, name="pop1", record=["spikes"])

# Create a sink neuron
neuron_params = {"Vspike": 1.0}

pop2 = snn.Population(size=1, neuron_model="lif_fugu", params=neuron_params, name="pop2", record=["spikes"])

# Make connections
# The relay neuron ignores both weight and delay.
# Instead, it always fires when it receives a spike, and imposes delay
# (set in neuron parameters) on the outgoing spike.
conns = []
conns.append([0, 0, 0, 0])
proj1 = snn.Projection(pre=stim, post=pop1, connections=conns)

conns = []
conns.append([0, 0, 10, 0])
proj2 = snn.Projection(pre=pop1, post=pop2, connections=conns)

# create a network and add population and projections
net = snn.Network("my network")
net.add(stim, pop1, pop2, proj1, proj2)

# select hardware and run network
hw = hardware.SpiNNaker2Chip(eth_ip="192.168.3.24")
timesteps = 30  # Sufficient for delay 17 to show up.
hw.run(net, timesteps)

# get results and plot

# get_spikes() returns a dictionary with:
#  - keys: neuron indices
#  - values: lists of spike times per neurons
spike_times = pop2.get_spikes()
print("spikes:  ", spike_times)

fig, (ax1, ax3) = plt.subplots(2, 1, sharex=True)

indices, times = helpers.spike_times_dict_to_arrays(input_spikes)
ax1.plot(times, indices, "|", ms=20)
ax1.set_ylabel("input spikes")
ax1.set_ylim((-0.5, stim.size - 0.5))

indices, times = helpers.spike_times_dict_to_arrays(spike_times)
ax3.plot(times, indices, "|", ms=20)
ax3.set_ylabel("output spikes")
ax3.set_xlabel("time step")
ax3.set_ylim((-0.5, pop1.size - 0.5))
fig.suptitle("lif_neuron")
plt.show()
