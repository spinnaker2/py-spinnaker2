"""
Example for running multiple experiments with same snn.Network using reset
mechanism.
"""

import time

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, snn

# track execution time excluding plotting
t_start = time.time()

neuron_params = {
    "threshold": 1.5,
    "alpha_decay": 0.9,
    "i_offset": 0.0,
}

stim = snn.Population(size=1, neuron_model="spike_list", params={}, name="stim")

pop1 = snn.Population(size=1, neuron_model="lif", params=neuron_params, name="pop1", record=["spikes"])

w = 1.0  # weight
d = 0  # delay
conns = []
conns.append([0, 0, w, d])

proj = snn.Projection(pre=stim, post=pop1, connections=conns)

net = snn.Network("my network")
net.add(stim, pop1, proj)

# multiple runs with different input spike frequency
inter_spike_intervals = [20, 16, 12, 8, 4]
timesteps = 200
output_spike_counts = []

for i, isi in enumerate(inter_spike_intervals):
    net.reset()
    # generate spike trains
    stim.params = {0: np.arange(0, timesteps, isi, dtype=np.uint32)}
    hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.59")
    hw.run(net, timesteps, debug=False)
    spike_times = pop1.get_spikes()
    print(spike_times)
    output_spike_counts.append(len(spike_times[0]))
    del hw  # make sure there is no data left

t_end = time.time()
duration = t_end - t_start
print(f"Duration: {duration:.3f} seconds")

# plot transfer function
input_rates = [1.0 / isi for isi in inter_spike_intervals]
output_rates = [count / timesteps for count in output_spike_counts]

plt.plot(input_rates, output_rates)
plt.xlabel("input rate [spikes per timestep]")
plt.ylabel("output rate [spikes per timestep]")
plt.show()
