"""
Example for LIF neuron with exponentially decaying current input
"""

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, helpers, snn

# create stimulus population with 1 spike source
input_spikes = {0: [3, 10, 20, 22, 24, 26, 28]}
stim = snn.Population(size=1, neuron_model="spike_list", params=input_spikes, name="stim")

# create lif_curr_exp population with 2 neuron
neuron_params = {
    "threshold": 10.0,
    "alpha_decay": 0.9,
    "i_offset": 0.0,
    "v_init": 0.0,
    "v_reset": -0.2,  # only used for reset="reset_to_v_reset"
    "reset": "reset_by_subtraction",  # "reset_by_subtraction" or "reset_to_v_reset"
    "exc_decay": 0.5,
    "inh_decay": 0.2,
    "t_refrac": 0,
}

pop1 = snn.Population(size=2, neuron_model="lif_curr_exp", params=neuron_params, name="pop1", record=["spikes", "v"])

# create connection between stimulus neuron and lif_curr_exp neuron
# each connection has 4 entries: [pre_index, post_index, weight, delay]
# for connections to a `lif_curr_exp` population:
#  - weight: integer in range [-15, 15]
#  - delay: integer in range [0, 7]. Actual delay on the hardware is: delay+1
conns = [
    [0, 0, 2, 1],  # excitatory synapse with weight 2 and delay 1
    [0, 1, -3, 2],  # inhibitory synapse with weight -3 and delay 2
]

proj = snn.Projection(stim, pop1, conns)

# create a network and add population and projections
net = snn.Network("my network")
net.add(stim, pop1, proj)

# select hardware and run network
hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.72")
timesteps = 50
hw.run(net, timesteps)

# get results and plot

# get_spikes() returns a dictionary with:
#  - keys: neuron indices
#  - values: lists of spike times per neurons
spike_times = pop1.get_spikes()

# get_voltages() returns a dictionary with:
#  - keys: neuron indices
#  - values: numpy arrays with 1 float value per timestep per neuron
voltages = pop1.get_voltages()

fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True, height_ratios=(1, 2, 1))

indices, times = helpers.spike_times_dict_to_arrays(input_spikes)
ax1.plot(times, indices, "|", ms=20)
ax1.set_ylabel("input spikes")
ax1.set_ylim((-0.5, stim.size - 0.5))

voltages = pop1.get_voltages()
times = np.arange(timesteps)
ax2.plot(times, voltages[0], label="Neuron 0")
ax2.plot(times, voltages[1], label="Neuron 1")
ax2.axhline(neuron_params["threshold"], ls="--", c="0.5", label="threshold")
ax2.axhline(0, ls="-", c="0.8", zorder=0)
ax2.set_xlim(0, timesteps)
ax2.set_ylabel("voltage")
ax2.legend()

indices, times = helpers.spike_times_dict_to_arrays(spike_times)
ax3.plot(times, indices, "|", ms=20)
ax3.set_ylabel("output spikes")
ax3.set_xlabel("time step")
ax3.set_ylim((-0.5, pop1.size - 0.5))
fig.suptitle("lif_curr_exp")
plt.show()
