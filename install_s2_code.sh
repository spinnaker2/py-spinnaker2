#!/bin/bash

cd ..
git clone git@gitlab.com:spinnaker2/s2-sim2lab-app.git

cd s2-sim2lab-app
make

cd host/experiment/app/
cmake CMakeLists.txt
make
cd -

cd host/experiment/
./make_all_apps.py 
