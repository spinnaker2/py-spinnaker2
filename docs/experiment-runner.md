# Experiment Runner

The SpiNNaker2 experiment runner provides an abstraction for running batch
experiments on the SpiNNaker2 chip. For now, only the single-chip board and the
FPGA prototype are supported.

1. **Experiment Spec `spec.json`** Each experiment is described by so-called _experiment specification_. The experiment is defined as a JSON file called `spec.json`. The experiment spec contains information about the used processing elements, the corresponding mem-files, input data, and the memory regions that shall be read after an experiment. It also allows to define how long the experiment runs or whether to trigger a synchronous start of all PEs via a feedthrough interrupt.
2. **Experiment execution:** A C++ executable `experiment` loads the experiment specification, runs the experiment on the chip, reads the predefined memory blocks and saves them in result file.
3. **Experiment results `results.json`:** contains the raw memory content of the memory regions. Each 32 bit word in memory is represented by a 32-bit unsigned integer. 

## Installation

The source code is located under directory `host-software/rev2/experiment`.

To compile the executable, `cmake` is required. Minimum version: 3.10.

To compile the app:

1. `cd app`
2. `cmake .`
3. `make`

This compile a C++ program to an executable `experiment`.

There is currently no installation to a central location.

Installation has been tested on Ubuntu and Gentoo.

# Experiment execution

To run the app:

`./experiment`

The program expects a file `spec.json` in the folder where the program is executed. **TODO: describe command line arguments and how to specify IP address.** After the experiment, the program writes the results to a file `results.json`.

## Experiment specification
Example `spec.json`:
```json
{
  "active_pes": [[0, 1, 0]],
  "mem_files": ["/home/vogginger/project/spinnaker2/qpe-software-fixed/pe-software/quadpe-testcases/fpga_4qpe.tc_host_pe_interaction/binaries/arm.mem"],
  "duration_in_s": 1,
  "mem_regions_to_read": {
    "result": [1006780416, 1]
  },
  "mem_data_to_send": [[1006788608, [10]]]
}
```

**Description:**

- `active_pes`: list of active PEs. Each PE is described by a list of 3 integers: [`QPE_X`, `QPE_Y`, `PE_ID`]
- `mem_files`: list of mem-files for the PEs. Must have the same size as `active_pes`, i.e., for each active PE one mem-file is needed.
- `duration_in_s`: Experiment duration in seconds. After starting the PEs, the host waits for `duration_in_s` seconds until it starts to read the results.
- `mem_regions_to_read`: JSON-object with key-value pairs defining memory regions to be read after the experiment.
    - `key`: name tag for memory region to read. Need to be unique in the spec.
    - `value`: array with 2 elements. 
        1. _start memory address (word address)_
        2. _number of words_ to read
- `mem_data_to_send`: list of memory data blocks to send before the experiment starts. Each entry consists of 2 elements:
    1. _start memory address (word address)_
    2. list of 32-bit words represented by unsigned integers
