# spinnaker2.configuration

Example:

```py
from spinnaker2.configuration import PEConfig, ExperimentConfig, MemoryRegion, Experiment
from spinnaker2.coordinates import ByteAddr, WordAddr, PE
import spinnaker2.hardware

pe = PE(1,1,0)
pe_cfg = PEConfig(pe, "test_app", "path/to/test/binaries/s2app_arm.mem")
pe_cfg.add_mem_data_to_send(ByteAddr(0x10000).to_WordAddr(), [10,20,30])
pe_cfg.add_mem_region_to_read("my_result", MemoryRegion(ByteAddr(0x10000).to_WordAddr(), 1))

exp_config = ExperimentConfig(duration_in_s=1.0)
exp_config.add(pe_config)

experiment = Experiment(exp_config, spinnaker2.hardware._experiment_app_path_s2_chip())
cmd_opts = ['-e', "192.168.1.59"]
experiment.run(cmd_opts)

my_result = experiment.get_result(pe, "my_result")
```

Classes:

::: spinnaker2.configuration
