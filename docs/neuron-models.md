# Neuron Models

[TOC]

## Leaky Integrate&Fire neuron ('lif')
| Topic | Description / value |
|-------|---------------------|
| name | `lif` |
| max neurons per core | 250 |
| equation | `v[t] = alpha_decay*v[t-1] + I_syn[t] + i_offset` where `v` is the membrane voltage and `I_syn[t]` the sum of received weights for time step `t` |
| parameter `reset` | `'reset_by_subtraction'` (default) or `'reset_to_v_reset'`, see details below|
| parameter `threshold` | spike threshold (float) |
| parameter `alpha_decay` | voltage decay factor (float), range: `0-1` |
| parameter `i_offset` | offset/bias current (float)|
| paramter  `v_init` | initial voltage (float)|
| parameter `v_reset` | reset voltage (float), only used for reset method "reset_to_v_reset"|
| recordables | `['spikes', 'v', 'v_last']` |
| delays | The range of delays for incoming synaptic connections is `0-7` (3 bits). A delay of `0` means that spikes are immediately processed in next time step. |
| weights | Incoming weights have 5 bits. weight range: `[-15,15]`|

### Reset mechanisms
If the voltage reaches the threshold (`v >= threshold`) after updating `v` in above equation, one of the following resets is applied in the same time step (before recording).
- `'reset_by_subtraction'`: `v = v - thresh`
- `'reset_to_v_reset'`: `v = v_reset`

## Leaky Integrate&Fire neuron without delays ('lif_no_delay')
Same model like **LIF** but with 8-bit weights and a fixed delay of 0.

| Topic | Description / value |
|-------|---------------------|
| name | `lif_no_delay` |
| delays | Fixed 0 delay for incoming synapses. Spikes are immediately processed in next time step. |
| weights | Incoming weights have 8 bits. weight range: `[-127,127]`|

## Leaky Integrate&Fire neuron without delays and 1024 neurons per core('lif_no_delay_1024')
Same model like **LIF** but with 5-bit weights, fixed delay of 0, and up to 1024 neurons per core

| Topic                | Description / value                                                                      |
|----------------------|------------------------------------------------------------------------------------------|
| name                 | `lif_no_delay_1024`                                                                      |
| max neurons per core | 1024                                                                                     |
| delays               | Fixed 0 delay for incoming synapses. Spikes are immediately processed in next time step. |
| weights              | Incoming weights have 8 bits. weight range: `[-31,31]`                                   |


## Leaky Integrate&Fire neuron exponentially decaying current input ('lif_curr_exp')
### Equation
```python
I_syn_exc[t] = exc_decay*I_syn_exc[t-1] + accumulated excitatory weights
I_syn_inh[t] = inh_decay*I_syn_inh[t-1] + accumulated inhibitory weights
v[t] = alpha_decay*v[t-1] + I_syn_exc[t] - I_syn_inh[t] + i_offset
```

| Topic | Description / value |
|-------|---------------------|
| name | `lif_curr_exp` |
| max neurons per core | 250 |
| equation | `v[t] = alpha_decay*v[t-1] + I_syn_exc[t] - I_syn_inh[t] + i_offset` where `v` is the membrane voltage and `I_syn_exc` and `I_syn_inh` are the excitatory and inhibitory synaptic input currents.|
| parameter `reset` | `'reset_by_subtraction'` (default) or `'reset_to_v_reset'`, see details below|
| parameter `threshold` | spike threshold (float) |
| parameter `alpha_decay` | voltage decay factor (float), range: `0-1` |
| parameter `i_offset` | offset/bias current (float)|
| paramter  `v_init` | initial voltage (float)|
| parameter `v_reset` | reset voltage (float), only used for reset method "reset_to_v_reset"|
| parameter `exc_decay` | decay factor of excitatory input current (float), range: `0-1`|
| parameter `inh_decay` | decay factor of inhibitory input current (float), range: `0-1`|
| parameter `t_refrac` | refractory time (unsigned int), range: `0 - 4,294,967,295`|
| recordables | `['spikes', 'v', 'v_last']` |
| delays | The range of delays for incoming synaptic connections is `0-7` (3 bits). A delay of `0` means that spikes are immediately processed in next time step. |
| weights | Incoming weights have 5 bits. weight range: `[-15,15]`, one bit is used for the sign (excitatory or inhibitory)|

### Reset mechanisms
If the voltage reaches the threshold (`v >= threshold`) after updating `v` in above equation, one of the following resets is applied in the same time step (before recording).
- `'reset_by_subtraction'`: `v = v - thresh`
- `'reset_to_v_reset'`: `v = v_reset`

## Leaky Integrate&Fire neuron exponentially decaying current input without delays ('lif_curr_exp_no_delay')
Same model like **lif_curr_exp** but with 8-bit weights and a fixed delay of 0.

| Topic | Description / value |
|-------|---------------------|
| name | `lif_no_delay` |
| delays | Fixed 0 delay for incoming synapses. Spikes are immediately processed in next time step. |
| weights | Incoming weights have 8 bits. weight range: `[-127,127]`|

## Spike list ('spike_list')
Spike sources with pre-defined spike times. Spike packets are stored in SRAM of PE before start of simulations and released at defined time step.

| Topic | Description / value |
|-------|---------------------|
| name                 | `spike_list` |
| max neurons per core | 500 (limit can be increased) |
| params | `dict` with keys (`int`) representing the neuron id within in the population and values being a list of spike times. Spike times are multiples of the time step and should be unsigned integers. |
| max input spikes per core |  worst case: 4600, best case: 23000 |
| recordables | none |

## Spikes from array values according to latency code ('spikes_from_array_latency_code')
This neuron model generates input spikes from values of an array according to a latency code. Each element of the array represents on spike source and each spike source can spike at maximum once.
The spike time `t` for array value `x` is computed according to:
```python
t = t_max*(x_max-x)/x_max
```
The `data` array as well as the parameters `x_max` and `t_max` are transmitted to the SpiNNaker2 core and the translation of array values to spikes is performed on the hardware.

| Topic | Description / value |
|-------|---------------------|
| name                 | `spikes_from_array_latency_code` |
| max neurons per core | 4096 (limit can be increased) |
| equation | `t = t_max*(x_max-x)/x_max`
| t_max | maximum time step to consider for conversion |
| x_max | maximum value to consider for conversion |
| data  | input array (`numpy.array` with dtype `numpy.uint16`), range: `0-65535` |
| recordables | none |

## Float list ('float_list')
Similar to spike list, but sends floating point numbers. Designed for the rf neuron application. Each time step it sends two floating point numbers, one real and one imag. The packets are stored in SRAM of PE before start of simulations and released at each time step.

| Topic | Description / value |
|-------|---------------------|
| name                 | `float_list` |
| max neurons per core | 1 |
| params | `dict` with keys (`int`) representing the neuron id (current only 1 neuron per core) and values being a list of complex numbers. |
| recordables | none |


## Convolutional input layer with rate coded integrate-and-fire neurons ('conv2d_if_neuron_rate_code')

| Topic | Description / value |
|-------|---------------------|
| name                 | `conv2d_if_neuron_rate_code` |
| max neurons per core | 1024 |
| To-do | To-do |

## Resonate&Fire neuron ('rfneuron')

### Equation
```python
v1 = neuron->v1*neuron->dc - neuron->v2*neuron->ds + exc_input
v2 = neuron->v1*neuron->ds + neuron->v2*neuron->dc
```
where dc and ds are decay constants, exc_input is the excitatory/real input, v1 and v2 are the real and imag. voltages

| Topic | Description / value |
|-------|---------------------|
| name | `rfneuron` |
| max neurons per core | 250 |
| parameter `threshold` | spike threshold (float) |
| parameter `freq` | frequency (float) |
| parameter `spike_prob` | spike probability (currently not supported) (float)|
| parameter `decay_const` | decay constant (float)|
| parameter `v1`, `v2`, `dc`, `ds`,| real and imag. voltage, cos and sin decay (float)|
| recordables | `['spikes', 'v']` |
| weights | Incoming weights have 5 bits. weight range: `[-15,15]`|

## Charge&Spike neuron ('charge_and_spike')

"Charge&Spike" neuron model from [Lopez-Randulfe et al. 2022](https://doi.org/10.1109/TC.2022.3162708) with a charging stage and a spiking stage. After `t_silent`, the neuron switches from the charging to the spiking stage. During the spiking stage, the neuron can only spike once.

### Equation
```python
if T < t_silent:
    I_syn[t] = I_syn[t-1] + input_this_timestep
    v[t] = v[t-1] + I_syn[t]
else:
    v[t] = v[t-1] + i_offset
```
where `v` is the membrane voltage and `input_this_timestep` refers to the accumulated weights in this time step.

| Topic | Description / value |
|-------|---------------------|
| name | `charge_and_spike` |
| max neurons per core | 250 |
| parameter `threshold` | spike threshold (float), only effective during spiking stage|
| parameter `i_offset` | offset/bias current (float) during spiking stage|
| paramter  `v_init` | initial voltage (float)|
| parameter `t_silent` | duration of silent stage (int, number of time steps) |
| recordables | `['spikes', 'v', 'v_last']` |
| delays | Fixed 0 delay for incoming synapses. Spikes are immediately processed in next time step. |
| weights | Incoming weights have 8 bits. weight range: `[-127,127]`|

## Relay neuron ('relay')

Intermediate neuron which overwrites delays of incoming and outgoing connections. Does not consider connection weights. If it receives an input spike, it relays it for `delay` time steps.

| Topic | Description / value |
|-------|---------------------|
| name | `relay` |
| max neurons per core | 250 |
| parameter `delay` | relay delay (int). Incoming spikes are immediately recorded and relayed after `delay` time steps |
| recordables | `['spikes', 'v']` |

## Fugu neuron ('lif_fugu')

A variant of LIF Neuron that supports setting initial voltages, probabilistic firing, and uses 7 bits for delays by default.
Parameter names deviate from classical py-spinnaker naming conventions, and are based on Fugu neurons developed at Sandia National Labs.

| Topic | Description / value | LIF equivalent |
|-------|---------------------|----------------|
| name | `lif_fugu` ||
| max neurons per core | 250 |
| equation | `v[t] = decay*v[t-1] + I_syn[t] + Vbias` where `v` is the membrane voltage and `I_syn[t]` the sum of received weights for time step `t` |
| parameter `decay` | voltage decay factor (float), range: `0-1` | `alpha_decay` |
| parameter `Vspike` | spike threshold (float) | `threshold` |
| parameter `Vbias` | offset/bias current (float)| `i_offset` |
| paramter  `V` | initial voltage (float)| `v_init` |
| parameter `Vreset` | reset voltage (float), default reset method | `v_reset` and `reset = 'reset_to_v_reset'` |
| paramter  `p` |  Probability of firing if membrane voltage crosses the spike threshold. Range: `[0,1]`.| |
| parameter `delay`| Number cycles before spike is received by downstream neuron. |`[...,d]` in connections|
| recordables | `['spikes', 'v']` |
| delays | LIF Fugu uses sending-side delays, see parameter above. A delay of `0` means that spikes are immediately processed in next time step. Up to 32 steps of sending-side delay. A shift buffer supports multiple spikes in flight. |
| weights | Incoming weights have 7 bits. weight range: `[-127, 127]`|
