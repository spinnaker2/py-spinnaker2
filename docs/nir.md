# NIR - Neuromorphic Intermediate Representation

py-spinnaker2 supports the import of models defined in the [Neuromorphic Intermediate Representation (NIR)](https://github.com/neuromorphs/NIR).
This allows to train SNN models in frameworks such as
[snnTorch](https://github.com/jeshraghian/snntorch),
[Norse](https://github.com/norse/norse),
[Sinabs](https://github.com/synsense/sinabs),
[Rockpool](https://github.com/synsense/rockpool),
[Nengo](https://nengo.ai/),
[Lava-DL](https://github.com/lava-nc/lava-dl) or
[Spyx](https://github.com/kmheckel/spyx/)
and deploy them on SpiNNaker2 chips via py-spinnaker2.


## Usage

### Load NIR graph

```python
import nir
from spinnaker2 import hardware, s2_nir

model_path = "snn_model.nir"
nir_graph = nir.read(model_path)
```

### Convert NIR graph into py-spinnaker2 network

```python
# Configuration for converting NIR graph to SpiNNaker2
conversion_cfg = s2_nir.ConversionConfig()
conversion_cfg.output_record = ["v", "spikes"]
conversion_cfg.dt = 0.0001
conversion_cfg.conn_delay = 0
conversion_cfg.scale_weights = True # Scale weights to dynamic range on chip
conversion_cfg.weight_scale_percentile = 100 # percentage for percentile-based weight scaling
conversion_cfg.reset = s2_nir.ResetMethod.ZERO # Reset voltage to zero at spike
conversion_cfg.integrator = s2_nir.IntegratorMethod.FORWARD # Euler-Forward

net, inp, outp = s2_nir.from_nir(nir_graph, conversion_cfg)
```

The returned objects in the last line are:

- `net`: a `spinnaker2.snn.Network` object
- `inp`: list of input populations of the network. Used later to set input spike times prior to network execution.
- `outp`: list of output populations of the network. Used to obtain spikes and voltages after network execution.

### Run on SpiNNaker2 chip

```python
# set input spike times
inp[0].params = spike_times

# run on hardware
hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.25")
hw.run(net, n_timesteps)

# obtain results
voltages = outp[0].get_voltages()
spikes = outp[0].get_spikes()
```

### Customizing your simulation

#### Use ConversionConfig

#### ::: spinnaker2.s2_nir.ConversionConfig

#### Adjust maximum number of neurons per core

- This can be done as described in [here for py-spinnaker2](snn-other.md#Adapt-number-of-neurons-per-core).
- Example for NIR: [See Jupyter notebook for spiking CNN](https://github.com/Jegp/NIR-demo-repo/blob/main/spinnaker2_demo/demo.ipynb)


## Supported NIR primitives

| Primitive | mapped to py-spinnaker2                               |
| --------- | ----------------------------------------------------- |
| IF        | `lif` with leakage disabled                           |
| LIF       | `lif`                                                 |
| CubaLIF   | `lif_curr_exp`                                        |
| Input     | `spike_list`                                          |
| Output    | enables recording on pre-node                         |
| Linear    | Projection                                            |
| Affine    | Projection + `i_offset` in target population for bias |
| Conv2d    | Conv2dProjection                                      |
| Flatten   | merged into Projection                                |
| SumPool2d | Projection or Conv2dProjection                        |


## Examples

### Examples from [NIR Paper](https://arxiv.org/abs/2311.14641)

1. LIF neuron stimulated with spikes:

    - [Script for running experiment on SpiNNaker2 chip](https://github.com/neuromorphs/NIR/blob/main/paper/01_lif/lif_spinnaker2.py)

2. Spiking convolutional neural network for neuromorphic MNIST:

    - [Jupyter notebook to run single samples on SpiNNaker2 chip](https://github.com/Jegp/NIR-demo-repo/blob/main/spinnaker2_demo/demo.ipynb)
    - [Script for running whole dataset on SpiNNaker2 chip](https://github.com/neuromorphs/NIR/blob/main/paper/02_cnn/s2_apply_onchip.py)
    - [Script for running whole dataset using py-spinnaker2's brian2 backend](https://github.com/neuromorphs/NIR/blob/main/paper/02_cnn/s2_apply.py)

3. Spiking recurrent neural network for braille letter reading
    - [Script for running whole dataset on SpiNNaker2 chip](https://github.com/neuromorphs/NIR/blob/main/paper/03_rnn/Braille_inference_spinnaker2.py)

### Dummy example in py-spinnaker2

Folder [examples/nir](https://gitlab.com/spinnaker2/py-spinnaker2/-/tree/main/examples/nir) contains two scripts:

- `generate_model.py` to generate a dummy NIR model and save it to file
- `nir_example.py` to load the NIR model from file and run it on a SpiNNaker2 chip


## Neuron model translation

This section provides the details about how the NIR primitives `nir.LIF` and
`nir.CubaLIF` are translated into py-spinnaker2 neuron parameters.

*The equations are best viewed on gitlab.com:* [nir.md](https://gitlab.com/spinnaker2/py-spinnaker2/-/blob/main/docs/nir.md?ref_type=heads#neuron-model-translation)

### LIF

The `nir.LIF` model has the following equations:

**NIR Differential Equation**
```math
\tau \dot{v} = (v_{leak} - v) + R i
```

**NIR Forward-Euler**
```math
v[t] = (1-\frac{dt}{\tau}) v[t-1] + \frac{dt}{\tau} v_{leak} + \frac{dt}{\tau} R i[t]
```

**SpiNNaker2 implementation (`lif` neuron)**
```
v[t] = alpha_decay * v[t-1] + I_syn[t] + i_offset
```

**Translation**

| parameter | Forward-Euler | Exponential-Euler |
|-----------|----------------------|--|
| `alpha_decay`| $1 - \frac{dt}{\tau}$| $\exp (-\frac{dt}{\tau})$|
| `threshold` | $v_{threshold} \frac{tau}{dt\cdot R}$ | $v_{threshold} \left ((1 - \exp (-\frac{dt}{\tau})) \cdot R \right)^{-1}$ |
| `i_offset` | $\frac{v_{leak}}{R}  + bias$| $\frac{v_{leak}}{R}  + bias$ |
| `v_reset`   | 0.0 | 0.0 |


### CubaLIF

The `nir.CubaLIF` model has the following equations:

**NIR Differential Equation**
```math
\tau_{syn} \dot{u} = -u + w_{in} i \\
\tau_{mem} \dot{v} = v_{leak} - v + R u
```

**NIR Forward-Euler**
```math
u[t] = (1-\frac{dt}{\tau_{syn}}) u[t-1] + \frac{dt}{\tau_{syn}} w_{in} i[t] \\
v[t] = (1-\frac{dt}{\tau_{mem}}) v[t-1] + \frac{dt}{\tau_{mem}} v_{leak} + \frac{dt}{\tau_{mem}} R u[t-1]
```

**SpiNNaker2 implementation (`lif_curr_exp` neuron)**

```
I_syn_exc[t] = exc_decay*I_syn_exc[t-1] + accumulated excitatory weights
I_syn_inh[t] = inh_decay*I_syn_inh[t-1] + accumulated inhibitory weights
v[t] = alpha_decay*v[t-1] + I_syn_exc[t] - I_syn_inh[t] + i_offset
```

**Translation**

| parameter | Forward-Euler | Exponential-Euler |
|-----------|----------------------|---|
| `alpha_decay`| $1 - \frac{dt}{\tau_{mem}}$| $\exp (-\frac{dt}{\tau_{mem}})$ |
| `exc_decay`  | $1 - \frac{dt}{\tau_{syn}}$| $\exp (-\frac{dt}{\tau_{syn}})$ |
| `inh_decay`  | $1 - \frac{dt}{\tau_{syn}}$| $\exp (-\frac{dt}{\tau_{syn}})$ |
| `threshold`  | $v_{threshold} \frac{\tau_{mem}}{dt\cdot R} \frac{\tau_{syn}}{dt\cdot w_{in}}$ | $v_{threshold} \left ((1 - \exp (-\frac{dt}{\tau_{mem}})) \cdot R \right)^{-1} \left ((1 - \exp (-\frac{dt}{\tau_{syn}})) \cdot w_{in} \right)^{-1}$ |
| `i_offset`   | $v_{leak} \frac{1}{R} \frac{\tau_{syn}}{dt\cdot w_{in}} + bias \frac{dt}{\tau_{syn}}$ | $v_{leak} \frac{1}{R} \left ((1 - \exp (-\frac{dt}{\tau_{syn}})) \cdot w_{in} \right)^{-1} + bias \left (1 - \exp (-\frac{dt}{\tau_{syn}})\right)^{-1}$|
| `v_reset`   | 0.0 | 0.0 |
| `t_refrac` | 0 | 0 |
