# Welcome to py-spinnaker2

py-spinnaker2 provides a light-weight Python interface for running experiments on the SpiNNaker2 neuromorphic chip.

## Installation
See [README](https://gitlab.com/spinnaker2/py-spinnaker2#installation).

## SNN Python Interface
High-level users can define spiking neural networks (SNN) and hybrid DNN/SNN models in py-spinnaker2. The networks are automatically partitioned and mapped to a SpiNNaker2 chip, the SNN simulation is executed via the experiment runner and results are provided back to the user in the Python interface. The API for defining SNN is inspired by [PyNN](http://neuralensemble.org/PyNN/). If no SpiNNaker2 chip is available, SNN models can also be executed using the [Brian 2 simulator](https://briansimulator.org/). Deep SNN (SNN trained with deep learning methods) are supported via the [Neuromorphic Intermediate Representation (NIR)](https://github.com/neuromorphs/NIR).


See [SNN Python Interface](snn-api.md).

## Experiment Runner
For low-level users, the so-called *experiment runner* allows to define and run reproducible batch experiments on single SpiNNaker2 chips. An experiment is comprised of the *mem-files* for the SpiNNaker2 ARM cores (i.e. the instruction code), input data to be sent to specific memory locations, and memory regions to be read after the experiment.

See [Experiment Runner](experiment-runner.md).

## SpiNNaker2 Hardware
For a general intro to the SpiNNaker2 chip, see [spinnaker2.gitlab.io](https://spinnaker2.gitlab.io).
