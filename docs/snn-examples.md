# Examples

## Neuron models

- Leaky Integrate&Fire neuron: [`examples/snn/lif_neuron.py`](https://gitlab.com/spinnaker2/py-spinnaker2/-/blob/main/examples/snn/lif_neuron.py)
- Leaky Integrate&Fire neuron with exponentially decaying current input: [`examples/snn/lif_curr_exp.py`](https://gitlab.com/spinnaker2/py-spinnaker2/-/blob/main/examples/snn/lif_curr_exp.py)

## Synfire chain
- Chain of neuron populations that allow the stable propagation of pulse packets
- Code: [examples/synfire_chain/synfire_chain.py](https://gitlab.com/spinnaker2/py-spinnaker2/-/blob/main/examples/synfire_chain/synfire_chain.py)

## Hybrid network for MNIST digit recognition

- Network consisting of 1 'conv2d_if_neuron_rate_code', one hidden `lif_no_delay` and a `lif_no_delay` output layer.
- Code: [`examples/hybrid/mnist_cnn/snn.py`](https://gitlab.com/spinnaker2/py-spinnaker2/-/blob/main/examples/hybrid/mnist_cnn/snn.py)
