# Papers using py-spinnaker2

Py-spinnaker2 was used in the following articles:

- Pedersen, J.E., Abreu, S., Jobst, M., Lenz, G., Fra, V., Bauer, F., Muir, D.R., Zhou, P., Vogginger, B., Heckel, K., Urgese, G., Shankar, S., Stewart, T.C., Eshraghian, J.K., & Sheik, S. (2023). Neuromorphic Intermediate Representation: A Unified Instruction Set for Interoperable Brain-Inspired Computing. *arXiv preprint arXiv:2311.14641*. [https://doi.org/10.48550/arXiv.2311.14641](https://doi.org/10.48550/arXiv.2311.14641)
- Gerhards, P., Weih, M., Huang, J., Knobloch, K., & Mayr, C. G. (2023, October). Hybrid Spiking and Artificial Neural Networks for Radar-Based Gesture Recognition. In 2023 *8th International Conference on Frontiers of Signal Processing (ICFSP)* (pp. 83-87). IEEE. [https://doi.org/10.1109/ICFSP59764.2023.10372930](https://doi.org/10.1109/ICFSP59764.2023.10372930)
- Lopez-Randulfe, J., Reeb, N., & Knoll, A. (2023). Integrate-and-fire circuit for converting analog signals to spikes using phase encoding. *Neuromorphic Computing and Engineering*, 3(4), [https://doi.org/10.1088/2634-4386/acfe36](https://doi.org/10.1088/2634-4386/acfe36)
