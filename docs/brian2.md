# Brian 2 simulation backend

We provide a simulation backend which allows to define SNNs using the [py-spinnaker2 SNN-API](snn-api.md) and simulate the network using the [Brian 2 simulator](https://briansimulator.org/). This allows to prepare models for SpiNNaker2 without access to SpiNNaker2 hardware. The quantization of weights and delays and the recording is identical to running the SNN model on SpiNNaker2.

## Instructions
1) Just install py-spinnaker as described in the [README](https://gitlab.com/spinnaker2/py-spinnaker2/-/blob/main/README.md). `brian2` is a dependency of `py-spinnaker2` and will be installed automatically.
```
pip install -e .
```
2) In your code replace
```python
from spinnaker2 import hardware
...
hw = hardware.SpiNNaker2Chip()
hw.run(net, timesteps)
```
with the following
```python
from spinnaker2 import brian2_sim
...
brian2 = brian2sim.Brian2Backend()
brian2.run(net, timesteps)
```

## Neuron models in brian2 simulation backend
### Supported
- `lif`
- `lif_no_delay`
- `spike_list`
- `lif_curr_exp`
- `lif_curr_exp_no_delay`

### Not supported yet
- `charge_and_spike`
- `qubo_neuron`

### No support planned
- `conv2d_if_neuron_rate_code`
- `spikes_from_array_latency_code`
- `rfneuron`
- `float_list`

### Deviation for `lif_curr_exp*` models:
For the `lif_curr_exp*` models, there is a minimal deviation of the `brian2` implementation compared to `spinnaker2`. In `brian2`, the synaptic currents `I_syn_exc` and`I_syn_inh` are only integrated in the next timestep.

**SpiNNaker2 Equation**
```python
I_syn_exc[t] = exc_decay*I_syn_exc[t-1] + accumulated excitatory weights
I_syn_inh[t] = inh_decay*I_syn_inh[t-1] + accumulated inhibitory weights
v[t] = alpha_decay*v[t-1] + I_syn_exc[t] - I_syn_inh[t] + i_offset
```

**brian2 Equation**
```python
I_syn_exc[t] = exc_decay*I_syn_exc[t-1] + accumulated excitatory weights
I_syn_inh[t] = inh_decay*I_syn_inh[t-1] + accumulated inhibitory weights
v[t] = alpha_decay*v[t-1] + I_syn_exc[t-1] - I_syn_inh[t-1] + i_offset
```
