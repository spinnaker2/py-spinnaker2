# Customizing your simulation

## Individual neuron parameters

One can set individual parameters for each neuron in a Population.
The values of the dictionairy passed as `params` can be either scalar values or
lists with a length equal to the size of the population:

```python
neuron_params = {
    "alpha_decay": [0.6, 0.65, 0.7, 0.75, 0.8],
    "threshold"  : 1.0,
    "i_offset"   : [0.0, 0.01, 0.02, 0.03, 0.04],
    "v_reset"    : 0.0,
    "reset"      : "reset_to_v_reset"
    }

pop1 = snn.Population(
    size=5,
    neuron_model="lif",
    params=neuron_params,
    name="pop1",
    record=["spikes", "v"])
```
*Note that the `reset` parameter is a global variable of the population.*
On the SpiNNaker2 chip all other parameters are stored individually for each neurons, shared values are duplicated.

## Choosing SpiNNaker2 chip

py-spinnaker2 supports both single-chip SpiNNaker2 boards and the FPGA prototype:
```python
# 1) Use Ethernet with fixed IP
hw = hardware.SpiNNaker2Chip(eth_ip='192.168.1.48')

# 2) Use Ethernet with IP from environment variable (recommended)
import os
hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv('S2_IP'))

# 3) Use jtag with dedicated ID
hw = hardware.SpiNNaker2Chip(jtag_id=0)

# 4) default, uses jtag_id=0
hw = hardware.SpiNNaker2Chip()

# 5) Use FPGA prototype with IP address 192.168.5.111
hw = hardware.FPGA_Rev2()
```
- It is recommended to use Ethernet for communication for being faster and easier to set up than the JTAG connection.
- It is recommended to specify the IP address as environment variable `S2_IP` as shown in variant 2). This way code does not need to be changed when changing the hardware setup.
- *Note that the FPGA prototype will no longer be supported in the future.* (Status: December 2023)


## Set duration of simulation time step
As described in [Units of time and other variables](snn-api.md#units-of-time-and-other-variables), time is represented
by discrete steps without unit.

One can adjust how long a time step takes during simulation on SpiNNaker2 by passing the keyword argument `sys_tick_in_s` to the `hardware.run()` function:
```python
hw = hardware.SpiNNaker2Chip()
timesteps = 50
hw.run(net, timesteps, sys_tick_in_s=0.01)
```
This sets the duration of a *system tick* to 0.01 seconds. Thus, neuron updates are triggered every 10 ms on the SpiNNaker2 hardware. Technically, this is realized by programming the timers of the ARM cores such that a timer interrupt is triggered every 10 ms to start the next time step calculation.

Parameter `sys_tick_in_s` ("System tick in seconds") defaults to `0.001` seconds.

*Changing this parameter might be necessary if the compute load for some ARM cores is too high to complete within 0.001 seconds. This may happen if too many spike events arrive within one time step or if too many neurons fire at the same time. You may use `time_done` recording to find such cases, see [here](snn-api.md#obtaining-and-plotting-execution-times).*


## Run mapping without SpiNNaker2
If you don't have a SpiNNaker2 hardware available, you can already debug your code for the network creation and mapping to hardware.
Add the keyword argument `mapping_only=True` to the `run(..)` function of the hardware:

```python
hw.run(net, timesteps, mapping_only=True)
```

This will run the experiment until the generation of the experiment specification for the hardware. It will provide feedback if the network is too large to fit onto the available cores.

**In addition you can debug the functionality of the model using the [brian2 simulation backend](Brian2-simulation-backend)**

## Adapt number of neurons per core
In can happen that not all incoming synapses fit into the SRAM of a SpiNNaker2 processor element (core). One way to resolve this is to reduce the maximum number of atoms (aka neurons) per core for the mapping algorithm.

```python
pop1 = snn.Population(size=20, neuron_model="lif", params=neuron_params)
pop1.set_max_atoms_per_core(50) # reduce to 50 instead of 250 per default
```
*Note: you can find the default number of neurons per core in the section **Neuron models**.*
