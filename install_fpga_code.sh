#!/bin/bash

cd ..
git clone --branch py-spinnaker2 git@gitlab.com:tud-hpsn/internal/qpe-software-fixed.git

cd qpe-software-fixed
source quadpe_env.sh

cd host-software/rev2/experiment/app/
cmake CMakeLists.txt
make
cd -

cd host-software/rev2/experiment/
./make_all_apps.py 
