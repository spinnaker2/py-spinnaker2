import copy
import os

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from spinnaker2 import hardware, snn
from spinnaker2.mla import conv2d, mla_helpers


def run_spiking_conv_layer(
    input_array: np.ndarray,
    weights: np.ndarray,
    conv_params: dict,
    neuron_params: dict,
    record: list = ["spikes"],
    pop2: bool = False,
) -> dict:
    """run experiment with single spiking conv2d layer on SpiNNaker2.

    Args:
        input_array: binary input array with shape (T,C,H,W)
        weights: uint8 weights with shape (CI,CO,H,W)
        conv_params: parameters of Conv2DProjection
        neuron_params: neuron parameters
        record: list of variables to record out of `["v", "spikes", "time_done"]`
        pop2: add a 2nd population of `lif` neurons for receiving spikes from
            the `lif_conv2d` population with 1-to-1 connections.

    Returns:
        dictionary with results depending on arguments `record` and `pop2`
        Keys:
            spikes: spikes from `lif_conv2d` population if `spikes` in `record`
            voltages: voltages from `lif_conv2d` population if `v` in `record`
            time_done: time done values from `lif_conv2d` population if `time_done` in `record`
            spikes_pop2: spikes if from 2nd population if `pop2` is True
    """

    input_size = np.prod(input_array.shape[1:])
    # create spiking input population
    input_spikes = {}
    print("Spikes per time step:", np.count_nonzero(input_array, axis=(1, 2, 3)))
    input_array_reshaped = np.reshape(input_array, (input_array.shape[0], input_size))
    for i, spike_array in enumerate(input_array_reshaped.T):
        input_spikes[i] = np.where(spike_array == 1)[0].astype(int).tolist()

    input_pop = snn.Population(size=input_size, neuron_model="spike_list", params=input_spikes, name="input")

    n_steps, in_channels, in_height, in_width = input_array.shape
    out_channels = weights.shape[1]
    filter_height = weights.shape[2]
    filter_width = weights.shape[3]

    # compute output size (needed for population size)
    output_shape = (
        out_channels,  # C
        mla_helpers.conv1d_output_size((in_height // conv_params["pool_x"]), filter_height, conv_params["stride_y"])
        + (conv_params["pad_top"] + conv_params["pad_bottom"]) // conv_params["stride_x"],  # H
        mla_helpers.conv1d_output_size((in_width // conv_params["pool_y"]), filter_width, conv_params["stride_x"])
        + (conv_params["pad_left"] + conv_params["pad_right"]) // conv_params["stride_y"],  # W
    )
    output_size = np.prod(output_shape)

    # create population
    conv2d_pop = snn.Population(
        size=output_size,
        neuron_model="lif_conv2d",
        params=neuron_params,
        name="conv2d_pop",
        record=record,
    )
    print("conv2d_pop.shape:", output_shape)
    print("conv2d_pop.size:", conv2d_pop.size)
    neurons_per_core = np.prod(output_shape[1:])
    print("conv2d_pop.neurons_per_core:", neurons_per_core)
    conv2d_pop.set_max_atoms_per_core(neurons_per_core)

    conv2d_proj = snn.Conv2DProjection(input_pop, conv2d_pop, weights=weights, params=conv_params)

    net = snn.Network("my network")
    net.add(input_pop, conv2d_pop, conv2d_proj)

    if pop2:
        # add outging projection from pop to test sending of spikes
        lif_params = {
            "threshold": 1.0,
            "alpha_decay": 1.0,
            "i_offset": 0.0,
            "v_reset": 0.0,
            "reset": "reset_by_subtraction",
        }
        rcv_pop = snn.Population(output_size, "lif", params=lif_params, name="rcv_pop", record=["spikes"])

        # create one-to-one connections between pop and rcv_pop
        w = 1.0  # weight
        d = 0  # delay
        conns2 = []
        for i in range(output_size):
            conns2.append([i, i, w, d])

        proj2 = snn.Projection(pre=conv2d_pop, post=rcv_pop, connections=conns2)
        net.add(rcv_pop, proj2)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP", "192.168.1.59"))
    hw.run(net, n_steps, mapping_only=False, debug=False)

    result = {}
    if "v" in record:
        result["voltages"] = conv2d_pop.get_voltages()
    if "spikes" in record:
        result["spikes"] = conv2d_pop.get_spikes()
    if "time_done" in record:
        result["time_done"] = conv2d_pop.get_time_done_times()
    if pop2:
        result["spikes_pop2"] = rcv_pop.get_spikes()

    return result


def get_conv_params(im_size, stride, pool, pad):
    conv_params = {
        "in_height": im_size,
        "in_width": im_size,
        "stride_x": stride,
        "stride_y": stride,
        "pool_x": pool,
        "pool_y": pool,
        "pad_top": pad,
        "pad_bottom": pad,
        "pad_left": pad,
        "pad_right": pad,
    }
    return conv_params


def get_input_image(input_shape, input_spike_threshold, rng):
    input_image = rng.integers(0, 2**8, size=input_shape, dtype=np.uint8)
    input_image_spikes = input_image > input_spike_threshold

    return input_image, input_image_spikes


def lif_conv2d_single_step(conv_params, in_channels, out_channels, filter_size, n_steps, rng):
    """test voltages after first time step with LIF as integrator."""

    # parameters of convolution layer
    in_width = conv_params["in_width"]
    in_height = conv_params["in_height"]
    stride_x = conv_params["stride_x"]
    stride_y = conv_params["stride_y"]
    pool_x = conv_params["pool_x"]
    pool_y = conv_params["pool_y"]
    pad_top = conv_params["pad_top"]
    pad_bottom = conv_params["pad_bottom"]
    pad_left = conv_params["pad_left"]
    pad_right = conv_params["pad_right"]

    filter_height = filter_size[0]
    filter_width = filter_size[1]

    # create random input data in required shape

    # Input format: NCHW
    input_shape = (n_steps, in_channels, in_height, in_width)
    threshold = 250
    input_image, input_image_spikes = get_input_image(input_shape, threshold, rng)

    # Weight format: (H,W,CI,CO)
    weights_shape = (in_channels, out_channels, filter_height, filter_width)
    conv_weights = rng.integers(-(2**7), 2**7, size=weights_shape, dtype=np.int8)

    # Use neurons just as integrators
    neuron_params = {
        "threshold": 1.0e20,  # high threshold to disable spiking
        "alpha_decay": 1.0,  # no decay
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_by_subtraction",
    }

    results = run_spiking_conv_layer(input_image_spikes, conv_weights, conv_params, neuron_params, ["v"])

    # compute convolution with tensorflow
    inp = np.moveaxis(input_image_spikes.astype(np.int32), 1, -1)  # reshape from NCHW to NHWC
    if pool_x != 1 or pool_y != 1:
        inp = tf.nn.avg_pool2d(inp.astype(np.float64) * 4, 2, 2, "VALID")
    tf_result = tf.nn.conv2d(
        input=inp,
        filters=np.moveaxis(conv_weights, [0, 1], [-2, -1]),  # reshape from (CI,CO,H,W) to (H,W,CI,CO)
        strides=[stride_y, stride_x],
        padding=[[0, 0], [pad_top, pad_bottom], [pad_left, pad_right], [0, 0]],
        data_format="NHWC",
    )

    voltages = results["voltages"]
    output_shape = tf_result.shape[1:]
    output_size = np.prod(output_shape)
    assert output_size == len(voltages)

    tf_result_flat = np.moveaxis(tf_result, -1, 1).reshape(tf_result.shape[0], output_size)
    s2_result_flat = np.zeros((n_steps, output_size), dtype=np.float32)
    for i, vs in voltages.items():
        s2_result_flat[:, i] = vs
    s2_result = s2_result_flat[-1, :].reshape((output_shape[2], output_shape[0], output_shape[1]))

    # check that tf result at t=0 is almost the same as S2 result at t=1
    result = np.allclose(tf_result_flat[0], s2_result_flat[1])
    diff = tf_result_flat[0] - s2_result_flat[1]
    if result == True:
        print("PASS!")
    else:
        print("FAIL!")
    print(
        "mean abs diff: ",
        np.mean(np.abs(diff)),
        "std: ",
        np.std(diff),
        "absmax: ",
        np.max(np.abs(diff)),
    )

    return not result


def test_lif_conv2d_single_step_integrator_only():
    rng = np.random.default_rng(16)  # fails for seed = 12
    errors = 0
    runs = 0

    # parameters of convolution layer
    n_steps = 2

    errors += lif_conv2d_single_step(
        get_conv_params(im_size=16, stride=1, pool=1, pad=0),
        in_channels=1,
        out_channels=1,
        filter_size=(3, 3),
        n_steps=n_steps,
        rng=rng,
    )
    runs += 1
    errors += lif_conv2d_single_step(
        get_conv_params(im_size=34, stride=2, pool=1, pad=1),
        in_channels=2,
        out_channels=16,
        filter_size=(5, 5),
        n_steps=n_steps,
        rng=rng,
    )
    runs += 1
    errors += lif_conv2d_single_step(
        get_conv_params(im_size=16, stride=1, pool=1, pad=1),
        in_channels=16,
        out_channels=16,
        filter_size=(3, 3),
        n_steps=n_steps,
        rng=rng,
    )
    runs += 1
    errors += lif_conv2d_single_step(
        get_conv_params(im_size=16, stride=1, pool=2, pad=1),
        in_channels=16,
        out_channels=16,
        filter_size=(3, 3),
        n_steps=n_steps,
        rng=rng,
    )
    runs += 1
    errors += lif_conv2d_single_step(
        get_conv_params(im_size=16, stride=2, pool=2, pad=1),
        in_channels=16,
        out_channels=16,
        filter_size=(3, 3),
        n_steps=n_steps,
        rng=rng,
    )
    runs += 1
    errors += lif_conv2d_single_step(
        get_conv_params(im_size=16, stride=2, pool=2, pad=1),
        in_channels=16,
        out_channels=32,
        filter_size=(3, 3),
        n_steps=n_steps,
        rng=rng,
    )
    runs += 1

    print(f"\nRESULT:\n{runs-errors}/{runs} PASSED!\n")

    assert errors == 0


def test_lif_conv2d_spiking():
    """test lif_conv2d the spike counts in a rate-coding scenario.

    Method:
    1. A single input image is generated, turned into spikes by a threshold and
       replicated for `n_steps`. This leads to a constant input current for
       each neuron.
    2. Neurons are set in `rate-coding` mode (IF neurons with reset by
       subtraction). The spike threshold is set such that the neuron with the
       highest synaptic input current spikes in each timestep.
    3. Spikes are recorded and the spike counts are compared to the calculated
       expected spike count


    In addition we check that the `conv2d_lif` population can forward spikes.
    For this, the Conv2D population is connected to a 2nd population with
    1-to-1 connection. The receiving population is set up to spike once
    incoming spike.
    """
    rng = np.random.default_rng(42)  # fails for seed = 12

    in_width = 28
    in_height = 28
    in_channels = 1
    out_channels = 4
    filter_height = 3
    filter_width = 3

    # parameters of convolution layer
    conv_params = {}
    conv_params["in_width"] = in_width
    conv_params["in_height"] = in_height
    conv_params["stride_x"] = 2
    conv_params["stride_y"] = 2
    conv_params["pool_x"] = 1
    conv_params["pool_y"] = 1
    conv_params["pad_top"] = 0
    conv_params["pad_bottom"] = 0
    conv_params["pad_left"] = 0
    conv_params["pad_right"] = 0

    n_steps = 50

    # create random input data in required shape

    # Input format: NCHW
    input_shape = (n_steps, in_channels, in_height, in_width)
    input_image = rng.integers(0, 2**8, size=input_shape[1:], dtype=np.uint8)
    threshold = 250
    input_image_spikes = input_image > threshold
    input_image_spikes_all_steps = np.zeros(input_shape, dtype=input_image_spikes.dtype)
    input_image_spikes_all_steps[:] = input_image_spikes

    # Weight format: (H,W,CI,CO)
    weights_shape = (in_channels, out_channels, filter_height, filter_width)
    conv_weights = rng.integers(-(2**7), 2**7, size=weights_shape, dtype=np.int8)

    # compute convolution with tensorflow
    inp = np.moveaxis(input_image_spikes_all_steps.astype(np.int32), 1, -1)  # reshape from NCHW to NHWC
    tf_result = tf.nn.conv2d(
        input=inp,
        filters=np.moveaxis(conv_weights, [0, 1], [-2, -1]),  # reshape from (CI,CO,H,W) to (H,W,CI,CO)
        strides=[conv_params["stride_y"], conv_params["stride_x"]],
        padding=[
            [0, 0],
            [conv_params["pad_top"], conv_params["pad_bottom"]],
            [conv_params["pad_left"], conv_params["pad_right"]],
            [0, 0],
        ],
        data_format="NHWC",
    )

    # set threshold such that neuron with highest activation spikes in time step 1.
    v_threshold = np.max(tf_result[0])

    # Use neurons just as integrators
    neuron_params = {
        "threshold": v_threshold,
        "alpha_decay": 1.0,  # no decay
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_by_subtraction",
    }

    results = run_spiking_conv_layer(
        input_image_spikes_all_steps,
        conv_weights,
        conv_params,
        neuron_params,
        ["spikes", "time_done"],
        pop2=True,
    )

    spikes = results["spikes"]
    output_shape = tf_result.shape[1:]
    output_size = np.prod(output_shape)
    assert output_size == len(spikes)

    # select t=0 and mv from HWC to CHW
    tf_result = np.moveaxis(tf_result[0], -1, 0)
    s2_result_flat = np.zeros((n_steps, output_size), dtype=np.float32)
    s2_spike_counts_flat = np.zeros(output_size, dtype=np.int32)
    for i, s in spikes.items():
        s2_spike_counts_flat[i] = len(s)
    s2_spike_counts = s2_spike_counts_flat.reshape((output_shape[2], output_shape[0], output_shape[1]))

    expected_spike_counts = tf_result.clip(min=0) * (n_steps - 1) // v_threshold

    # check that spike counts are as expected
    assert np.array_equal(expected_spike_counts, s2_spike_counts)

    # check that spikes were received by 2nd populaton 1 time step later
    spikes_pop2 = results["spikes_pop2"]
    for nrn_id in spikes:
        for t_pop, t_rcv_pop in zip(spikes[nrn_id], spikes_pop2[nrn_id]):
            assert t_pop + 1 == t_rcv_pop


if __name__ == "__main__":
    # test_lif_conv2d_single_step_integrator_only()
    test_lif_conv2d_spiking()
