"""
test for setting individual parameters for the LIF neuron
"""

import os

import numpy as np
from spinnaker2 import hardware, snn

rng = np.random.default_rng()


def test_lif_neuron_individual_params():
    n_neurons = 300  # larger than max neurons per core
    params = {
        "threshold": rng.uniform(0.8, 1.2, n_neurons),  # use np.array
        "alpha_decay": 1.0,
        "i_offset": rng.uniform(0.0, 0.4, n_neurons).tolist(),  # use list
        "reset": "reset_by_subtraction",
    }

    pop1 = snn.Population(
        size=n_neurons,
        neuron_model="lif_no_delay",
        params=params,
        name="pop1",
        record=["spikes"],
    )

    net = snn.Network("my network")
    net.add(pop1)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    timesteps = 100
    hw.run(net, timesteps)

    spikes = pop1.get_spikes()
    spike_counts = [len(spikes[key]) for key in range(n_neurons)]
    expected_spike_counts = [
        int(timesteps * i_off / thresh) for i_off, thresh in zip(params["i_offset"], params["threshold"])
    ]

    assert expected_spike_counts == spike_counts


if __name__ == "__main__":
    test_lif_neuron_individual_params()
