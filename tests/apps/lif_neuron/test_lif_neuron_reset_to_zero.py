"""
test voltage reset to v_reset for LIF neuron
"""

import os

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, snn


def test_lif_neuron_reset_to_zero(do_plot=False):
    params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.12,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
    }

    pop1 = snn.Population(size=1, neuron_model="lif", params=params, name="pop1", record=["spikes", "v"])

    net = snn.Network("my network")
    net.add(pop1)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    timesteps = 100
    hw.run(net, timesteps)

    # get results and plot
    spikes = pop1.get_spikes()[0]
    voltages = pop1.get_voltages()

    # check that neuron spikes
    assert len(spikes) > 0

    # check that some voltages are identical to v_reset, and not the 1st one
    assert len(np.where(voltages[0][1:] == params["v_reset"])) > 0

    if do_plot:
        times = np.arange(timesteps)
        plt.plot(times, voltages[0], label="Neuron 0")
        plt.xlim(0, timesteps)
        plt.xlabel("time step")
        plt.ylabel("voltage")
        plt.legend()
        plt.show()


if __name__ == "__main__":
    test_lif_neuron_reset_to_zero(do_plot=True)
