"""
test for the 'i_offset' parameter of the LIF neuron
"""

import os

from spinnaker2 import hardware, snn


def test_lif_neuron_i_offset():
    params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.32,
        "reset": "reset_by_subtraction",
    }

    pop1 = snn.Population(size=1, neuron_model="lif", params=params, name="pop1", record=["spikes"])

    net = snn.Network("my network")
    net.add(pop1)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    timesteps = 100
    hw.run(net, timesteps)

    spikes = pop1.get_spikes()[0]

    expected_spike_count = int(timesteps * params["i_offset"] / params["threshold"])
    actual_spike_count = len(spikes)
    print("Actual spikes:", actual_spike_count)
    print("Expected spikes:", expected_spike_count)

    assert expected_spike_count == actual_spike_count


if __name__ == "__main__":
    test_lif_neuron_i_offset()
