"""
test basic functionality of lif neuron models
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pytest
from spinnaker2 import hardware, helpers, snn


@pytest.mark.parametrize("neuron_model", ["lif", "lif_no_delay", "lif_no_delay_1024"])
def test_lif_neuron(neuron_model, do_plot=False, plot_times_done=False):
    # create LIF population with 1 neuron
    neuron_params = {
        "threshold": 10.0,
        "alpha_decay": 0.9,
        "i_offset": 0.0,
        "v_init": 1.1,
        "v_reset": 0.0,  # only used for reset="reset_to_v_reset"
        "reset": "reset_by_subtraction",  # "reset_by_subtraction" or "reset_to_v_reset"
    }

    timesteps = 100
    n_stim = 2
    n_nrns = 1

    # create stimulus population with 2 spike sources
    input_spikes = {0: [20, 22, 24, 26], 1: [10]}
    stim = snn.Population(n_stim, "spike_list", params=input_spikes, name="stim")

    pop = snn.Population(
        size=n_nrns,
        neuron_model=neuron_model,
        params=neuron_params,
        name="pop",
        record=["spikes", "v", "time_done"],
    )

    # create connections with weight identical to post neuron
    conns = []
    delay = 0
    conns.append([0, 0, 4, delay])  # excitatory synapse with weight 4 and delay 1
    conns.append([1, 0, -3, delay])  # inhibitory synapse with weight -3 and delay 1

    proj = snn.Projection(stim, pop, conns)

    # add outging projection from pop to test sending of spikes
    rcv_lif_params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_init": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
    }
    rcv_pop = snn.Population(n_nrns, neuron_model, params=rcv_lif_params, name="rcv_pop", record=["spikes"])

    # create one-to-one connections between pop and rcv_pop
    w = 2.0  # weight
    d = 0  # delay
    conns2 = []
    for i in range(n_nrns):
        conns2.append([i, i, w, d])

    proj2 = snn.Projection(pre=pop, post=rcv_pop, connections=conns2)

    net = snn.Network("my network")
    net.add(stim, pop, proj, rcv_pop, proj2)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    hw.run(net, timesteps)

    #########################
    # test correct behavior #
    #########################
    voltages = pop.get_voltages()[0]
    spikes = pop.get_spikes()[0]

    # 1. initial phase without external input
    assert neuron_params["v_init"] >= voltages[0] > 0.0  # test that initialization to a positive value works
    assert voltages[0] > voltages[1]  # test decay
    assert neuron_params["alpha_decay"] == pytest.approx(voltages[1] / voltages[0])  # test decay factor

    # 2. inhibitory input spike
    assert voltages[11] < 0.0  # voltage negative
    assert neuron_params["alpha_decay"] == pytest.approx(voltages[12] / voltages[11])  # test decay factor

    # 3. excitatory input & spike
    assert voltages[23] > 0.0  # voltage positive after excitatory input
    assert spikes[0] == 27  # neuron spikes after receiving 4 excitatory input spikes

    # 4. decay to v_rest at the end
    assert voltages[timesteps - 1] == pytest.approx(0.0, abs=0.001)  # test decay to 0.0

    # spikes were sent to rcv_pop which spike 1 time step later
    rcv_nrn_spikes = rcv_pop.get_spikes()[0]
    for t_nrn, t_rcv_nrn in zip(spikes, rcv_nrn_spikes):
        assert t_nrn + 1 == t_rcv_nrn

    if do_plot:
        times = np.arange(timesteps)
        fig, ax = plt.subplots(1, 1)
        ax.plot(times, voltages, label="Neuron 0")
        ax.axhline(neuron_params["threshold"], ls="--", c="0.5", label="threshold")
        ax.axhline(0, ls="-", c="0.8", zorder=0)
        ax.set_xlim(0, timesteps)
        ax.set_ylabel("voltage")

        spikes = pop.get_spikes()[0]
        plt.show()

    # manually test time_done recording
    if plot_times_done:
        time_done_times = pop.get_time_done_times()
        helpers.save_dict_to_npz(time_done_times, "time_done_times")
        helpers.plot_times_done_multiple_pes_one_plot_vertical("time_done_times.npz")
        plt.show()


if __name__ == "__main__":
    do_plot = True
    plot_times_done = True
    # test_lif_neuron("lif", do_plot, plot_times_done)
    # test_lif_neuron("lif_no_delay", do_plot, plot_times_done)
    test_lif_neuron("lif_no_delay_1024", do_plot, plot_times_done)
