"""
test lif_curr_exp
"""

import os

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, snn


def test_lif_curr_exp(do_plot=False):
    params = {
        "threshold": 1000.0,
        "alpha_decay": 0.9,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
        "exc_decay": 0.5,
        "inh_decay": 0.2,
        "t_refrac": 0,
    }

    n_neurons = 20
    pop1 = snn.Population(
        size=n_neurons,
        neuron_model="lif_curr_exp",
        params=params,
        name="pop1",
        record=["spikes", "v"],
    )

    stim = snn.Population(1, "spike_list", {0: [2]}, "stim")

    # create connections with negative weight identical to post neuron
    conns = []
    for i in range(n_neurons):
        conns.append([0, i, -i, 0])

    proj = snn.Projection(stim, pop1, conns)

    net = snn.Network("my network")
    net.add(stim, pop1, proj)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    timesteps = 20
    hw.run(net, timesteps)

    voltages = pop1.get_voltages()
    min_voltages = [vs.min() for vs in voltages.values()]

    def is_strictly_decreasing(array):
        return all(array[i] > array[i + 1] for i in range(len(array) - 1))

    # the lif_curr_exp model has weights in range [-15,15].
    # The first 16 min voltages should be strictly decreasing.
    assert is_strictly_decreasing(min_voltages[:16])

    # The remaining min voltages should be equal.
    assert len(set(min_voltages[15:])) == 1

    if do_plot:
        times = np.arange(timesteps)
        for i, vs in enumerate(voltages.values()):
            plt.plot(times, vs, label=str(i))
        plt.xlim(0, timesteps)
        plt.xlabel("time step")
        plt.ylabel("voltage")
        plt.legend()
        plt.show()


if __name__ == "__main__":
    test_lif_curr_exp(do_plot=True)
