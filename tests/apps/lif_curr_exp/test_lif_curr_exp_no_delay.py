"""
test lif_curr_exp_no_delay
"""

import os

import matplotlib.pyplot as plt

# get results and plot
import numpy as np
from spinnaker2 import hardware, snn


def test_lif_curr_exp_no_delay(do_plot=False):
    params = {
        "threshold": 1000.0,  # high value so neurons don't spike
        "alpha_decay": 0.9,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
        "exc_decay": 0.5,
        "inh_decay": 0.2,
        "t_refrac": 0,
    }

    n_neurons = 128
    pop1 = snn.Population(
        size=n_neurons,
        neuron_model="lif_curr_exp_no_delay",
        params=params,
        name="pop1",
        record=["spikes", "v"],
    )

    pop2 = snn.Population(
        size=n_neurons,
        neuron_model="lif_curr_exp_no_delay",
        params=params,
        name="pop1",
        record=["spikes", "v"],
    )

    stim = snn.Population(1, "spike_list", {0: [2]}, "stim")

    # create connections with weight identical to post neuron
    conns = []
    for i in range(n_neurons):
        conns.append([0, i, i, 0])

    proj = snn.Projection(stim, pop1, conns)

    # create connections with negative weight identical to post neuron
    conns2 = []
    for i in range(n_neurons):
        conns2.append([0, i, -i, 0])

    proj2 = snn.Projection(stim, pop2, conns2)

    net = snn.Network("my network")
    net.add(stim, pop1, proj, pop2, proj2)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    timesteps = 20
    hw.run(net, timesteps)

    voltages_1 = pop1.get_voltages()
    voltages_2 = pop2.get_voltages()
    max_voltages = [vs.max() for vs in voltages_1.values()]
    min_voltages = [vs.min() for vs in voltages_2.values()]

    def is_strictly_increasing(array):
        return all(array[i] < array[i + 1] for i in range(len(array) - 1))

    def is_strictly_decreasing(array):
        return all(array[i] > array[i + 1] for i in range(len(array) - 1))

    assert is_strictly_increasing(max_voltages)
    assert is_strictly_decreasing(min_voltages)

    if do_plot:
        times = np.arange(timesteps)
        for i, vs in enumerate(voltages_1.values()):
            plt.plot(times, vs, label=str(i))

        for i, vs in enumerate(voltages_2.values()):
            plt.plot(times, vs, label=str(-i))
        plt.xlim(0, timesteps)
        plt.xlabel("time step")
        plt.ylabel("voltage")
        plt.legend()
        plt.show()


if __name__ == "__main__":
    test_lif_curr_exp_no_delay(do_plot=True)
