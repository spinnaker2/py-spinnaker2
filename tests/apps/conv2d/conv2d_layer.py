import json

import helper
import numpy as np
from spinnaker2 import hardware, snn

np.random.seed(46)

timesteps = 14

# parameters of convolution layer
in_width = 32
in_height = 32
in_channels = 1
out_channels = 4
filter_height = 3
filter_width = 3
stride_y = 1
stride_x = 1

test_images = np.random.randint(
    0, 2**8 - 1, size=(timesteps, 1, in_height, in_width), dtype=np.uint8
)  # 15 images in sequence

c_weights = np.random.randint(
    -(2**7), 2**7 - 1, size=(filter_height, filter_width, in_channels, out_channels), dtype=np.int8
)

output_scaling_factor = helper.get_scaling_factor(test_images, c_weights, (stride_x, stride_y), output_is_signed=True)

params_conv = helper.params_conv
params_conv.update(
    {
        "in_pix_row_s": in_height,
        "in_pix_col_s": in_width,
        "fil_pix_row_s": filter_height,
        "fil_pix_col_s": filter_width,
        "in_channel_s": in_channels,
        "out_channel_s": out_channels,
        "stride_in_pix_row": stride_y,
        "stride_in_pix_col": stride_x,
        "output_mode": 0,
        "output_shift_width": output_scaling_factor,
    }
)
params_conv, output_shape, conv_res_host = helper.get_mem_tensors(
    params_conv, c_weights, test_images, output_scaling_factor
)

mla_config = params_conv["mla_config"]

seq_meta = {"image_addr": 0xC000, "conv_res_addr": 0xD000}


image_storage_params = {"image_addr": seq_meta["image_addr"], "images": params_conv["images"]}

conv_res_size = int(np.prod(output_shape)) + output_shape[1] % 16 * output_shape[0] * output_shape[2]
conv2d_params = {
    "mla_config": mla_config,
    "image_addr": seq_meta["image_addr"],
    "image_size": in_height * in_width * in_channels,
    "conv_res_addr": seq_meta["conv_res_addr"],
    "conv_res_size": conv_res_size,
    "conv_weights": params_conv["weights"],
}

image_storage_pop = snn.Population(
    size=1,
    neuron_model="image_storage",
    params=image_storage_params,
    name="image_storage",
    record=[],
)
conv2d_pop = snn.Population(size=1, neuron_model="conv2d", params=conv2d_params, name="conv2d_pop", record=[])


dummy_conn = helper.build_conns(1, 1, [[0], [0]])

in_conv_conn = snn.Projection(pre=image_storage_pop, post=conv2d_pop, connections=dummy_conn)

net = snn.Network("my network")
net.add(image_storage_pop, conv2d_pop, in_conv_conn)

hw = hardware.FPGA_Rev2()
hw.run(network=net, time_steps=timesteps)

with open("results.json", "r") as f:
    results = json.load(f)

conv_out = results["PE_5_result"]
helper.check_convolution(mla_config, conv_out, conv_res_host[timesteps - 1])

print("done")
