"""
Test of spikes_from_array_latency_code

This population creates input spikes based on array values and the following
translation of values to time:
    t = t_max*(x_max-x)/x_max

The approach is tested by generating random input data of a given size.
The stim population is connected in a one-to-one fashion to an equally-sized
'lif' population with leakage turned off.
The 'lif' neurons fire in the subsequent time step in case the input neuron fired.
The recorded spike times are compared to expected spike times.
"""

import os

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, helpers, snn

rng = np.random.default_rng()


def test_spikes_from_array_latency_code(plot_times_done=False):
    n_stim = 3750
    t_max = 100
    x_max = 1000
    input_data = rng.integers(low=0, high=x_max + 1, size=n_stim, dtype=np.uint16)

    params = {
        "t_max": t_max,
        "x_max": x_max,
        "data": input_data,  # uint16
    }

    neuron_params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
    }

    stim = snn.Population(n_stim, "spikes_from_array_latency_code", params=params, name="stim", record=["time_done"])
    pop1 = snn.Population(n_stim, "lif", params=neuron_params, name="pop1", record=["spikes"])

    # create one-to-one connections between stim and receiver population
    w = 2.0  # weight
    d = 0  # delay

    conns = []
    for i in range(n_stim):
        conns.append([i, i, w, d])

    proj = snn.Projection(pre=stim, post=pop1, connections=conns)

    net = snn.Network("my network")
    net.add(pop1, stim, proj)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    hw.run(net, t_max + 2, debug=False)

    # manually test time_done recording
    if plot_times_done:
        time_done_times = stim.get_time_done_times()
        helpers.save_dict_to_npz(time_done_times, "time_done_times")
        helpers.plot_times_done_multiple_pes_one_plot_vertical("time_done_times.npz")
        plt.show()

    spiketimes_dict = pop1.get_spikes()
    expected_times = np.zeros_like(input_data)
    for i, x in enumerate(input_data):
        expected_times[i] = params["t_max"] * (params["x_max"] - x) / params["x_max"]

    for actual, expected in zip(spiketimes_dict.values(), expected_times):
        assert len(actual) == 1, (actual, expected)
        assert actual[0] == expected + 1  # 1 timestep delay


def test_spikes_from_array_latency_code_split():
    """test whether automatic splitting to multiple cores works"""
    n_stim = 2000
    t_max = 100
    x_max = 1000
    input_data = rng.integers(low=0, high=x_max + 1, size=n_stim, dtype=np.uint16)

    params = {
        "t_max": t_max,
        "x_max": x_max,
        "data": input_data,  # uint16
    }

    neuron_params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
    }

    stim = snn.Population(n_stim, "spikes_from_array_latency_code", params=params, name="stim")
    stim.set_max_atoms_per_core(451)  # some odd number
    pop1 = snn.Population(n_stim, "lif", params=neuron_params, name="pop1", record=["spikes"])

    # create one-to-one connections between stim and receiver population
    w = 2.0  # weight
    d = 0  # delay

    conns = []
    for i in range(n_stim):
        conns.append([i, i, w, d])

    proj = snn.Projection(pre=stim, post=pop1, connections=conns)

    net = snn.Network("my network")
    net.add(pop1, stim, proj)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    hw.run(net, t_max + 2, debug=False)

    spiketimes_dict = pop1.get_spikes()
    expected_times = np.zeros_like(input_data)
    for i, x in enumerate(input_data):
        expected_times[i] = params["t_max"] * (params["x_max"] - x) / params["x_max"]

    for actual, expected in zip(spiketimes_dict.values(), expected_times):
        assert len(actual) == 1, (actual, expected)
        assert actual[0] == expected + 1  # 1 timestep delay


if __name__ == "__main__":
    test_spikes_from_array_latency_code(plot_times_done=True)
    # test_spikes_from_array_latency_code_split()
