import json

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, snn
from spinnaker2.helpers import read_log
from spinnaker2.neuron_models.spikes_from_array_latency_code import (
    SpikesFromArrayLatencyCodeApplication,
)

SpikesFromArrayLatencyCodeApplication.profiling = True

rng = np.random.default_rng(42)


# helper functions
def value_to_time(x, t_max, x_max):
    return int((x_max - x) * t_max / x_max)


def time_to_value(t, t_max, x_max):
    x = x_max - t * x_max / t_max
    return int(x)


def run_model_mapping_only(n_stim, timesteps):
    x_max = 1000
    input_data = rng.integers(1000, size=n_stim, dtype=np.uint16)

    params = {
        "t_max": timesteps - 1,
        "x_max": x_max,
        "data": input_data,  # uint16
    }

    stim = snn.Population(n_stim, "spikes_from_array_latency_code", params=params, name="stim")
    net = snn.Network("my network")
    net.add(stim)

    hw = hardware.FPGA_Rev2()
    hw.run(net, timesteps, mapping_only=True)


def profile_memory():
    timesteps = 100
    best_n_stim = 0
    # for n_stim in range(1000,50000+1,1000):
    for n_stim in range(36800, 50000 + 1, 1):
        try:
            run_model_mapping_only(n_stim, timesteps)
            best_n_stim = n_stim
        except AssertionError:
            print(f"Could not map network with {n_stim} pre neurons")
            break
    print(best_n_stim)
    # 36808 is the limit


def run_model_time_profiling():
    """
    run model and do time profiling

    idea: t[0] -> 0 spikes, t[1] -> 1 spike, t[2] -> 2 spikes etc.
    """
    # generate input data
    params = {
        "t_max": 100,
        "x_max": 100,
    }
    input_data = []
    max_spikes_per_timestep = 50
    for i in range(max_spikes_per_timestep):
        for j in range(i):
            input_data.append(time_to_value(i, params["t_max"], params["x_max"]))
    input_data = np.array(input_data, dtype=np.uint16)
    n_stim = len(input_data)
    params["data"] = input_data

    neuron_params = {
        "threshold": max_spikes_per_timestep,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_by_subtraction",
    }

    stim = snn.Population(n_stim, "spikes_from_array_latency_code", params=params, name="stim")
    pop1 = snn.Population(1, "lif", params=neuron_params, name="pop1", record=["spikes"])
    # pop1.set_max_atoms_per_core(50)

    w = 1  # weight
    d = 0  # delay
    conns = []
    ctr = 0
    for i in range(max_spikes_per_timestep):
        for j in range(i):
            conns.append([ctr, 0, w, d])
            ctr += 1

    proj = snn.Projection(pre=stim, post=pop1, connections=conns)

    net = snn.Network("my network")
    net.add(stim, pop1, proj)

    hw = hardware.FPGA_Rev2()
    timesteps = params["t_max"] + 1
    hw.run(net, timesteps, debug=True)

    spiketimes_dict = pop1.get_spikes()
    n_expected_spikes = int(sum(range(max_spikes_per_timestep)) / max_spikes_per_timestep)
    assert len(spiketimes_dict[0]) == n_expected_spikes

    with open("results.json", "r") as f:
        results = json.load(f)
        log_raw = results["PE_4_log"]  # FPGA
        # log_raw = results["PE_37_log"] # S2 chip
        log_string = read_log(log_raw)
        log_string = log_string.decode("ASCII")
        t_spikes_all = []
        for line in log_string.split("\n"):
            if line.startswith("Profiling"):
                print(line)
                times = line.split(":")[1].split(",")
                times = [int(t) for t in times]
                print(times)
                t_spikes = times[0] - times[1]
                print("t_spikes:", t_spikes)
                t_spikes_all.append(t_spikes)
    plt.plot(t_spikes_all)
    plt.xlabel("spikes per time step")
    plt.ylabel("timer clock cycles")
    plt.title(f"{n_stim} inputs")
    plt.show()


def profile_time():
    pass


def test_helper_function():
    x_max = 100
    t_max = 100
    for x in range(255):
        t = value_to_time(x, t_max, x_max)
        x_ = time_to_value(t, t_max, x_max)
        assert x == x_, print(x, x_)


if __name__ == "__main__":
    # test_helper_function()
    run_model_time_profiling()
    # profile_memory()
