"""
test 'charge_and_spike' neuron model
"""

import os

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, helpers, snn

rng = np.random.default_rng(42)


def v_silent(input_spikes, weights, t):
    ts = np.ones_like(input_spikes) * t
    t_diff = ts - input_spikes
    t_diff = np.maximum(t_diff, 0)
    return np.dot(t_diff, weights)


def reference_model(input_spikes, weights, neuron_params, time_steps):
    """reference model of the `charge_and_spike` model."""
    n_nrns = weights.shape[1]
    assert isinstance(input_spikes, np.ndarray)
    assert isinstance(weights, np.ndarray)
    t_silent = neuron_params["t_silent"]
    i_offset = neuron_params["i_offset"]
    threshold = neuron_params["threshold"]
    assert time_steps > t_silent

    voltages = np.zeros((time_steps, n_nrns), dtype=np.float32)
    spike_times = {i: [] for i in range(n_nrns)}

    for t in range(t_silent):
        voltages[t, :] = v_silent(input_spikes, weights, t)

    neuron_has_not_spiked = np.ones(n_nrns, dtype=bool)
    for t in range(t_silent, time_steps):
        voltages_old = voltages[t - 1, :]
        voltages_new = voltages_old + neuron_has_not_spiked * i_offset
        spikes = voltages_new >= threshold
        neuron_has_not_spiked &= ~spikes

        # reset
        voltages_new = voltages_new * (np.ones(n_nrns, dtype=np.float32) - spikes)
        voltages[t, :] = voltages_new

        # fill spike_times
        for nrn in np.nonzero(spikes)[0]:
            spike_times[nrn].append(t)

    return voltages, spike_times


def test_charge_and_spike_neuron(do_plot=False, plot_times_done=False):
    """
    basic test of `charge_and_spike` neuron

    if T < t_silent:
        I_syn[t] = I_syn[t-1] + input_this_timestep
        v[t] = v[t-1] + I_syn[t]
    else:
        v[t] = v[t-1] + i_offset

    The neuron spikes only once (refractory period is set to INT32_MAX).
    """
    params = {
        "threshold": 20000.0,  # high value so neurons don't spike
        "i_offset": 500.0,
        "t_silent": 50,
    }

    time_steps = 100
    n_stim = 10
    n_nrns = 3

    # input spikes: one spike per neuron
    input_spikes = np.arange(0, n_stim, dtype=np.int32)

    max_abs_weight = 2**7 - 1
    weights = rng.integers(-max_abs_weight, max_abs_weight + 1, size=(n_stim, n_nrns), dtype=np.int8)

    spike_times = {}
    for i, t in enumerate(input_spikes):
        spike_times[i] = [t]
    stim = snn.Population(n_stim, "spike_list", params=spike_times, name="stim")

    pop = snn.Population(
        size=n_nrns,
        neuron_model="charge_and_spike",
        params=params,
        name="pop",
        record=["spikes", "v", "time_done"],
    )

    # create connections with weight identical to post neuron
    conns = []
    delay = 0
    for i in range(weights.shape[0]):
        for j in range(weights.shape[1]):
            conns.append([i, j, weights[i, j], delay])
    proj = snn.Projection(stim, pop, conns)

    # add outging projection from pop to test sending of spikes
    lif_params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
    }
    rcv_pop = snn.Population(n_nrns, "lif", params=lif_params, name="rcv_pop", record=["spikes"])

    # create one-to-one connections between pop and rcv_pop
    w = 2.0  # weight
    d = 0  # delay
    conns2 = []
    for i in range(n_nrns):
        conns2.append([i, i, w, d])

    proj2 = snn.Projection(pre=pop, post=rcv_pop, connections=conns2)

    net = snn.Network("my network")
    net.add(stim, pop, proj, rcv_pop, proj2)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    hw.run(net, time_steps)

    #########################
    # test correct behavior #
    #########################
    s2_voltages = pop.get_voltages()
    s2_spikes = pop.get_spikes()
    ref_voltages, ref_spikes = reference_model(input_spikes, weights, params, time_steps)

    # voltages
    for i, vs in enumerate(s2_voltages.values()):
        assert np.array_equal(vs, ref_voltages[:, i])

    # spikes
    for nrn_id in s2_spikes:
        assert np.array_equal(s2_spikes[nrn_id], ref_spikes[nrn_id])

    # spikes were sent to rcv_pop which spike 1 time step later
    rcv_pop_spikes = rcv_pop.get_spikes()
    for nrn_id in s2_spikes:
        for t_pop, t_rcv_pop in zip(s2_spikes[nrn_id], rcv_pop_spikes[nrn_id]):
            assert t_pop + 1 == t_rcv_pop

    if do_plot:
        times = np.arange(time_steps)
        for i, vs in enumerate(s2_voltages.values()):
            plt.plot(times, vs, label=f"S2 {i}")

        plt.gca().set_prop_cycle(None)
        for nrn in range(ref_voltages.shape[1]):
            plt.plot(ref_voltages[:, nrn], "--", label=f"ref {nrn}")

        plt.xlim(0, time_steps)
        plt.xlabel("time step")
        plt.ylabel("voltage")
        plt.legend()
        plt.show()

    # manually test time_done recording
    if plot_times_done:
        time_done_times = pop.get_time_done_times()
        helpers.save_dict_to_npz(time_done_times, "time_done_times")
        helpers.plot_times_done_multiple_pes_one_plot_vertical("time_done_times.npz")
        plt.show()


if __name__ == "__main__":
    test_charge_and_spike_neuron(do_plot=True, plot_times_done=True)
