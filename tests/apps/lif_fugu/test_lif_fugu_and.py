"""
test V reset to Vreset for Fugu LIF
This is very similar to the example for LIF Fugu.
"""

import os

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, helpers, snn


def test_lif_fugu_and(do_plot=False):
    # create stimulus population with 2 spike sources
    # Notice that neurons 5-9 reflect neurons 0-4 (flipped pattern).
    input_spikes = {0: [1, 2], 1: [1, 2, 3], 2: [2], 3: [3], 4: [1], 5: [1], 6: [3], 7: [2], 8: [1, 2, 3], 9: [1, 2]}
    # The AND neurons parallel the arrangement of the two input groups.
    # We expect the output to be two cycle delayed (one cycle for AND, one
    # cycle for sink neurons).
    expected = {0: [3], 1: [5], 2: [4], 3: [5], 4: [3]}

    stim = snn.Population(size=10, neuron_model="spike_list", params=input_spikes, name="stim")

    # Create 5 AND neurons
    # Most of these parameters are the same as default.
    neuron_params = {
        "V": 0.0,  # Start at same point as reset.
        "Vspike": 127.0,
        "decay": 0.0,  # 100% decay between cycles. Each time step is independent.
        "Vbias": 0.0,  # Only respond to input spikes, not internal drive.
        "Vreset": 0.0,
        "p": 1.0,  # always fire when we reach threshold
        "delay": 1,
    }
    pop1 = snn.Population(size=5, neuron_model="lif_fugu", params=neuron_params, name="pop1")

    # Create connection between stimulus neurons and LIF Fugu neurons.
    # LIF Fugu uses sending-side delays, so it ignores the last parameter.
    # LIF Fugu uses 7 bits for weight: [-127, 127]
    # Any weight less than 127 is sufficient to create the AND effect.
    # However, we go for roughly 0.75 of range, so 95.
    conns = []
    conns.append([0, 0, 95, 0])
    conns.append([1, 1, 95, 0])
    conns.append([2, 2, 95, 0])
    conns.append([3, 3, 95, 0])
    conns.append([4, 4, 95, 0])
    conns.append([5, 0, 95, 0])
    conns.append([6, 1, 95, 0])
    conns.append([7, 2, 95, 0])
    conns.append([8, 3, 95, 0])
    conns.append([9, 4, 95, 0])
    proj1 = snn.Projection(pre=stim, post=pop1, connections=conns)

    # Create 5 sink neurons, to verify that LIF Fugu cand send
    neuron_params = {"Vspike": 1.0}
    pop2 = snn.Population(size=5, neuron_model="lif_fugu", params=neuron_params, name="pop2", record=["spikes"])

    conns = []
    conns.append([0, 0, 10, 0])
    conns.append([1, 1, 10, 0])
    conns.append([2, 2, 10, 0])
    conns.append([3, 3, 10, 0])
    conns.append([4, 4, 10, 0])
    proj2 = snn.Projection(pre=pop1, post=pop2, connections=conns)

    # create a network and add population and projections
    net = snn.Network("my network")
    net.add(stim, pop1, pop2, proj1, proj2)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    timesteps = 6
    hw.run(net, timesteps)

    # get results and plot
    spike_times = pop2.get_spikes()

    # check that neuron spikes
    assert spike_times == expected

    if do_plot:
        fig, (ax1, ax3) = plt.subplots(2, 1, sharex=True)

        indices, times = helpers.spike_times_dict_to_arrays(input_spikes)
        ax1.plot(times, indices, "|", ms=20)
        ax1.set_ylabel("input spikes")
        ax1.set_ylim((-0.5, stim.size - 0.5))

        indices, times = helpers.spike_times_dict_to_arrays(spike_times)
        ax3.plot(times, indices, "|", ms=20)
        ax3.set_ylabel("output spikes")
        ax3.set_xlabel("time step")
        ax3.set_ylim((-0.5, pop2.size - 0.5))
        fig.suptitle("lif_fugu")
        plt.show()


if __name__ == "__main__":
    test_lif_fugu_and(do_plot=True)
