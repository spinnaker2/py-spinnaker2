"""
script to test synchronous start of all PEs
"""

import os

from spinnaker2 import hardware, snn


def test_routing_all_pes():
    n_stim = 1
    n_tgt_pes = 151
    spike_times = {0: [1, 5, 10]}
    stim = snn.Population(n_stim, "spike_list", params=spike_times, name="stim")

    lif_params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
    }
    rcv_pop = snn.Population(n_tgt_pes, "lif", params=lif_params, name="rcv_pop", record=["spikes"])
    rcv_pop.set_max_atoms_per_core(1)

    # create one-to-one connections between stim and receiver population
    w = 2.0  # weight
    d = 1  # delay

    conns = []
    for i in range(n_tgt_pes):
        conns.append([0, i, w, d])

    proj = snn.Projection(pre=stim, post=rcv_pop, connections=conns)

    net = snn.Network("my network")
    net.add(stim, rcv_pop, proj)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    time_steps = 20
    hw.run(net, time_steps, debug=False)

    # spikes were sent to rcv_pop which spike d + 1 time steps later
    rcv_pop_spikes = rcv_pop.get_spikes()
    for nrn_id in rcv_pop_spikes:
        for t_pop, t_rcv_pop in zip(spike_times[0], rcv_pop_spikes[nrn_id]):
            assert t_pop + d + 1 == t_rcv_pop


if __name__ == "__main__":
    test_routing_all_pes()
