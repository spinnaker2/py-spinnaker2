"""
script to test spike routing in lif_neuron apps.
"""

import os

from spinnaker2 import hardware, snn


def test_routing_with_lif():
    neuron_params = {
        "threshold": 1.0,
        "alpha_decay": 0.9,
    }

    n_stim = 1
    stim = snn.Population(1, "spike_list", params={0: [1, 2, 3]}, name="stim")
    pop1 = snn.Population(1, "lif", params=neuron_params, name="pop1", record=["spikes"])
    pop2 = snn.Population(1, "lif", params=neuron_params, name="pop2", record=["spikes"])

    # create dummy connections between stim and receiver population
    w = 2.0  # weight
    d = 1  # delay
    conns = [[0, 0, w, d]]
    conns2 = [[0, 0, w, d]]

    proj1 = snn.Projection(pre=stim, post=pop1, connections=conns)
    proj2 = snn.Projection(pre=pop1, post=pop2, connections=conns2)

    net = snn.Network("my network")
    net.add(stim, pop1, pop2, proj1, proj2)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    time_steps = 20
    hw.run(net, time_steps, debug=True, jtag_id=0)

    spike_times = pop1.get_spikes()
    assert len(spike_times[0]) > 0

    spike_times2 = pop2.get_spikes()
    assert len(spike_times2[0]) > 0


if __name__ == "__main__":
    test_routing_with_lif()
