"""
basic test that routing to spike_receiver population works
"""

import os

from spinnaker2 import hardware, snn


def test_routing_with_spike_receiver():
    n_stim = 10
    n_nrns = 20
    stim = snn.Population(n_stim, "spike_list", params={0: [1, 2, 3], 5: [20, 30]}, name="stim")
    pop1 = snn.Population(1, "spike_receiver", params=None, name="rcvr")
    pop2 = snn.Population(1, "spike_receiver", params=None, name="rcvr2")

    # create dummy connections between stim and receiver population
    w = 2.0  # weight
    d = 1  # delay

    conns = []
    for i in range(n_stim):
        conns.append([i, 0, w, d])

    proj = snn.Projection(pre=stim, post=pop1, connections=conns)
    proj2 = snn.Projection(pre=stim, post=pop2, connections=conns)

    net = snn.Network("my network")
    net.add(stim, pop1, pop2, proj, proj2)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    time_steps = 50
    hw.run(net, time_steps)


if __name__ == "__main__":
    test_routing_with_spike_receiver()
