"""
Test of issue13 `spike_buffer get full very fast due to dumb spike target
routing`
"""

import os

from spinnaker2 import hardware, snn


def run_model(n_neurons, neurons_per_core=None, sys_tick_in_s=1.0e-3):
    """
    create a model with a stimulus `spike_list``and a receiving population
    `lif` with one-to-one connections.

    All stimulus neurons spike in the same time step and all receiving neurons
    are supposed to spike in the subsequent time step.
    If this condition is not fulfilled, an assertion is raised.
    """

    time_steps = 100

    neuron_params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
    }

    # all neurons spike at timestep 1
    t_spike = 1
    params = {}
    for i in range(n_neurons):
        params[i] = [t_spike]

    stim = snn.Population(n_neurons, "spike_list", params=params, name="stim")
    pop1 = snn.Population(n_neurons, "lif", params=neuron_params, name="pop1", record=["spikes"])

    if neurons_per_core is not None:
        stim.set_max_atoms_per_core(neurons_per_core)
        pop1.set_max_atoms_per_core(neurons_per_core)

    # create one-to-one connections between stim and receiver population
    w = 2.0  # weight
    d = 0  # delay

    conns = []
    for i in range(n_neurons):
        conns.append([i, i, w, d])

    proj = snn.Projection(pre=stim, post=pop1, connections=conns)

    net = snn.Network("my network")
    net.add(pop1, stim, proj)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    hw.run(net, time_steps, debug=True, sys_tick_in_s=sys_tick_in_s, mapping_only=False)

    spiketimes_dict = pop1.get_spikes()

    expected_times = [t_spike + 1]

    for idx, spike_times in spiketimes_dict.items():
        assert spike_times == expected_times, idx


def test_issue13_few_neurons_per_core():
    run_model(n_neurons=240, neurons_per_core=30, sys_tick_in_s=1.0e-3)


def test_issue13_many_neurons_per_core():
    run_model(n_neurons=512, neurons_per_core=250, sys_tick_in_s=5.0e-3)


if __name__ == "__main__":
    # test_issue13_few_neurons_per_core()
    test_issue13_many_neurons_per_core()
