import os
import subprocess

import spinnaker2.hardware
from spinnaker2.configuration import (
    Experiment,
    ExperimentConfig,
    MemoryRegion,
    PEConfig,
)
from spinnaker2.coordinates import (
    PE,
    ByteAddr,
    WordAddr,
    align_addr_to_next_multiple_of_other,
)
from spinnaker2.helpers import read_log
from spinnaker2.neuron_models.common import (
    add_log_memory_region,
    collect_routing_targets,
    format_routing_targets,
)
from spinnaker2.neuron_models.spike_list import get_input_spike_rows


def abs_from_rel_path(rel_path):
    """returns the the absolute path from a path relative to this file."""
    return os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), rel_path))


def test_multi_run():
    """
    Test for multiple runs triggered by a master.

    PE1: Master PE2: variant of `input_spikes_with_routing`, sends spikes to
    PE3 PE3: variant of `input_spikes_with_routing`, only used for receiving
    spikes

    The actual experiment is performed by PE2 and PE3 two times.

    How it works:
        1. All PEs are started and then wait for an interrupt to start the time
           step based simulation
        2. Host send global interrupt via 'feedthrough interrupt 0', this will
           trigger Master to start
        3. Master will trigger a 'global feedthrough interrupt 1', which will
           start PE2 and PE3 to run an actual experiment.
        4. Master will wait for 31 systicks (1 more than the PE2 and PE3)
        5. Master will trigger another 'global feedthrough interrupt 1', which
           will start the 2nd experiment run on PE2 and PE3. It will use the
           same input data as in the first run.


    Testcase works.

    To-Dos for improvement:
        - make the number of outer runs flexible and not fixed to 2
        - make PE2 to use a different input data in 2nd run, 3rd run etc.
        - show example how master sends configuration data for each run to the
          two PEs: e.g. new input data for PE2 and a 'label' for PE3.
        - add assert statements to the test
    """
    # 1.) create spike times in correct format
    spike_times = [
        [3, 5, 25],
        [1, 2, 3],
        [1, 3, 20],
        [3, 10],
        [3, 10, 12, 20, 22, 23],
        [1, 2, 3],
    ]
    spike_times_raw = get_input_spike_rows(spike_times, key_offset=0)

    # 2.) compile PE software
    pe_testcase_dir_spike_source = abs_from_rel_path(
        "../../../../s2-sim2lab-app/chip/app-pe/s2app/multi_run_input_spikes"
    )
    subprocess.run(["make"], shell=True, check=True, cwd=pe_testcase_dir_spike_source)  # noqa

    pe_testcase_dir_master = abs_from_rel_path("../../../../s2-sim2lab-app/chip/app-pe/s2app/multi_run_master")
    subprocess.run(["make"], shell=True, check=True, cwd=pe_testcase_dir_master)  # noqa

    # 3.) run experiment

    # simulation config
    sim_cfg = {"timer_period": 1000, "n_simulation_ticks": 30}
    sim_cfg_master = {
        "timer_period": 1000,
        "n_simulation_ticks": 31,
    }  # one tick more to make sure the other PEs finish

    pe1 = PE(1, 1, 0)
    pe2 = PE(1, 1, 1)
    pe3 = PE(1, 1, 2)

    memfile_spike_source = os.path.join(pe_testcase_dir_spike_source, "binaries/s2app_arm.mem")
    memfile_master = os.path.join(pe_testcase_dir_master, "binaries/s2app_arm.mem")

    ##########
    # Master #
    ##########
    pe1_config = PEConfig(pe1, "master", memfile_master)
    # add memory region for debugging (log_info)
    add_log_memory_region(pe1_config)

    data_spec_addr = ByteAddr(0x9000)
    data_spec_size = 16  # default: 16 words

    # timer config #
    timer_config_addr = align_addr_to_next_multiple_of_other(
        ByteAddr(data_spec_addr + data_spec_size * 4), ByteAddr(0x10)
    )
    timer_config_addr = ByteAddr(timer_config_addr)
    sim_config = [sim_cfg_master["timer_period"], sim_cfg_master["n_simulation_ticks"]]
    pe1_config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

    log_addr = ByteAddr(0x0001B000)

    # data specification #
    data_spec = [
        0xAD130AD6,  # magic_number
        0x00010000,  # version
        timer_config_addr,  # start of timer config
        log_addr,  # start of log (dummy)
    ]

    pe1_config.add_mem_data_to_send(data_spec_addr.to_WordAddr(), data_spec)

    ##############
    # spike list #
    ##############
    pe2_config = PEConfig(pe2, "spike_source", memfile_spike_source)
    # add memory region for debugging (log_info)
    add_log_memory_region(pe2_config)
    log_addr = ByteAddr(0x0001B000)

    data_spec_addr = ByteAddr(0x9000)
    data_spec_size = 16  # default: 16 words

    # routing info
    target_cores = [pe3]
    key_offset = 0  # key offset is already included in spike rows

    rt_address = align_addr_to_next_multiple_of_other(ByteAddr(data_spec_addr + data_spec_size * 4), ByteAddr(0x10))
    rt_address = ByteAddr(rt_address)

    # collect and format routing targets
    tgt_qpes_and_pes = collect_routing_targets(target_cores)
    n_targets = len(tgt_qpes_and_pes)
    print("Routing targets:", tgt_qpes_and_pes)
    routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
    print("Routing targets raw:")
    for tgt_raw in routing_targets_raw:
        print("0x{:08X}".format(tgt_raw))
    routing_targets_addr = ByteAddr(rt_address + 3 * 4)  # start address of routing table
    rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw

    pe2_config.add_mem_data_to_send(rt_address.to_WordAddr(), rt_data)

    # timer config
    timer_config_addr = align_addr_to_next_multiple_of_other(ByteAddr(rt_address + len(rt_data) * 4), ByteAddr(0x10))
    timer_config_addr = ByteAddr(timer_config_addr)
    sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
    pe2_config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

    # input spikes
    input_spikes_addr = align_addr_to_next_multiple_of_other(
        ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
    )
    input_spikes_addr = ByteAddr(input_spikes_addr)
    pe2_config.add_mem_data_to_send(input_spikes_addr.to_WordAddr(), spike_times_raw)

    # data specification
    data_spec = [
        0xAD130AD6,  # magic_number
        0x00010000,  # version
        rt_address,  # start of routing table
        timer_config_addr,  # start of timer pe2_config
        input_spikes_addr,  # start of input spike rows
        log_addr,  # start of log (dummy)
    ]

    pe2_config.add_mem_data_to_send(data_spec_addr.to_WordAddr(), data_spec)

    ###################
    # spike receivers #
    ###################

    pe3_config = PEConfig(pe3, "spike_source", memfile_spike_source)

    # add memory region for debugging (log_info)
    add_log_memory_region(pe3_config)
    log_addr = ByteAddr(0x0001B000)

    data_spec_addr = ByteAddr(0x9000)
    data_spec_size = 16  # default: 16 words

    # routing info
    target_cores = []
    key_offset = 0  # key offset is already included in spike rows

    rt_address = align_addr_to_next_multiple_of_other(ByteAddr(data_spec_addr + data_spec_size * 4), ByteAddr(0x10))
    rt_address = ByteAddr(rt_address)

    # collect and format routing targets
    tgt_qpes_and_pes = collect_routing_targets(target_cores)
    n_targets = len(tgt_qpes_and_pes)
    print("Routing targets:", tgt_qpes_and_pes)
    routing_targets_raw = format_routing_targets(tgt_qpes_and_pes)
    print("Routing targets raw:")
    for tgt_raw in routing_targets_raw:
        print("0x{:08X}".format(tgt_raw))
    routing_targets_addr = ByteAddr(rt_address + 3 * 4)  # start address of routing table
    rt_data = [key_offset, n_targets, routing_targets_addr] + routing_targets_raw

    pe3_config.add_mem_data_to_send(rt_address.to_WordAddr(), rt_data)

    # timer config
    timer_config_addr = align_addr_to_next_multiple_of_other(ByteAddr(rt_address + len(rt_data) * 4), ByteAddr(0x10))
    timer_config_addr = ByteAddr(timer_config_addr)
    sim_config = [sim_cfg["timer_period"], sim_cfg["n_simulation_ticks"]]
    pe3_config.add_mem_data_to_send(timer_config_addr.to_WordAddr(), sim_config)

    # input spikes
    input_spikes_addr = align_addr_to_next_multiple_of_other(
        ByteAddr(timer_config_addr + len(sim_config) * 4), ByteAddr(0x10)
    )
    input_spikes_addr = ByteAddr(input_spikes_addr)
    empty_spike_times_raw = [0, 0, 0, 0]
    pe3_config.add_mem_data_to_send(input_spikes_addr.to_WordAddr(), empty_spike_times_raw)

    # data specification
    data_spec = [
        0xAD130AD6,  # magic_number
        0x00010000,  # version
        rt_address,  # start of routing table
        timer_config_addr,  # start of timer pe3_config
        input_spikes_addr,  # start of input spike rows
        log_addr,  # start of log (dummy)
    ]

    pe3_config.add_mem_data_to_send(data_spec_addr.to_WordAddr(), data_spec)

    exp_config = ExperimentConfig(runtime_in_s=1.0)
    exp_config.add(pe1_config, pe2_config, pe3_config)
    exp_config.synchronous_start = True

    experiment = Experiment(exp_config, spinnaker2.hardware._experiment_app_path_s2_chip())
    experiment.run(cmd_opts=["-e", os.getenv("S2_IP")])

    results = experiment.results

    for pe in [pe1, pe2, pe3]:
        log_raw = experiment.get_result(pe, "log")
        result_string = read_log(log_raw)
        print("Log ", pe)
        print(result_string.decode("ASCII"))


if __name__ == "__main__":
    test_multi_run()
