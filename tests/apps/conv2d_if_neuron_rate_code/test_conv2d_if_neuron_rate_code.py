import os

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, helpers, snn
from spinnaker2.mla import conv2d, mla_helpers


def test_conv2d_if_neuron_rate_code(plot_times_done=False):
    rng = np.random.default_rng(42)  # fails for seed = 12

    # parameters of convolution layer
    in_width = 28
    in_height = 28
    in_channels = 1
    out_channels = 4
    filter_height = 3
    filter_width = 3
    stride_y = 2
    stride_x = 2

    # create random input data in required shape

    # Input format: CHW
    input_shape = (in_channels, in_height, in_width)
    input_image = rng.integers(0, 2**8, size=input_shape, dtype=np.uint8)

    # Weight format: (H,W,CI,CO)
    weights_shape = (filter_height, filter_width, in_channels, out_channels)
    conv_weights = rng.integers(-(2**7), 2**7, size=weights_shape, dtype=np.int8)

    params = {
        "image": input_image,
        "weights": conv_weights,
        "scale": 1.0e-3,  # multiplier of weighted sum to I_offset
        "threshold": 100.0,  # spike threshold
        "stride_x": stride_x,
        "stride_y": stride_y,
    }

    timesteps = 50

    # compute output size (needed for population size)
    output_shape = (
        out_channels,  # C
        mla_helpers.conv1d_output_size(in_height, filter_height, stride_y),  # H
        mla_helpers.conv1d_output_size(in_width, filter_width, stride_x),
    )  # W
    output_size = np.prod(output_shape)

    # create population
    input_pop = snn.Population(
        size=output_size,
        neuron_model="conv2d_if_neuron_rate_code",
        params=params,
        name="input_pop",
        record=["spikes", "time_done"],
    )

    net = snn.Network("my network")
    net.add(input_pop)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    hw.run(net, timesteps)

    # manually test time_done recording
    if plot_times_done:
        time_done_times = input_pop.get_time_done_times()
        helpers.save_dict_to_npz(time_done_times, "time_done_times")
        helpers.plot_times_done_multiple_pes_one_plot_vertical("time_done_times.npz")
        plt.show()

    # get results and compare spike times to expected ones
    spike_times = input_pop.get_spikes()  # dict with neuron_ids as keys and list of spike times as value
    spike_counts = np.zeros(input_pop.size, dtype=np.int32)
    for neuron_id, spikes in spike_times.items():
        spike_counts[neuron_id] = len(spikes)
    # print("Actual spike counts:\n", spike_counts)

    # compute expected spike count

    # 1st step: calculate conv2d on host
    # change from CHW to NHWC (needed by conv2d.conv2d)
    input_image_reorded = np.moveaxis(input_image, 0, -1)
    input_image_reorded = np.expand_dims(input_image_reorded, 0)  # add batch dimension

    conv_out = conv2d.conv2d(
        input_image_reorded.astype(np.int32),
        conv_weights.astype(np.int32),
        pad="VALID",
        stride=(stride_y, stride_x),
    )

    conv_out = np.moveaxis(conv_out, -1, 1)  # change from NHWC to NCHW
    out_flat = conv_out.flatten()
    out_flat = out_flat.clip(min=0).astype(np.int32)

    # 2nd step: calc expected spike depending on scale and threshold
    # max_theo_output_value = filter_width * filter_height * 2**7 *2**8
    # print("Max theoretical output value:", max_theo_output_value)
    # print("Max output value (actual):", out_flat.max())
    spike_counts_expected = out_flat * np.single(params["scale"]) * timesteps / np.single(params["threshold"])
    spike_counts_expected_f = np.copy(spike_counts_expected)
    # round down to integer
    spike_counts_expected = np.floor(spike_counts_expected).astype(np.int32)
    spike_counts_expected = spike_counts_expected.clip(max=timesteps)
    # print("Expected spike counts:\n", spike_counts_expected)

    check = np.array_equal(spike_counts, spike_counts_expected)
    assert check
    if check == False:
        print("FAIL!")
        print(spike_counts_expected - spike_counts)
        wrong_indices = np.where(spike_counts != spike_counts_expected)
        for idx in wrong_indices[0]:
            print(
                idx,
                ": actual_count:",
                spike_counts[idx],
                "theo count: ",
                spike_counts_expected_f[idx],
            )


if __name__ == "__main__":
    test_conv2d_if_neuron_rate_code()
