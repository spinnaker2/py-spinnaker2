"""
Test for more than 5 input spikes per time step
"""

import os

from spinnaker2 import hardware, snn


def test_many_input_spikes():
    n_stim = 12
    spike_times = {}
    time_offset = 0
    for i in range(n_stim):
        spike_times[i] = range(time_offset + i, time_offset + n_stim)
    stim = snn.Population(n_stim, "spike_list", params=spike_times, name="stim")
    lif_params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
    }
    rcv_pop = snn.Population(n_stim, "lif", params=lif_params, name="rcv_pop", record=["spikes"])

    # create one-to-one connections between stim and receiver population
    w = 2.0  # weight
    d = 1  # delay

    conns = []
    for i in range(n_stim):
        conns.append([i, i, w, d])

    proj = snn.Projection(pre=stim, post=rcv_pop, connections=conns)

    net = snn.Network("my network")
    net.add(rcv_pop, stim, proj)

    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    time_steps = 50
    hw.run(net, time_steps)

    # spikes were sent to rcv_pop which spike d + 1 time steps later
    rcv_pop_spikes = rcv_pop.get_spikes()
    for nrn_id in spike_times:
        for t_pop, t_rcv_pop in zip(spike_times[nrn_id], rcv_pop_spikes[nrn_id]):
            assert t_pop + d + 1 == t_rcv_pop


if __name__ == "__main__":
    test_many_input_spikes()
