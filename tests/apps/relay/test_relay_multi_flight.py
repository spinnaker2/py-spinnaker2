"""
test realy neuron with multiple spikes in flight
This is very similar to the example for using the relay neuron.
"""

import os

import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import hardware, helpers, snn


def test_relay_multi_flight(do_plot=False):
    # create stimulus
    input_spikes = {0: [1, 7, 23]}

    stim = snn.Population(size=1, neuron_model="spike_list", params=input_spikes, name="stim")

    # Create relay neuron
    neuron_params = {"delay": 10}

    pop1 = snn.Population(size=1, neuron_model="relay", params=neuron_params, name="pop1", record=["spikes"])

    # Create a sink neuron
    neuron_params = {"Vspike": 1.0}

    pop2 = snn.Population(size=1, neuron_model="lif_fugu", params=neuron_params, name="pop2", record=["spikes"])

    # Make connections
    # The relay neuron ignores both weight and delay.
    # Instead, it always fires when it receives a spike, and imposes delay
    # (set in neuron parameters) on the outgoing spike.
    conns = []
    conns.append([0, 0, 0, 0])
    proj1 = snn.Projection(pre=stim, post=pop1, connections=conns)

    conns = []
    conns.append([0, 0, 10, 0])
    proj2 = snn.Projection(pre=pop1, post=pop2, connections=conns)

    # create a network and add populations and projections
    net = snn.Network("my network")
    net.add(stim, pop1, pop2, proj1, proj2)

    # select hardware and run network
    hw = hardware.SpiNNaker2Chip(eth_ip=os.getenv("S2_IP"))
    timesteps = 40  # Sufficient for delay 17 to show up.
    hw.run(net, timesteps)

    # get results and plot
    spike_times = pop2.get_spikes()
    spikes = spike_times[0]

    # check spike pattern
    assert len(spikes) == 3
    assert spikes[0] == 12 and spikes[1] == 18 and spikes[2] == 34

    if do_plot:
        fig, (ax1, ax3) = plt.subplots(2, 1, sharex=True)

        indices, times = helpers.spike_times_dict_to_arrays(input_spikes)
        ax1.plot(times, indices, "|", ms=20)
        ax1.set_ylabel("input spikes")
        ax1.set_ylim((-0.5, stim.size - 0.5))

        indices, times = helpers.spike_times_dict_to_arrays(spike_times)
        ax3.plot(times, indices, "|", ms=20)
        ax3.set_ylabel("output spikes")
        ax3.set_xlabel("time step")
        ax3.set_ylim((-0.5, pop2.size - 0.5))
        fig.suptitle("relay neuron")
        plt.show()


if __name__ == "__main__":
    test_relay_multi_flight(do_plot=True)
