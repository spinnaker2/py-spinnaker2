import copy

import numpy as np
from spinnaker2.mla import conv2d

params_conv = {
    "op_a_use_noc": 1,
    "op_a_is_signed": 1,
    "op_b_is_signed": 0,
    "op_a_is_16bit": 0,
    "op_b_is_16bit": 0,
    "output_shift_width": 8,
    "output_mode": 0,  # only tested for 32 bit output
    "relu_en": 1,
    "in_batch_s": 1,
    "in_pix_row_s": 32,
    "in_pix_col_s": 32,
    "fil_pix_row_s": 3,
    "fil_pix_col_s": 3,
    "in_channel_s": 1,
    "out_channel_s": 4,
    "stride_in_pix_row": 0,
    "stride_in_pix_col": 0,
    # 'signed_i': 0,
    # 'signed_w': 1,
    # 'dtype_i': 0,
    "dtype_w": 0,
    # 'dtype_o': 2,
    # 'output_shift': 0,
}


def get_sequence(Dataset, sample, time_steps):
    frames = np.array([])
    for i in range(time_steps):
        frames = np.append(frames, [Dataset.img[sample][i]])
    frames = np.reshape(frames, (time_steps, 32, 32))
    return frames


def permutation(in_shape):
    a = np.zeros(np.prod(in_shape))
    for i in range(len(a)):
        a[i] = i
    a = np.reshape(a, in_shape)
    b = copy.copy(a)
    b = np.moveaxis(b, 2, 0)
    a_fl = np.reshape(a, -1)
    b_fl = np.reshape(b, -1)
    # c = np.ones((2048,20))
    # for i in range(2048):
    #     c[i,:] = i
    b_fl = b_fl.astype(int)
    # c_sorted = c[list(b_fl)]

    return b_fl


def build_conns(pre, post, weights):
    conns = []
    delay = 0
    for i in range(0, pre):
        for j in range(0, post):
            conns.append([i, j, weights[i][j], delay])  # weights from LIF layer of model, delay is zero
    return conns


def calc_pad(images: list, filter: list, stride: list):
    images, filter, stride = np.array(images), np.array(filter), np.array(stride)
    # pad_mode = ''
    pad = [[0, 0], [0, 0]]

    # pad_val = ((stride_v-1)*input_height-stride_v+filter_height)
    output_size = ((images - filter) / stride) + 1
    for i, elements in enumerate(output_size):
        if isinstance(elements, float):
            # pad_mode = 'SAME'
            output_size[i] = int(np.ceil(elements))
            sym_pad = ((output_size[i] - 1) * stride[i] - images[i] + filter[i]) / 2
            if isinstance(sym_pad, int):
                pad[i][0], pad[i][1] = sym_pad, sym_pad

            elif isinstance(sym_pad, float):
                pad[i][0] = int(np.floor(sym_pad))
                pad[i][1] = int(np.ceil(sym_pad))
        else:
            # pad_mode = 'INVALID'
            raise ValueError
        pad[i] = tuple(pad[i])

    # image_padded = np.pad(images, [(pad_first[0], pad_second[0]), (pad_first[1], pad_second[1])], mode='constant')
    return output_size, pad


def apply_pad(images: np.ndarray, padding: list):
    padded_im = []
    for image in images:
        padded_im.append(np.pad(image, padding, mode="constant"))
    return np.stack(padded_im, axis=-1)


def pack_int8_into_int32(data_to_pack):  # data_to_pack should be a 1D np.array
    packed_list = list()
    data_to_pack = data_to_pack.astype(np.uint8)
    for i in range(0, len(data_to_pack) - 3, 4):
        packed_list.append(
            (
                (data_to_pack[i] | data_to_pack[i + 1] << 8 | data_to_pack[i + 2] << 16 | data_to_pack[i + 3] << 24)
                & 0xFFFFFFFF
            ).tolist()
        )
    packed_list = np.array(packed_list, dtype=np.uint32)
    packed_list = packed_list.tolist()
    return packed_list


def unpack_int8_from_int32(data_to_unpack):
    data_to_unpack = np.array(data_to_unpack, dtype=np.uint32)
    unpacked_array = np.array([], dtype=np.int8)
    for i in data_to_unpack:
        unpacked_array = np.append(
            unpacked_array,
            [
                (i & 0x000000FF).astype(np.int8),
                ((i >> 8) & 0x000000FF).astype(np.int8),
                ((i >> 16) & 0x000000FF).astype(np.int8),
                ((i >> 24) & 0x000000FF).astype(np.int8),
            ],
        )
    return unpacked_array


def quantize_sequence(sequence, dtype=np.int8):
    results = []
    for sample in sequence:
        results.append(np.reshape(quantize_matrix(sample, dtype), np.shape(sequence)[1:]))
    return np.stack(results, axis=0)


def quantize_matrix(matrix, dtype=np.int8):
    out_dtype = np.uint16 if dtype == np.uint8 else np.int16
    quantized_image = np.array([], dtype=dtype)
    scaling_factor = 255 if dtype == np.uint8 else 127
    for value in matrix:
        quantized_image = np.append(quantized_image, np.array([np.floor(value * scaling_factor)]))
    return quantized_image.astype(out_dtype)


def check_classification(label, output_pop):
    output_voltages = output_pop.get_voltages()
    for key, value in output_voltages.items():
        output_voltages[key] = value[-1].tolist()
    classification_pe = list(output_voltages.values()).index(max(output_voltages.values()))
    class_list = label.tolist()
    classification_host = class_list.index(max(class_list))
    if classification_host == classification_pe:
        return 1
    else:
        return 0


def estimate_scaling_factor(weights, dataset, strides):
    sf = []
    for sample in dataset:
        sf.append(get_scaling_factor(sample, weights, strides))
    return np.max(sf)


def get_scaling_factor(sequence, weights, strides, output_is_signed=True):
    """From quantized images and weights, calculate the output scaling factor"""
    if output_is_signed:
        base = 7
    else:
        base = 8
    conv_result = []
    im_heigt, im_width = sequence[0, 0].shape
    filter_height, filter_width, _, _ = weights.shape
    stride_v, stride_h = 1 << strides[1], 1 << strides[0]
    output_size, paddings = calc_pad([im_heigt, im_width], [filter_height, filter_width], [stride_v, stride_h])
    for image in sequence:
        padded_image = apply_pad(image, paddings)
        padded_image = np.expand_dims(padded_image, axis=0)
        conv_result.append(
            conv2d.conv2d(
                padded_image.astype(np.int32),
                weights.astype(np.int32),
                pad="VALID",
                stride=(stride_v, stride_h),
            )
        )
    conv_result = np.stack(conv_result, axis=0)
    scaling_factor = np.ceil(np.log2(np.max(np.abs(conv_result))) - base) if np.max(np.abs(conv_result)) != 0 else 0
    return int(scaling_factor)


def prepare_input_sequence(sequence):
    return pack_int8_into_int32(sequence.ravel())


def check_convolution(mla_config, conv_out, expected_out):
    expected_out = expected_out.ravel()
    if mla_config["output_mode"] == 2:
        conv_out = np.array(conv_out, dtype=np.uint32)

        if (conv_out == expected_out).all():
            print("SUCCESS!")
        else:
            print("FAIL!")
            wrong_indices = np.where(conv_out != expected_out)
            for idx in wrong_indices[0]:
                print(f"{idx}: actual value: {conv_out[idx]}, theoretical value: {expected_out[idx]}")

    elif mla_config["output_mode"] == 0:
        conv_out = unpack_int8_from_int32(conv_out)
        conv_out = conv_out.astype(np.int8)
        # conv_out = conv_out[:len(expected_out)]
        first_half = conv_out[: len(expected_out)]
        second_half = conv_out[len(expected_out) :]

        if (first_half == expected_out).all() or (second_half == expected_out).all():
            print("SUCCESS!")
        else:
            print("FAIL!")
            wrong_indices1 = np.where(first_half != expected_out)
            wrong_indices2 = np.where(second_half != expected_out)
            print(f"{min(len(wrong_indices1[0]), len(wrong_indices2[0]))} wrong indices")


def get_mem_tensors(conv_config, c_weights, images, output_scaling_factor):
    print("NOTE: Get Tensors...")
    batch_size = conv_config["in_batch_s"]
    input_height = conv_config["in_pix_row_s"]
    input_width = conv_config["in_pix_col_s"]
    filter_height = conv_config["fil_pix_row_s"]
    filter_width = conv_config["fil_pix_col_s"]
    in_channels = conv_config["in_channel_s"]
    out_channels = conv_config["out_channel_s"]
    stride_ver = conv_config["stride_in_pix_row"]
    stride_hor = conv_config["stride_in_pix_col"]
    stride_v = 1 << stride_ver
    stride_h = 1 << stride_hor

    output_size, paddings = calc_pad([input_height, input_width], [filter_height, filter_width], [stride_v, stride_h])

    output_height = int(output_size[0])
    output_width = int(output_size[1])
    input_height = int(input_height + sum(paddings[0]))
    input_width = int(input_width + sum(paddings[1]))

    # conv_config['in_pix_row_s'] = input_height
    # conv_config['in_pix_col_s'] = input_width

    dtype_w = conv_config["dtype_w"]

    zero_fill = lambda x, y: (y - (x % y)) % y
    mem_align = lambda x, y: x + (zero_fill(x, y))

    # get random data
    data_type_i, low_i, high_i, bmask_i, align_i = (np.uint8, 0, 2**8 - 1, 0xFF, 16)
    data_type_w, low_w, high_w, bmask_w, align_w = (np.int8, -(2**7), 2**7 - 1, 0xFF, 4)
    data_type_o, low_o, high_o, bmask_o, align_o = (np.int32, -(2**31), 2**31 - 1, 0xFFFFFFFF, 16)

    if dtype_w == 0:
        num_rows = 4
    elif dtype_w == 1:
        num_rows = 2
    else:
        raise ValueError

    shape_i = [batch_size, input_height, input_width, in_channels]
    shape_w = [filter_height, filter_width, in_channels, out_channels]
    shape_o = [batch_size, output_height, output_width, out_channels]

    zeros_shape_i = [batch_size, in_channels, input_height, zero_fill(input_width, align_i)]
    zeros_shape_w = [in_channels, filter_height, filter_width, zero_fill(out_channels, align_w)]
    zeros_shape_o = [batch_size, out_channels, output_height, zero_fill(output_width, align_o)]
    shape_w_inter = [
        in_channels,
        filter_height,
        filter_width,
        mem_align(out_channels, align_w) // num_rows,
        num_rows,
    ]

    rand_w = c_weights
    rand_w = rand_w.reshape(shape_w)
    # bring axis to shape inside the memory
    # change axis for correct dimension order
    rand_w_spin = np.moveaxis(rand_w, 2, 0)
    # fill memory line with 0 zeros due to alignment
    rand_w_spin = np.concatenate((rand_w_spin, np.zeros(zeros_shape_w, dtype=data_type_w)), axis=-1)

    # split out_channel into num_rows batches
    rand_w_spin = np.reshape(rand_w_spin, shape_w_inter)
    # change axis for correct dimension order
    rand_w_spin = np.moveaxis(rand_w_spin, 3, 0)
    # flatten tensor and prepare for file generation
    rand_w_spin = np.ravel(rand_w_spin)

    rank_images = np.ndim(images)
    if rank_images == 3:
        images = np.expand_dims(images, axis=1)

    rand_i_array = np.array([])
    rand_i_array2 = np.array([])
    rand_o_array = np.array([])
    rand_o_shifted = []

    for test_image in images:
        padded_image = apply_pad(test_image, paddings)
        rand_i = padded_image
        rand_i = np.reshape(rand_i, shape_i)
        rand_o = conv2d.conv2d(
            rand_i.astype(np.int32),
            rand_w.astype(np.int32),
            pad="VALID",
            stride=(stride_v, stride_h),
        )
        rand_o = np.int32(rand_o)  # cast so shift works
        if not isinstance(rand_o, np.ndarray):
            rand_o = np.array([rand_o])
            rand_o = np.reshape(rand_o, shape_o)

        # rand_o[rand_o < low_o] = low_o
        # rand_o[rand_o >= high_o] = high_o
        rand_o = np.array(rand_o, dtype=data_type_o)

        # bring axis to shape inside the memory
        # change axis for correct dimension order
        rand_i = np.moveaxis(rand_i, 3, 1)
        # [Batch, in_Channel, Height, Width]
        # fill memory line with 0 zeros due to alignment
        rand_i = np.concatenate((rand_i, np.zeros(zeros_shape_i, dtype=data_type_i)), axis=-1)

        # flatten tensor and prepare for file generation
        rand_i = np.ravel(rand_i)
        rand_i_array = np.append(rand_i_array, np.ravel(test_image))
        rand_i_array2 = np.append(rand_i_array2, rand_i)
        # bring axis to shape inside the memory
        # change axis for correct dimension order
        rand_o = np.moveaxis(rand_o, 3, 1)
        # [Batch, Out_Channel, Height, Width]
        # fill memory line with 0 zeros due to alignment
        rand_o = np.concatenate((rand_o, np.zeros(zeros_shape_o, dtype=data_type_o)), axis=-1)
        # flatten tensor and prepare for file generation
        rand_o_flatten = np.ravel(rand_o)
        rand_o_array = np.append(rand_o_array, rand_o_flatten)

        # rand_o[rand_o > 100] = 127
        rand_o = rand_o.astype(np.int32)
        rand_o = np.round(rand_o / 2**output_scaling_factor)

        if conv_config["output_mode"] == 0:
            rand_o = rand_o.astype(np.int8)
        elif conv_config["output_mode"] == 1:
            rand_o = rand_o.astype(np.int16)
        elif conv_config["output_mode"] == 2:
            rand_o = rand_o.astype(np.int32)

        if conv_config["relu_en"] == 1:
            rand_o[rand_o < 0] = 0
        rand_o_shifted.append(rand_o)

    rand_o_shifted = np.squeeze(np.stack(rand_o_shifted, axis=1), axis=0)
    conv_config["rand_i"] = rand_i_array
    conv_config["rand_w"] = rand_w
    conv_config["rand_o"] = rand_o_array

    # images = rand_i
    images = pack_int8_into_int32(rand_i_array)
    rand_i_array2 = pack_int8_into_int32(rand_i_array2)
    conv_weights = rand_w_spin
    conv_weights = conv_weights.flatten()
    conv_weights = pack_int8_into_int32(conv_weights)

    params_conv = {
        "mla_config": conv_config,
        "images": images,
        "weights": conv_weights,
        "images_proc": rand_i_array2,
    }
    output_shapes = [output_height, output_width, out_channels]

    return params_conv, output_shapes, rand_o_shifted
