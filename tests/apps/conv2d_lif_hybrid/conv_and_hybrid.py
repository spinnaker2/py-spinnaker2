import copy
import json

import helper
import numpy as np
from spinnaker2 import hardware, snn

np.random.seed(45)  # TODO: Not all random seeds work -> rounding issues

# global params
n_hidden_nrns = 50
n_output_nrns = 12
timesteps = 40

# parameters of convolution layer 1
in_width = 32
in_height = 32
in_channels = 1
out_channels = 4
filter_height = 3
filter_width = 3
stride_y = 1
stride_x = 1

test_images = np.random.randint(0, 2**8 - 1, size=(timesteps, 1, in_height, in_width), dtype=np.uint8)  # 15 images

c_weights = np.random.randint(
    -(2**7), 2**7 - 1, size=(filter_height, filter_width, in_channels, out_channels), dtype=np.int8
)

output_scaling_factor = helper.get_scaling_factor(test_images, c_weights, (stride_x, stride_y), output_is_signed=True)

params_conv = copy.copy(helper.params_conv)
params_conv.update(
    {
        "in_pix_row_s": in_height,
        "in_pix_col_s": in_width,
        "fil_pix_row_s": filter_height,
        "fil_pix_col_s": filter_width,
        "in_channel_s": in_channels,
        "out_channel_s": out_channels,
        "stride_in_pix_row": stride_y,
        "stride_in_pix_col": stride_x,
        "output_mode": 0,
        "output_shift_width": output_scaling_factor,
    }
)
params_conv, output_shape, conv_result = helper.get_mem_tensors(
    params_conv, c_weights, test_images, output_scaling_factor
)

# parameters of convolution layer 2
out_channels2 = 8
filter_height2 = 3
filter_width2 = 3
stride_y2 = 1
stride_x2 = 1

c_weights2 = np.random.randint(
    -(2**7),
    2**7 - 1,
    size=(filter_height2, filter_width2, output_shape[-1], out_channels2),
    dtype=np.int8,
)

output_size2, paddings2 = helper.calc_pad(
    [output_shape[0], output_shape[1]],
    [filter_height2, filter_width2],
    [1 << stride_y2, 1 << stride_x2],
)

lif_weights2 = np.random.randint(
    -(2**7),
    2**7 - 1,
    size=(np.prod(output_size2, dtype=int) * out_channels2, n_hidden_nrns),
    dtype=np.int16,
)

recurrent_lif_weights2 = np.random.randint(-(2**7), 2**7 - 1, size=(n_hidden_nrns * n_hidden_nrns), dtype=np.int16)

out_weights2 = np.random.randint(-(2**7), 2**7 - 1, size=(n_hidden_nrns, n_output_nrns), dtype=np.int16)


output_scaling_factor2 = helper.get_scaling_factor(
    conv_result, c_weights2, (stride_x2, stride_y2), output_is_signed=True
)

params_conv2 = copy.copy(helper.params_conv)
params_conv2.update(
    {
        "in_pix_row_s": output_shape[0],
        "in_pix_col_s": output_shape[1],
        "fil_pix_row_s": filter_height2,
        "fil_pix_col_s": filter_width2,
        "in_channel_s": output_shape[2],
        "out_channel_s": out_channels2,
        "stride_in_pix_row": stride_y2,
        "stride_in_pix_col": stride_x2,
        "output_mode": 0,
        "output_shift_width": output_scaling_factor2,
    }
)

params_conv2, output_shape2, conv_result2 = helper.get_mem_tensors(
    params_conv2, c_weights2, conv_result, output_scaling_factor2
)
# conv result is padded in width, therefore 16 instead of 8

neuron_params = {"alpha_decay": 0.9, "threshold": 1}

permuted_quant_lif_weights = lif_weights2[list(helper.permutation(output_shape2))]
transposed_quant_lif_weights = np.ravel(permuted_quant_lif_weights)
packed_quantized_recurrent_lif_weights = helper.pack_int8_into_int32(recurrent_lif_weights2.flatten())

lif_weights = helper.pack_int8_into_int32(transposed_quant_lif_weights)
rec_lif_weights = packed_quantized_recurrent_lif_weights

mla_config = params_conv["mla_config"]
mla_config2 = params_conv2["mla_config"]

seq_meta = {"image_addr": 0xC000, "conv_res_addr": 0xD000}

image_storage_params = {"image_addr": seq_meta["image_addr"], "images": params_conv["images"]}

conv_res_size = int(np.prod(output_shape)) + output_shape[1] % 16 * output_shape[0] * output_shape[2]
conv2d_params = {
    "mla_config": mla_config,
    "image_addr": seq_meta["image_addr"],
    "image_size": int(in_height * in_width),
    "conv_res_addr": seq_meta["conv_res_addr"],
    "conv_res_size": conv_res_size,
    "conv_weights": params_conv["weights"],
}

conv_res_size2 = int(np.prod(output_shape2)) + output_shape2[1] % 16 * output_shape2[0] * output_shape2[2]
hybrid_params = {
    "mla_config": mla_config2,
    "image_addr": seq_meta["image_addr"],
    "image_size": conv2d_params["conv_res_size"],
    "conv_res_addr": seq_meta["conv_res_addr"],
    "conv_res_size": conv_res_size2,
    "conv_weights": params_conv2["weights"],
    "forw_lif_weights": lif_weights,
    "rec_lif_weights": rec_lif_weights,
    "neuron_params": neuron_params,
}

image_storage_pop = snn.Population(
    size=1,
    neuron_model="image_storage",
    params=image_storage_params,
    name="image_storage",
    record=[],
)
conv2d_pop = snn.Population(size=1, neuron_model="conv2d", params=conv2d_params, name="conv2d_pop", record=[])
conv_lif_hybrid_pop = snn.Population(
    size=n_hidden_nrns,
    neuron_model="conv_lif_hybrid",
    params=hybrid_params,
    name="conv_lif_hybrid_pop",
    record=["spikes"],
)
# if_pop = snn.Population(size=n_output_nrns, neuron_model="lif_no_delay", params=lif_no_delay_params,
#                         name="if_pop", record=['spikes'])


dummy_conn = helper.build_conns(1, 1, [[0], [0]])
# output_conn = helper.build_conns(conv_lif_hybrid_pop.size, if_pop.size, out_weights)

in_conv_conn = snn.Projection(pre=image_storage_pop, post=conv2d_pop, connections=dummy_conn)
conv_hybrid_conn = snn.Projection(pre=conv2d_pop, post=conv_lif_hybrid_pop, connections=dummy_conn)
# out_projection = snn.Projection(pre=conv_lif_hybrid_pop, post=if_pop, connections=output_conn)

net = snn.Network("my network")
net.add(image_storage_pop, conv2d_pop, conv_lif_hybrid_pop, in_conv_conn, conv_hybrid_conn)

hw = hardware.FPGA_Rev2()
hw.run(network=net, time_steps=timesteps)

# %% Check Results
with open("results.json", "r") as f:
    results = json.load(f)

conv_out = results["PE_5_result"]
helper.check_convolution(mla_config, conv_out, conv_result[timesteps - 1])

conv_out2 = results["PE_6_result"]
helper.check_convolution(mla_config2, conv_out2, conv_result2[timesteps - 2])

print("done")
