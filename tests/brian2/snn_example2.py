import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import brian2_sim, hardware, snn

n_stim = 10
n_nrns = 20

neuron_params = {
    "threshold": 2.0,
    "alpha_decay": 0.9,
}

neuron_params2 = {
    "threshold": [2.0 + 0.001 * i for i in range(n_nrns)],
    "alpha_decay": [0.9 + 0.001 * i for i in range(n_nrns)],
}

stim = snn.Population(n_stim, "spike_list", params={0: [1, 2, 3], 5: [20, 30]}, name="stim")
pop1 = snn.Population(n_nrns, "lif", params=neuron_params, name="pop1", record=["spikes", "v"])
pop2 = snn.Population(n_nrns, "lif", params=neuron_params2, name="pop2", record=["spikes", "v"])


w = 20.0  # weight
d = 0  # delay
conns = []
conns.append([0, 1, w, d])
conns.append([0, 0, -w, d])
conns.append([5, 3, w, d])
conns.append([5, 4, w, d])
conns.append([5, 5, -w, d])

proj = snn.Projection(pre=stim, post=pop1, connections=conns)


net = snn.Network("my network")
net.add(stim, pop1, proj)

if True:
    conns2 = []
    conns2.append([1, 0, w, d])
    proj2 = snn.Projection(pre=pop1, post=pop2, connections=conns2)
    net.add(pop2, proj2)

timesteps = 50

hw = hardware.FPGA_Rev2()
hw.run(net, timesteps)

# brian2 = brian2_sim.Brian2Backend()
# brian2.run(net, timesteps)

spike_times = pop1.get_spikes()
print(spike_times)
spike_times2 = pop2.get_spikes()
print(spike_times2)

voltages = pop1.get_voltages()

times = np.arange(timesteps)
plt.plot(times, voltages[0], label="Neuron 0")
plt.plot(times, voltages[1], label="Neuron 1")
plt.plot(times, voltages[2], label="Neuron 2")
plt.plot(times, voltages[3], label="Neuron 3")
plt.plot(times, voltages[4], label="Neuron 4")
plt.plot(times, voltages[5], label="Neuron 5")
plt.xlim(0, timesteps)
plt.xlabel("time step")
plt.ylabel("voltage")
plt.legend()
plt.show()
