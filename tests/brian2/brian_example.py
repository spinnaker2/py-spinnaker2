#!/usr/bin/env python
"""
Brian2 example for lif neuron
"""
import numpy as np
from brian2 import *

defaultclock.dt = 1 * ms
dt = defaultclock.dt
print(dt)

n_in = 1  # number of stimuli
n_out = 1  # number of output neurons

duration = 1 * second

v_th = 1.0

alpha_decay = 0.9
tau = dt / (1 - alpha_decay)
print(f"tau: {tau}")


#############
# Equations #
#############

# Derivation
"""
v = alpha_decay*v

v = v -v*dt/tau
  = v(1-dt/tau)
alpha_decay = (1-dt/tau)
dt/tau = 1-alpha_decay
tau = dt/(1-alpha_decay)
"""


# Leaky integrate and fire model
eqs_lif = """
dv/dt = -v / tau : 1
# """


# reset by subtraction
reset_by_subtraction = "v -= v_th"

# Input neurons
stim = SpikeGeneratorGroup(n_in, indices=[0, 0, 0], times=[1, 2, 3] * ms)

# Output neurons
group = NeuronGroup(
    n_out,
    eqs_lif,
    threshold="v > v_th",
    reset=reset_by_subtraction,
    refractory=1 * ms,
    method="euler",
)

S = Synapses(stim, group, model="""w:1""", on_pre="v+=w")
S.connect()  # connect all neuron pairs (fully connected layer)

S.w[0, 0] = 2.0

# set up monitors
s_monitor = SpikeMonitor(group)
s_monitor_stim = SpikeMonitor(stim)
v_monitor = StateMonitor(group, "v", record=True)

# run simulation
run(duration)

# plot results
figure()
plot(s_monitor_stim.t / ms, s_monitor_stim.i, ".k")
xlabel("Time (ms)")
ylabel("Neuron index")
title("Stimulus spikes")

figure()
plot(s_monitor.t / ms, s_monitor.i, ".k")
xlabel("Time (ms)")
ylabel("Neuron index")
title("Neuron spikes")

# plot membrane voltage of first and last neuron
figure()
plot(v_monitor.t / ms, v_monitor.v[0], label="Neuron 0")
# plot(v_monitor.t/ms, v_monitor.v[n_out-1]/mV, label='Neuron {}'.format(n_out-1))
xlabel("Time (ms)")
ylabel("v")
legend()
title("Membrane voltage trace")

show()
