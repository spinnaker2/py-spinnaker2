import matplotlib.pyplot as plt
import numpy as np
from spinnaker2 import brian2_sim, hardware, snn

plot_flag = False  # global flag for plotting results


def run_model(backend, input_spikes, neuron_model, neuron_params, weight, delay, timesteps):
    """
    Runs a simple model with 1 spike source and 1 neuron.
    """

    stim = snn.Population(size=1, neuron_model="spike_list", params={0: input_spikes}, name="stim")

    pop1 = snn.Population(size=1, neuron_model=neuron_model, params=neuron_params, name="pop1", record=["spikes", "v"])

    conns = [[0, 0, weight, delay]]

    proj = snn.Projection(pre=stim, post=pop1, connections=conns)

    net = snn.Network("my network")
    net.add(stim, pop1, proj)

    if backend == "fpga":
        hw = hardware.FPGA_Rev2()
        hw.run(net, timesteps)
    elif backend == "spinnaker2":
        hw = hardware.SpiNNaker2Chip()
        hw.run(net, timesteps)
    elif backend == "brian2":
        brian2 = brian2_sim.Brian2Backend()
        brian2.run(net, timesteps)
    else:
        raise ValueError("backend must be 'fpga', 'spinnaker2', or 'brian2'")

    voltages = pop1.get_voltages()[0]
    spikes = pop1.get_spikes()[0]
    return dict(voltages=voltages, spikes=spikes)


def compare(backends, input_spikes, neuron_model, neuron_params, weight, delay, timesteps, plot=False):
    """compare two backends with each other"""
    results = {}
    assert len(backends) == 2
    for backend in backends:
        results[backend] = run_model(backend, input_spikes, neuron_model, neuron_params, weight, delay, timesteps)

    test_results = {}

    # spike counts
    spike_count_equal = len(results[backends[0]]["spikes"]) == len(results[backends[1]]["spikes"])
    test_results["spike_count"] = spike_count_equal

    if spike_count_equal:
        # spike times are exactly the same
        if results[backends[0]]["spikes"] == results[backends[1]]["spikes"]:
            test_results["spike_times"] = True
        else:
            test_results["spike_times"] = False

        # spike times deviate by maximum 1 timestep
        diff = np.array(results[backends[0]]["spikes"]) - np.array(results[backends[1]]["spikes"])
        diff_smaller_than_1 = np.abs(diff) <= 1
        test_results["spike_times_within_1_timestep"] = diff_smaller_than_1.all()
    else:
        print(backends[0], results[backends[0]]["spikes"])
        print(backends[1], results[backends[1]]["spikes"])

    if plot:
        times = np.arange(timesteps)
        for backend in backends:
            plt.plot(times, results[backend]["voltages"], label=backend)
        plt.axhline(y=neuron_params["threshold"], ls="--", c="0.5")
        plt.xlim(0, timesteps)
        plt.xlabel("time step")
        plt.ylabel("voltage")
        plt.legend()
        plt.show()
    return test_results


def compare_lif_curr_exp():
    neuron_model = "lif_curr_exp"
    neuron_params = {
        "threshold": 8.01,
        "alpha_decay": 1.0,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_by_subtraction",
        "exc_decay": 0.5,
        "inh_decay": 0.2,
        "t_refrac": 0,
    }
    input_spikes = [1, 7, 10, 15]
    weight = 3.0
    delay = 2
    timesteps = 20
    backends = ["brian2", "fpga"]
    test_results = compare(
        backends,
        input_spikes,
        neuron_model,
        neuron_params,
        weight,
        delay,
        timesteps,
        plot=plot_flag,
    )

    print("compare_lif_curr_exp")
    if test_results["spike_times"] or test_results["spike_times_within_1_timestep"]:
        print("SUCCESS")
    else:
        print("FAIL")


def compare_lif_curr_exp_no_delay():
    neuron_model = "lif_curr_exp_no_delay"
    neuron_params = {
        "threshold": 5.01,
        "alpha_decay": 0.9,
        "i_offset": 0.0,
        "v_reset": 0.0,
        "reset": "reset_to_v_reset",
        "exc_decay": 0.3,
        "inh_decay": 0.4,
        "t_refrac": 2,
    }
    input_spikes = [10, 15, 30, 32, 34]
    weight = -20.0
    delay = 0
    timesteps = 50
    backends = ["brian2", "fpga"]
    test_results = compare(
        backends,
        input_spikes,
        neuron_model,
        neuron_params,
        weight,
        delay,
        timesteps,
        plot=plot_flag,
    )

    print("compare_lif_curr_exp_no_delay")
    if test_results["spike_times"] or test_results["spike_times_within_1_timestep"]:
        print("SUCCESS")
    else:
        print("FAIL")


def compare_lif():
    neuron_model = "lif"
    neuron_params = {
        "threshold": 4.01,
        "alpha_decay": 0.9,
        "v_reset": -1.0,
        "i_offset": 0.0,
        "reset": "reset_to_v_reset",
    }
    input_spikes = [1, 7, 10, 15]
    weight = 3.0
    delay = 3
    timesteps = 20
    backends = ["brian2", "fpga"]
    test_results = compare(
        backends,
        input_spikes,
        neuron_model,
        neuron_params,
        weight,
        delay,
        timesteps,
        plot=plot_flag,
    )

    print("compare_lif")
    if test_results["spike_times"]:
        print("SUCCESS")
    else:
        print("FAIL")


def compare_lif_no_delay():
    neuron_model = "lif_no_delay"
    neuron_params = {
        "threshold": 4.01,
        "alpha_decay": 0.9,
        "v_reset": 0.0,
        "i_offset": 0.8,
        "reset": "reset_by_subtraction",
    }
    input_spikes = [1, 7, 10, 15]
    weight = -18.0
    delay = 0
    timesteps = 50
    backends = ["brian2", "fpga"]
    test_results = compare(
        backends,
        input_spikes,
        neuron_model,
        neuron_params,
        weight,
        delay,
        timesteps,
        plot=plot_flag,
    )

    print("compare_lif_no_delay")
    if test_results["spike_times"]:
        print("SUCCESS")
    else:
        print("FAIL")


if __name__ == "__main__":
    compare_lif()
    compare_lif_no_delay()
    compare_lif_curr_exp()
    compare_lif_curr_exp_no_delay()
