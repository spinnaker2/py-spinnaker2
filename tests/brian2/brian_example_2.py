#!/usr/bin/env python
"""
Brian2 example for lif_curr_exp
"""
import numpy as np
from brian2 import *

defaultclock.dt = 1 * ms
dt = defaultclock.dt
print(dt)

n_in = 1  # number of stimuli
n_out = 1  # number of output neurons

duration = 0.1 * second

alpha_decay = 0.9
exc_decay = 0.3
inh_decay = 0.4


#############
# Equations #
#############

eqs_lif_curr_exp = """
dI_syn_exc/dt = -I_syn_exc/tau_exc : 1
dI_syn_inh/dt = -I_syn_inh/tau_inh : 1
dv/dt = -v / tau  + I_syn_exc/dt - I_syn_inh/dt + i_offset/dt: 1 (unless refractory)
v_th : 1
i_offset : 1
v_reset : 1
tau : second
tau_exc : second
tau_inh : second
ref : second
"""

# reset by subtraction
reset_by_subtraction = "v -= v_th"
reset_to_v_reset = "v = v_reset"

net = Network()

# Input neurons
stim = SpikeGeneratorGroup(n_in, indices=[0, 0, 0, 0, 0], times=[10, 15, 30, 32, 34] * ms)

# Output neurons
group = NeuronGroup(
    n_out,
    eqs_lif_curr_exp,
    threshold="v > v_th",
    reset=reset_to_v_reset,
    refractory="ref",
    method="euler",
)
group.i_offset = 0.0
group.v_reset = 0.0
group.v_th = 5.01
group.tau = dt / (1 - alpha_decay)
group.tau_exc = dt / (1 - exc_decay)
group.tau_inh = dt / (1 - inh_decay)
group.ref = 3 * dt


S = Synapses(stim, group, model="""w:1""", on_pre="I_syn_inh+=w")
S.connect()  # connect all neuron pairs (fully connected layer)

S.w[0, 0] = 20.0
S.delay[0, 0] = 0 * dt

# set up monitors
s_monitor = SpikeMonitor(group)
s_monitor_stim = SpikeMonitor(stim)
v_monitor = StateMonitor(group, "v", record=True)
I_inh_monitor = StateMonitor(group, "I_syn_inh", record=True)

net.add(stim, group, S, s_monitor, s_monitor_stim, v_monitor, I_inh_monitor)
net.schedule = ["start", "groups", "synapses", "thresholds", "resets", "end"]

# run simulation
net.run(duration)

# plot results
"""
figure()
plot(s_monitor_stim.t/ms, s_monitor_stim.i, '.k')
xlabel('Time (ms)')
ylabel('Neuron index');
title("Stimulus spikes")

figure()
plot(s_monitor.t/ms, s_monitor.i, '.k')
xlabel('Time (ms)')
ylabel('Neuron index');
title("Neuron spikes")
"""

# plot membrane voltage of first and last neuron
figure()
plot(v_monitor.t / ms, v_monitor.v[0], label="Neuron 0")
# plot(v_monitor.t/ms, v_monitor.v[n_out-1]/mV, label='Neuron {}'.format(n_out-1))
xlabel("Time (ms)")
ylabel("v")
legend()
title("Membrane voltage trace")

figure()
plot(I_inh_monitor.t / ms, I_inh_monitor.I_syn_inh[0], label="Neuron 0")
xlabel("Time (ms)")
ylabel("I_syn_inh")
legend()
title("Synaptic input current")

show()
