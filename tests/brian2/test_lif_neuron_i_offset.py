"""
test for the 'i_offset' parameter of the LIF neuron
"""

from spinnaker2 import brian2_sim, hardware, snn


def test_lif_neuron_i_offset():
    params = {
        "threshold": 1.0,
        "alpha_decay": 1.0,
        "i_offset": 0.123,
        "reset": "reset_by_subtraction",
    }

    pop1 = snn.Population(size=1, neuron_model="lif", params=params, name="pop1", record=["spikes"])

    net = snn.Network("my network")
    net.add(pop1)

    brian2 = brian2_sim.Brian2Backend()
    timesteps = 100
    brian2.run(net, timesteps)

    spikes = pop1.get_spikes()[0]

    expected_spike_count = int(timesteps * params["i_offset"] / params["threshold"])
    actual_spike_count = len(spikes)
    print("Actual spikes:", actual_spike_count)
    print("Expected spikes:", expected_spike_count)

    if expected_spike_count == actual_spike_count:
        print("SUCCESS")
    else:
        print("FAIL")
    assert expected_spike_count == actual_spike_count


if __name__ == "__main__":
    test_lif_neuron_i_offset()
