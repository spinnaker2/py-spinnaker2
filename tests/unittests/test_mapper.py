import unittest

from spinnaker2 import mapper
from spinnaker2.coordinates import PE
from spinnaker2.neuron_models.common import format_routing_table


class TestSynapseWordSpec(unittest.TestCase):

    def test_enum(self):
        s16 = mapper.SynapseWordSize.SIZE_16
        self.assertEqual(s16.value, 16)

        s32 = mapper.SynapseWordSize.SIZE_32
        self.assertEqual(s32.value, 32)

        self.assertRaises(Exception, mapper.SynapseWordSize, 1)
        self.assertRaises(Exception, mapper.SynapseWordSize, 8)

    def test_16(self):
        sw_spec = mapper.SynapseWordSpec(
            word_size=mapper.SynapseWordSize.SIZE_16, weight=4, delay=3, synapse_type=1, target=8
        )

        self.assertEqual(sw_spec.max_weight(), 15)
        self.assertEqual(sw_spec.max_delay(), 7)

    def test_32(self):
        sw_spec = mapper.SynapseWordSpec(
            word_size=mapper.SynapseWordSize.SIZE_32, weight=16, delay=4, synapse_type=1, target=11
        )

        self.assertEqual(sw_spec.max_weight(), 2**16 - 1)
        self.assertEqual(sw_spec.max_delay(), 15)


class TestRoutingTable(unittest.TestCase):

    def test_single_targets(self):
        shift = 16
        for x in [0, 1]:
            for y in [1, 2]:
                for pe in [0, 1, 2, 3]:
                    routing_targets = [PE(x, y, pe)]
                    shift -= 1
                    expected = 1 << shift
                    routing_table = format_routing_table(routing_targets)
                    self.assertEqual(routing_table, expected)

    def test_one_target_per_pe(self):
        routing_targets = [PE(0, 1, 0), PE(0, 2, 1), PE(1, 1, 2), PE(1, 2, 3)]
        expected = 0b1000010000100001
        routing_table = format_routing_table(routing_targets)
        self.assertEqual(routing_table, expected)

    def test_many_targets(self):
        routing_targets = [
            PE(0, 1, 0),
            PE(0, 1, 2),
            PE(0, 2, 0),
            PE(0, 2, 2),
            PE(0, 2, 3),
            PE(1, 1, 0),
            PE(1, 1, 3),
            PE(1, 2, 1),
        ]
        expected = 0b1010101110010100
        routing_table = format_routing_table(routing_targets)
        self.assertEqual(routing_table, expected)

    def test_range_check(self):
        bad_coordinates = [
            PE(0, 0, 0),
            PE(-1, 1, 0),
            PE(0, 3, 2),
            PE(2, 2, 0),
            PE(1, 2, 4),
            PE(1, 2, -2),
        ]
        for pe in bad_coordinates:
            with self.assertRaises(Exception):
                format_routing_table([pe])


if __name__ == "__main__":
    unittest.main()
