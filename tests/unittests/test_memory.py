import pprint
import unittest

from spinnaker2 import hardware
from spinnaker2.configuration import MemoryRegion
from spinnaker2.coordinates import WordAddr
from spinnaker2.neuron_models.application import MemoryRegionType, NamedMemoryRegion, PEMemory

pe_mem = PEMemory()
pe_mem.instruction_memory()
pe_mem.insert(MemoryRegion(WordAddr(0x9000 / 4), 20))  # can this be done?
pe_mem.insert(NamedMemoryRegion("exit_code", MemoryRegionType.OUTPUT, WordAddr(0x8000 / 4), 1))
pe_mem.insert(MemoryRegion(WordAddr(0x9004 / 4), 20))  # can this be done?

for r in pe_mem.regions:
    print(r)


# data specification


class TestPEMemory(unittest.TestCase):

    def test_basic(self):
        pe_mem = PEMemory()
        pe_mem.instruction_memory()
        pe_mem.insert(NamedMemoryRegion("exit_code", MemoryRegionType.OUTPUT, WordAddr(0x8000 / 4), 1))
        pe_mem.insert(MemoryRegion(WordAddr(0x9000 / 4), 20))  # can this be done?


# Example SNN software
# define regions for each application
regions = [
    "data_specification",
    "debug",  # log_info
    "spike_record",  # similar to debug
    "master_population_table",
    "synapse_rows",
    "routing_table",  # information on where to route the results!
    "neuron_params",
]

regions_simple = [
    "data_specification",
    "debug",
    "master_population_table",
    "synapse_rows",
]  # log_info
