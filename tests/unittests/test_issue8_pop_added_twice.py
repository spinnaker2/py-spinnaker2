"""
Test of issue #8 `Check for pop or projection being added twice to a snn.Network`
"""

import pytest
from spinnaker2 import snn


def test_issue8_pop_added_twice():
    stim = snn.Population(1, "spike_list", params={}, name="stim")
    net = snn.Network("my network")
    net.add(stim)
    with pytest.raises(Exception):
        net.add(stim)


def test_issue8_proj_added_twice():
    stim = snn.Population(1, "spike_list", params={}, name="stim")
    pop1 = snn.Population(1, "lif", params={}, name="pop1")
    conns = [[0, 0, 15, 1]]
    proj = snn.Projection(pre=stim, post=pop1, connections=conns)

    net = snn.Network("my network")
    net.add(stim, pop1, proj)
    with pytest.raises(Exception):
        net.add(proj)


if __name__ == "__main__":
    test_issue8_pop_added_twice()
    test_issue8_proj_added_twice()
