import unittest

from spinnaker2 import hardware, mapper, snn


class TestMapping(unittest.TestCase):

    def test_add(self):
        m = mapper.Mapping()

        p1 = snn.Population(20, "lif", params=None)

        pe_0_0_0 = hardware.PE(0, 0, 0)
        pe_0_0_1 = hardware.PE(0, 0, 1)
        p1_1 = mapper.PopulationSlice(p1, 0, 10)
        p1_2 = mapper.PopulationSlice(p1, 10, 20)

        m.add(p1_1, pe_0_0_0)

        # cannot add twice to the same PE
        self.assertRaises(Exception, m.add, p1_2, pe_0_0_0)

        m.add(p1_2, pe_0_0_1)

    def test_access(self):
        m = mapper.Mapping()
        p1 = snn.Population(20, "lif", params=None)
        p2 = snn.Population(20, "lif", params=None)
        p3 = snn.Population(10, "lif", params=None)  # not mapped

        pe_0_0_0 = hardware.PE(0, 0, 0)
        pe_0_0_1 = hardware.PE(0, 0, 1)
        pe_0_0_2 = hardware.PE(0, 0, 2)
        pe_0_0_3 = hardware.PE(0, 0, 3)
        pe_0_1_0 = hardware.PE(0, 1, 0)

        m.add(mapper.PopulationSlice(p1, 0, 10), pe_0_0_0)
        m.add(mapper.PopulationSlice(p1, 10, 20), pe_0_0_1)
        m.add(mapper.PopulationSlice(p2, 0, 1), pe_0_0_2)
        m.add(mapper.PopulationSlice(p2, 1, 4), pe_0_0_3)
        m.add(mapper.PopulationSlice(p2, 4, 20), pe_0_1_0)

        #####################
        # test get_hardware #
        #####################
        hw0, idx0 = m.get_hardware(p1, 5)
        self.assertEqual(pe_0_0_0, hw0)
        self.assertEqual(hardware.PE(0, 0, 0), hw0)
        self.assertEqual(5, idx0)

        hw1, idx1 = m.get_hardware(p1, 19)
        self.assertEqual(pe_0_0_1, hw1)
        self.assertEqual(9, idx1)

        # test index out of range
        self.assertRaises(Exception, m.get_hardware, p1, 20)

        # test pop not mapped
        self.assertRaises(Exception, m.get_hardware, p3, 5)

        ########################
        # test get_neuron
        ########################
        for i in range(10):
            pop, idx = m.get_neuron(pe_0_0_1, i)
            self.assertEqual(pop, p1)
            self.assertEqual(idx, i + 10)

        # test index out of range
        self.assertRaises(Exception, m.get_neuron, pe_0_0_1, 10)

        # test core not used
        self.assertRaises(KeyError, m.get_neuron, hardware.PE(1, 1, 2), 10)

        #############################
        # test get_population_slice #
        #############################
        self.assertEqual(m.get_population_slice(pe_0_0_2), mapper.PopulationSlice(p2, 0, 1))
        self.assertEqual(m.get_population_slice(pe_0_1_0), mapper.PopulationSlice(p2, 4, 20))
        self.assertNotEqual(m.get_population_slice(pe_0_0_3), mapper.PopulationSlice(p2, 4, 20))

        # test core not used
        self.assertRaises(KeyError, m.get_population_slice, hardware.PE(1, 1, 1))


class TestPE(unittest.TestCase):

    def test_eq(self):
        pe_a = hardware.PE(1, 1, 1)
        pe_b = hardware.PE(1, 1, 1)
        pe_c = hardware.PE(0, 0, 1)
        pe_d = hardware.PE(5, 0, 0)
        pe_e = hardware.PE(0, 2, 0)

        self.assertEqual(pe_a, pe_b)
        self.assertNotEqual(pe_a, pe_c)
        self.assertNotEqual(pe_a, pe_d)
        self.assertNotEqual(pe_a, pe_e)

    def test_hash(self):
        pe_a = hardware.PE(0, 0, 0)
        pe_b = hardware.PE(0, 0, 0)
        pe_c = hardware.PE(0, 0, 1)

        self.assertEqual(hash(pe_a), hash(pe_b))
        self.assertNotEqual(hash(pe_a), hash(pe_c))


if __name__ == "__main__":
    unittest.main()
