"""
Test of issue #83:
"spike_list: don't load input spikes that happen after end of the simulation"
"""

import pytest
from spinnaker2 import hardware, snn


def test_issue83_input_spikes_larger_than_simulation_time():
    """test for issue #83.

    We generate a spike_list population with so many input spikes that they
    don't fit into the SRAM of one PE.

    If all input spikes are within the simulation duration (number of
    timesteps), we expect a `MemoryError` to be raised.

    However, if the number of timesteps is smaller, only the input spikes for
    the given simulation duration shall be considered, such that no MemoryError
    is raised.
    """
    spike_times = {0: list(range(5000))}
    stim = snn.Population(1, "spike_list", params=spike_times, name="stim")
    net = snn.Network("my network")
    net.add(stim)

    hw = hardware.SpiNNaker2Chip()

    # raises an error for 5000 timesteps
    with pytest.raises(MemoryError):
        timesteps = 5000
        hw.run(net, timesteps, mapping_only=True)

    # but not for a small number of timesteps
    timesteps = 50
    hw.run(net, timesteps, mapping_only=True)
