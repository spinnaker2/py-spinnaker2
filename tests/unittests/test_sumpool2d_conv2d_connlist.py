import numpy as np
import tensorflow as tf
import torch
import torch.nn as nn
from spinnaker2 import ann2snn_helpers

tf.keras.utils.set_random_seed(42)


def test_connection_list_from_conv2d_weights_torch():
    input_shape = (8, 16, 19)
    C_out = 7
    filt_shape = (3, 5)
    strides = 2
    padding = (1, 1)

    class SimpleCNN(nn.Module):
        def __init__(self):
            super(SimpleCNN, self).__init__()
            # Define a 2D convolutional layer with 1 input channel, 3 output channels, and a kernel size of 3
            self.pool = nn.AvgPool2d(kernel_size=(2, 2), stride=2, padding=0, divisor_override=1)
            self.conv1 = nn.Conv2d(
                input_shape[0],
                C_out,
                kernel_size=filt_shape,
                stride=strides,
                padding=padding,
                bias=False,
            )

        def forward(self, x):
            # Forward pass through the network
            x = self.pool(x)
            y = self.conv1(x)
            return x, y

    # Create an instance of the SimpleCNN model
    model = SimpleCNN()

    x0 = np.random.random(input_shape).astype(np.float32)
    x, y = model(torch.from_numpy(x0))
    x = x.detach().numpy()
    y = y.detach().numpy()
    w = model.conv1.weight.detach().numpy()

    sumpool2d_list, sumpool2d_shape = ann2snn_helpers.connection_list_for_sumpool2d(
        input_shape=input_shape,
        stride=2,
        padding=0,
        kernel_size=(2, 2),
        delay=1,
        data_order="torch",
    )
    conv2d_list, output_shape = ann2snn_helpers.connection_list_from_conv2d_weights(
        w, input_shape=sumpool2d_shape, stride=strides, padding=padding, delay=1, data_order="torch"
    )
    connection_list = ann2snn_helpers.join_conn_lists(sumpool2d_list, conv2d_list)

    assert output_shape == y.shape, f"Shape mismatch! Got {output_shape}, {y.shape}"
    assert sumpool2d_shape == x.shape, f"Shape mismatch! Got {sumpool2d_shape}, {x.shape}"

    def compute_result_with_connection_list(connection_list, output_shape, x):
        y = np.zeros(np.prod(output_shape))
        for pre, post, wgt, _ in connection_list:
            y[post] += x[pre] * wgt
        return y

    x_pred = compute_result_with_connection_list(sumpool2d_list, sumpool2d_shape, x0.flatten())
    x_pred_0 = x_pred
    x_pred = x_pred.reshape(sumpool2d_shape)
    assert np.isclose(x, x_pred, rtol=1e-4, atol=1e-6).all(), "Error in computation too large!"
    print("sumpool2d correct!")

    conv_pred = compute_result_with_connection_list(conv2d_list, output_shape, x_pred_0)
    conv_pred = conv_pred.reshape(output_shape)
    assert np.isclose(y, conv_pred, rtol=1e-4, atol=1e-6).all(), "Error in computation too large!"
    print("conv2d correct!")

    y_pred = compute_result_with_connection_list(connection_list, output_shape, x0.flatten())
    y_pred = y_pred.reshape(output_shape)
    assert np.isclose(y, y_pred, rtol=1e-4, atol=1e-6).all(), "Error in computation too large!"
    print("combined correct!")
    print("done")


if __name__ == "__main__":
    test_connection_list_from_conv2d_weights_torch()
