import unittest

from spinnaker2.coordinates import ByteAddr, WordAddr


class TestMemoryAddresses(unittest.TestCase):

    def test_base(self):
        w_addr = WordAddr(10)
        b_addr = w_addr.to_ByteAddr()

        self.assertEqual(b_addr, ByteAddr(10 << 2))
        self.assertEqual(b_addr, 10 << 2)
        self.assertEqual(w_addr, b_addr.to_WordAddr())

        # self.assertGreaterEqual(w_addr, b_addr)
        self.assertGreaterEqual(b_addr, w_addr)

        b_addr2 = ByteAddr(111)
        self.assertRaises(Exception, b_addr2.to_WordAddr)

    @unittest.expectedFailure
    def test_compare_word_and_byte_address(self):
        self.assertNotEqual(WordAddr(10), ByteAddr(10))


if __name__ == "__main__":
    unittest.main()
