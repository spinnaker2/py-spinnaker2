import numpy as np
import tensorflow as tf
import torch
import torch.nn as nn
from spinnaker2 import ann2snn_helpers

tf.keras.utils.set_random_seed(42)


def test_connection_list_from_conv2d_weights_tf():
    input_shape = (5, 6, 7)
    C_out = 9
    filt_shape = (3, 5)
    strides = 2
    padding = "same"
    inp = tf.keras.layers.Input(input_shape)
    x = tf.keras.layers.Conv2D(C_out, filt_shape, strides=strides, padding=padding)(inp)
    model = tf.keras.Model(inp, x)

    x0 = np.random.random(input_shape)
    y = model.predict(np.expand_dims(x0, axis=0))[0, :, :, :]
    w = model.layers[1].get_weights()[0]

    connection_list, output_shape = ann2snn_helpers.connection_list_from_conv2d_weights(
        w, input_shape=input_shape, stride=strides, padding=padding, delay=1, data_order="tf"
    )

    assert output_shape == y.shape, "Shape mismatch!"

    def compute_result_with_connection_list(connection_list, output_shape, x):
        y = np.zeros(np.prod(output_shape))
        for pre, post, wgt, _ in connection_list:
            y[post] += x[pre] * wgt
        return y

    y_pred = compute_result_with_connection_list(connection_list, output_shape, x0.flatten())
    y_pred = y_pred.reshape(output_shape)

    assert np.isclose(y, y_pred, rtol=1e-4, atol=1e-6).all(), "Error in computation too large!"

    print("done")


def test_connection_list_from_conv2d_weights_torch():
    input_shape = (8, 9, 10)
    C_out = 7
    filt_shape = (3, 5)
    strides = 2
    padding = (1, 2)

    class SimpleCNN(nn.Module):
        def __init__(self):
            super(SimpleCNN, self).__init__()
            # Define a 2D convolutional layer with 1 input channel, 3 output channels, and a kernel size of 3
            self.conv1 = nn.Conv2d(
                input_shape[0],
                C_out,
                kernel_size=filt_shape,
                stride=strides,
                padding=padding,
                bias=False,
            )

        def forward(self, x):
            # Forward pass through the network
            x = self.conv1(x)
            return x

    # Create an instance of the SimpleCNN model
    model = SimpleCNN()

    x0 = np.random.random(input_shape).astype(np.float32)
    y = model(torch.from_numpy(x0)).detach().numpy()
    w = model.conv1.weight

    connection_list, output_shape = ann2snn_helpers.connection_list_from_conv2d_weights(
        w, input_shape=input_shape, stride=strides, padding=padding, delay=1, data_order="torch"
    )

    assert output_shape == y.shape, "Shape mismatch!"

    def compute_result_with_connection_list(connection_list, output_shape, x):
        y = np.zeros(np.prod(output_shape))
        for pre, post, wgt, _ in connection_list:
            y[post] += x[pre] * wgt
        return y

    y_pred = compute_result_with_connection_list(connection_list, output_shape, x0.flatten())
    y_pred = y_pred.reshape(output_shape)
    assert np.isclose(y, y_pred, rtol=1e-4, atol=1e-6).all(), "Error in computation too large!"

    print("done")


if __name__ == "__main__":
    test_connection_list_from_conv2d_weights_torch()
    test_connection_list_from_conv2d_weights_tf()
