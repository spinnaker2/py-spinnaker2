import unittest

from spinnaker2.mla.mla_config import Conv2DConfig


class TestStride(unittest.TestCase):

    def test_base(self):
        c = Conv2DConfig()

        # stride_x
        for n in range(4):
            c.set_stride_x(2**n)
            self.assertEqual(c.stride_x_2_power_n, n)
            self.assertEqual(c.get_stride_x(), 2**n)

        self.assertRaises(ValueError, c.set_stride_x, 0)  # too low
        self.assertRaises(ValueError, c.set_stride_x, 3)
        self.assertRaises(ValueError, c.set_stride_x, 4.5)
        self.assertRaises(ValueError, c.set_stride_x, 10)
        self.assertRaises(ValueError, c.set_stride_x, 16)  # too large

        # stride_y
        for n in range(4):
            c.set_stride_y(2**n)
            self.assertEqual(c.stride_y_2_power_n, n)
            self.assertEqual(c.get_stride_y(), 2**n)

        self.assertRaises(ValueError, c.set_stride_y, 0)  # too low
        self.assertRaises(ValueError, c.set_stride_y, 5)
        self.assertRaises(ValueError, c.set_stride_y, 2.1)
        self.assertRaises(ValueError, c.set_stride_y, 9)
        self.assertRaises(ValueError, c.set_stride_y, 16)  # too large


if __name__ == "__main__":
    unittest.main()
