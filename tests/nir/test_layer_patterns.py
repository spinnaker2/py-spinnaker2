"""test specific layer patterns can be translated from NIR to SpiNNaker2."""

import tempfile

import nir
import numpy as np
import pytest
from spinnaker2 import hardware, s2_nir, snn

rng = np.random.default_rng()


def run_mapping_only(graph: nir.NIRGraph):
    """run mapping to SpiNNaker2 but do not execute."""

    with tempfile.TemporaryFile() as tmp:
        nir.write(tmp, graph)
        new_graph = nir.read(tmp)

    # convert to SpiNNaker2 and set input spikes
    cfg = s2_nir.ConversionConfig(dt=0.001, output_record=["v", "spikes"], conn_delay=0)
    net, inp, outp = s2_nir.from_nir(new_graph, cfg)

    s2_hw = hardware.SpiNNaker2Chip()
    s2_hw.run(net, time_steps=10, mapping_only=True)
    return net, inp, outp


def test_affine_lif():
    n_neurons = 10
    nir_graph = nir.NIRGraph(
        {
            "in": nir.Input(input_type=np.array([3])),
            "affine": nir.Affine(
                weight=np.array([[0.8] * n_neurons]).T,
                bias=np.array([0.9] * n_neurons),
            ),
            "lif": nir.LIF(
                tau=rng.uniform(0.01, 0.9, n_neurons),
                r=rng.uniform(0.01, 2.0, n_neurons),
                v_leak=rng.uniform(-1.0, 1.0, n_neurons),
                v_threshold=rng.uniform(1.0, 10, n_neurons),
            ),
            "out": nir.Output(output_type=np.array([1])),
        },
        edges=[("in", "affine"), ("affine", "lif"), ("lif", "out")],
    )
    run_mapping_only(nir_graph)


@pytest.mark.xfail(reason="Conv1d conversion not implemented in s2_nir")
def test_conv1d_lif():
    in_channels = 8
    in_size = 10
    out_channels = 16
    kernel_size = 3
    padding = 1
    lif_shape = (out_channels, in_size)
    nir_graph = nir.NIRGraph(
        {
            "in": nir.Input(input_type=np.array([in_channels, in_size])),
            "conv1d": nir.Conv1d(
                input_shape=in_size,
                weight=rng.uniform(-1, 1, (out_channels, in_channels, kernel_size)),  # Weight C_out * C_in * N
                stride=1,
                padding=padding,
                dilation=1,
                groups=1,
                bias=rng.uniform(-1, 1, (out_channels)),
            ),
            "lif": nir.LIF(
                tau=rng.uniform(0.01, 0.9, lif_shape),
                r=rng.uniform(0.01, 2.0, lif_shape),
                v_leak=rng.uniform(-1.0, 1.0, lif_shape),
                v_threshold=rng.uniform(1.0, 10, lif_shape),
            ),
            "out": nir.Output(output_type=np.array([1])),
        },
        edges=[("in", "conv1d"), ("conv1d", "lif"), ("lif", "out")],
    )
    net, inp, outp = run_mapping_only(nir_graph)

    # check for correct size of lif population
    lif_pop = next(pop for pop in net.populations if pop.name == "lif")
    assert lif_pop.size == np.prod(lif_shape)


def test_conv2d_lif():
    in_channels = 4
    in_shape = (16, 16)
    out_channels = 8
    kernel_size = 3
    padding = 1
    lif_shape = (out_channels, in_shape[0], in_shape[1])
    nir_graph = nir.NIRGraph(
        {
            "in": nir.Input(input_type=np.array([in_channels, in_shape[0], in_shape[1]])),
            "conv2d": nir.Conv2d(
                input_shape=in_shape,
                weight=rng.uniform(
                    -1, 1, (out_channels, in_channels, kernel_size, kernel_size)
                ),  # Weight C_out * C_in * N
                stride=1,
                padding=padding,
                dilation=1,
                groups=1,
                bias=rng.uniform(-1, 1, (out_channels)),
            ),
            "lif": nir.LIF(
                tau=rng.uniform(0.01, 0.9, lif_shape),
                r=rng.uniform(0.01, 2.0, lif_shape),
                v_leak=rng.uniform(-1.0, 1.0, lif_shape),
                v_threshold=rng.uniform(1.0, 10, lif_shape),
            ),
            "out": nir.Output(output_type=np.array([1])),
        },
        edges=[("in", "conv2d"), ("conv2d", "lif"), ("lif", "out")],
    )
    nir_graph.infer_types()
    net, inp, outp = run_mapping_only(nir_graph)

    # check for correct size of lif population
    lif_pop = next(pop for pop in net.populations if pop.name == "lif")
    assert lif_pop.size == np.prod(lif_shape)


if __name__ == "__main__":
    test_conv2d_lif()
