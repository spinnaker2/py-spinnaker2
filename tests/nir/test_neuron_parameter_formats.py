"""test the correct handling of NIR neuron parameters for SpiNNaker2.

Issue #76 (https://gitlab.com/spinnaker2/py-spinnaker2/-/issues/76) reported
problems when the same parameter for all neurons in a NIR layer was passed.
This can happen if neuron parameters are not trained but constant. Then, the
neuron node has a size of 1 and the shape is inferred from the incoming layers
(e.g. Linear).

Here we check that the S2 NIR interface can handle the processing of neuron parameters in the following settings:
- one-to-one: Size of neuron parameters and the number of neurons is 1
- one-to-many: Size of neuron parameters is 1 and the number of neurons larger
  than one. Requires `nir_graph.infer_types()` to work.
- many-to-many: Size of neuron parameters and the number of neurons is equal and larger than one.

The 3 checks are applied for LIF, IF, and CubaLIF nodes.
"""

import math
import tempfile

import nir
import numpy as np
from spinnaker2 import hardware, s2_nir, snn

rng = np.random.default_rng()


def create_and_map_graph(nir_node: nir.NIRNode, n_neurons: int, infer_types: bool):
    """helper function to create NIR graph and run mapping for SpiNNaker2."""

    # create NIR graph
    nir_graph = nir.NIRGraph(
        {
            "in": nir.Input(input_type=np.array([3])),
            "affine": nir.Affine(
                weight=np.array([[0.8] * n_neurons]).T,
                bias=np.array([0.9] * n_neurons),
            ),
            "node_under_test": nir_node,
            "out": nir.Output(output_type=np.array([1])),
        },
        edges=[("in", "affine"), ("affine", "node_under_test"), ("node_under_test", "out")],
    )

    with tempfile.TemporaryFile() as tmp:
        nir.write(tmp, nir_graph)
        nir_graph = nir.read(tmp)

    if infer_types:
        nir_graph.infer_types()

    # convert to SpiNNaker2 and set input spikes
    cfg = s2_nir.ConversionConfig(dt=0.001, output_record=["v", "spikes"], conn_delay=0)
    net, inp, outp = s2_nir.from_nir(nir_graph, cfg)

    s2_hw = hardware.SpiNNaker2Chip()
    s2_hw.run(net, time_steps=10, mapping_only=True)


def create_if_with_random_params(size):
    return nir.IF(
        r=rng.uniform(0.01, 2.0, size),
        v_threshold=rng.uniform(1.0, 10, size),
    )


def create_lif_with_random_params(size):
    return nir.LIF(
        tau=rng.uniform(0.01, 0.9, size),
        r=rng.uniform(0.01, 2.0, size),
        v_leak=rng.uniform(-1.0, 1.0, size),
        v_threshold=rng.uniform(1.0, 10, size),
    )


def create_cubalif_with_random_params(size):
    return nir.CubaLIF(
        tau_syn=rng.uniform(0.01, 0.9, size),
        tau_mem=rng.uniform(0.01, 0.9, size),
        r=rng.uniform(0.01, 2.0, size),
        v_leak=rng.uniform(-1.0, 1.0, size),
        v_threshold=rng.uniform(1.0, 10, size),
        w_in=rng.uniform(0.01, 2.0, size),
    )


def test_if_one_to_one():
    n_neurons = 1
    node = create_if_with_random_params(n_neurons)
    create_and_map_graph(node, n_neurons, infer_types=False)


def test_if_one_to_many():
    node = create_if_with_random_params(1)
    create_and_map_graph(node, n_neurons=3, infer_types=True)


def test_if_many_to_many():
    n_neurons = 10
    node = create_if_with_random_params(n_neurons)
    create_and_map_graph(node, n_neurons, infer_types=False)


def test_lif_one_to_one():
    n_neurons = 1
    node = create_lif_with_random_params(n_neurons)
    create_and_map_graph(node, n_neurons, infer_types=False)


def test_lif_one_to_many():
    node = create_lif_with_random_params(1)
    create_and_map_graph(node, n_neurons=3, infer_types=True)


def test_lif_many_to_many():
    n_neurons = 10
    node = create_lif_with_random_params(n_neurons)
    create_and_map_graph(node, n_neurons, infer_types=False)


def test_cubalif_one_to_one():
    n_neurons = 1
    node = create_cubalif_with_random_params(n_neurons)
    create_and_map_graph(node, n_neurons, infer_types=False)


def test_cubalif_one_to_many():
    node = create_cubalif_with_random_params(1)
    create_and_map_graph(node, n_neurons=3, infer_types=True)


def test_cubalif_many_to_many():
    n_neurons = 10
    node = create_cubalif_with_random_params(n_neurons)
    create_and_map_graph(node, n_neurons, infer_types=False)


if __name__ == "__main__":
    test_lif_one_to_one()
