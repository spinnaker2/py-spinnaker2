"""test conversion of NIR neuron models to SpiNNaker2."""

import math

import matplotlib.pyplot as plt
import nir
import numpy as np
from spinnaker2 import brian2_sim, hardware, s2_nir, snn

#############################
# Reference implementations #
#############################


class LIFImplementation(object):
    """Numpy forward Euler implmementation of LIF Node."""

    def __init__(self, dt, lif_node):
        self.dt = dt  # scalar
        self.node = lif_node
        self.v = np.zeros_like(self.node.v_threshold)

    def forward(self, x):
        self.v += (self.dt / self.node.tau) * (self.node.v_leak - self.v + self.node.r * x)
        z = self.v > self.node.v_threshold
        self.v -= z * self.node.v_threshold
        return z, self.v


class CubaLIFImplementation(object):
    """Numpy forward Euler implmementation of CubaLIF Node."""

    def __init__(self, dt, cuba_node):
        self.dt = dt  # scalar
        self.node = cuba_node
        self.v = np.zeros_like(self.node.v_threshold)
        self.I = np.zeros_like(self.node.v_threshold)

    def forward(self, x):
        I_last = self.I.copy()
        v_last = self.v.copy()
        self.I = I_last + (self.dt / self.node.tau_syn) * (-I_last + self.node.w_in * x)
        self.v = v_last + (self.dt / self.node.tau_mem) * (self.node.v_leak - v_last + self.node.r * I_last)
        z = self.v > self.node.v_threshold
        self.v -= z * self.node.v_threshold
        return z, self.v


########################################################
# Helper functions to compare reference and SpiNNaker2 #
########################################################


def run_comparison(nir_node, weight, bias, input_spikes, n_steps, node_impl, s2_hw, dt, do_plot=False):
    """run comparison of single NIR neuron and SpiNNaker2"""

    ref_model = node_impl(dt, nir_node)

    # generate input data for ref model
    input_data = np.ones((n_steps, 1)) * bias
    for t in input_spikes:
        input_data[t, 0] += weight

    # run output model
    ref_voltages = np.zeros_like(input_data)
    ref_spike_array = np.zeros_like(input_data)

    for t in range(n_steps):
        z, v = ref_model.forward(input_data[t, :])
        ref_voltages[t, :] = v
        ref_spike_array[t, :] = z

    ref_spike_times = np.nonzero(ref_spike_array[:, 0] == 1)[0].tolist()

    # create NIR graph
    nir_graph = nir.NIRGraph(
        {
            "in": nir.Input(input_type=np.array([3])),
            "affine": nir.Affine(
                weight=np.array([[weight]]).T,
                bias=np.array([bias]),
            ),
            "node_under_test": nir_node,
            "out": nir.Output(output_type=np.array([1])),
        },
        edges=[("in", "affine"), ("affine", "node_under_test"), ("node_under_test", "out")],
    )

    # convert to SpiNNaker2 and set input spikes
    cfg = s2_nir.ConversionConfig(dt=dt, output_record=["v", "spikes"], conn_delay=0)
    net, inp, outp = s2_nir.from_nir(nir_graph, cfg)
    assert len(inp) == 1
    inp[0].params[0] = input_spikes

    timesteps = n_steps
    s2_hw.run(net, timesteps)

    # get results
    s2_spike_times = outp[0].get_spikes()
    s2_voltages = outp[0].get_voltages()
    v_scale = outp[0].nir_v_scale

    result = {
        "ref_voltages": ref_voltages[:, 0],
        "s2_voltages": s2_voltages[0] / v_scale[0],
        "ref_spikes": ref_spike_times,
        "s2_spikes": s2_spike_times[0],
    }

    if do_plot:
        plt.plot(result["ref_voltages"], "--", label="ref")
        plt.plot(result["s2_voltages"], "-", label="S2")
        plt.legend()
        plt.show()

    return result


def create_cubalif(**params):
    return nir.CubaLIF(
        tau_syn=np.array([params["tau_syn"]]),
        tau_mem=np.array([params["tau_mem"]]),
        r=np.array([params["r"]]),
        v_leak=np.array([params["v_leak"]]),
        v_threshold=np.array([params["v_threshold"]]),
        w_in=np.array([params["w_in"]]),
    )


def create_lif(**params):
    return nir.LIF(
        tau=np.array([params["tau"]]),
        r=np.array([params["r"]]),
        v_leak=np.array([params["v_leak"]]),
        v_threshold=np.array([params["v_threshold"]]),
    )


def get_s2_hardware_backend():
    """helper to choose SpiNNaker2 Backend"""
    # s2_hw = brian2_sim.Brian2Backend()
    s2_hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.25")
    return s2_hw


#############
# LIF tests #
#############


def test_lif_bias_no_input(do_plot=False):
    """test LIF convergence to same voltage for given bias without input."""
    dt = 0.001
    weight = 0.0
    bias = 0.33
    input_spikes = []
    n_steps = 200

    lif = create_lif(tau=0.01, r=0.5, v_leak=0.0, v_threshold=1.0)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(lif, weight, bias, input_spikes, n_steps, LIFImplementation, s2_hw, dt, do_plot)

    # compare for voltages being close after a few time steps
    start_idx = 50
    assert np.allclose(result["s2_voltages"][start_idx:], result["ref_voltages"][start_idx:], atol=0.001)


def test_lif_v_leak_no_input(do_plot=False):
    """test LIF convergence to v_leak without input."""
    dt = 0.001
    weight = 0.0
    bias = 0.0
    input_spikes = []
    n_steps = 200

    lif = create_lif(tau=0.03, r=1.5, v_leak=-0.2, v_threshold=1.0)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(lif, weight, bias, input_spikes, n_steps, LIFImplementation, s2_hw, dt, do_plot)

    # compare for voltages being close after a few time steps
    start_idx = 100
    assert np.allclose(result["s2_voltages"][start_idx:], result["ref_voltages"][start_idx:], atol=0.001)


def test_lif_subthreshold(do_plot=False):
    """test LIF sub-threshold behavior with spike input"""
    dt = 0.001
    weight = 3.0
    bias = 0.2
    input_spikes = [10, 20, 35, 55, 88]
    n_steps = 200

    lif = create_lif(tau=0.009, r=0.47, v_leak=-0.11, v_threshold=1.0)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(lif, weight, bias, input_spikes, n_steps, LIFImplementation, s2_hw, dt, do_plot)

    # compare for voltages being close after a few time steps with time shift of 1 for delay
    start_idx = 5
    assert np.allclose(result["s2_voltages"][start_idx + 1 :], result["ref_voltages"][start_idx:-1], atol=0.01)


def test_lif_spiking_regular_input(do_plot=False):
    """test LIF spike count behavior with regular spike input"""
    dt = 0.001
    weight = 2.0
    bias = 0.11
    input_spikes = np.arange(10, 200, 10, dtype=int)
    n_steps = 200

    lif = create_lif(tau=0.009, r=1.1, v_leak=0.0, v_threshold=1.5)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(lif, weight, bias, input_spikes, n_steps, LIFImplementation, s2_hw, dt, do_plot)

    # compare for voltages being close after a few time steps with time shift of 1
    start_idx = 5
    assert np.allclose(result["s2_voltages"][start_idx + 1 :], result["ref_voltages"][start_idx:-1], atol=0.02)
    assert len(result["s2_spikes"]) == len(result["ref_spikes"])


def test_lif_spiking_random_input(do_plot=False):
    """test LIF spike count behavior with random spike input"""
    dt = 0.002
    weight = 4.0
    bias = 0.0
    n_steps = 200

    # generate random input spikes
    rng = np.random.default_rng()
    random_values = rng.random(n_steps)
    p_spike = 0.2
    input_spikes = np.nonzero(random_values <= p_spike)[0].tolist()

    lif = create_lif(tau=0.02, r=1.5, v_leak=0.0, v_threshold=1.0)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(lif, weight, bias, input_spikes, n_steps, LIFImplementation, s2_hw, dt, do_plot)

    # check that spike count differs at maximum by 1.
    assert math.isclose(len(result["s2_spikes"]), len(result["ref_spikes"]), abs_tol=1)


#################
# CubaLIF tests #
#################


def test_cubalif_bias_no_input(do_plot=False):
    """test CubaLIF convergence to same voltage for given bias without input."""
    dt = 0.001
    weight = 0.0
    bias = 0.33
    input_spikes = []
    n_steps = 200

    cuba = create_cubalif(tau_syn=0.005, tau_mem=0.01, r=0.5, v_leak=0.0, v_threshold=0.5, w_in=1.1)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(cuba, weight, bias, input_spikes, n_steps, CubaLIFImplementation, s2_hw, dt, do_plot)

    # compare for voltages being close after a few time steps
    start_idx = 50
    assert np.allclose(result["s2_voltages"][start_idx:], result["ref_voltages"][start_idx:], atol=0.001)


def test_cubalif_v_leak_no_input(do_plot=False):
    """test CubaLIF convergence to v_leak without input."""
    dt = 0.001
    weight = 0.0
    bias = 0.0
    input_spikes = []
    n_steps = 200

    cuba = create_cubalif(tau_syn=0.005, tau_mem=0.01, r=0.5, v_leak=0.37, v_threshold=0.5, w_in=1.1)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(cuba, weight, bias, input_spikes, n_steps, CubaLIFImplementation, s2_hw, dt, do_plot)

    # compare for voltages being close after a few time steps
    start_idx = 100
    assert np.allclose(result["s2_voltages"][start_idx:], result["ref_voltages"][start_idx:], atol=0.001)


def test_cubalif_subthreshold(do_plot=False):
    """test CubaLIF sub-threshold behavior with spike input"""
    dt = 0.001
    weight = 3.0
    bias = 0.2
    input_spikes = [10, 20, 35, 55, 88]
    n_steps = 200

    lif = create_lif(tau=0.009, r=0.47, v_leak=-0.11, v_threshold=1.0)
    cuba = create_cubalif(tau_syn=0.005, tau_mem=0.009, r=0.47, v_leak=-0.11, v_threshold=1.0, w_in=0.87)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(cuba, weight, bias, input_spikes, n_steps, CubaLIFImplementation, s2_hw, dt, do_plot)

    # compare for voltages being close after a few time steps
    start_idx = 50
    assert np.allclose(result["s2_voltages"][start_idx:], result["ref_voltages"][start_idx:], atol=0.01)


def test_cubalif_spiking_regular_input(do_plot=False):
    """test CubaLIF spike count behavior with regular spike input"""
    dt = 0.001
    weight = 3.0
    bias = 0.11
    input_spikes = np.arange(10, 200, 10, dtype=int)
    n_steps = 200

    cuba = create_cubalif(tau_syn=0.01, tau_mem=0.009, r=1.1, v_leak=0.0, v_threshold=0.9, w_in=2.2)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(cuba, weight, bias, input_spikes, n_steps, CubaLIFImplementation, s2_hw, dt, do_plot)

    # check that spike count differs at maximum by 1.
    assert len(result["s2_spikes"]) == len(result["ref_spikes"])


def test_cubalif_spiking_random_input(do_plot=False):
    """test CubaLIF spike count behavior with random spike input"""
    dt = 0.002
    weight = 4.0
    bias = 0.0
    n_steps = 200

    # generate random input spikes
    rng = np.random.default_rng()
    random_values = rng.random(n_steps)
    p_spike = 0.2
    input_spikes = np.nonzero(random_values <= p_spike)[0].tolist()

    cuba = create_cubalif(tau_syn=0.01, tau_mem=0.02, r=1.5, v_leak=0.0, v_threshold=1.0, w_in=0.8)
    s2_hw = get_s2_hardware_backend()
    result = run_comparison(cuba, weight, bias, input_spikes, n_steps, CubaLIFImplementation, s2_hw, dt, do_plot)

    # check that spike count differs at maximum by 1.
    assert math.isclose(len(result["s2_spikes"]), len(result["ref_spikes"]), abs_tol=1)


if __name__ == "__main__":
    # test_lif_bias_no_input(do_plot=True)
    # test_lif_v_leak_no_input(do_plot=True)
    # test_lif_subthreshold(do_plot=True)
    # test_lif_spiking_regular_input(do_plot=True)
    # test_lif_spiking_random_input(do_plot=True)

    # test_cubalif_bias_no_input(do_plot=True)
    # test_cubalif_v_leak_no_input(do_plot=True)
    # test_cubalif_subthreshold(do_plot=True)
    # test_cubalif_spiking_regular_input(do_plot=True)
    test_cubalif_spiking_random_input(do_plot=True)
